#!/usr/bin/env bash
# This script expected to be set as a cron
# with user called clari5
# There must be an ssh key exchange between
# clari5@localhost and mvnrep@192.168.5.56
# and
# clari5@localhost and cxsvn@192.168.5.56
# for the build to work unattended.
# Output of the build (timestamps) can be checked at
# http://192.168.5.56/resources/cxpsdev-resources/4.9/published/installers/dev/49.1_0/
# and
# http://192.168.5.56/resources/cxpsdev-resources/4.9/published/installers/dev/ (look for cc-platform-49.1_0-installer.bash)
# The build logs can be checked via the mail command.

# on the build server we are redirecting stdin and stdout on the command line so parameters will exist.
if [ "$#" -eq 0 ]; then
   unset publish 
   unset these
   localbuild=true
else
    publish="publish"
    these="--all"
    unset localbuild
fi

. ${HOME}/.bashrc 

smd cc 48.D14P2
svn update
version 4.9
fbuild.sh  ${publish} ${these}

if [ -z ${localbuild} ]; then
    scp cc-platform-49.1_0-installer.bash \
    cxsvn@192.168.5.56:www/resources/cxpsdev-resources/4.9/published/installers/dev/49.1_0/cc-platform-49.1_0-installer.bash
fi

smd efm 48.D14P2
svn update
version 4.9
fbuild.sh ${publish} ${these}

smd la 49.1_0
svn update
version 4.9
fbuild.sh ${publish} ${these}

#smd genie 49.0_0
#svn update
#version 4.9
#fbuild.sh publish --all

sw Clari5 EFM-4.8-dev-baseline

ws
srel 49.1_0
svn update
rel
creinstaller.sh

if [ -z ${localbuild} ]; then
    scp efm-product-dev-49.1_0-installer.tgz \
    cxsvn@192.168.5.56:www/resources/cxpsdev-resources/4.9/published/installers/dev/49.1_0/efm-product-dev-49.1_0-installer.tgz
fi

sdb mysql
fbuild.sh
creplugin.sh --all

if [ -z ${localbuild} ]; then
    scp dev-plugins-49.1_0.tgz \
    cxsvn@192.168.5.56:www/resources/cxpsdev-resources/4.9/published/installers/dev/49.1_0/dev-plugins-49.1_0.tgz
fi
