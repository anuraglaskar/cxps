// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_BALANCE_ENQUIRY", Schema="rice")
public class NFT_BalanceEnquiryEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String branchIdDesc;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field(size=20) public String channel;
       @Field(size=20) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String custId;
       @Field(size=20) public String menuId;
       @Field(size=20) public String empId;
       @Field(size=20) public String acctName;
       @Field(size=20) public String custCardId;
       @Field(size=20) public Double avlBal;
       @Field(size=2) public String hostId;


    @JsonIgnore
    public ITable<NFT_BalanceEnquiryEvent> t = AEF.getITable(this);

	public NFT_BalanceEnquiryEvent(){}

    public NFT_BalanceEnquiryEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("BalanceEnquiry");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBranchIdDesc(json.getString("branch_id_desc"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setChannel(json.getString("channel"));
            setBranchId(json.getString("branch_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setMenuId(json.getString("menu_id"));
            setEmpId(json.getString("emp_id"));
            setAcctName(json.getString("acct_name"));
            setCustCardId(json.getString("cust_card_id"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setHostId(json.getString("host_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "BE"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public String getChannel(){ return channel; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getMenuId(){ return menuId; }

    public String getEmpId(){ return empId; }

    public String getAcctName(){ return acctName; }

    public String getCustCardId(){ return custCardId; }

    public Double getAvlBal(){ return avlBal; }

    public String getHostId(){ return hostId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setMenuId(String val){ this.menuId = val; }
    public void setEmpId(String val){ this.empId = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setHostId(String val){ this.hostId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_BalanceEnquiry");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "BalanceEnquiry");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}