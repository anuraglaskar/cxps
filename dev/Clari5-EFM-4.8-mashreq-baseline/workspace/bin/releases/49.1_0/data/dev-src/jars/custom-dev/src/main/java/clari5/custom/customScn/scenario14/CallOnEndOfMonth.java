package clari5.custom.customScn.scenario14;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;


public class CallOnEndOfMonth {
    private static final CxpsLogger logger = CxpsLogger.getLogger(CallOnEndOfMonth.class);
    private static ThreadInfo threadInfp=new ThreadInfo();

    public static ThreadInfo getThreadInfp() {
        return threadInfp;
    }

    public static void setThreadInfp(ThreadInfo threadInfp) {
        CallOnEndOfMonth.threadInfp = threadInfp;
    }

    Map<Integer, Date> monthDateMap= new HashMap<Integer, Date>();
    public int year = 0;
    private int NO_OF_MONTH = 12;
    private int dayOfMonthWhenWantToExecute;

    private int hoursToExecute;
    private int minuteToExecute;
    private int secondsToExecute;


    public void getTimeStamp(int year, int currentMonthIdx, int dayOfMonWhenWantToExecute, int hrsToExecute, int minToExecute, int secToExecute, String threadName, long threadId, java.sql.Date sqlDate, String insertQuery, String updateQuery, String selectQuery){
        monthDateMap = new HashMap<Integer, Date>();

        dayOfMonthWhenWantToExecute = dayOfMonWhenWantToExecute;
        hoursToExecute = hrsToExecute;
        minuteToExecute = minToExecute;
        secondsToExecute = secToExecute;

        threadInfp.setCurrentThreadName(threadName);
        threadInfp.setUpdateQry(updateQuery);
        threadInfp.setSelectQry(selectQuery);
        threadInfp.setThrdId(threadId);
        threadInfp.setInsrtQry(insertQuery);
        threadInfp.setThreadDate(sqlDate);
        CallOnEndOfMonth.setThreadInfp(threadInfp);

        LocalDateTime currentTime = LocalDateTime.now();
        int currentDate = currentTime.getDayOfMonth();

        for(int monthCount = 0; NO_OF_MONTH > monthCount; monthCount++) {
            Calendar calendar = Calendar.getInstance();
            int date = 1;
            calendar.set(year, monthCount, date);
//TODO Need to comment for cutsom time for testing
            int totalDaysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            dayOfMonthWhenWantToExecute = totalDaysInMonth;
            calendar.set(year, monthCount, dayOfMonthWhenWantToExecute, hoursToExecute, minuteToExecute, secondsToExecute);
            //calendar.set(year, monthCount, dayOfMonthWhenWantToExecute, hoursToExecute, minuteToExecute, secondsToExecute);
            Date timeStamp = calendar.getTime();

            monthDateMap.put(monthCount, timeStamp);
        }
        //System.out.println("---"+monthDateMap);

        callMethodOnEndOfMonth(currentMonthIdx, currentDate);
    }
    public static void insert(String threadName, long threadId, java.sql.Date date,String insertQuery){
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();
            PreparedStatement insertQueryStatement=connection.prepareStatement(insertQuery);)
            {
            insertQueryStatement.setString(1,threadName);// thread name
            insertQueryStatement.setLong(2,threadId); // id
            insertQueryStatement.setDate(3,date);
            insertQueryStatement.setString(4,"NEW");
            insertQueryStatement.setString(5,threadName+"-"+threadId+"-"+date);
            insertQueryStatement.setTimestamp(6,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.setTimestamp(7,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.execute();
            connection.commit();
            logger.info("CountDaemon] Insertion Completed");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("[CountDaemon] insert()-> insertion Failed"+ e.getCause() + " "+e.getMessage());
        }
    }

    public void callMethodOnEndOfMonth(int currentMonthIdx, int currentDate){
        try{
            for (Map.Entry<Integer, Date> entry : monthDateMap.entrySet()){
                int date = entry.getValue().getDate();
                int key = entry.getKey();
                if(key == currentMonthIdx && date >= currentDate){
                    Date dateTimeVal = entry.getValue();

                    Timer timer = new Timer();
                    timer.schedule(new RemindTask(), dateTimeVal);
                    //System.out.println("key : "+key+" || dateTimeVal : "+dateTimeVal);

                    int timeToSleepInDay = 0;
                    if((currentMonthIdx+1) > 11){
                        currentMonthIdx = 0;
                        timeToSleepInDay = getSecond(31);

                        //System.out.println("1. next month : "+getMonthInStringFormat(currentMonthIdx)+" -- date to start :"+monthDateMap.get(currentMonthIdx).getDate());
                    } else{
                        currentMonthIdx = currentMonthIdx+1;
                        //System.out.println("2.next month : "+getMonthInStringFormat(currentMonthIdx)+" -- date to start :"+monthDateMap.get(currentMonthIdx).getDate());
                        timeToSleepInDay = getSecond(monthDateMap.get(currentMonthIdx).getDate());
                    }
                    Thread.sleep(timeToSleepInDay);
                } else {
                    continue;
                }
            }

            year = year+1;
            getTimeStamp(year, currentMonthIdx, dayOfMonthWhenWantToExecute, hoursToExecute, minuteToExecute, secondsToExecute, threadInfp.getCurrentThreadName(), threadInfp.getThrdId(), threadInfp.getThreadDate(),threadInfp.getInsrtQry(),threadInfp.getInsrtQry(), threadInfp.getSelectQry());
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public int getSecond(int days){
        int hoursInDay = 24;
        int minutesInHrs = 60;
        int secondsInMin = 60;
        return days * hoursInDay * minutesInHrs * secondsInMin;
    }

    public int getMonthInIntFormat(String mthInString){
        int month = 0;
        try{
            Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(mthInString);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            month = cal.get(Calendar.MONTH);
        } catch (Exception e){
            e.printStackTrace();
        }
        return month;
    }

    public String getMonthInStringFormat(int monthIdx){
        String monthName = "";
        try{
            monthName = new DateFormatSymbols().getMonths()[monthIdx].toString();
        } catch (Exception e){
            e.printStackTrace();
        }

        return monthName;
    }
}
