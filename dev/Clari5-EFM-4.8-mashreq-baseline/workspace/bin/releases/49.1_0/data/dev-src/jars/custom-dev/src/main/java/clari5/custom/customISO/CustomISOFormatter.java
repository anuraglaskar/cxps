package clari5.custom.customISO;

import clari5.iso8583.core.ISO8583;
import clari5.iso8583.formatter.FieldFormatter;
import clari5.iso.formatter.DefaultISOFieldFormatter;
import clari5.platform.util.Hocon;
import java.nio.charset.Charset;

public class CustomISOFormatter extends DefaultISOFieldFormatter{

    static  Hocon hocon= new Hocon();
    static  String proceccing_code="";
    static  String mti_code="";

    static {

        hocon.loadFromContext("procode_list.conf");
        proceccing_code= hocon.getString("proc_code");
        mti_code = hocon.getString("mti_code");
    }

    @Override
    public FieldFormatter skip(FieldFormatter formatter, ISO8583 iso8583, Charset charset, String rawMessage) {

        // Example to set formatter skip
        String mtiCode = formatter.getMti().trim();
        String proc_code = formatter.getField3();
        String proc = proc_code.substring(0, 2);

        if (mti_code.contains(mtiCode)) {
            formatter.setSkip(true);
            formatter.setResponseCode("05");
            formatter.setSkipRemarks("Skipping as passed MTI code is not required to process. [" + mtiCode + "]" );
        }

       /* if (formatter.getField39() != "00") {
            formatter.setSkip(true);
            formatter.setResponseCode("05");
            formatter.setSkipRemarks("Skipping as transaction is not success at switch. Resp code [" + formatter.getField39() + "]");

        }*/

       if(proceccing_code.contains(proc)){
            formatter.setSkip(true);
            formatter.setResponseCode("05");
            formatter.setSkipRemarks("Skipping as passed proc code  is not require to process. Resp code [" + proc + "]");
            }
        return formatter;
    }
}
