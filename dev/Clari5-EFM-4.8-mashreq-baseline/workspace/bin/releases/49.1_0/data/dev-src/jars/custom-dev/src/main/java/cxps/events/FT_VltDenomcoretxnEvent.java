// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_VLT_DENOMCORETXN", Schema="rice")
public class FT_VltDenomcoretxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String denomcode;
       @Field(size=50) public String branchCode;
       @Field(size=50) public String tillId;
       @Field(size=20) public Double outflow;
       @Field(size=500) public String xref;
       @Field(size=50) public String ccycode;
       @Field(size=20) public Double inflow;
       @Field public java.sql.Timestamp postingdate;
       @Field public java.sql.Timestamp insTime;
       @Field(size=20) public Double denomMulInflow;


    @JsonIgnore
    public ITable<FT_VltDenomcoretxnEvent> t = AEF.getITable(this);

	public FT_VltDenomcoretxnEvent(){}

    public FT_VltDenomcoretxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("VltDenomcoretxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setDenomcode(json.getString("denomcode"));
            setBranchCode(json.getString("branch_code"));
            setTillId(json.getString("till_id"));
            setOutflow(EventHelper.toDouble(json.getString("outflow")));
            setXref(json.getString("xref"));
            setCcycode(json.getString("ccycode"));
            setInflow(EventHelper.toDouble(json.getString("inflow")));
            setPostingdate(EventHelper.toTimestamp(json.getString("postingdate")));
            setInsTime(EventHelper.toTimestamp(json.getString("ins_time")));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setDenomMulInflow(cxps.events.CustomDerivator.getdenomMulInflow(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "VLT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getDenomcode(){ return denomcode; }

    public String getBranchCode(){ return branchCode; }

    public String getTillId(){ return tillId; }

    public Double getOutflow(){ return outflow; }

    public String getXref(){ return xref; }

    public String getCcycode(){ return ccycode; }

    public Double getInflow(){ return inflow; }

    public java.sql.Timestamp getPostingdate(){ return postingdate; }

    public java.sql.Timestamp getInsTime(){ return insTime; }
    public Double getDenomMulInflow(){ return denomMulInflow; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setDenomcode(String val){ this.denomcode = val; }
    public void setBranchCode(String val){ this.branchCode = val; }
    public void setTillId(String val){ this.tillId = val; }
    public void setOutflow(Double val){ this.outflow = val; }
    public void setXref(String val){ this.xref = val; }
    public void setCcycode(String val){ this.ccycode = val; }
    public void setInflow(Double val){ this.inflow = val; }
    public void setPostingdate(java.sql.Timestamp val){ this.postingdate = val; }
    public void setInsTime(java.sql.Timestamp val){ this.insTime = val; }
    public void setDenomMulInflow(Double val){ this.denomMulInflow = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String branchKey= h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(), this.branchCode);
        wsInfoSet.add(new WorkspaceInfo("Branch", branchKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_VltDenomcoretxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "VltDenomcoretxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}