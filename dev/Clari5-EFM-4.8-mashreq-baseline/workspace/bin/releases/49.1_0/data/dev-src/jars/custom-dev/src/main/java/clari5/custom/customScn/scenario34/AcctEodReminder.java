package clari5.custom.customScn.scenario34;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;


public class AcctEodReminder extends TimerTask {
    private static final CxpsLogger logger = CxpsLogger.getLogger(AcctEodReminder.class);

    private static ThreadDump threadDump=new ThreadDump();


    public void run() {
        Hocon hocon= new Hocon();
        hocon.loadFromContext("execution_time.conf");
        String selectQuery=hocon.getString("selectQuery");
        String insertQuery=hocon.getString("insertQuery");
        String updateQuery=hocon.getString("updateQuery");

        /* applying synchronized for multi thread */
        synchronized (this){
            Thread thread=new Thread();
            long threadId = thread.getId();
            Date date= new Date();
            java.sql.Date rcre_time=new java.sql.Date(date.getTime());// current date
            thread.setName("acctmatch");
            String threadName =thread.getName();
            logger.info("[AcctEodReminder] Accquired Lock"+threadName +" with id"+threadId);
            threadDump.setCurrentThreadName(threadName);
            threadDump.setThrdId(threadId);
            threadDump.setThreadDate(rcre_time);
            threadDump.setInsrtQry(insertQuery);
            threadDump.setSelectQry(selectQuery);
            threadDump.setCurrentUpdateQuery(updateQuery);
            AcctEodReminder.setThreadDump(threadDump);
            try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
                Connection connection=rdbmsSession.getCxConnection();) {
                //PreparedStatement queryStatement=connection.prepareStatement(selectQuery);
                PreparedStatement updateQueryStatement=connection.prepareStatement(updateQuery);
                //queryStatement.setString(1,threadName);
                //queryStatement.setDate(2,rcre_time);
                //ResultSet resultSet=queryStatement.executeQuery();
                /*if(resultSet.next()){
                    logger.info("[AcctEodReminder] One thread is already processing.Hence quit");
                }*/
                //else {
                    logger.info("[AcctEodReminder] No entry found for this Daemon thread! Hence Inserting...");
                    AcctMqWriter acctMqWriter = new AcctMqWriter();
                    acctMqWriter.writeJson();
                    updateQueryStatement.setString(1,"C");
                    updateQueryStatement.setTimestamp(2,new java.sql.Timestamp(new java.util.Date().getTime()));
                    updateQueryStatement.setString(3,threadDump.getCurrentThreadName());
                    updateQueryStatement.setDate(4,threadDump.getThreadDate());
                    updateQueryStatement.executeUpdate();
                    connection.commit();
                    threadDump=null;
                //}

        } catch (Exception e) {
            e.printStackTrace();
        }

      }
    }

    public static ThreadDump getThreadDump() {
        return threadDump;
    }

    public static void setThreadDump(ThreadDump threadDump) {
        AcctEodReminder.threadDump = threadDump;
    }

    public static void insert(String threadName, long threadId, java.sql.Date rcre_time, String insertQuery) {
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();) {
           PreparedStatement statement=connection.prepareStatement(insertQuery);
            statement.setString(1,threadName);
            statement.setLong(2,threadId);
            statement.setDate(3,rcre_time);
            statement.setString(4,"NEW");
            statement.setString(5,threadName+"-"+threadId+"-"+rcre_time);
            statement.setTimestamp(6,new java.sql.Timestamp(new java.util.Date().getTime()));
            statement.setTimestamp(7,new java.sql.Timestamp(new java.util.Date().getTime()));
            statement.executeUpdate();
            connection.commit();
            logger.info("[AcctEodReminder] Insertion Completed");
        }
        catch (SQLException e) {
            logger.error("[AcctEodReminder] insert()-> insertion Failed"+ e.getCause() + " "+e.getMessage());
        }

    }

    public static  void callonEOD(int hrsToExecute,int minToExecute,int secToExecute){
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH,0);
            calendar.add(Calendar.YEAR,0);
            calendar.set(Calendar.HOUR_OF_DAY,hrsToExecute);
            calendar.set(Calendar.MINUTE,minToExecute);
            calendar.set(Calendar.SECOND,secToExecute);
            Date date= calendar.getTime();
            Timer timer = new Timer();
            //Date date = new Date();
            while (true){
                Date currentDate=new Date();
                if(date.compareTo(currentDate)>0){
                    timer.schedule(new AcctEodReminder(), date);
                    logger.info("[AcctEodReminder] Call on EOD..Time has been scheduled");
                    int timetosleeep = (24 * 60 * 60 * 1000);
                    Thread.sleep(timetosleeep);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
