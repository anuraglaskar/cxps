// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AcctOpenedSameDetailsEventMapper extends EventMapper<NFT_AcctOpenedSameDetailsEvent> {

public NFT_AcctOpenedSameDetailsEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AcctOpenedSameDetailsEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AcctOpenedSameDetailsEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AcctOpenedSameDetailsEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AcctOpenedSameDetailsEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AcctOpenedSameDetailsEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AcctOpenedSameDetailsEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getNewAcctcloseddate());
            preparedStatement.setString(i++, obj.getNewCustdob());
            preparedStatement.setString(i++, obj.getMatchedTradelicense());
            preparedStatement.setTimestamp(i++, obj.getOldAcctcloseddate());
            preparedStatement.setString(i++, obj.getOldCompmis2());
            preparedStatement.setString(i++, obj.getOldEmail());
            preparedStatement.setString(i++, obj.getOldCompmis1());
            preparedStatement.setString(i++, obj.getNewMobile());
            preparedStatement.setString(i++, obj.getOldAccountno());
            preparedStatement.setString(i++, obj.getMatchedEid());
            preparedStatement.setString(i++, obj.getOldBranchid());
            preparedStatement.setString(i++, obj.getNewCompmis1());
            preparedStatement.setString(i++, obj.getMatchedPassport());
            preparedStatement.setString(i++, obj.getNewCompmis2());
            preparedStatement.setString(i++, obj.getNewPassport());
            preparedStatement.setString(i++, obj.getOldCustname());
            preparedStatement.setTimestamp(i++, obj.getNewAcctopendate());
            preparedStatement.setString(i++, obj.getOldTradelicense());
            preparedStatement.setString(i++, obj.getNewTradelicense());
            preparedStatement.setTimestamp(i++, obj.getOldAcctopendate());
            preparedStatement.setString(i++, obj.getOldMobile());
            preparedStatement.setString(i++, obj.getOldCif());
            preparedStatement.setString(i++, obj.getOldEid());
            preparedStatement.setString(i++, obj.getOldCustdob());
            preparedStatement.setString(i++, obj.getNewCustname());
            preparedStatement.setString(i++, obj.getNewAccountno());
            preparedStatement.setString(i++, obj.getOldPassport());
            preparedStatement.setString(i++, obj.getMatchedMobile());
            preparedStatement.setString(i++, obj.getNewCif());
            preparedStatement.setString(i++, obj.getNewEid());
            preparedStatement.setString(i++, obj.getNewBranchid());
            preparedStatement.setString(i++, obj.getNewEmail());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_ACCT_OPENED_SAME_DETAILS]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AcctOpenedSameDetailsEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AcctOpenedSameDetailsEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCT_OPENED_SAME_DETAILS"));
        putList = new ArrayList<>();

        for (NFT_AcctOpenedSameDetailsEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "NEW_ACCTCLOSEDDATE", String.valueOf(obj.getNewAcctcloseddate()));
            p = this.insert(p, "NEW_CUSTDOB",  obj.getNewCustdob());
            p = this.insert(p, "MATCHED_TRADELICENSE",  obj.getMatchedTradelicense());
            p = this.insert(p, "OLD_ACCTCLOSEDDATE", String.valueOf(obj.getOldAcctcloseddate()));
            p = this.insert(p, "OLD_COMPMIS2",  obj.getOldCompmis2());
            p = this.insert(p, "OLD_EMAIL",  obj.getOldEmail());
            p = this.insert(p, "OLD_COMPMIS1",  obj.getOldCompmis1());
            p = this.insert(p, "NEW_MOBILE",  obj.getNewMobile());
            p = this.insert(p, "OLD_ACCOUNTNO",  obj.getOldAccountno());
            p = this.insert(p, "MATCHED_EID",  obj.getMatchedEid());
            p = this.insert(p, "OLD_BRANCHID",  obj.getOldBranchid());
            p = this.insert(p, "NEW_COMPMIS1",  obj.getNewCompmis1());
            p = this.insert(p, "MATCHED_PASSPORT",  obj.getMatchedPassport());
            p = this.insert(p, "NEW_COMPMIS2",  obj.getNewCompmis2());
            p = this.insert(p, "NEW_PASSPORT",  obj.getNewPassport());
            p = this.insert(p, "OLD_CUSTNAME",  obj.getOldCustname());
            p = this.insert(p, "NEW_ACCTOPENDATE", String.valueOf(obj.getNewAcctopendate()));
            p = this.insert(p, "OLD_TRADELICENSE",  obj.getOldTradelicense());
            p = this.insert(p, "NEW_TRADELICENSE",  obj.getNewTradelicense());
            p = this.insert(p, "OLD_ACCTOPENDATE", String.valueOf(obj.getOldAcctopendate()));
            p = this.insert(p, "OLD_MOBILE",  obj.getOldMobile());
            p = this.insert(p, "OLD_CIF",  obj.getOldCif());
            p = this.insert(p, "OLD_EID",  obj.getOldEid());
            p = this.insert(p, "OLD_CUSTDOB",  obj.getOldCustdob());
            p = this.insert(p, "NEW_CUSTNAME",  obj.getNewCustname());
            p = this.insert(p, "NEW_ACCOUNTNO",  obj.getNewAccountno());
            p = this.insert(p, "OLD_PASSPORT",  obj.getOldPassport());
            p = this.insert(p, "MATCHED_MOBILE",  obj.getMatchedMobile());
            p = this.insert(p, "NEW_CIF",  obj.getNewCif());
            p = this.insert(p, "NEW_EID",  obj.getNewEid());
            p = this.insert(p, "NEW_BRANCHID",  obj.getNewBranchid());
            p = this.insert(p, "NEW_EMAIL",  obj.getNewEmail());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_ACCT_OPENED_SAME_DETAILS"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_ACCT_OPENED_SAME_DETAILS]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_ACCT_OPENED_SAME_DETAILS]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AcctOpenedSameDetailsEvent obj = new NFT_AcctOpenedSameDetailsEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setNewAcctcloseddate(rs.getTimestamp("NEW_ACCTCLOSEDDATE"));
    obj.setNewCustdob(rs.getString("NEW_CUSTDOB"));
    obj.setMatchedTradelicense(rs.getString("MATCHED_TRADELICENSE"));
    obj.setOldAcctcloseddate(rs.getTimestamp("OLD_ACCTCLOSEDDATE"));
    obj.setOldCompmis2(rs.getString("OLD_COMPMIS2"));
    obj.setOldEmail(rs.getString("OLD_EMAIL"));
    obj.setOldCompmis1(rs.getString("OLD_COMPMIS1"));
    obj.setNewMobile(rs.getString("NEW_MOBILE"));
    obj.setOldAccountno(rs.getString("OLD_ACCOUNTNO"));
    obj.setMatchedEid(rs.getString("MATCHED_EID"));
    obj.setOldBranchid(rs.getString("OLD_BRANCHID"));
    obj.setNewCompmis1(rs.getString("NEW_COMPMIS1"));
    obj.setMatchedPassport(rs.getString("MATCHED_PASSPORT"));
    obj.setNewCompmis2(rs.getString("NEW_COMPMIS2"));
    obj.setNewPassport(rs.getString("NEW_PASSPORT"));
    obj.setOldCustname(rs.getString("OLD_CUSTNAME"));
    obj.setNewAcctopendate(rs.getTimestamp("NEW_ACCTOPENDATE"));
    obj.setOldTradelicense(rs.getString("OLD_TRADELICENSE"));
    obj.setNewTradelicense(rs.getString("NEW_TRADELICENSE"));
    obj.setOldAcctopendate(rs.getTimestamp("OLD_ACCTOPENDATE"));
    obj.setOldMobile(rs.getString("OLD_MOBILE"));
    obj.setOldCif(rs.getString("OLD_CIF"));
    obj.setOldEid(rs.getString("OLD_EID"));
    obj.setOldCustdob(rs.getString("OLD_CUSTDOB"));
    obj.setNewCustname(rs.getString("NEW_CUSTNAME"));
    obj.setNewAccountno(rs.getString("NEW_ACCOUNTNO"));
    obj.setOldPassport(rs.getString("OLD_PASSPORT"));
    obj.setMatchedMobile(rs.getString("MATCHED_MOBILE"));
    obj.setNewCif(rs.getString("NEW_CIF"));
    obj.setNewEid(rs.getString("NEW_EID"));
    obj.setNewBranchid(rs.getString("NEW_BRANCHID"));
    obj.setNewEmail(rs.getString("NEW_EMAIL"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_ACCT_OPENED_SAME_DETAILS]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AcctOpenedSameDetailsEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AcctOpenedSameDetailsEvent> events;
 NFT_AcctOpenedSameDetailsEvent obj = new NFT_AcctOpenedSameDetailsEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCT_OPENED_SAME_DETAILS"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AcctOpenedSameDetailsEvent obj = new NFT_AcctOpenedSameDetailsEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setNewAcctcloseddate(EventHelper.toTimestamp(getColumnValue(rs, "NEW_ACCTCLOSEDDATE")));
            obj.setNewCustdob(getColumnValue(rs, "NEW_CUSTDOB"));
            obj.setMatchedTradelicense(getColumnValue(rs, "MATCHED_TRADELICENSE"));
            obj.setOldAcctcloseddate(EventHelper.toTimestamp(getColumnValue(rs, "OLD_ACCTCLOSEDDATE")));
            obj.setOldCompmis2(getColumnValue(rs, "OLD_COMPMIS2"));
            obj.setOldEmail(getColumnValue(rs, "OLD_EMAIL"));
            obj.setOldCompmis1(getColumnValue(rs, "OLD_COMPMIS1"));
            obj.setNewMobile(getColumnValue(rs, "NEW_MOBILE"));
            obj.setOldAccountno(getColumnValue(rs, "OLD_ACCOUNTNO"));
            obj.setMatchedEid(getColumnValue(rs, "MATCHED_EID"));
            obj.setOldBranchid(getColumnValue(rs, "OLD_BRANCHID"));
            obj.setNewCompmis1(getColumnValue(rs, "NEW_COMPMIS1"));
            obj.setMatchedPassport(getColumnValue(rs, "MATCHED_PASSPORT"));
            obj.setNewCompmis2(getColumnValue(rs, "NEW_COMPMIS2"));
            obj.setNewPassport(getColumnValue(rs, "NEW_PASSPORT"));
            obj.setOldCustname(getColumnValue(rs, "OLD_CUSTNAME"));
            obj.setNewAcctopendate(EventHelper.toTimestamp(getColumnValue(rs, "NEW_ACCTOPENDATE")));
            obj.setOldTradelicense(getColumnValue(rs, "OLD_TRADELICENSE"));
            obj.setNewTradelicense(getColumnValue(rs, "NEW_TRADELICENSE"));
            obj.setOldAcctopendate(EventHelper.toTimestamp(getColumnValue(rs, "OLD_ACCTOPENDATE")));
            obj.setOldMobile(getColumnValue(rs, "OLD_MOBILE"));
            obj.setOldCif(getColumnValue(rs, "OLD_CIF"));
            obj.setOldEid(getColumnValue(rs, "OLD_EID"));
            obj.setOldCustdob(getColumnValue(rs, "OLD_CUSTDOB"));
            obj.setNewCustname(getColumnValue(rs, "NEW_CUSTNAME"));
            obj.setNewAccountno(getColumnValue(rs, "NEW_ACCOUNTNO"));
            obj.setOldPassport(getColumnValue(rs, "OLD_PASSPORT"));
            obj.setMatchedMobile(getColumnValue(rs, "MATCHED_MOBILE"));
            obj.setNewCif(getColumnValue(rs, "NEW_CIF"));
            obj.setNewEid(getColumnValue(rs, "NEW_EID"));
            obj.setNewBranchid(getColumnValue(rs, "NEW_BRANCHID"));
            obj.setNewEmail(getColumnValue(rs, "NEW_EMAIL"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCT_OPENED_SAME_DETAILS]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCT_OPENED_SAME_DETAILS]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"NEW_ACCTCLOSEDDATE\",\"NEW_CUSTDOB\",\"MATCHED_TRADELICENSE\",\"OLD_ACCTCLOSEDDATE\",\"OLD_COMPMIS2\",\"OLD_EMAIL\",\"OLD_COMPMIS1\",\"NEW_MOBILE\",\"OLD_ACCOUNTNO\",\"MATCHED_EID\",\"OLD_BRANCHID\",\"NEW_COMPMIS1\",\"MATCHED_PASSPORT\",\"NEW_COMPMIS2\",\"NEW_PASSPORT\",\"OLD_CUSTNAME\",\"NEW_ACCTOPENDATE\",\"OLD_TRADELICENSE\",\"NEW_TRADELICENSE\",\"OLD_ACCTOPENDATE\",\"OLD_MOBILE\",\"OLD_CIF\",\"OLD_EID\",\"OLD_CUSTDOB\",\"NEW_CUSTNAME\",\"NEW_ACCOUNTNO\",\"OLD_PASSPORT\",\"MATCHED_MOBILE\",\"NEW_CIF\",\"NEW_EID\",\"NEW_BRANCHID\",\"NEW_EMAIL\"" +
              " FROM EVENT_NFT_ACCT_OPENED_SAME_DETAILS";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [NEW_ACCTCLOSEDDATE],[NEW_CUSTDOB],[MATCHED_TRADELICENSE],[OLD_ACCTCLOSEDDATE],[OLD_COMPMIS2],[OLD_EMAIL],[OLD_COMPMIS1],[NEW_MOBILE],[OLD_ACCOUNTNO],[MATCHED_EID],[OLD_BRANCHID],[NEW_COMPMIS1],[MATCHED_PASSPORT],[NEW_COMPMIS2],[NEW_PASSPORT],[OLD_CUSTNAME],[NEW_ACCTOPENDATE],[OLD_TRADELICENSE],[NEW_TRADELICENSE],[OLD_ACCTOPENDATE],[OLD_MOBILE],[OLD_CIF],[OLD_EID],[OLD_CUSTDOB],[NEW_CUSTNAME],[NEW_ACCOUNTNO],[OLD_PASSPORT],[MATCHED_MOBILE],[NEW_CIF],[NEW_EID],[NEW_BRANCHID],[NEW_EMAIL]" +
              " FROM EVENT_NFT_ACCT_OPENED_SAME_DETAILS";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`NEW_ACCTCLOSEDDATE`,`NEW_CUSTDOB`,`MATCHED_TRADELICENSE`,`OLD_ACCTCLOSEDDATE`,`OLD_COMPMIS2`,`OLD_EMAIL`,`OLD_COMPMIS1`,`NEW_MOBILE`,`OLD_ACCOUNTNO`,`MATCHED_EID`,`OLD_BRANCHID`,`NEW_COMPMIS1`,`MATCHED_PASSPORT`,`NEW_COMPMIS2`,`NEW_PASSPORT`,`OLD_CUSTNAME`,`NEW_ACCTOPENDATE`,`OLD_TRADELICENSE`,`NEW_TRADELICENSE`,`OLD_ACCTOPENDATE`,`OLD_MOBILE`,`OLD_CIF`,`OLD_EID`,`OLD_CUSTDOB`,`NEW_CUSTNAME`,`NEW_ACCOUNTNO`,`OLD_PASSPORT`,`MATCHED_MOBILE`,`NEW_CIF`,`NEW_EID`,`NEW_BRANCHID`,`NEW_EMAIL`" +
              " FROM EVENT_NFT_ACCT_OPENED_SAME_DETAILS";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_ACCT_OPENED_SAME_DETAILS (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"NEW_ACCTCLOSEDDATE\",\"NEW_CUSTDOB\",\"MATCHED_TRADELICENSE\",\"OLD_ACCTCLOSEDDATE\",\"OLD_COMPMIS2\",\"OLD_EMAIL\",\"OLD_COMPMIS1\",\"NEW_MOBILE\",\"OLD_ACCOUNTNO\",\"MATCHED_EID\",\"OLD_BRANCHID\",\"NEW_COMPMIS1\",\"MATCHED_PASSPORT\",\"NEW_COMPMIS2\",\"NEW_PASSPORT\",\"OLD_CUSTNAME\",\"NEW_ACCTOPENDATE\",\"OLD_TRADELICENSE\",\"NEW_TRADELICENSE\",\"OLD_ACCTOPENDATE\",\"OLD_MOBILE\",\"OLD_CIF\",\"OLD_EID\",\"OLD_CUSTDOB\",\"NEW_CUSTNAME\",\"NEW_ACCOUNTNO\",\"OLD_PASSPORT\",\"MATCHED_MOBILE\",\"NEW_CIF\",\"NEW_EID\",\"NEW_BRANCHID\",\"NEW_EMAIL\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[NEW_ACCTCLOSEDDATE],[NEW_CUSTDOB],[MATCHED_TRADELICENSE],[OLD_ACCTCLOSEDDATE],[OLD_COMPMIS2],[OLD_EMAIL],[OLD_COMPMIS1],[NEW_MOBILE],[OLD_ACCOUNTNO],[MATCHED_EID],[OLD_BRANCHID],[NEW_COMPMIS1],[MATCHED_PASSPORT],[NEW_COMPMIS2],[NEW_PASSPORT],[OLD_CUSTNAME],[NEW_ACCTOPENDATE],[OLD_TRADELICENSE],[NEW_TRADELICENSE],[OLD_ACCTOPENDATE],[OLD_MOBILE],[OLD_CIF],[OLD_EID],[OLD_CUSTDOB],[NEW_CUSTNAME],[NEW_ACCOUNTNO],[OLD_PASSPORT],[MATCHED_MOBILE],[NEW_CIF],[NEW_EID],[NEW_BRANCHID],[NEW_EMAIL]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`NEW_ACCTCLOSEDDATE`,`NEW_CUSTDOB`,`MATCHED_TRADELICENSE`,`OLD_ACCTCLOSEDDATE`,`OLD_COMPMIS2`,`OLD_EMAIL`,`OLD_COMPMIS1`,`NEW_MOBILE`,`OLD_ACCOUNTNO`,`MATCHED_EID`,`OLD_BRANCHID`,`NEW_COMPMIS1`,`MATCHED_PASSPORT`,`NEW_COMPMIS2`,`NEW_PASSPORT`,`OLD_CUSTNAME`,`NEW_ACCTOPENDATE`,`OLD_TRADELICENSE`,`NEW_TRADELICENSE`,`OLD_ACCTOPENDATE`,`OLD_MOBILE`,`OLD_CIF`,`OLD_EID`,`OLD_CUSTDOB`,`NEW_CUSTNAME`,`NEW_ACCOUNTNO`,`OLD_PASSPORT`,`MATCHED_MOBILE`,`NEW_CIF`,`NEW_EID`,`NEW_BRANCHID`,`NEW_EMAIL`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

