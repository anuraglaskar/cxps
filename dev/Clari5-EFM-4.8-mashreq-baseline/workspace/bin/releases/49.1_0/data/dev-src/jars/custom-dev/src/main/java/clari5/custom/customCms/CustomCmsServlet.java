package clari5.custom.customCms;

import clari5.custom.customCms.cardblockUnblock.*;
import clari5.custom.customCms.freezUnfreez.FreezeUnfreezeController;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.json.JSONArray;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;

import static jdk.nashorn.internal.runtime.Context.printStackTrace;

public class CustomCmsServlet extends HttpServlet  {
    private static CxpsLogger logger = CxpsLogger.getLogger(CustomCmsServlet.class);
    private FreezeUnfreezeController freezeUnfreezeController = new FreezeUnfreezeController();
    private CrdBlk_UnBlk_Controller crdBlk_unBlk_controller= new CrdBlk_UnBlk_Controller();
    JiraUpdate jiraUpdate = new JiraUpdate();


    protected void doGet(HttpServletRequest request,HttpServletResponse response) {
        System.out.println("Do Get is calling");
        doEither(request, response);
    }


    protected void doPost(HttpServletRequest request,HttpServletResponse response)  {
        doEither(request, response);
    }



    public  void doEither(HttpServletRequest req , HttpServletResponse res){

        logger.debug("coming inside doEither method of CustomCmsServlet");
        Hocon h = new Hocon();
        h.loadFromContext("FreezUnfreezBlockApiDetails.conf");
        String fApi=h.getString("freez_UnFrz_api");
        String cApi=h.getString("cardblock_UnBlk_api");
        int time=h.getInt("timeToWaitForRes");
        String envType=h.getString("env_type");
        logger.info("this is freez api"+fApi+"block api --"+cApi+"time ---"+time);
          String data=req.getParameter("data");
          JSONObject json = new JSONObject(data);

          String act = json.get("action").toString();
          String custNo = json.get("customerNumber").toString();
          String username = json.get("username").toString();
        logger.info("action --"+act+"custNo --"+custNo);

        PrintWriter out=null;
        String resp=null;
        res.setContentType("application/javascript");
        try {

            if (act.equalsIgnoreCase("F")) {
                System.out.println("inside freeze in servlet");
                out=res.getWriter();
                //call freezing api
                boolean flag=freezeUnfreezeController.callingToFreez(custNo,fApi, time,envType);
                if(flag==true) {
                    resp="({\"resp\":\"FS\"})";
                    String data1="{\n" +
                            "  \"custId\": "+custNo+",\n" +
                            "  \"cardno\": "+""+",\n" +
                            "  \"status\": "+"Applied"+",\n" +
                            "  \"username\": "+""+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);

                } else {
                    resp="({\"resp\":\"FF\"})";

                }
                out.print(resp);
                out.flush();

            }
            else if (act.equalsIgnoreCase("UZ")) {

                logger.debug(" un freez method");
                out=res.getWriter();
                //call unfreez api
                boolean flag=freezeUnfreezeController.callingToUnFreez(custNo,fApi,time,envType);
                if (flag==true){
                    resp="({\"resp\":\"FS\"})";
                    String data1="{\n" +
                            "  \"custId\": "+custNo+",\n" +
                            "  \"cardno\": "+""+",\n" +
                            "  \"status\": "+"Not Applied"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                } else {
                    resp="({\"resp\":\"FF\"})";
                }
                out.print(resp);
                out.flush();

            } else if (act.equalsIgnoreCase("B")) {

                logger.debug("this is block method");
                System.out.println("inside block in CustomServlet");
                out=res.getWriter();
                //calling api
                boolean flag=crdBlk_unBlk_controller.callingToBlkCard(custNo,cApi, time,envType);
                if(flag==true){
                    resp="({\"resp\":\"CS\"})";
                    String data1="{\n" +
                            "  \"custId\": "+""+",\n" +
                            "  \"cardno\": "+custNo+",\n" +
                            "  \"status\": "+"CardBolked"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                } else {
                    resp="({\"resp\":\"CF\"})";
                }
                out.print(resp);
                out.flush();



            } else if (act.equalsIgnoreCase("UB")) {

                logger.debug("this is unblock method");
                out=res.getWriter();
                boolean flag=crdBlk_unBlk_controller.callingToUnBlock(custNo,cApi, time,envType);
                if(flag==true){
                    resp="({\"resp\":\"CS\"})";
                    String data1="{\n" +
                            "  \"custId\": "+""+",\n" +
                            "  \"cardno\": "+custNo+",\n" +
                            "  \"status\": "+"CardUnBlocked"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                } else {
                    resp="({\"resp\":\"CF\"})";


                }
                out.print(resp);
                out.flush();

            } else {
                //if status is not proper print on log file
                logger.debug(" Status getting from jira is not correct");
                logger.debug(" Expected status is F or UF or B or UB");
                logger.debug(" Status we are getting" + act);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ///method for saving audit log for freezUnfreez and cardblockunblock
    public static String saveaudit(String payload){
        logger.debug("in getCustTree");
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        System.out.println(payload);
        JSONObject jsonObject = new JSONObject(payload);
        String memberId = jsonObject.get("custId").toString();
        String cardno=jsonObject.get("cardno").toString();
        String status=jsonObject.getString("status");
        String username=jsonObject.getString("username");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Connection conn = null;
        Statement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="INSERT INTO Audit_Log (Cust_Id,Card_number,Freez_status,Freez_Date,User_Name)\n" +
                    "VALUES ('"+memberId+"','"+cardno+"','"+status+"','"+timestamp+"','"+username+"');";
            logger.debug("query to get ActDetails : "+sql);
            stmt.executeUpdate(sql);
            conn.commit();
            obj1.put("msg","S");
            return obj1.toString();
        }catch (Exception e){
            printStackTrace(e);
        }
        finally {

            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";
    }
}
