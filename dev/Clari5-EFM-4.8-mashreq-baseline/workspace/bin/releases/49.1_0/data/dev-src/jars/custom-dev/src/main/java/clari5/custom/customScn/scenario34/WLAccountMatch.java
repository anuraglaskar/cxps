package clari5.custom.customScn.scenario34;

import clari5.aml.web.wl.search.WlQuery;
import clari5.aml.web.wl.search.WlRecordDetails;
import clari5.aml.web.wl.search.WlResult;
import clari5.aml.web.wl.search.WlSearcher;
import clari5.aml.wle.MatchedRecord;
import clari5.aml.wle.MatchedResults;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.List;

public class WLAccountMatch {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WLAccountMatch.class);

    public  void getMatchedResult() {
        AccountData accountData = new AccountData();
        List<AccountPojo> datalist = accountData.getFieldstoMacth();
        for (AccountPojo finaldata: datalist) {
            String resp="";
            String ruleName = "MASHREQACCT";
            JSONObject obj = new JSONObject();
            JSONArray array = new JSONArray();
            obj.put("ruleName", ruleName);
            JSONObject jo1 = new JSONObject();
            jo1.put("name", "nationalId");
            jo1.put("value", finaldata.getNeweid());
            array.put(jo1);
            JSONObject jo2 = new JSONObject();
            jo2.put("name", "custMobile1");
            jo2.put("value", finaldata.getNewmobile());
            array.put(jo2);
            JSONObject jo3 = new JSONObject();
            jo3.put("name", "custTradeNo");
            jo3.put("value", finaldata.getNewtradelicense());
            array.put(jo3);
            JSONObject jo4 = new JSONObject();
            jo4.put("name", "custPassportNo");
            jo4.put("value", finaldata.getNewpassport());
            array.put(jo4);
            obj.put("fields", array);
            logger.info("object of json --"+obj);
            resp = WLAccountMatch.getResponse(obj);
            AcctMqWriter.resposeFormatter(resp,finaldata.getNewcif(), finaldata.getNewaccountno());

        }
    }
    private static String getResponse(Object obj) {
        String response = "";
        try {
            WlQuery query = new WlQuery();
            query.from(obj.toString());
            logger.info("Query object created from json : " + obj.toString());

            WlSearcher searcher = new WlSearcher();
            WlResult result = searcher.search(query);
            MatchedResults matched = result.getMatchedResults();
            Collection<MatchedRecord> recordList = matched.getMatchedRecords();
            if (recordList != null) {
                for (MatchedRecord record : recordList) {
                    WlRecordDetails wlDetails = new WlRecordDetails();
                    String outputDetails = wlDetails.getRecordDetails(record.getDocId());
                    record.setDetails(outputDetails);
                }
                response = result.toJson();
                logger.info("Response for Account opened in another branch: " + response);
            }
        } catch (Exception e) {
            logger.error("unable to fetch response from WL for URL: ---> " + obj.toString());
            e.getCause();
            e.printStackTrace();
        }
        return response;
    }
}
