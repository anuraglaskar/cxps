package clari5.aml.reports.rbi;

import clari5.aml.reports.rbi.fiu.ReportConstant;
import clari5.aml.reports.rbi.goaml.xml.Report;
import clari5.aml.reports.rbi.goaml.xml.*;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.util.List;

public class GoAmlCustomValueFetcher implements IGoAmlCustomValueFetcher {

    private void addTransactionDetails(Report.Transaction transaction){

        transaction.setTransmodeCode(ConductionType.EFT);
        transaction.setAmountLocal(new BigDecimal(480000));
        setTFromMyClientObj(transaction);
        setTTo(transaction);
    }

    private void setTTo(Report.Transaction transaction){

        Report.Transaction.TTo tToObj = new Report.Transaction.TTo();
        tToObj.setToFundsCode(FundsType.CHEQ);

        setTAccountData(tToObj);

        tToObj.setToCountry(CountryType.EC);
        transaction.setTTo(tToObj);
    }

    private void setTAccountData(Report.Transaction.TTo tTo){
        TAccount tAccount = new TAccount();
        tAccount.setInstitutionName(new JAXBElement<>(new QName("institution_name"),String.class,"EQUITY BANK"));
        tAccount.setSwift("EQBLUGKA");
        tAccount.setAccount("100265311900");

        setSignatory(tAccount.getSignatory());
        tTo.setToAccount(tAccount);
    }

    private void setSignatory(List<TAccount.Signatory> signatoryList){
        TAccount.Signatory signatory = new TAccount.Signatory();
        TPerson tPerson = new TPerson();
        tPerson.setFirstName("EDWARD");
        tPerson.setLastName("NAMBAFU");
        signatory.setTPerson(tPerson);
        signatory.setIsPrimary(true);
        signatoryList.add(signatory);
    }

    private void setTFromMyClientObj(Report.Transaction transaction){

        Report.Transaction.TFromMyClient tFromMyClient = new Report.Transaction.TFromMyClient();

        tFromMyClient.setFromFundsCode(FundsType.CASH);
        setForeignCurrencyObj(tFromMyClient);
        setToAccountForMyClient(tFromMyClient);
        tFromMyClient.setFromCountry(CountryType.UG);

        transaction.setTFromMyClient(tFromMyClient);

    }
    private void setToAccountForMyClient(Report.Transaction.TFromMyClient tFromMyClient){
        TAccountMyClient tAccountMyClient = new TAccountMyClient();
        tAccountMyClient.setInstitutionName(new JAXBElement<>(new QName("institution_name"), String.class,"dfcu Bank"));
        tAccountMyClient.setSwift("DFCUUGKA");
        tAccountMyClient.setAccount("01061128000001");
        tAccountMyClient.setAccountName(ReportUtils.validateGoAmlFieldLength(ReportConstant.GOAML_ACCOUNT_NAME_LEN,tAccountMyClient.getAccountName()));
        setSignatoryMyClient(tAccountMyClient);
        tFromMyClient.setFromAccount(tAccountMyClient);
    }

    private void setSignatoryMyClient(TAccountMyClient tAccountMyClient){
        TAccountMyClient.Signatory signatory = new TAccountMyClient.Signatory();
        signatory.setIsPrimary(true);

        TPersonMyClient tPersonMyClient = new TPersonMyClient();
        tPersonMyClient.setFirstName("James");
        tPersonMyClient.setLastName("Mukiibi");
        tPersonMyClient.setBirthdate(ReportUtils.getXmlDateCalender("1973-12-12T00:00:00"));
        setPhoneData(tPersonMyClient);
        validateAddressData(tPersonMyClient);
        signatory.setTPerson(tPersonMyClient);
        tAccountMyClient.getSignatory().add(signatory);
    }

    private void validateAddressData(TPersonMyClient tPersonMyClient){
        TPersonMyClient.Addresses addresses = new TPersonMyClient.Addresses();
        TAddress address = new TAddress();
        address.setAddressType(ContactType.WORK);
        address.setAddress("DFCU head office");
        address.setCity("Uganda");
        address.setCountryCode(CountryType.UG);
        addresses.getAddress().add(address);
        tPersonMyClient.setAddresses(addresses);
    }

    private void setPhoneData(TPersonMyClient tPersonMyClient){
        TPersonMyClient.Phones phones = new TPersonMyClient.Phones();
        TPhone phone = new TPhone();
        phone.setTphContactType(ContactType.PERS);
        phone.setTphCommunicationType(CommunicationType.TEL);
        phone.setTphNumber("63778421");
        phones.getPhone().add(phone);

        tPersonMyClient.setPhones(phones);

    }
    private void setForeignCurrencyObj(Report.Transaction.TFromMyClient tFromMyClient){
        TForeignCurrency foreignCurrency = new TForeignCurrency();
        foreignCurrency.setForeignCurrencyCode(CurrencyType.UGX);
        foreignCurrency.setForeignAmount(new BigDecimal(4000));
        foreignCurrency.setForeignExchangeRate(new BigDecimal(50));

        tFromMyClient.setFromForeignCurrency(foreignCurrency);
    }

    @Override
    public ReportLuw populateDetails(ReportLuw reportLuw, ReportType reportType) {
        List<Report.Transaction> transactionList = reportLuw.getReport().getTransaction();
        for (Report.Transaction transaction : transactionList) {
            addTransactionDetails(transaction);
        }
        reportLuw.getReport().setAction("Transaction have been analysed and statements provided to the FIA.");
        reportLuw.getReport().setReason("Business transactions on personal account.");
        Report.ReportIndicators indicators = new Report.ReportIndicators();
        indicators.getIndicator().add(ReportIndicatorType.TE);
        indicators.getIndicator().add(ReportIndicatorType.BRIS);
        reportLuw.getReport().setReportIndicators(indicators);
        return reportLuw;
    }
}
