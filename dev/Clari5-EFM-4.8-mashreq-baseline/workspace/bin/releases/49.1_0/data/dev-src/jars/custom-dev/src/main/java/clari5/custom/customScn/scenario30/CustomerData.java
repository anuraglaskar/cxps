package clari5.custom.customScn.scenario30;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import org.apache.kafka.common.protocol.types.Field;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;


public class CustomerData {
    public static clari5.platform.logger.CxpsLogger logger = CxpsLogger.getLogger(CustomerData.class);
    public  static CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw  new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }
    public static  List<CustomerResultPojo> getCustmobile_email(){
        CxConnection connection = null;
        List<CustomerResultPojo> customerist= new ArrayList<>();
        String todaydate = todayDate();
        //select * from CUSTOMER where CONVERT (varchar,created_on,23) = '' or CONVERT (varchar,updated_on,23) = '';
        String query="select * from CUSTOMER where  (CONVERT (varchar,created_on,23) = '"+todaydate+"' or CONVERT (varchar,updated_on,23) = '"+todaydate+"') ";
        logger.info("query to get today's customer data"+query);
        try{
            connection= getConnection();
            PreparedStatement ps=connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CustomerResultPojo customerResultPojos= new CustomerResultPojo();
                String custemail1=rs.getString("custEmail1");
                String custMobile1=rs.getString("custMobile1");
                String cxCifID=rs.getString("cxCifID");
                customerResultPojos.setNew_email(custemail1);
                customerResultPojos.setNew_mobile(custMobile1);
                customerResultPojos.setNew_cxcif(cxCifID);
                customerist.add(customerResultPojos);
            }
        }catch (Exception e){
            logger.info("There is no any customer created or updated today in customer Table.");
            e.printStackTrace();
        }
        finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return customerist;
    }
    public  static CustomerResultPojo getCustomerNewData(String newcif){
        CxConnection connection = null;
        String todaydate = todayDate();
        String query ="select * from CUSTOMER where cxCifID = ? and (CONVERT (varchar,created_on,23) = '"+todaydate+"' or CONVERT (varchar,updated_on,23) = '"+todaydate+"')";
          //List<CustomerResultPojo> newdalalist= new ArrayList<>();
        CustomerResultPojo custnewdata = new CustomerResultPojo();
        try {
             connection= getConnection();
            PreparedStatement ps=connection.prepareStatement(query);
            ps.setString(1,newcif);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                custnewdata.setNew_cxcif(rs.getString("cxCifID"));
                custnewdata.setNew_custName(rs.getString("custName"));
                custnewdata.setNew_custType(rs.getString("custType"));
                custnewdata.setNew_eid(rs.getString("nationalId"));
                custnewdata.setNew_passport(rs.getString("custPassportNo"));
                custnewdata.setNew_email(rs.getString("custEmail1"));
                custnewdata.setNew_mobile(rs.getString("custMobile1"));
                custnewdata.setNew_compmis1(rs.getString("COMPMIS1"));
                custnewdata.setNew_compmis2(rs.getString("COMPMIS2"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return custnewdata;
    }
    public  static  List<CustomerResultPojo> getCustomerMatchedDatalist(String custid){
        String query ="select * from customer where cxCifID = ?";
        CxConnection connection = null;
        CustomerResultPojo custolddata = new CustomerResultPojo();
        List<CustomerResultPojo> finaldatalist= new ArrayList<>();
        try {
            connection= getConnection();
            PreparedStatement ps=connection.prepareStatement(query);
            ps.setString(1,custid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                custolddata.setOld_cust_name(rs.getString("custName"));
                custolddata.setOld_custType(rs.getString("custType"));
                custolddata.setOld_eid(rs.getString("nationalId"));
                custolddata.setOld_passportNo(rs.getString("custPassportNo"));
                custolddata.setOld_cif(custid);
                custolddata.setOld_email(rs.getString("custEmail1"));
                custolddata.setOld_mobile(rs.getString("custMobile1"));
                custolddata.setOld_compmis1(rs.getString("COMPMIS1"));
                custolddata.setOld_compmis2(rs.getString("COMPMIS2"));

                finaldatalist.add(custolddata);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return finaldatalist;
    }

    public  static String todayDate(){
        String todaydate="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            Date date= new Date();
            todaydate=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return todaydate;

    }
    public static void main(String[] args) {
        System.out.println("Into main");
    }
}
