package clari5.custom.customScn.scenario34;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;

import java.time.LocalDateTime;

public class AccoutDaemon extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(AccoutDaemon.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
        logger.info("inside getData to call AccEodReminder");
        LocalDateTime currentTime = LocalDateTime.now();
        Hocon hocon= new Hocon();
        hocon.loadFromContext("execution_time.conf");
        int hrsToExecute =hocon.getInt("scn34hrs");
        int minToExecute =hocon.getInt("scn34mins");
        int secToExecute =hocon.getInt("scn34sec");
        AcctEodReminder.callonEOD(hrsToExecute,minToExecute,secToExecute);
        return null;
    }
    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside the Process Data for Count Daemon");
    }
    @Override
    public void configure(Hocon h) {
        logger.debug("this is hocon configuration");
    }
}
