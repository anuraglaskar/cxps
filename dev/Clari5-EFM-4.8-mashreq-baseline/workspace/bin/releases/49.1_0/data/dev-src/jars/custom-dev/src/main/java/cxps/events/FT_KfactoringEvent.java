// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_KFACTORING", Schema="rice")
public class FT_KfactoringEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String factoringContractNumber;
       @Field(size=50) public String invoicenoDiscountedChequeNo;
       @Field public java.sql.Timestamp invoiceChequeDate;
       @Field public java.sql.Timestamp contractEndDateValidity;
       @Field(size=20) public Double invoiceChequeAmount;
       @Field(size=50) public String customerAccountNumber;
       @Field(size=50) public String cifId;
       @Field(size=20) public String invoiceChequeCurrency;


    @JsonIgnore
    public ITable<FT_KfactoringEvent> t = AEF.getITable(this);

	public FT_KfactoringEvent(){}

    public FT_KfactoringEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Kfactoring");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setFactoringContractNumber(json.getString("factoring_contract_number"));
            setInvoicenoDiscountedChequeNo(json.getString("invoiceno_discounted_cheque_no"));
            setInvoiceChequeDate(EventHelper.toTimestamp(json.getString("invoice_cheque_date")));
            setContractEndDateValidity(EventHelper.toTimestamp(json.getString("contract_end_date_validity")));
            setInvoiceChequeAmount(EventHelper.toDouble(json.getString("invoice_cheque_amount")));
            setCustomerAccountNumber(json.getString("customer_account_number"));
            setCifId(json.getString("cif_id"));
            setInvoiceChequeCurrency(json.getString("invoice_cheque_currency"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "KFA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getFactoringContractNumber(){ return factoringContractNumber; }

    public String getInvoicenoDiscountedChequeNo(){ return invoicenoDiscountedChequeNo; }

    public java.sql.Timestamp getInvoiceChequeDate(){ return invoiceChequeDate; }

    public java.sql.Timestamp getContractEndDateValidity(){ return contractEndDateValidity; }

    public Double getInvoiceChequeAmount(){ return invoiceChequeAmount; }

    public String getCustomerAccountNumber(){ return customerAccountNumber; }

    public String getCifId(){ return cifId; }

    public String getInvoiceChequeCurrency(){ return invoiceChequeCurrency; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setFactoringContractNumber(String val){ this.factoringContractNumber = val; }
    public void setInvoicenoDiscountedChequeNo(String val){ this.invoicenoDiscountedChequeNo = val; }
    public void setInvoiceChequeDate(java.sql.Timestamp val){ this.invoiceChequeDate = val; }
    public void setContractEndDateValidity(java.sql.Timestamp val){ this.contractEndDateValidity = val; }
    public void setInvoiceChequeAmount(Double val){ this.invoiceChequeAmount = val; }
    public void setCustomerAccountNumber(String val){ this.customerAccountNumber = val; }
    public void setCifId(String val){ this.cifId = val; }
    public void setInvoiceChequeCurrency(String val){ this.invoiceChequeCurrency = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.invoicenoDiscountedChequeNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Kfactoring");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Kfactoring");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}