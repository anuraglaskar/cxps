package clari5.custom.customCms.manualAlert;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBOperation {

    private static CxpsLogger logger = CxpsLogger.getLogger(DBOperation.class);

    public  boolean insert(String parrentEntityType,String parrentEntityId,String childEntityType,String childEntityId,String scnName,String riskLevel,String riskScore,String message,String casedisplaykey,String eventid,String date,String evidence){
        boolean flg=true;
        PreparedStatement pst=null;
        CxConnection connection=null;
        String triggerId=scnName+parrentEntityType+parrentEntityId;
        //trigger_id is mandatory
        String query="insert into CL5_INCIDENT (trigger_id,project,[source] ,caseentity_name,caseentity_id,entity_name,entity_id,fact_name,risk_level,incident_score,message,module_id,event_id,syncrvn,rvn,status,flag,case_score,casedisplaykey,date_time,opened_date,evidence,opinion_detail,displaykey,summary) VALUES ('"+triggerId+"','MANUAL_ALERT','SAM','"+parrentEntityType+"','"+parrentEntityId+"','"+childEntityType+"','"+childEntityId+"','"+scnName+"','"+riskLevel+"','"+riskScore+"','"+message+"','EFM','"+eventid+"','-1','0','A','F','"+riskScore+"','"+casedisplaykey+"','"+date+"','"+date+"','"+evidence+"','{}','"+childEntityId+"','"+message+"')";
        logger.info("query ---"+query);


        try{
            connection= getConnection();
            pst=connection.prepareStatement(query);
            flg =pst.execute();
            connection.commit();
            return flg;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
    protected  CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw  new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }
}
