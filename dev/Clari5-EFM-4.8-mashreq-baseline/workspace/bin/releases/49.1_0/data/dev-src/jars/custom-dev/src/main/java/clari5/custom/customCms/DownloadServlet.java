package clari5.custom.customCms;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;

public class DownloadServlet extends HttpServlet {
    public static String path="";
    static {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("audit-export-file.conf");
        path=hocon.getString("filePath");
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println(path);
        response.setContentType("application/xlsx");
        PrintWriter out = response.getWriter();
        String filename = "Audit_Log.xlsx";
        String filepath = path;
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");

        FileInputStream fileInputStream = new FileInputStream(filepath + filename);

        int i;
        while ((i=fileInputStream.read()) != -1) {
            out.write(i);
        }
        fileInputStream.close();
        out.close();
    }

}