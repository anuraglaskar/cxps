package clari5.custom.customScn.scenario34;
import java.sql.Date;

public class AccountPojo {
    private String newemail;
    private String newmobile;
    private String newcif;
    private String newpassport;
    private String newcustname;
    private String newcompmis1;
    private String newcompmis2;
    private String newtradelicense;
    private String newaccountno;
    private Date newcustdob;
    private String newbranchid;
    private String neweid;
    private Date newacctopendate;
    private Date newacctcloseddate;
    private String oldemail;
    private String oldmobile;
    private String oldcif;
    private String oldpassport;
    private String oldcustname;
    private String oldcompmis1;
    private String oldcompmis2;
    private String oldtradelicense;
    private String oldaccountno;
    private Date oldcustdob;
    private String oldbranchid;
    private String oldeid;
    private Date oldacctopendate;
    private Date oldacctcloseddate;
    private String matchedmobile;
    private String matchedeid;
    private String matchedpassport;
    private String matchedtradelicense;

    public String getNewemail() {
        return newemail;
    }

    public void setNewemail(String newemail) {
        this.newemail = newemail;
    }

    public String getNewmobile() {
        return newmobile;
    }

    public void setNewmobile(String newmobile) {
        this.newmobile = newmobile;
    }

    public String getNewcif() {
        return newcif;
    }

    public void setNewcif(String newcif) {
        this.newcif = newcif;
    }

    public String getNewpassport() {
        return newpassport;
    }

    public void setNewpassport(String newpassport) {
        this.newpassport = newpassport;
    }

    public String getNewcustname() {
        return newcustname;
    }

    public void setNewcustname(String newcustname) {
        this.newcustname = newcustname;
    }

    public String getNewcompmis1() {
        return newcompmis1;
    }

    public void setNewcompmis1(String newcompmis1) {
        this.newcompmis1 = newcompmis1;
    }

    public String getNewcompmis2() {
        return newcompmis2;
    }

    public void setNewcompmis2(String newcompmis2) {
        this.newcompmis2 = newcompmis2;
    }

    public String getNewtradelicense() {
        return newtradelicense;
    }

    public void setNewtradelicense(String newtradelicense) {
        this.newtradelicense = newtradelicense;
    }

    public String getNewaccountno() {
        return newaccountno;
    }

    public void setNewaccountno(String newaccountno) {
        this.newaccountno = newaccountno;
    }

    public String getNewbranchid() {
        return newbranchid;
    }

    public void setNewbranchid(String newbranchid) {
        this.newbranchid = newbranchid;
    }

    public Date getNewacctopendate() {
        return newacctopendate;
    }

    public void setNewacctopendate(Date newacctopendate) {
        this.newacctopendate = newacctopendate;
    }

    public Date getNewacctcloseddate() {
        return newacctcloseddate;
    }

    public void setNewacctcloseddate(Date newacctcloseddate) {
        this.newacctcloseddate = newacctcloseddate;
    }

    public String getOldemail() {
        return oldemail;
    }

    public void setOldemail(String oldemail) {
        this.oldemail = oldemail;
    }

    public String getOldmobile() {
        return oldmobile;
    }

    public void setOldmobile(String oldmobile) {
        this.oldmobile = oldmobile;
    }

    public String getOldcif() {
        return oldcif;
    }

    public void setOldcif(String oldcif) {
        this.oldcif = oldcif;
    }

    public String getOldpassport() {
        return oldpassport;
    }

    public void setOldpassport(String oldpassport) {
        this.oldpassport = oldpassport;
    }

    public String getOldcustname() {
        return oldcustname;
    }

    public void setOldcustname(String oldcustname) {
        this.oldcustname = oldcustname;
    }

    public String getOldcompmis1() {
        return oldcompmis1;
    }

    public void setOldcompmis1(String oldcompmis1) {
        this.oldcompmis1 = oldcompmis1;
    }

    public String getOldcompmis2() {
        return oldcompmis2;
    }

    public void setOldcompmis2(String oldcompmis2) {
        this.oldcompmis2 = oldcompmis2;
    }

    public String getOldtradelicense() {
        return oldtradelicense;
    }

    public void setOldtradelicense(String oldtradelicense) {
        this.oldtradelicense = oldtradelicense;
    }

    public String getOldaccountno() {
        return oldaccountno;
    }

    public void setOldaccountno(String oldaccountno) {
        this.oldaccountno = oldaccountno;
    }

    public String getOldbranchid() {
        return oldbranchid;
    }

    public void setOldbranchid(String oldbranchid) {
        this.oldbranchid = oldbranchid;
    }

    public Date getOldacctopendate() {
        return oldacctopendate;
    }

    public void setOldacctopendate(Date oldacctopendate) {
        this.oldacctopendate = oldacctopendate;
    }

    public Date getOldacctcloseddate() {
        return oldacctcloseddate;
    }

    public void setOldacctcloseddate(Date oldacctcloseddate) {
        this.oldacctcloseddate = oldacctcloseddate;
    }

    public String getMatchedmobile() {
        return matchedmobile;
    }

    public void setMatchedmobile(String matchedmobile) {
        this.matchedmobile = matchedmobile;
    }

    public String getMatchedeid() {
        return matchedeid;
    }

    public void setMatchedeid(String matchedeid) {
        this.matchedeid = matchedeid;
    }

    public String getMatchedpassport() {
        return matchedpassport;
    }

    public void setMatchedpassport(String matchedpassport) {
        this.matchedpassport = matchedpassport;
    }

    public String getMatchedtradelicense() {
        return matchedtradelicense;
    }

    public void setMatchedtradelicense(String matchedtradelicense) {
        this.matchedtradelicense = matchedtradelicense;
    }

    public String getNeweid() {
        return neweid;
    }

    public void setNeweid(String neweid) {
        this.neweid = neweid;
    }

    public String getOldeid() {
        return oldeid;
    }

    public void setOldeid(String oldeid) {
        this.oldeid = oldeid;
    }
    public Date getNewcustdob() {
        return newcustdob;
    }

    public void setNewcustdob(Date newcustdob) {
        this.newcustdob = newcustdob;
    }

    public Date getOldcustdob() {
        return oldcustdob;
    }

    public void setOldcustdob(Date oldcustdob) {
        this.oldcustdob = oldcustdob;
    }
}
