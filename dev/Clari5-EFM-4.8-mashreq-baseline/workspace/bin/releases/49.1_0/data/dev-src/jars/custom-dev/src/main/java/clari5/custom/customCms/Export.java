package clari5.custom.customCms;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import clari5.platform.util.Hocon;
import java.io.IOException;
import java.sql.*;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
public class Export {
    public static String path="";
    static {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("audit-export-file.conf");
         path=hocon.getString("path");
    }
    public void export(String custid) throws ClassNotFoundException {
       
        String excelFilePath = path;

        try (Connection connection = Rdbms.getAppConnection()) {
            String sql = "SELECT * from Audit_Log where Cust_Id='"+custid+"'";

            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Audit_Log");

            writeHeaderLine(sheet);

            writeDataLines(result, workbook, sheet);

            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            //workbook.close();

            statement.close();

        } catch (SQLException e) {
            System.out.println("Datababse error:");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File IO error:");
            e.printStackTrace();
        }
    }
////
/////
    private void writeHeaderLine(XSSFSheet sheet) {

        Row headerRow = sheet.createRow(0);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("Customer_Id");

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("Status");

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("Date");

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("User_Name");
    }

    private void writeDataLines(ResultSet result, XSSFWorkbook workbook,
                                XSSFSheet sheet) throws SQLException {
        int rowCount = 1;

        while (result.next()) {
            String courseName = result.getString("Cust_Id");
            String rating = result.getString("Freez_Status");
            String timestamp = result.getString("Freez_Date");
            String comment = result.getString("User_Name");

            Row row = sheet.createRow(rowCount++);

            int columnCount = 0;
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(courseName);
            sheet.autoSizeColumn(0);
            cell = row.createCell(columnCount++);
            cell.setCellValue(rating);
            sheet.autoSizeColumn(1);
            cell = row.createCell(columnCount++);
            cell.setCellValue(timestamp);
            sheet.autoSizeColumn(2);
            cell = row.createCell(columnCount);
            cell.setCellValue(comment);
            sheet.autoSizeColumn(3);

        }
    }
    ////
    public  void exportCard(String custid) throws ClassNotFoundException {


        String excelFilePath = path;
        try (Connection connection = Rdbms.getAppConnection() ) {
            String sql = "SELECT * from Audit_Log where Card_number='"+custid+"'";

            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Audit_Log");

            writeHeaderLine1(sheet);

            writeDataLines1(result, workbook, sheet);

            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
          //  workbook.close();

            statement.close();

        } catch (SQLException e) {
            System.out.println("Datababse error:");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File IO error:");
            e.printStackTrace();
        }
    }
    private void writeHeaderLine1(XSSFSheet sheet) {

        Row headerRow = sheet.createRow(0);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("Card_Number");

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("Status");

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("Date");

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("User_Name");
    }

    private void writeDataLines1(ResultSet result, XSSFWorkbook workbook,
                                XSSFSheet sheet) throws SQLException {
        int rowCount = 1;

        while (result.next()) {
            String courseName = result.getString("Card_number");
            String rating = result.getString("Freez_Status");
            String timestamp = result.getString("Freez_Date");
            String comment = result.getString("User_Name");

            Row row = sheet.createRow(rowCount++);

            int columnCount = 0;
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(courseName);
            sheet.autoSizeColumn(0);
            cell = row.createCell(columnCount++);
            cell.setCellValue(rating);
            sheet.autoSizeColumn(1);
            cell = row.createCell(columnCount++);
            cell.setCellValue(timestamp);
            sheet.autoSizeColumn(2);
            cell = row.createCell(columnCount);
            cell.setCellValue(comment);
            sheet.autoSizeColumn(3);

        }
    }


}
