// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCT_STATUS_CHANGE", Schema="rice")
public class NFT_AcctStatusChangeEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String acctStatusChangeCheck;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=10) public String flag;
       @Field(size=20) public String accountId;
       @Field(size=50) public String userId;
       @Field(size=20) public Double avlBal;
       @Field(size=10) public String channel;
       @Field(size=50) public String branchId;
       @Field(size=20) public String custId;
       @Field(size=20) public String intialAcctStatus;
       @Field(size=20) public String finalAcctStatus;
       @Field(size=20) public String acctName;


    @JsonIgnore
    public ITable<NFT_AcctStatusChangeEvent> t = AEF.getITable(this);

	public NFT_AcctStatusChangeEvent(){}

    public NFT_AcctStatusChangeEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AcctStatusChange");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acct_open_date")));
            setFlag(json.getString("flag"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setChannel(json.getString("channel"));
            setBranchId(json.getString("branch_id"));
            setCustId(json.getString("cust_id"));
            setIntialAcctStatus(json.getString("intial_acct_status"));
            setFinalAcctStatus(json.getString("final_acct_status"));
            setAcctName(json.getString("acct_name"));

        setDerivedValues();

    }


    private void setDerivedValues() {
       setAcctStatusChangeCheck(cxps.events.CustomDerivator.getUserIdForAccountId(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "AST"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getFlag(){ return flag; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public Double getAvlBal(){ return avlBal; }

    public String getChannel(){ return channel; }

    public String getBranchId(){ return branchId; }

    public String getCustId(){ return custId; }

    public String getIntialAcctStatus(){ return intialAcctStatus; }

    public String getFinalAcctStatus(){ return finalAcctStatus; }

    public String getAcctName(){ return acctName; }
    public String getAcctStatusChangeCheck(){ return acctStatusChangeCheck; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setFlag(String val){ this.flag = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setIntialAcctStatus(String val){ this.intialAcctStatus = val; }
    public void setFinalAcctStatus(String val){ this.finalAcctStatus = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setAcctStatusChangeCheck(String val){ this.acctStatusChangeCheck = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        wsInfoSet.add(new WorkspaceInfo("User", userKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AcctStatusChange");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AcctStatusChange");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
