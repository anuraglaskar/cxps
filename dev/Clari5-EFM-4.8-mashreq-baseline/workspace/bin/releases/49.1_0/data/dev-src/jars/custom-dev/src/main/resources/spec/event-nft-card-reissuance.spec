cxps.events.event.nft-card-reissuance{
  table-name : EVENT_NFT_CARD_REISSUANCE
  event-mnemonic: CR
  workspaces : {
    
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
card-no: {db : true ,raw_name : card_no ,type : "string:200"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
new-card-flg: {db : true ,raw_name : new_card_flg ,type : "string:200"}
card-valid-frm: {db : true ,raw_name : card_valid_frm ,type : "string:200"}
card-valid-to: {db : true ,raw_name :card_valid_to,type : "string:200"}
card-atm-lmt: {db :true ,raw_name : card_atm_lmt ,type : "number:20,3"}
card-pos-lmt: {db :true ,raw_name : card_pos_lmt ,type : "number:20,3"}
card-type: {db : true ,raw_name : card_type ,type : "string:200"}
channel: {db : true ,raw_name : channel ,type : "string:200"}
}
}

