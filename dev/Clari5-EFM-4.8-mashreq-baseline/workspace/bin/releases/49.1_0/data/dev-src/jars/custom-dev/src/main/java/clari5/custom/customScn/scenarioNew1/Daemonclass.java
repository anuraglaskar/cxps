package clari5.custom.customScn.scenarioNew1;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Daemonclass extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(Daemonclass.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
        logger.info("inside process data into application Data --");
        Hocon hocon= new Hocon();
        hocon.loadFromContext("execution_time.conf");

        int hrsToExecute =hocon.getInt("scnNew1hrs");
        int minToExecute =hocon.getInt("scnNew1mins");
        int secToExecute =hocon.getInt("scnNew1sec");

        String selectQuery=hocon.getString("selectQuery");
        String insertQuery=hocon.getString("insertQuery");
        String updateQuery=hocon.getString("updateQuery");

        /*applying synchronized*/
        synchronized (this) {
            String threadName=Thread.currentThread().getName();
            long threadId=Thread.currentThread().getId();
            Date utilDate=new Date();
            java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());// current date
            logger.info("[Daemonclass] Accquired Lock"+threadName +" with id"+threadId);
            try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
                Connection connection=rdbmsSession.getCxConnection();
                PreparedStatement selectQueryStatement=connection.prepareStatement(selectQuery)){
                selectQueryStatement.setString(1,threadName);
                selectQueryStatement.setDate(2,sqlDate);
                ResultSet resultSet=selectQueryStatement.executeQuery();
                if(resultSet.next()){
                    logger.info("[Daemonclass] One of the thread has already processed.Hence quiting");
                }
                else{
                   EodReminder.callonEOD(hrsToExecute, minToExecute, secToExecute,threadName,threadId,sqlDate,insertQuery,updateQuery,selectQuery);
                }

            } catch (SQLException e) {
                logger.error(e.getCause() + " "+e.getMessage());
            }
        }


        return null;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside the Process Data for Count Daemon");
    }
    @Override
    public void configure(Hocon h) {
        logger.debug("this is hocon configuration");
    }
}
