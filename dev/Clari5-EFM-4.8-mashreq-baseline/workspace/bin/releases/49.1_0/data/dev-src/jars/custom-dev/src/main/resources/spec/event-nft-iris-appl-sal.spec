cxps.events.event.nft-iris-appl-sal{  
  table-name : EVENT_NFT_IRIS_APPL_SAL
  event-mnemonic: IAS
  workspaces : {
    NONCUSTOMER: "app-ref-no + iban-number"  
}
 event-attributes : {

ip-address: {db:true ,raw_name : ip_address ,type:"string:20"}
account-id: {db:true ,raw_name : account_id ,type:"string:20"}
app-ref-no: {db :true ,raw_name : app_ref_no ,type : "string:20"}
product: {db:true ,raw_name : product ,type:"string:20"}
eida: {db:true ,raw_name : eida ,type:"string:20"}
cust-id: {db:true ,raw_name : cust_id ,type:"string:20"}
ph-no: {db:true ,raw_name : ph_no ,type:"string:20"}
email-id: {db:true ,raw_name : email_id ,type:"string:20"}
channel: {db:true ,raw_name : channel ,type:"string:20"}
succ-fail-flag: {db:true ,raw_name : succ_fail_flg ,type:"string:20"}
ip-country: {db:true ,raw_name : ip_country ,type:"string:20"}
appl-stage: {db:true ,raw_name : appl_stage ,type:"string:50"}
branch-id: {db:true ,raw_name : branch_id ,type:"string:20"}
dob: {db:true ,raw_name : dob ,type:timestamp}
nationality: {db:true ,raw_name : nationality ,type:"string:50"}
address1: {db:true ,raw_name : address1 ,type:"string:200"}
address2: {db:true ,raw_name : address2 ,type:"string:200"}
landline-no: {db:true ,raw_name : landline_no ,type:"string:50"}
po-box: {db:true ,raw_name : po_box ,type:"string:50"}
passport-no: {db:true ,raw_name : passport_no ,type:"string:50"}
driving-lnc: {db:true ,raw_name : driving_lnc ,type:"string:50"}
trade-license: {db:true ,raw_name : trade_license ,type:"string:50"}
device-id: {db :true ,raw_name : device_id ,type : "string:50"}
tran-date: {db:true ,raw_name : tran_date ,type:timestamp}
vat: {db:true ,raw_name : vat ,type:"string:50"}
employer-id: {db:true ,raw_name : employer_id ,type:"string:50"}
is-employer-nonsfa: {db:true ,raw_name : is_employer_nonsfa ,type:"string:50"}
employee-id: {db:true ,raw_name : employee_id ,type:"string:50"}
npa-writtenoff: {db:true ,raw_name : npa_writtenoff ,type:"string:100" }
npa-product: {db:true ,raw_name : npa_product ,type:"string:50"}
npa-date: {db:true ,raw_name : npa_date ,type:timestamp}
iban-number: {db:true ,raw_name : iban_number ,type:"string:50"}
is-iban-external: {db:true ,raw_name : is_iban_external ,type:"string:20"}
off-address1: {db:true ,raw_name : off_address1 ,type:"string:50"}
off-address2: {db:true ,raw_name : off_address2 ,type:"string:50"}
off-landline: {db:true ,raw_name : off_landline ,type:"string:50"}
off-po-box: {db:true ,raw_name : off_po_box ,type:"string:50"}
res-address1: {db:true ,raw_name : res_address1 ,type:"string:50"}
res-address2: {db:true ,raw_name : res_address2 ,type:"string:50"}
res-landline: {db:true ,raw_name : res_landline ,type:"string:50"}
res-po-box: {db:true ,raw_name : res_po_box ,type:"string:50"}
per-address1: {db:true ,raw_name : per_address1 ,type:"string:50"}
per-address2: {db:true ,raw_name : per_address2 ,type:"string:50"}
per-landline: {db:true ,raw_name : per_landline ,type:"string:50"}
per-po-box: {db:true ,raw_name : per_po_box ,type:"string:50"}
comm-address1: {db:true ,raw_name : comm_address1 ,type:"string:50"}
comm-address2: {db:true ,raw_name : comm_address2 ,type:"string:50"}
comm-landline: {db:true ,raw_name : comm_landline ,type:"string:50"}
comm-po-box: {db:true ,raw_name : comm_po_box ,type:"string:50"}
created-on { name : created_on, type : timestamp}
updated-on { name : updated_on, type : timestamp}
sys-time: {db:true ,raw_name : sys_time,type:timestamp}
seller-id: {db:true ,raw_name : seller_id ,type:"string:50"}
seller-name: {db:true ,raw_name : seller_name ,type:"string:50"}
matched-eid{db:true, raw_name : matched_eid , type: "string:5"}
matched-dob:{db:true, raw_name : matched_dob , type: "string:5"}
matched-phno:{db:true, raw_name : matched_phno, type: "string:5"}
matched-ppno:{db:true, raw_name : matched_ppno , type: "string:5"}
salary-date: {db:true ,raw_name : salary_date,type:timestamp}
salary-amt:{db:true, raw_name : salary_amt , type: "number:20,3"}
debit-amount:{db:true, raw_name : debit_amount , type: "number:20,3"}
salary-tran-type:{db:true, raw_name : salary_tran_type , type: "string:5"}
salary-narative:{db:true, raw_name : salary_narative , type: "string:5"}
acctopendate: {db : true ,raw_name : acctopendate ,type : timestamp}
}
}
