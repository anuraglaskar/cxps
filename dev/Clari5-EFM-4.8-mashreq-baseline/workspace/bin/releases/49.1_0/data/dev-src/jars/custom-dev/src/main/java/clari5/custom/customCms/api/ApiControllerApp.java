package clari5.custom.customCms.api;

import org.apache.htrace.fasterxml.jackson.annotation.JsonIgnore;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ApiControllerApp extends Application {

    private Set<Object> singletons = new HashSet();

    public ApiControllerApp() {

        singletons.add(new ApiController());

    }

    @JsonIgnore
    public Set<Object> getSingletons()
    {
        return singletons;
    }


}
