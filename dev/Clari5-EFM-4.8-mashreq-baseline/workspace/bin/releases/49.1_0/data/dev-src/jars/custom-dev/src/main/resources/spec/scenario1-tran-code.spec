cxps.noesis.glossary.entity.scenario1-tran-code {
       db-name = scenario1_TRAN_CODE
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = tran-code, column = TRAN_CODE, type = "string:20", key=true }
               ]
       }
