#package where the class will be generated
clari5.custom.customop.db {
        entity {
          card-details {
          attributes = [
                { name : app-id, type : "string:10"}
                { name : freeze-url, type : "string:100",key: true}
                { name : port, type : "string:5"}
                { name : connection-timeout, type : "string:10"}
                { name : freeze-req-stepid, type : "string:10"}
                { name : freeze-tranid, type : "string:10"}
                { name : freeze-error-tranid, type : "string:10"}
                { name : unfreeze-error-tranid, type : "string:10"}
                { name : unfreeze-tranid, type : "string:10"}
                { name : unfreeze-req-stepid, type : "string:10"}
                { name : last-modified-by, type : "string:100"}
                { name : last-modified-on, type :timestamp}
                ]
                 criteria-query {
                         name:CardDetails
                          summary = [app-id,freeze-url,port,connection-timeout,freeze-req-stepid,freeze-tranid,freeze-error-tranid,unfreeze-req-stepid,unfreeze-error-tranid,unfreeze-tranid,last-modified-by,last-modified-on]
                          where {
                       freeze-url: equal-clause
                    }
                 }
                }
        }
}


