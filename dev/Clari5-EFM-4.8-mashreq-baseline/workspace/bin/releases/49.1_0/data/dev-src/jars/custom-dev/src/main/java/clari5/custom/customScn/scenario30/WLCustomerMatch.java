package clari5.custom.customScn.scenario30;

import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import clari5.aml.web.wl.search.WlQuery;
import clari5.aml.web.wl.search.WlRecordDetails;
import clari5.aml.web.wl.search.WlResult;
import clari5.aml.web.wl.search.WlSearcher;
import clari5.aml.wle.MatchedRecord;
import clari5.aml.wle.MatchedResults;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class WLCustomerMatch {

    private static final CxpsLogger logger = CxpsLogger.getLogger(WLCustomerMatch.class);

    public  void getMatchedResult() {
        CustomerData customerData = new CustomerData();
        List<CustomerResultPojo> datalist = customerData.getCustmobile_email();
        logger.info("[WLCustomerMatch] Fetching Matched Result");
        if(datalist.size()>0){
            logger.info("[WLCustomerMatch] Customer inserted or updated on today's date are: "+datalist.size());
        }
        for (CustomerResultPojo finaldata: datalist) {
                String resp="";
                String ruleName = "MASHREQCUST";
                JSONObject obj = new JSONObject();
                JSONArray array = new JSONArray();
                obj.put("ruleName", ruleName);
                JSONObject jo1 = new JSONObject();
                jo1.put("name", "custEmail1");
                jo1.put("value", finaldata.getNew_email());
                array.put(jo1);
                JSONObject jo2 = new JSONObject();
                jo2.put("name", "custMobile1");
                jo2.put("value", finaldata.getNew_mobile());
                array.put(jo2);
                obj.put("fields", array);
                logger.info("object of json --> "+obj);
                resp = WLCustomerMatch.getResponse(obj);
                logger.info("[WLCustomerMatch] Data Sent for"+finaldata.getNew_cxcif());
                MqWriter.resposeFormatter(resp, finaldata.getNew_cxcif());
                //resp = WLCustomerMatch.getResponse(obj);
                //MqWriter.resposeFormatter(resp, finaldata.getNew_cxcif());
                logger.info("[WLCustomerMatch] Writing completed");
            }
    }

    private static String getResponse(Object obj) {
        String response = "";
        try {
            WlQuery query = new WlQuery();
            query.from(obj.toString());
            logger.info("[WLCustomerMatch] getResponse(): Query object created from json : " + obj.toString());
            WlSearcher searcher = new WlSearcher();
            WlResult result = searcher.search(query);
            MatchedResults matched = result.getMatchedResults();
            Collection<MatchedRecord> recordList = matched.getMatchedRecords();
            if (recordList != null) {
                logger.info("[WLCustomerMatch] Found Matched Results"+recordList);
                for (MatchedRecord record : recordList) {
                    WlRecordDetails wlDetails = new WlRecordDetails();
                    String outputDetails = wlDetails.getRecordDetails(record.getDocId());
                    record.setDetails(outputDetails);
                }
                response = result.toJson();
            }
            logger.info("[WLCustomerMatch] getResponse():  For Multiple customer ::" + response);
        } catch (Exception e) {
            logger.error("[WLCustomerMatch] unable to fetch response from WL for URL: ---> " + obj.toString());
            e.getCause();
            e.printStackTrace();
        }
        return response;
    }
    private static int getRecordCount(String response) {
        int size = 0;
        try {
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(response).getAsJsonObject();
            if (jsonObject.has("matchedResults")) {
                jsonObject = jsonObject.getAsJsonObject("matchedResults");
                if (jsonObject.has("matchedRecords")) {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("matchedRecords");
                    size = jsonArray.size();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("Number of matched ::" + size);
        return size;
    }

}