package clari5.custom.customCms.freezUnfreez;

import clari5.custom.customCms.services.CallingApi;
import clari5.custom.customCms.services.StringToXmlParser;
import clari5.platform.logger.CxpsLogger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.*;

public class FreezeUnfreezeController {

    private String req = "";
    private boolean opFromdb;
    Response response = new Response();
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private Date date = new Date();
    private Random rand = new Random();

    private FreezeUnfreezeDao freezeUnfreezeDao = new FreezeUnfreezeDao();
    private static CxpsLogger logger = CxpsLogger.getLogger(FreezeUnfreezeController.class);
    private CallingApi callingApi = new CallingApi();
    StringToXmlParser stringToXmlParser = new StringToXmlParser();
    Map m=null;
    private boolean cretOrMofify=false;
    public boolean callingToFreez(String customerNo, String fAPi, int time,String envType) {
        logger.info("info inside calling to freez method");
        logger.info("this is customer no-------->"+customerNo+"     freeeze api ------------>"+fAPi+"");
        String custom_No=null;
        String frd_Flag=null;

        HashMap hm=freezeUnfreezeDao.checkCustomerExistForFirstTime(customerNo);
            custom_No=hm.get("cust_no").toString();
            frd_Flag=hm.get("flag").toString();
            logger.info("this is op from checkCustomerExistForFirstTime ----> "+ custom_No  +"this is frd_flag------->"+frd_Flag);
        if(custom_No.equalsIgnoreCase("noValue")){
            //this is for create
            cretOrMofify=true;
            logger.info("cretOrModify----------->"+cretOrMofify);
            req="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://www.mbcdm.customerservices.com/CreateMarkFreeze\" xmlns:mbc=\"http://www.mbcdm.header.com\" xmlns:mbc1=\"http://www.mbcdm.customer.com\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <cre:EAIServices>\n" +
                    "         <cre:Header>\n" +
                    "            <mbc:SrcAppId>"+envType+"</mbc:SrcAppId>\n" +
                    "            <mbc:SrcMsgId>"+rand.nextInt()+"</mbc:SrcMsgId>\n" +
                    "            <mbc:SrvCode>TCRMKFZ</mbc:SrvCode>\n" +
                    "            <mbc:OrgId>AE</mbc:OrgId>\n" +
                    "            <mbc:UserId>EFMUSER</mbc:UserId>\n" +
                    "         </cre:Header>\n" +
                    "         <cre:Body>\n" +
                    "            <cre:CreateMarkFreezeRequest>\n" +
                    "               <mbc1:CifId>"+customerNo+"</mbc1:CifId>\n" +
                    "               <mbc1:FraudFreeze>Y</mbc1:FraudFreeze>\n" +
                    "            </cre:CreateMarkFreezeRequest>\n" +
                    "         </cre:Body>\n" +
                    "      </cre:EAIServices>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("this is request for freez--->",req);
        }
            else if(frd_Flag.equalsIgnoreCase("N")){
                cretOrMofify=false;
                req="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mod=\"http://www.mbcdm.customerservices.com/ModifyMarkFreeze\" xmlns:mbc=\"http://www.mbcdm.header.com\" xmlns:mbc1=\"http://www.mbcdm.customer.com\">\n" +
                        "   <soapenv:Header/>\n" +
                        "   <soapenv:Body>\n" +
                        "      <mod:EAIServices>\n" +
                        "         <mod:Header>\n" +
                        "            <mbc:SrcAppId>"+envType+"</mbc:SrcAppId>\n" +
                        "            <mbc:SrcMsgId>"+rand.nextInt()+"</mbc:SrcMsgId>\n" +
                        "            <mbc:SrvCode>TMDMAFZ</mbc:SrvCode>\n" +
                        "            <mbc:OrgId>AE</mbc:OrgId>\n" +
                        "            <mbc:UserId>EFMUSER</mbc:UserId>\n" +
                        "         </mod:Header>\n" +
                        "         <mod:Body>\n" +
                        "            <mod:ModifyMarkFreezeRequest>\n" +
                        "               <mbc1:CifId>"+customerNo+"</mbc1:CifId>\n" +
                        "               <mbc1:FraudFreeze>Y</mbc1:FraudFreeze>\n" +
                        "            </mod:ModifyMarkFreezeRequest>\n" +
                        "         </mod:Body>\n" +
                        "      </mod:EAIServices>\n" +
                        "   </soapenv:Body>\n" +
                        "</soapenv:Envelope>";

            logger.info("this is cretOrModify"+cretOrMofify);

            }
            else
        {
            logger.warn("this is customer no and freeze status from db",custom_No," ", frd_Flag);
        }
        String op = callingApi.callingMashreqApi(fAPi,req,time);

        //logger.info("o/p from callingToFreez api",op);
        //based on the above response need to change
        if (op.equalsIgnoreCase("time out exception")) {

            response.setErrorCode("");
            response.setStatus("");
            response.setErrorDescription("time out exception");
            opFromdb = freezeUnfreezeDao.checkCustomerExist(customerNo, "fz", true, response);
            return opFromdb;
        } else {

            if(cretOrMofify==true) {
                 m = stringToXmlParser.freezeparsing(op);
                logger.info("return map for Freeze :: " + m);
            }
            else if(cretOrMofify==false)
            {
                m = stringToXmlParser.unfreezeparsing(op);
                logger.info("return map for Freeze :: " + m);

            }
            else
            {
                logger.warn("");
            }
            if(m.get("error_desc").toString().equalsIgnoreCase("Success"))
            {
                response.setErrorCode(m.get("error_code").toString());
                response.setErrorDescription(m.get("error_desc").toString());
                response.setStatus(m.get("status").toString());
                opFromdb = freezeUnfreezeDao.checkCustomerExist(customerNo, "fz", false, response);
                return opFromdb;
            }
            else
            {
                response.setErrorCode(m.get("error_code").toString());
                response.setErrorDescription(m.get("error_desc").toString());
                response.setStatus(m.get("status").toString());
                opFromdb = freezeUnfreezeDao.checkCustomerExist(customerNo, "fz", false, response);
                //this is failure response from api so i am returning false response
                return false;
            }
        }
    }
        //this method to pass req to mashreq api calling method
        public boolean callingToUnFreez (String customerNo,String fAPi, int time,String envType){

            req="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mod=\"http://www.mbcdm.customerservices.com/ModifyMarkFreeze\" xmlns:mbc=\"http://www.mbcdm.header.com\" xmlns:mbc1=\"http://www.mbcdm.customer.com\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <mod:EAIServices>\n" +
                    "         <mod:Header>\n" +
                    "            <mbc:SrcAppId>"+envType+"</mbc:SrcAppId>\n" +
                    "            <mbc:SrcMsgId>"+rand.nextInt()+"</mbc:SrcMsgId>\n" +
                    "            <mbc:SrvCode>TMDMAFZ</mbc:SrvCode>\n" +
                    "            <mbc:OrgId>AE</mbc:OrgId>\n" +
                    "            <mbc:UserId>EFMUSER</mbc:UserId>\n" +
                    "         </mod:Header>\n" +
                    "         <mod:Body>\n" +
                    "            <mod:ModifyMarkFreezeRequest>\n" +
                    "               <mbc1:CifId>"+customerNo+"</mbc1:CifId>\n" +
                    "               <mbc1:FraudFreeze>N</mbc1:FraudFreeze>\n" +
                    "            </mod:ModifyMarkFreezeRequest>\n" +
                    "         </mod:Body>\n" +
                    "      </mod:EAIServices>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";
            String op = callingApi.callingMashreqApi(fAPi,req,time);
            logger.info("o/p from callingToUnFreez api",op);
            if (op.equalsIgnoreCase("time out exception")) {
                response.setErrorCode("");
                response.setStatus("");
                response.setErrorDescription("time out exception");
                opFromdb = freezeUnfreezeDao.checkCustomerExist(customerNo, "uf", true, response);
                return opFromdb;
            } else {
                Map m = stringToXmlParser.unfreezeparsing(op);
                logger.info("final map is for UnFreeze :: "+m);

                if(m.get("error_desc").toString().equalsIgnoreCase("Success"))
                {
                    response.setErrorCode(m.get("error_code").toString());
                    response.setErrorDescription(m.get("error_desc").toString());
                    response.setStatus(m.get("status").toString());
                    opFromdb = freezeUnfreezeDao.checkCustomerExist(customerNo, "uf", false, response);
                    return opFromdb;

                }
                else
                {
                    response.setErrorCode(m.get("error_code").toString());
                    response.setErrorDescription(m.get("error_desc").toString());
                    response.setStatus(m.get("status").toString());
                    opFromdb = freezeUnfreezeDao.checkCustomerExist(customerNo, "uf", false, response);
                    //this is failure response from api so i am returning false response
                    return false;

                }


            }
        }

    }