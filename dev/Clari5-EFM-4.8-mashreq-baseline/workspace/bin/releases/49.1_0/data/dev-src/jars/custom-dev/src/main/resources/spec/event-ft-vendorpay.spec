cxps.events.event.ft-vendorpay{  
  table-name : EVENT_FT_VENDORPAY
  event-mnemonic: VEN
  workspaces : {
    NONCUSTOMER: "vendor-iban"
  }
event-attributes : {
vendor-name: {db:true ,raw_name : vendor_name ,type:"string:20"}
invoice-date: {db:true ,raw_name : invoice_date ,type:"string:20"}
invoice-amount: {db :true ,raw_name : invoice_amount ,type : "string:50"}
invoice-curr: {db:true ,raw_name : invoice_curr ,type:"string:20"}
invoice-no: {db:true ,raw_name : invoice_no ,type:"string:50"}
vendor-iban: {db:true ,raw_name : vendor_iban ,type:"string:20"}
}
}
