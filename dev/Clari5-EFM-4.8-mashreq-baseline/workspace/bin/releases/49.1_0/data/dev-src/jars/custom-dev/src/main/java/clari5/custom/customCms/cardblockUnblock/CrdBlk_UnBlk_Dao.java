package clari5.custom.customCms.cardblockUnblock;

import clari5.custom.customCms.cardblockUnblock.CrdBlk_UnBlk_Controller;
import clari5.custom.customCms.cardblockUnblock.Response;
import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;


public class CrdBlk_UnBlk_Dao {
    private Response response= null;
    Connection conn = null;
    Statement stmt = null;
    private static CxpsLogger logger = CxpsLogger.getLogger(CrdBlk_UnBlk_Controller.class);
    //this is to check is customer no is present or not
    public boolean checkCustomerExist(String custNo, String condT0CheckFZUfz, boolean flagToException, Response response) {

        boolean op=false;
        String cust_no = "";
        String query = "select * from CRDBLK_UNBLK_DETAILS where cardNo='" + custNo + "'";
        logger.info("select query in  card checkCustomerExist"+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                cust_no = rs.getString(1);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (cust_no == "" || cust_no == null) {
            op= insert(custNo, flagToException, response);
        } else {

            if (condT0CheckFZUfz.equalsIgnoreCase("cb")) {
                op=  updateToBlock(custNo,flagToException, response);
            } else if (condT0CheckFZUfz.equalsIgnoreCase("cu")) {
                op= updateToUnBlock(custNo, flagToException, response);

            }
        }
        return op;

    }

    private boolean insert(String custNo, boolean flag, Response response) {
        String query;
        if(flag)
        {
            query = "INSERT INTO CRDBLK_UNBLK_DETAILS (cardNo,blkStatus, status,code,  description) values('" + custNo + "','P','','','time out exception')";

        }
        else if(response.getStatus().equalsIgnoreCase("s")){

            query = "INSERT INTO CRDBLK_UNBLK_DETAILS (cardNo, blkStatus,status,code,description) values('" + custNo + "','Y','"+response.getStatus()+"','"+response.getErrorCode() +"','"+response.getErrorDescription()+" ')";
        }
        else {

            query = "INSERT INTO CRDBLK_UNBLK_DETAILS (cardNo, blkStatus, status,code,description) values('" + custNo + "','N','"+response.getStatus()+"','"+response.getErrorCode() +"','"+response.getErrorDescription()+" ')";
        }
        System.out.println("insert query for Card :: "+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            int i = stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean updateToUnBlock(String custNo , boolean flag, Response response) {
        String query;
        if(flag){

            query = "update CRDBLK_UNBLK_DETAILS set blkStatus='P',status='',code='',description='time out exception' where cardNo='" + custNo + "'";
        }
        else if(response.getStatus().equalsIgnoreCase("s")){

            query = "update CRDBLK_UNBLK_DETAILS set blkStatus='N',status='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where cardNo='" + custNo + "'";

        }
        else {
            query = "update CRDBLK_UNBLK_DETAILS set blkStatus='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where cardNo='" + custNo + "'";
        }
        logger.info("update to Unblok ::"+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean updateToBlock(String custNo, boolean flag, Response response) {
        String query;
        if(flag){

            query = "update CRDBLK_UNBLK_DETAILS set blkStatus='P',status='',code='',description='time out exception' where cardNo='" + custNo + "'";
        }
        else if(response.getStatus().equalsIgnoreCase("s")){

            query = "update CRDBLK_UNBLK_DETAILS set blkStatus='Y',status='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where cardNo='" + custNo + "'";

        }
        else {
            query = "update CRDBLK_UNBLK_DETAILS set blkStatus='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where cardNo='" + custNo + "'";
        }
        logger.info("update to block :: "+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

}
