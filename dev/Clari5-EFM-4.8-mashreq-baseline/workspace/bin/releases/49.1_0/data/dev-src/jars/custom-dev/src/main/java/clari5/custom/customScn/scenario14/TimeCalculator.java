package clari5.custom.customScn.scenario14;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeCalculator {
    public String curr_month_name(){
        String monthname="";
        try{
            Calendar c = Calendar.getInstance();
            monthname=c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH );
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return monthname;
    }
    public String last_month_name(){
        String last_month_name="";
        try{
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, -1);
            last_month_name =c.getDisplayName(Calendar.MONTH, Calendar.LONG , Locale.ENGLISH );

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return last_month_name;
    }
    public  String fistdateofCurrentMonth(){
        String firtdate="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();   // this takes current date
            c.set(Calendar.DAY_OF_MONTH, 1);
            Date date  = c.getTime();
            firtdate=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return firtdate;

    }
    public  String todayDate(){
        String todaydate="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            Date date= new Date();
            todaydate=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return todaydate;

    }
    public  String getSysTime(){
        String systime="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= new Date();
            systime=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return systime;

    }
    public String lastDatePreMonth(){
        String lastdatepremonth="";
        try{
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();   // this takes current date
            c.add(Calendar.MONTH, -1);
            c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE)); // changed calendar to cal
            Date date  = c.getTime();
            lastdatepremonth=simpleDateFormat.format(date).toString();
        }catch (Exception e){

        }
        return lastdatepremonth;
    }
    public String firstDatePreMonth(){
        String firstdatepremonth="";
        try{
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, -1);
            c.set(Calendar.DATE, 1);
            Date date  = c.getTime();
            String firtdate=simpleDateFormat.format(date).toString();
            return firtdate;
        }catch (Exception e){

        }
        return firstdatepremonth;
    }
    public  String lastdateofpreSixMonth(){
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();   // this takes current date
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE)); // changed calendar to cal
        Date date  = c.getTime();
        String lastdate=simpleDateFormat.format(date).toString();
        return lastdate;
    }
    public  String firstdatepresixMonth(){
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();   // this takes current date
        c.add(Calendar.MONTH, -6);
        c.set(Calendar.DATE, 1); // changed calendar to cal
        Date date  = c.getTime();
        String lastdate=simpleDateFormat.format(date).toString();
        return lastdate;
    }

}
