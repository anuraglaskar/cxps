cxps.events.event.nft-cheque-book-req{
  table-name : EVENT_NFT_CHEQUE_BOOK_REQUEST
  event-mnemonic: CB
  workspaces : {
    
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
account-id: {db : true ,raw_name : account_id ,type : "string:20"}
account-name: {db : true ,raw_name : account_name ,type : "string:20"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:200", derivation:"""cxps.events.CustomDerivator.getCustIdFromAccountChequeBookReq(this)"""}
beginchqnum: {db : true ,raw_name : beginchqnum ,type : "string:20"}
issuedlvs: {db : true ,raw_name : issuedlvs ,type : "string:20"}
beginalpha: {db : true ,raw_name : beginalpha ,type : "string:20"}
unusedlvs: {db : true ,raw_name : unusedlvs ,type : "string:20"}
endchqnum: {db : true ,raw_name : endchqnum ,type : "string:20"}
user-id: {db : true ,raw_name : user_id ,type : "string:20"}
branch-id: {db : true ,raw_name : branch_id ,type : "string:20"}
branch-id-desc: {db : true ,raw_name : branch_id_desc ,type : "string:20"}
emp-id: {db : true ,raw_name : emp_id ,type : "string:20"}
channel: {db : true ,raw_name : channel ,type : "string:20"}
}
}
