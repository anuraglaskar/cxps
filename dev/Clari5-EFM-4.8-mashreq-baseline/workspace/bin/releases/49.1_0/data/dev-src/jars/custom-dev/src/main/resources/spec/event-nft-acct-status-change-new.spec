cxps.events.event.nft-acct-status-change-new{
table-name : EVENT_NFT_ACCT_STATUS_CHANGE_NEW
event-mnemonic : ASTN
workspaces :
 { 
   USER : "user-id" 
}
event-attributes : {
channel : { db : true, raw_name : channel, type:"string:10" }
account-id : { db : true, raw_name : account_id, type:"string:20"}
cust-id : { db : true, raw_name : cust_id, type:"string:20"}
acct-name : { db : true, raw_name : acct_name, type:"string:20"}
user-id: {db : true ,raw_name : user_id ,type : "string:50"}
avl-bal :  { db : true, raw_name : avl_bal, type:"number:20,4"}
branch-id: { db : true ,raw_name : branch_id ,type : "string:50"}
intial-acct-status :  { db : true, raw_name : intial_acct_status, type:"string:20"}
final-acct-status :  { db : true, raw_name : final_acct_status, type:"string:20"}
acct-open-date :  { db : true, raw_name : acct_open_date, type: timestamp}
}
}






