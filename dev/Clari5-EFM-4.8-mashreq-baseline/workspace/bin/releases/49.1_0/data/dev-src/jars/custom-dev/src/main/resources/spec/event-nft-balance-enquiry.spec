cxps.events.event.nft-balance-enquiry{  
  table-name : EVENT_NFT_BALANCE_ENQUIRY
  event-mnemonic: BE
  workspaces : {
    CUSTOMER: "cust-id"
  }
 event-attributes : {

host-id: {db:true ,raw_name : host_id ,type:"string:2"}
account-id: {db:true ,raw_name : account_id ,type:"string:20"}
avl-bal: {db :true ,raw_name : avl_bal ,type : "number:20,3"}
menu-id: {db:true ,raw_name : menu_id ,type:"string:20"}
user-id: {db:true ,raw_name : user_id ,type:"string:20"}
cust-id: {db:true ,raw_name : cust_id ,type:"string:20"}
branch-id: {db:true ,raw_name : branch_id ,type:"string:20"}
branch-id-desc: {db:true ,raw_name : branch_id_desc ,type:"string:20"}
emp-id: {db:true ,raw_name : emp_id ,type:"string:20"}
acct-name: {db:true ,raw_name : acct_name ,type:"string:20"}
Channel: {db:true ,raw_name : channel ,type:"string:20"}
cust-card-id: {db:true ,raw_name : cust_card_id ,type:"string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
}
}
