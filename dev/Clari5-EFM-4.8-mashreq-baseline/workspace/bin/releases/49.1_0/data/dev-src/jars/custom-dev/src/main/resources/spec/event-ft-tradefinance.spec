cxps.events.event.ft-tradefinance{
  table-name : EVENT_FT_TRADEFINANCE
  event-mnemonic: TF
  workspaces : {
   NONCUSTOMER: "bill-of-lading"
}
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
corereferenceno: {db : true ,raw_name : corereferenceno ,type : "string:20"}
edmsreferenceno: {db : true ,raw_name : edmsreferenceno ,type : "string:20"}
product-type: {db : true ,raw_name : product_type,type : "string:20"}
cif-number: {db : true ,raw_name : cif_number,type: "string:20" }
amount: {db : true ,raw_name : amount ,type : "number:20,4"}
currency: {db : true ,raw_name : currency ,type : "string:20"}
applicant-name: {db : true ,raw_name : applicant_name ,type : "string:200"}
beneficiary-name: {db : true ,raw_name : beneficiary_name ,type : "string:200"}
bill-of-lading: {db : true ,raw_name : bill_of_lading ,type : "string:200"}
buyer-name: {db : true ,raw_name : buyer_name ,type : "string:200"}
seller-name: {db : true ,raw_name : seller_name ,type : "string:200"}
contract-expiry-date: {db : true ,raw_name : contract_expiry_date ,type : timestamp}
lc-number: {db : true ,raw_name : lc_number ,type : "string:200"}
bill-number: {db : true ,raw_name : bill_number ,type : "string:200"}
}
}

