package clari5.custom.test;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Body")
public class Body{
    CreateMarkFreezeRequest createMarkFreezeRequest;

    public CreateMarkFreezeRequest getCreateMarkFreezeRequest() {
        return createMarkFreezeRequest;
    }

    public void setCreateMarkFreezeRequest(CreateMarkFreezeRequest createMarkFreezeRequest) {
        this.createMarkFreezeRequest = createMarkFreezeRequest;
    }


}