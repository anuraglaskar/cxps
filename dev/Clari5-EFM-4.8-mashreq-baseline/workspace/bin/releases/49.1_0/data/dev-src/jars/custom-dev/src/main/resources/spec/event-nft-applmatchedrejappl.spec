cxps.events.event.nft-applmatchedrejappl{  
  table-name : EVENT_NFT_APPLMATCHEDREJAPPL
  event-mnemonic: NRA
  workspaces : {
    NONCUSTOMER: "current-date"
}
 event-attributes : {

current-date: {db:true ,raw_name : current_date ,type:"string:50"}
account-id: {db:true ,raw_name : account_id ,type:"string:20"}
app-ref-no: {db :true ,raw_name : app_ref_no ,type : "string:20"}
product: {db:true ,raw_name : product ,type:"string:20"}
eida: {db:true ,raw_name : eida ,type:"string:20"}
cust-id: {db:true ,raw_name : cust_id ,type:"string:20"}
ph-no: {db:true ,raw_name : ph_no ,type:"string:20"}
email-id: {db:true ,raw_name : email_id ,type:"string:20"}
channel: {db:true ,raw_name : channel ,type:"string:20"}
dob: {db:true ,raw_name : dob ,type:timestamp}
vat: {db:true ,raw_name : vat ,type:"string:50"}
driving-lnc: {db:true ,raw_name : driving_lnc ,type:"string:50"}
trade-license: {db:true ,raw_name : trade_license ,type:"string:50"}
is-employer-nonsfa: {db:true ,raw_name : is_employer_nonsfa ,type:"string:50"}
employee-id: {db:true ,raw_name : employee_id ,type:"string:50"}
passport-no: {db:true ,raw_name : passport_no ,type:"string:50"}
appl-date: {db:true ,raw_name : appl_date ,type:timestamp}
iban-no: {db:true ,raw_name : iban_no ,type:"string:50"}
appl-name: {db:true ,raw_name : appl_name ,type:"string:50"}
branch-id: {db:true ,raw_name : branch_id ,type:"string:50"}
employer-id: {db:true ,raw_name : employer_id ,type:"string:50"}
matched-mobileno: {db:true ,raw_name : matched_mobileno ,type:"string:20"}
matched-passportno: {db:true ,raw_name : matched_passportno ,type:"string:50"}
matched-eid: {db:true ,raw_name : matched_eid ,type:"string:20"}
matched-dateofbirth: {db:true ,raw_name : matched_dateofbirth ,type:timestamp}
rej-appl-date: {db:true ,raw_name : rej_appl_date ,type:timestamp}
rej-appl-id: {db:true ,raw_name : rej_appl_id ,type:"string:200"}
rej-cif: {db:true ,raw_name : rej_cif ,type:"string:50"}
rej-mobileno: {db:true ,raw_name : rej_mobileno ,type:"string:50"}
rej-passportno: {db:true ,raw_name : rej_passportno ,type:"string:50"}
rej-eid: {db:true ,raw_name : rej_eid ,type:"string:50"}
rej-dateofbirth: {db:true ,raw_name : rej_dateofbirth ,type:timestamp}
rej-product_code: {db :true ,raw_name : rej_product_code ,type : "string:50"}
rejection-date: {db:true ,raw_name : rejection_date ,type:timestamp}
decline-reason: {db :true ,raw_name : decline_reason ,type : "string:50"}
}
}