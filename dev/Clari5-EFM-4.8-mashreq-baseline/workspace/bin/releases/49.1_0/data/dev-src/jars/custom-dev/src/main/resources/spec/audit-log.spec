# Generated Code
cxps.noesis.glossary.entity.Audit_Log {
        db-name = Audit_Log
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
        { name= Cust-Id, column =Cust_Id, type ="string:10",key=false}
        { name= Card-number, column =Card_number, type ="string:18",key=false}
        { name= Freez-status, column =Freez_Status, type ="string:15",key=false}
        { name= Freez-Date, column =Freez_Date, type ="date",key=false}
        { name= User-Name, column =User_Name, type ="string:255",key=false}
          ]
        indexes {
        NCK_CUSTOMER_pe = [Cust_ID ]
    }
}

