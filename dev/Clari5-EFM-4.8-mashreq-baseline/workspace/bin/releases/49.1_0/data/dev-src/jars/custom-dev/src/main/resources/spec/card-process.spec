#package where the class will be generated
clari5.custom.customop.db{
        entity {
          card-process {
          attributes = [

        { name : id, type : "string:500", key : true}
		{ name : customer-no, type : "string:100"}
		{ name : req-type, type : "string:100"}
		{ name : customer-status, type : "string:100"}
		{ name : alert-id, type : "string:10"}
		{ name : req-date, type : timestamp}
		{ name : req-by, type : "string:30"}
		{ name : req-msg, type : "string:8000"}
		{ name : res-msg, type : "string:8000"}
		{ name : comments, type : "string:8000"}
		{ name : status, type : "string:5"}
		{ name : remarks, type : "string:8000"}
		{ name : alert-created-ts, type : timestamp}
		{ name : status-modification-ts, type :timestamp}

		]
                 criteria-query {
                         name:CardProcess
                          summary = [id,customer-no,comments,req-type,customer-status,alert-id,req-date,req-by,req-msg,res-msg,status,remarks,alert-created-ts,status-modification-ts]
                          where {
                       id: equal-clause
                    }
                 }
                }
        }
}

#req-type used to store the request received from the plugin
