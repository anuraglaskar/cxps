package clari5.custom.customScn.scenario24;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


public class ApplicationDaemon extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(ApplicationDaemon.class);
    @Override
    protected Object getData() throws RuntimeFatalException {
        logger.info("[ApplicationDaemon] Inside process data into application Data --");
        Hocon hocon= new Hocon();
        hocon.loadFromContext("execution_time.conf");
        int hrsToExecute =hocon.getInt("scn24hrs");
        int minToExecute =hocon.getInt("scn24mins");
        int secToExecute =hocon.getInt("scn24sec");
        String selectQuery=hocon.getString("selectQuery");
        String insertQuery=hocon.getString("insertQuery");
        String updateQuery=hocon.getString("updateQuery");
        logger.info("[ApplicationDaemon] getData() -> Thread In Process Name"+Thread.currentThread().getName()+" Id "+Thread.currentThread().getId());
        synchronized (this){
            String threadName=Thread.currentThread().getName();
            long threadId=Thread.currentThread().getId();
            Date utilDate=new Date();
            java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());// current date
            logger.info("[ApplicationDaemon] Accquired Lock"+threadName +" with id"+threadId);
            try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
                Connection connection=rdbmsSession.getCxConnection();
                PreparedStatement selectQueryStatement=connection.prepareStatement(selectQuery)){
                selectQueryStatement.setString(1,threadName);
                selectQueryStatement.setDate(2,sqlDate);
                ResultSet resultSet=selectQueryStatement.executeQuery();
                if(resultSet.next()){
                    logger.info("[ApplicationDaemon] One of the thread has already processed.Hence quiting");
                }
                else{
                    logger.info("[ApplicationDaemon] No entry Found for this Daemon! Hence Inserting...");
                    AppEodReminder.callonEOD(hrsToExecute, minToExecute, secToExecute,threadName,threadId,sqlDate,insertQuery,updateQuery,selectQuery);
                }
            }catch (SQLException se){
                logger.error(se.getCause() + " "+se.getMessage());
            }
        }
        return null;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside the Process Data for Count Daemon");
    }
    @Override
    public void configure(Hocon h) {
        logger.debug("this is hocon configuration");
    }
}
