package clari5.custom.customScn.scenarioNew1;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;


public class ApplData{
    public static clari5.platform.logger.CxpsLogger logger = CxpsLogger.getLogger(ApplData.class);

    public static CxConnection getConnection(){
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }

    public static ArrayList<ApplFields> getEventFields() {
        PreparedStatement ps = null;
        CxConnection connection = null;
        ResultSet rs = null;
        ArrayList<ApplFields> finaldatalist = new ArrayList<>();
        String query = "select  event_id ,CONVERT(varchar, event_date , 105) event_date ,eida ,ph_no ,passport_no , CONVERT(varchar, dob , 105) dob from EVENT_NFT_ACCTOPENING where CONVERT(varchar, event_date , 105) = '" + getdate() + "'";
        try {
            connection = getConnection();
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ApplFields applFields = new ApplFields();

                applFields.setEvent_id(rs.getString("event_id"));
                applFields.setAppldate(rs.getString("event_date"));
                applFields.setPhno(rs.getString("ph_no"));
                applFields.setDob(rs.getString("dob"));
                applFields.setEida(rs.getString("eida"));
                applFields.setPassportno(rs.getString("passport_no"));
                finaldatalist.add(applFields);
            }
            logger.info(finaldatalist);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return finaldatalist;
    }

    public static void getEventfields(String eventid,String phno, String ppno, String eid, String dob) {
        ApplFields applFields = new ApplFields();
        PreparedStatement ps = null;
        CxConnection connection = null;
        ResultSet rs = null;
        try {
            String query = "select * from IRIS_REJECTED_APPL where MobileNumber = ? or PassportNumber = ? or EmiratesID = ? or FORMAT(DateOfBirth, 'dd-MM-yyyy') = ?";
            applFields.setCurrentdate(getdate());
            connection = getConnection();
             ps = connection.prepareStatement(query);
            try {
                ps.setString(1, phno);
                ps.setString(2, ppno);
                ps.setString(3, eid);
                ps.setString(4, dob);
                rs = ps.executeQuery();

                while (rs.next()) {
                    applFields.setRejeid(rs.getString("EmiratesID"));
                    applFields.setRejdateofbirth(datetostring(rs.getDate("DateOfBirth")));
                    applFields.setRejmobileno(rs.getString("MobileNumber"));
                    applFields.setRejpassportno(rs.getString("PassportNumber"));
                    applFields.setRejapplid(rs.getString("application_id"));
                    applFields.setRejappldate(datetostring(rs.getDate("applicationDate")));
                    applFields.setRejcif(rs.getString("CIF"));
                    applFields.setRejproductcode(rs.getString("productCode"));
                    applFields.setRejectiondate(datetostring(rs.getDate("RejectionDate")));
                    applFields.setDeclinereason(rs.getString("DeclineReason"));
                    applFields.setMatchedmobileno(phno);
                    applFields.setMatchedpassportno(ppno);
                    applFields.setMatchedeid(eid);
                    applFields.setMatcheddateofbirth(dob);
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            logger.info("getting eventfields");
            String query1 = "select * from EVENT_NFT_ACCTOPENING where event_id = '" + eventid + "'";
            try {
                ps = connection.prepareStatement(query1);
                rs = ps.executeQuery();

                ApplMqwriter applMqwriter = new ApplMqwriter();
                while (rs.next()) {
                    applFields.setPhno(rs.getString("ph_no"));
                    applFields.setDob(datetostring(rs.getDate("dob")));
                    applFields.setEida(rs.getString("eida"));
                    applFields.setPassportno(rs.getString("passport_no"));
                    applFields.setCustid(rs.getString("cust_id"));
                    applFields.setChannel(rs.getString("channel"));
                    applFields.setApprefno(rs.getString("app_ref_no"));
                    applFields.setEmailid(rs.getString("email_id"));
                    applFields.setAccountid(rs.getString("account_id"));
                    applFields.setDrivinglnc(rs.getString("driving_lnc"));
                    applFields.setIsemployernonsfa(rs.getString("is_employer_nonsfa"));
                    applFields.setEmployeeid(rs.getString("employee_id"));
                    applFields.setProduct(rs.getString("product"));
                    applFields.setTradelicense(rs.getString("trade_license"));
                    applFields.setVat(rs.getString("vat"));
                    applFields.setAppldate(datetostring(rs.getDate("event_date")));
                    applFields.setIbanno(rs.getString("iban_number"));
                    applFields.setApplname(rs.getString("appl_name"));
                    applFields.setBranchid(rs.getString("branch_id"));
                    applFields.setEmployerid(rs.getString("employer_id"));
                }
                applMqwriter.writeJosntoMq(applFields);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
        public static String getdate(){
            //Calendar cal = Calendar.getInstance();
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String prevdate = formatter.format(date);
            // cal.add(Calendar.DATE, -1);
            // String prevdate = formatter.format(cal.getTime());
            return prevdate;
    }
     public static String datetostring(java.sql.Date date){
         String stringdate = null;
        if(date != null) {
            stringdate = new SimpleDateFormat("dd-MM-YYYY").format(date);
        }
        return stringdate;
     }
}