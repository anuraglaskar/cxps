// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_REMITTANCE", Schema="rice")
public class FT_RemittanceEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String remAdd1;
       @Field(size=50) public String cif;
       @Field(size=50) public String remType;
       @Field(size=50) public String remAdd3;
       @Field(size=50) public String remAdd2;
       @Field public java.sql.Timestamp trnDate;
       @Field(size=50) public String benCity;
       @Field(size=20) public String remBank;
       @Field(size=50) public String branch;
       @Field(size=50) public String benCntryCode;
       @Field(size=50) public String benAdd3;
       @Field(size=50) public String benAdd2;
       @Field(size=50) public String remAcctNo;
       @Field(size=50) public String remName;
       @Field(size=50) public String remIdNo;
       @Field(size=50) public String benAdd1;
       @Field(size=50) public String benBic;
       @Field(size=50) public String tranRefNo;
       @Field(size=20) public Double tranAmt;
       @Field(size=50) public String benfBank;
       @Field(size=50) public String accountId;
       @Field(size=50) public String benName;
       @Field(size=50) public String remInformation1;
       @Field(size=50) public String remInformation2;
       @Field(size=50) public String benAcctNo;
       @Field(size=50) public String remInformation3;
       @Field(size=50) public String remInformation4;
       @Field(size=20) public Double lcyAmount;
       @Field(size=50) public String remCntryCode;
       @Field(size=50) public String tranCurr;
       @Field(size=50) public String benfIdNo;
       @Field(size=50) public String remCity;
       @Field(size=50) public String remBic;


    @JsonIgnore
    public ITable<FT_RemittanceEvent> t = AEF.getITable(this);

	public FT_RemittanceEvent(){}

    public FT_RemittanceEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Remittance");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setRemAdd1(json.getString("rem_add1"));
            setCif(json.getString("cif"));
            setRemType(json.getString("rem_type"));
            setRemAdd3(json.getString("rem_add3"));
            setRemAdd2(json.getString("rem_add2"));
            setTrnDate(EventHelper.toTimestamp(json.getString("trn_date")));
            setBenCity(json.getString("ben_city"));
            setRemBank(json.getString("rem_bank"));
            setBranch(json.getString("branch"));
            setBenCntryCode(json.getString("ben_cntry_code"));
            setBenAdd3(json.getString("ben_add3"));
            setBenAdd2(json.getString("ben_add2"));
            setRemAcctNo(json.getString("rem_acct_no"));
            setRemName(json.getString("rem_name"));
            setRemIdNo(json.getString("rem_id_no"));
            setBenAdd1(json.getString("ben_add1"));
            setBenBic(json.getString("ben_bic"));
            setTranRefNo(json.getString("tran_ref_no"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setBenfBank(json.getString("benf_bank"));
            setAccountId(json.getString("account_id"));
            setBenName(json.getString("ben_name"));
            setRemInformation1(json.getString("rem_information1"));
            setRemInformation2(json.getString("rem_information2"));
            setBenAcctNo(json.getString("ben_acct_no"));
            setRemInformation3(json.getString("rem_information3"));
            setRemInformation4(json.getString("rem_information4"));
            setLcyAmount(EventHelper.toDouble(json.getString("lcy_amount")));
            setRemCntryCode(json.getString("rem_cntry_code"));
            setTranCurr(json.getString("tran_curr"));
            setBenfIdNo(json.getString("benf_id_no"));
            setRemCity(json.getString("rem_city"));
            setRemBic(json.getString("rem_bic"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FREM"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getRemAdd1(){ return remAdd1; }

    public String getCif(){ return cif; }

    public String getRemType(){ return remType; }

    public String getRemAdd3(){ return remAdd3; }

    public String getRemAdd2(){ return remAdd2; }

    public java.sql.Timestamp getTrnDate(){ return trnDate; }

    public String getBenCity(){ return benCity; }

    public String getRemBank(){ return remBank; }

    public String getBranch(){ return branch; }

    public String getBenCntryCode(){ return benCntryCode; }

    public String getBenAdd3(){ return benAdd3; }

    public String getBenAdd2(){ return benAdd2; }

    public String getRemAcctNo(){ return remAcctNo; }

    public String getRemName(){ return remName; }

    public String getRemIdNo(){ return remIdNo; }

    public String getBenAdd1(){ return benAdd1; }

    public String getBenBic(){ return benBic; }

    public String getTranRefNo(){ return tranRefNo; }

    public Double getTranAmt(){ return tranAmt; }

    public String getBenfBank(){ return benfBank; }

    public String getAccountId(){ return accountId; }

    public String getBenName(){ return benName; }

    public String getRemInformation1(){ return remInformation1; }

    public String getRemInformation2(){ return remInformation2; }

    public String getBenAcctNo(){ return benAcctNo; }

    public String getRemInformation3(){ return remInformation3; }

    public String getRemInformation4(){ return remInformation4; }

    public Double getLcyAmount(){ return lcyAmount; }

    public String getRemCntryCode(){ return remCntryCode; }

    public String getTranCurr(){ return tranCurr; }

    public String getBenfIdNo(){ return benfIdNo; }

    public String getRemCity(){ return remCity; }

    public String getRemBic(){ return remBic; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setRemAdd1(String val){ this.remAdd1 = val; }
    public void setCif(String val){ this.cif = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setRemAdd3(String val){ this.remAdd3 = val; }
    public void setRemAdd2(String val){ this.remAdd2 = val; }
    public void setTrnDate(java.sql.Timestamp val){ this.trnDate = val; }
    public void setBenCity(String val){ this.benCity = val; }
    public void setRemBank(String val){ this.remBank = val; }
    public void setBranch(String val){ this.branch = val; }
    public void setBenCntryCode(String val){ this.benCntryCode = val; }
    public void setBenAdd3(String val){ this.benAdd3 = val; }
    public void setBenAdd2(String val){ this.benAdd2 = val; }
    public void setRemAcctNo(String val){ this.remAcctNo = val; }
    public void setRemName(String val){ this.remName = val; }
    public void setRemIdNo(String val){ this.remIdNo = val; }
    public void setBenAdd1(String val){ this.benAdd1 = val; }
    public void setBenBic(String val){ this.benBic = val; }
    public void setTranRefNo(String val){ this.tranRefNo = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setBenfBank(String val){ this.benfBank = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setRemInformation1(String val){ this.remInformation1 = val; }
    public void setRemInformation2(String val){ this.remInformation2 = val; }
    public void setBenAcctNo(String val){ this.benAcctNo = val; }
    public void setRemInformation3(String val){ this.remInformation3 = val; }
    public void setRemInformation4(String val){ this.remInformation4 = val; }
    public void setLcyAmount(Double val){ this.lcyAmount = val; }
    public void setRemCntryCode(String val){ this.remCntryCode = val; }
    public void setTranCurr(String val){ this.tranCurr = val; }
    public void setBenfIdNo(String val){ this.benfIdNo = val; }
    public void setRemCity(String val){ this.remCity = val; }
    public void setRemBic(String val){ this.remBic = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.tranRefNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Remittance");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Remittance");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}