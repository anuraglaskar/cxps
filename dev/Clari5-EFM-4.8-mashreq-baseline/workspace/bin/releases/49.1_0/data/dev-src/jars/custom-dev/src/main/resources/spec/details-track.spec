cxps.noesis.glossary.entity.DETAILS_TRACK {      
   db-name = DETAILS_TRACK       
   generate = false     
   tablespace = CXPS_USERS      
   attributes = [     
   { name= account, column =Account, type ="string:20",key=false}    
   { name= cust-id, column =Cust_id, type ="string:20",key=false}     
   { name= user-id, column =UserID, type ="string:100",key=false}     
   { name= status, column =status, type ="string:10",key=false}                  
]
}

