package clari5.custom.customScn.scenarioNew1;

import clari5.platform.dbcon.CxConnection;
import cxps.apex.utils.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RejectedMatches {

        public static void getdata() {
            PreparedStatement ps = null;
            CxConnection connection = null;
            ResultSet rs = null;
            ApplData applData = new ApplData();
            ArrayList<ApplFields> datalist = applData.getEventFields();
            for(ApplFields applFields : datalist){
                try {
                    String query = "select MobileNumber, PassportNumber, EmiratesID, FORMAT(DateOfBirth, 'dd-MM-yyyy') DateOfBirth from IRIS_REJECTED_APPL where MobileNumber = ? or PassportNumber = ? or EmiratesID = ? or FORMAT(DateOfBirth, 'dd-MM-yyyy') = ?";

                     connection = applData.getConnection();
                     ps = connection.prepareStatement(query);
                     ps.setString(1, applFields.getPhno());
                     ps.setString(2, applFields.getPassportno());
                     ps.setString(3, applFields.getEida());
                     ps.setString(4, applFields.getDob());
                     rs = ps.executeQuery();

                         while (rs.next()) {
                             if (!StringUtils.isNullOrEmpty(rs.getString("MobileNumber")) && rs.getString("MobileNumber").equalsIgnoreCase(applFields.getPhno())) {
                                 applFields.setMatchedmobileno(rs.getString("MobileNumber"));
                             } else applFields.setMatchedmobileno("NA");
                             if (!StringUtils.isNullOrEmpty(rs.getString("PassportNumber")) && rs.getString("PassportNumber").equalsIgnoreCase(applFields.getPassportno())) {
                                 applFields.setMatchedpassportno(rs.getString("PassportNumber"));
                             } else applFields.setMatchedpassportno("NA");
                             if (!StringUtils.isNullOrEmpty(rs.getString("EmiratesID")) && rs.getString("EmiratesID").equalsIgnoreCase(applFields.getEida())) {
                                 applFields.setMatchedeid(rs.getString("EmiratesID"));
                             } else applFields.setMatchedeid("NA");
                             if (!StringUtils.isNullOrEmpty( rs.getString("DateOfBirth")) && rs.getString("DateOfBirth").equalsIgnoreCase(applFields.getDob())) {
                                 applFields.setMatcheddateofbirth(rs.getString("DateOfBirth"));
                             } else applFields.setMatcheddateofbirth("NA");
                             if(!applFields.getMatchedmobileno().equalsIgnoreCase("NA") || !applFields.getMatchedpassportno().equalsIgnoreCase("NA") || !applFields.getMatchedeid().equalsIgnoreCase("NA") || !applFields.getMatcheddateofbirth().equalsIgnoreCase("NA")){
                                  applData.getEventfields(applFields.getEvent_id(),applFields.getMatchedmobileno(),applFields.getMatchedpassportno(), applFields.getMatchedeid(), applFields.getMatcheddateofbirth());
                             }
                         }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }finally {
                    try {
                        if (ps != null) {
                            ps.close();
                        }
                        if (connection != null) {
                            connection.close();
                        }
                        if (rs != null) {
                            rs.close();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            }
}