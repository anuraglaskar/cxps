// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_LOGIN", Schema="rice")
public class NFT_LoginEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String userId;
       @Field public java.sql.Timestamp lastLogints;
       @Field(size=20) public String channel;
       @Field(size=200) public String errorDesc;
       @Field(size=50) public String ipCountry;
       @Field(size=20) public String succFailFlg;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=20) public String deviceId;
       @Field(size=200) public String lastLoginIp;
       @Field(size=200) public String addrNetwork;
       @Field(size=200) public String deviceType;
       @Field(size=20) public String hostId;


    @JsonIgnore
    public ITable<NFT_LoginEvent> t = AEF.getITable(this);

	public NFT_LoginEvent(){}

    public NFT_LoginEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Login");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setUserId(json.getString("user_id"));
            setLastLogints(EventHelper.toTimestamp(json.getString("last_logints")));
            setChannel(json.getString("channel"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setDeviceId(json.getString("device_id"));
            setLastLoginIp(json.getString("last_login_ip"));
            setAddrNetwork(json.getString("addr_network"));
            setDeviceType(json.getString("device_type"));
            setHostId(json.getString("host_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setIpCountry(cxps.events.CustomDerivator.getIpCountry(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "LG"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getUserId(){ return userId; }

    public java.sql.Timestamp getLastLogints(){ return lastLogints; }

    public String getChannel(){ return channel; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getDeviceId(){ return deviceId; }

    public String getLastLoginIp(){ return lastLoginIp; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getDeviceType(){ return deviceType; }

    public String getHostId(){ return hostId; }
    public String getIpCountry(){ return ipCountry; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setLastLogints(java.sql.Timestamp val){ this.lastLogints = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setLastLoginIp(String val){ this.lastLoginIp = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setDeviceType(String val){ this.deviceType = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.addrNetwork);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Login");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Login");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}