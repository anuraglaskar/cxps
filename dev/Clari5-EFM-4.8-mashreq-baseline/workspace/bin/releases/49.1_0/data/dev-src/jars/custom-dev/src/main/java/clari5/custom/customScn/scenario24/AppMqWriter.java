package clari5.custom.customScn.scenario24;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cxps.apex.utils.StringUtils;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class AppMqWriter {
    public static CxpsLogger logger = CxpsLogger.getLogger(AppMqWriter.class);
    private static ThreadPojo threadPojo=AppEodReminder.getThreadPojo();
    static {
        ECClient.configure(null);
    }
    public static void writeJson(){
        logger.info("[AppMqWriter] Inside write JSON method");
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();){
            PreparedStatement preparedStatement=connection.prepareStatement(threadPojo.getSelectQry());
            preparedStatement.setString(1,threadPojo.getCurrentThreadName());
            preparedStatement.setDate(2,threadPojo.getThreadDate());
            ResultSet resultSet=preparedStatement.executeQuery();
            if(resultSet.next()){
                logger.info("[AppMqWriter] Data Already Exist in Thread Audit Table on today's date");
            }
            else{
                AppEodReminder.insert(threadPojo.getCurrentThreadName(),threadPojo.getThrdId(),threadPojo.getThreadDate(),threadPojo.getInsrtQry());
                logger.info("[AppMqWriter] MqWriter called...Calling WLApplicationMatch ");
                //WLApplicationMatch  wlApplicationMatch = new WLApplicationMatch();
                //wlApplicationMatch.getdata();
                WLApplicationMatch.getdata();
                //threadPojo=null;
            }
        }catch (Exception e){
                logger.info("[AppMqWriter] Error While Selecting Data from Thread Audit Table"+e.getCause() + " "+e.getMessage());
        }



    }
    public static  void  resposeFormatter(String response, String newAppRefNo) {
        String app_ref_no = "";
        String matched_eida = "NA";
        String matched_mobile = "NA";
        String matched_custTradeNo = "NA";
        String matched_email = "NA";
        //String score = "";
        String matched_score="NA";
        Double matched_score_eid=0.0;
        try {
            JsonParser parser = new JsonParser();
            if (!StringUtils.isNullOrEmpty(response)) {
                JsonObject jsonObject = parser.parse(response).getAsJsonObject();
            if (jsonObject.has("matchedResults")) {
                jsonObject = jsonObject.getAsJsonObject("matchedResults");
                if (jsonObject.has("matchedRecords")) {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("matchedRecords");
                   logger.info("[AppMqWriter] matched size is :: "+jsonArray.size());
                    for (int j = 0; j < jsonArray.size(); j++) {
                        jsonObject = (JsonObject) jsonArray.get(j);
                        //score = jsonObject.get("score").toString().replaceAll("\"", "");
                        app_ref_no = jsonObject.get("id").toString().replaceAll("\"", "");
                        if (app_ref_no.equalsIgnoreCase(newAppRefNo)){
                            logger.info("both are same application reference number");
                        }else {
                        List<ApplicationField> oldDataList = ApplicationData.getAppOldData(app_ref_no);
                       // float finalscore = Float.valueOf(score);
                            JsonArray fieldjsonArray = (JsonArray) jsonObject.get("fields");
                            int size = fieldjsonArray.size();
                            for (ApplicationField olddata : oldDataList) {
                                for (int i = 0; i < size; i++) {
                                    JsonObject fieldjsonObject = (JsonObject) fieldjsonArray.get(i);
                                    String score = fieldjsonObject.get("score").toString().replaceAll("\"", "");
                                    Double finalscore = Double.parseDouble(score);

                                    String name = fieldjsonObject.get("name").toString().replaceAll("\"", "");
                                    String value = fieldjsonObject.get("value").toString().replaceAll("\"", "");
                                    String queryVal = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    if (name.equalsIgnoreCase("eida") && finalscore >= 0.75) {
                                        matched_eida = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                        matched_score = fieldjsonObject.get("score").toString().replaceAll("\"", "");
                                        matched_score_eid = Double.parseDouble(matched_score);
                                        matched_score_eid = matched_score_eid * 100;

                                    } else if (name.equalsIgnoreCase("ph_no") && finalscore == 1.0) {
                                        matched_mobile = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    } else if (name.equalsIgnoreCase("trade_license") && finalscore == 1.0) {
                                        matched_custTradeNo = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    } else if (name.equalsIgnoreCase("email_id") && finalscore == 1.0) {
                                        matched_email = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    }
                                }
                                logger.info("matched_eida--" + matched_eida + "mached mobile --" + matched_mobile + "matched_TradeNo--" + matched_custTradeNo + "matched_email--" + matched_email);
                                ApplicationField newdata = ApplicationData.getAppNewdata(newAppRefNo);
                                ApplicationField finaldata = new ApplicationField();

                                //new data
                                finaldata.setNewcif(newdata.getNewcif());
                                finaldata.setNewcustname(newdata.getNewcustname());
                                finaldata.setNewmobile(newdata.getNewmobile());
                                finaldata.setNewpassport(newdata.getNewpassport());
                                finaldata.setNewemail(newdata.getNewemail());
                                finaldata.setNeweid(newdata.getNeweid());
                                finaldata.setNewapplopendate(newdata.getNewapplopendate());
                                finaldata.setNewapprefno(newdata.getNewapprefno());
                                finaldata.setNewtradelincense(newdata.getNewtradelincense());
                                finaldata.setNewipaddress(newdata.getNewipaddress());
                                finaldata.setNewdeviceid(newdata.getNewdeviceid());
                                finaldata.setNewproduct(newdata.getNewproduct());
                                finaldata.setNewChannel(newdata.getNewChannel());
                                //olddata
                                finaldata.setOldcif(olddata.getOldcif());
                                finaldata.setOldcustname(olddata.getOldcustname());
                                finaldata.setOldmobile(olddata.getOldmobile());
                                finaldata.setOldpassport(olddata.getOldpassport());
                                finaldata.setOldemail(olddata.getOldemail());
                                finaldata.setOldeid(olddata.getOldeid());
                                finaldata.setOldapplopendate(olddata.getOldapplopendate());
                                finaldata.setOldapprefno(olddata.getOldapprefno());
                                finaldata.setOldtradelincense(olddata.getOldtradelincense());
                                finaldata.setOldipaddress(olddata.getOldipaddress());
                                finaldata.setOlddeviceid(olddata.getOlddeviceid());
                                finaldata.setOldproduct(olddata.getOldproduct());
                                finaldata.setOldChannel(olddata.getOldChannel());
                                //matched data
                                finaldata.setMatchedeid(matched_eida);
                                finaldata.setMatchedmobile(matched_mobile);
                                finaldata.setMatchedemail(matched_email);
                                finaldata.setMatchedtradelicense(matched_custTradeNo);
                                finaldata.setMatchedScoreeid(matched_score_eid.toString());
                                writeJosntoMq(finaldata);
                                matched_mobile = "NA";
                                matched_custTradeNo = "NA";
                                matched_email = "NA";
                                matched_score = "NA";
                                matched_score_eid = 0.0;
                                matched_eida = "NA";
                             }
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  static  boolean writeJosntoMq(ApplicationField finaldata){
        JSONObject json = new JSONObject();
        JSONObject msg = new JSONObject();
        String entity_id = "applicationopening";
        String event_id=getRandomNum();
        json.put("EventType", "nft");
        json.put("EventSubType", "applicationopening");
        json.put("event_name", "nft_applicationopening");
        msg.put("sys_time", getSysTime());
        msg.put("host_id", "F");
        msg.put("event_id", event_id.replaceAll("-",""));

        msg.put("new_app_refno", finaldata.getNewapprefno() != null ? finaldata.getNewapprefno() : "");
        msg.put("new_product", finaldata.getNewproduct() != null ? finaldata.getNewproduct() : "");
        msg.put("new_cif", finaldata.getNewcif() != null ? finaldata.getNewcif() : "");
        msg.put("new_eid", finaldata.getNeweid() != null ? finaldata.getNeweid() : "");
        msg.put("new_passport", finaldata.getNewpassport() !=null ? finaldata.getNewpassport() : "");
        msg.put("new_email", finaldata.getNewemail() != null ? finaldata.getNewemail() : "");
        msg.put("new_mobile", finaldata.getNewmobile() != null ? finaldata.getNewmobile() : "");
        msg.put("new_applopendate", finaldata.getNewapplopendate() != null ? finaldata.getNewapplopendate() : "");
        msg.put("new_custname", finaldata.getNewcustname() != null ? finaldata.getNewcustname() : "");
        msg.put("new_deviceid", finaldata.getNewdeviceid() != null ? finaldata.getNewdeviceid() : "");
        msg.put("new_ipaddress", finaldata.getNewipaddress() != null ? finaldata.getNewipaddress() : "");
        msg.put("new_tradelincense", finaldata.getNewtradelincense()!= null ? finaldata.getNewtradelincense() : "");
        msg.put("old_app_refno", finaldata.getOldapprefno() != null ? finaldata.getOldapprefno() : "");
        msg.put("old_product", finaldata.getOldproduct() != null ? finaldata.getOldproduct() : "");
        msg.put("old_cif", finaldata.getOldcif() != null ? finaldata.getOldcif() : "");
        msg.put("old_eid", finaldata.getOldeid() != null ? finaldata.getOldeid() : "");
        msg.put("old_passport", finaldata.getOldpassport() !=null ? finaldata.getOldpassport() : "");
        msg.put("old_email", finaldata.getOldemail() != null ? finaldata.getOldemail() : "");
        msg.put("old_mobile", finaldata.getOldmobile() != null ? finaldata.getOldmobile() : "");
        msg.put("old_applopendate", finaldata.getOldapplopendate() != null ? finaldata.getOldapplopendate() : "");
        msg.put("old_custname", finaldata.getOldcustname() != null ? finaldata.getOldcustname() : "");
        msg.put("old_deviceid", finaldata.getOlddeviceid() != null ? finaldata.getOlddeviceid() : "");
        msg.put("old_ipaddress", finaldata.getOldipaddress() != null ? finaldata.getOldipaddress() : "");
        msg.put("old_tradelincense", finaldata.getOldtradelincense()!= null ? finaldata.getOldtradelincense() : "");
        msg.put("matched_eid", finaldata.getMatchedeid() != null ? finaldata.getMatchedeid() : "");
        msg.put("matched_tradelicense", finaldata.getMatchedtradelicense() != null ? finaldata.getMatchedtradelicense() : "");
        msg.put("matched_email", finaldata.getMatchedemail() != null ? finaldata.getMatchedemail() : "");
        msg.put("matched_mobile", finaldata.getMatchedmobile() !=null ? finaldata.getMatchedmobile() : "");
        msg.put("matched_score_eid", finaldata.getMatchedScoreeid() !=null ? finaldata.getMatchedScoreeid() : "");
        msg.put("new_channel", finaldata.getNewChannel() !=null ? finaldata.getNewChannel() : "");
        msg.put("old_channel", finaldata.getOldChannel() !=null ? finaldata.getOldChannel() : "");



        json.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

        logger.info("[AppMqWriter] Final JSON: " + json.toString());
        boolean status = ECClient.enqueue("HOST", entity_id, event_id, json.toString());
        logger.info("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        return false;

    }
    public  static  String getRandomNum(){
        Random rand = new Random();
        int randomnum=rand.nextInt();

        return String.valueOf(randomnum);
    }
    public  static String getSysTime(){
        String systime="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= new Date();
            systime=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return systime;

    }
}
