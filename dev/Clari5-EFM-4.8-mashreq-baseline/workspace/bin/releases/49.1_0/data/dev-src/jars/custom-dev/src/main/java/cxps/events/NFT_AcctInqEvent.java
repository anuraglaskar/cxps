// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCT_INQ", Schema="rice")
public class NFT_AcctInqEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String acctBrCode;
       @Field(size=50) public String channel;
       @Field(size=5) public String isEligible;
       @Field(size=50) public String source;
       @Field(size=20) public String compmis1;
       @Field(size=50) public String empId;
       @Field(size=50) public String tranSubType;
       @Field(size=50) public String tranId;
       @Field(size=20) public String hostId;
       @Field(size=2) public String isStaff;
       @Field(size=50) public String accountId;
       @Field(size=50) public String userId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=50) public String custId;
       @Field(size=50) public String tranCrncyCode;
       @Field(size=50) public String tranRmks;
       @Field(size=50) public String tranCode;
       @Field(size=50) public String acctName;
       @Field public java.sql.Timestamp pstdDate;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=50) public String tranType;
       @Field(size=50) public String txnBrCode;
       @Field(size=50) public String transactionRefNo;
       @Field(size=50) public String productCode;
       @Field(size=50) public String partTranType;
       @Field(size=20) public String acctStatus;


    @JsonIgnore
    public ITable<NFT_AcctInqEvent> t = AEF.getITable(this);

	public NFT_AcctInqEvent(){}

    public NFT_AcctInqEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AcctInq");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctBrCode(json.getString("acct_br_code"));
            setChannel(json.getString("channel"));
            setIsEligible(json.getString("is_eligible"));
            setSource(json.getString("source"));
            setCompmis1(json.getString("compmis1"));
            setEmpId(json.getString("emp_id"));
            setTranSubType(json.getString("tran_sub_type"));
            setTranId(json.getString("tran_id"));
            setHostId(json.getString("host_id"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTranRmks(json.getString("tran_rmks"));
            setTranCode(json.getString("tran_code"));
            setAcctName(json.getString("acct_name"));
            setPstdDate(EventHelper.toTimestamp(json.getString("pstd_date")));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setTranType(json.getString("tran_type"));
            setTxnBrCode(json.getString("txn_br_code"));
            setTransactionRefNo(json.getString("transaction_ref_no"));
            setProductCode(json.getString("product_code"));
            setPartTranType(json.getString("part_tran_type"));
            setAcctStatus(json.getString("acct_status"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setIsStaff(cxps.events.CustomDerivator.getIsstaff(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "AI"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAcctBrCode(){ return acctBrCode; }

    public String getChannel(){ return channel; }

    public String getIsEligible(){ return isEligible; }

    public String getSource(){ return source; }

    public String getCompmis1(){ return compmis1; }

    public String getEmpId(){ return empId; }

    public String getTranSubType(){ return tranSubType; }

    public String getTranId(){ return tranId; }

    public String getHostId(){ return hostId; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getTranRmks(){ return tranRmks; }

    public String getTranCode(){ return tranCode; }

    public String getAcctName(){ return acctName; }

    public java.sql.Timestamp getPstdDate(){ return pstdDate; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getTranType(){ return tranType; }

    public String getTxnBrCode(){ return txnBrCode; }

    public String getTransactionRefNo(){ return transactionRefNo; }

    public String getProductCode(){ return productCode; }

    public String getPartTranType(){ return partTranType; }

    public String getAcctStatus(){ return acctStatus; }
    public String getIsStaff(){ return isStaff; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctBrCode(String val){ this.acctBrCode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setIsEligible(String val){ this.isEligible = val; }
    public void setSource(String val){ this.source = val; }
    public void setCompmis1(String val){ this.compmis1 = val; }
    public void setEmpId(String val){ this.empId = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranRmks(String val){ this.tranRmks = val; }
    public void setTranCode(String val){ this.tranCode = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setPstdDate(java.sql.Timestamp val){ this.pstdDate = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setTxnBrCode(String val){ this.txnBrCode = val; }
    public void setTransactionRefNo(String val){ this.transactionRefNo = val; }
    public void setProductCode(String val){ this.productCode = val; }
    public void setPartTranType(String val){ this.partTranType = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setIsStaff(String val){ this.isStaff = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.userId);
        wsInfoSet.add(new WorkspaceInfo("User", userKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AcctInq");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AcctInq");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}