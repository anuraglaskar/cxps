cxps.events.event.nft-withdrawal-limit{
  table-name : EVENT_NFT_WITHDRAWAL_LIMIT
  event-mnemonic: WR
  workspaces : {
    ACCOUNT: "account-id"
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
card-no: {db : true ,raw_name : card_no ,type : "string:200"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
account-id: {db:true ,raw_name : account_id ,type:"string:20"}
limitchangetype: {db :true ,raw_name : limitchangetype ,type : "number:20,3"}
old-limit: {db :true ,raw_name : old_limit ,type : "number:20,3"}
new-limit: {db :true ,raw_name : new_limit ,type : "number:20,3"}
channel: {db : true ,raw_name : channel ,type : "string:200"}
request:{db : true ,raw_name : request ,type : "string:20"}
source:{db : true ,raw_name : source ,type : "string:20"}
}
}
