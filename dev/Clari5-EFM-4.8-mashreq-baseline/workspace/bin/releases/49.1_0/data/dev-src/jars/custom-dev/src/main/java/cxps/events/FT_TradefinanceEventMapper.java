// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_TradefinanceEventMapper extends EventMapper<FT_TradefinanceEvent> {

public FT_TradefinanceEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_TradefinanceEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_TradefinanceEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_TradefinanceEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_TradefinanceEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_TradefinanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_TradefinanceEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getProductType());
            preparedStatement.setString(i++, obj.getBeneficiaryName());
            preparedStatement.setDouble(i++, obj.getAmount());
            preparedStatement.setString(i++, obj.getSellerName());
            preparedStatement.setString(i++, obj.getBillOfLading());
            preparedStatement.setString(i++, obj.getCorereferenceno());
            preparedStatement.setString(i++, obj.getApplicantName());
            preparedStatement.setString(i++, obj.getEdmsreferenceno());
            preparedStatement.setString(i++, obj.getBillNumber());
            preparedStatement.setString(i++, obj.getCifNumber());
            preparedStatement.setString(i++, obj.getBuyerName());
            preparedStatement.setString(i++, obj.getCurrency());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getLcNumber());
            preparedStatement.setTimestamp(i++, obj.getContractExpiryDate());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_TRADEFINANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_TradefinanceEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_TradefinanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADEFINANCE"));
        putList = new ArrayList<>();

        for (FT_TradefinanceEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "PRODUCT_TYPE",  obj.getProductType());
            p = this.insert(p, "BENEFICIARY_NAME",  obj.getBeneficiaryName());
            p = this.insert(p, "AMOUNT", String.valueOf(obj.getAmount()));
            p = this.insert(p, "SELLER_NAME",  obj.getSellerName());
            p = this.insert(p, "BILL_OF_LADING",  obj.getBillOfLading());
            p = this.insert(p, "COREREFERENCENO",  obj.getCorereferenceno());
            p = this.insert(p, "APPLICANT_NAME",  obj.getApplicantName());
            p = this.insert(p, "EDMSREFERENCENO",  obj.getEdmsreferenceno());
            p = this.insert(p, "BILL_NUMBER",  obj.getBillNumber());
            p = this.insert(p, "CIF_NUMBER",  obj.getCifNumber());
            p = this.insert(p, "BUYER_NAME",  obj.getBuyerName());
            p = this.insert(p, "CURRENCY",  obj.getCurrency());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "LC_NUMBER",  obj.getLcNumber());
            p = this.insert(p, "CONTRACT_EXPIRY_DATE", String.valueOf(obj.getContractExpiryDate()));
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_TRADEFINANCE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_TRADEFINANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_TRADEFINANCE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_TradefinanceEvent obj = new FT_TradefinanceEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setProductType(rs.getString("PRODUCT_TYPE"));
    obj.setBeneficiaryName(rs.getString("BENEFICIARY_NAME"));
    obj.setAmount(rs.getDouble("AMOUNT"));
    obj.setSellerName(rs.getString("SELLER_NAME"));
    obj.setBillOfLading(rs.getString("BILL_OF_LADING"));
    obj.setCorereferenceno(rs.getString("COREREFERENCENO"));
    obj.setApplicantName(rs.getString("APPLICANT_NAME"));
    obj.setEdmsreferenceno(rs.getString("EDMSREFERENCENO"));
    obj.setBillNumber(rs.getString("BILL_NUMBER"));
    obj.setCifNumber(rs.getString("CIF_NUMBER"));
    obj.setBuyerName(rs.getString("BUYER_NAME"));
    obj.setCurrency(rs.getString("CURRENCY"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setLcNumber(rs.getString("LC_NUMBER"));
    obj.setContractExpiryDate(rs.getTimestamp("CONTRACT_EXPIRY_DATE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_TRADEFINANCE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_TradefinanceEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_TradefinanceEvent> events;
 FT_TradefinanceEvent obj = new FT_TradefinanceEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADEFINANCE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_TradefinanceEvent obj = new FT_TradefinanceEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setProductType(getColumnValue(rs, "PRODUCT_TYPE"));
            obj.setBeneficiaryName(getColumnValue(rs, "BENEFICIARY_NAME"));
            obj.setAmount(EventHelper.toDouble(getColumnValue(rs, "AMOUNT")));
            obj.setSellerName(getColumnValue(rs, "SELLER_NAME"));
            obj.setBillOfLading(getColumnValue(rs, "BILL_OF_LADING"));
            obj.setCorereferenceno(getColumnValue(rs, "COREREFERENCENO"));
            obj.setApplicantName(getColumnValue(rs, "APPLICANT_NAME"));
            obj.setEdmsreferenceno(getColumnValue(rs, "EDMSREFERENCENO"));
            obj.setBillNumber(getColumnValue(rs, "BILL_NUMBER"));
            obj.setCifNumber(getColumnValue(rs, "CIF_NUMBER"));
            obj.setBuyerName(getColumnValue(rs, "BUYER_NAME"));
            obj.setCurrency(getColumnValue(rs, "CURRENCY"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setLcNumber(getColumnValue(rs, "LC_NUMBER"));
            obj.setContractExpiryDate(EventHelper.toTimestamp(getColumnValue(rs, "CONTRACT_EXPIRY_DATE")));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADEFINANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADEFINANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"PRODUCT_TYPE\",\"BENEFICIARY_NAME\",\"AMOUNT\",\"SELLER_NAME\",\"BILL_OF_LADING\",\"COREREFERENCENO\",\"APPLICANT_NAME\",\"EDMSREFERENCENO\",\"BILL_NUMBER\",\"CIF_NUMBER\",\"BUYER_NAME\",\"CURRENCY\",\"HOST_ID\",\"LC_NUMBER\",\"CONTRACT_EXPIRY_DATE\"" +
              " FROM EVENT_FT_TRADEFINANCE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [PRODUCT_TYPE],[BENEFICIARY_NAME],[AMOUNT],[SELLER_NAME],[BILL_OF_LADING],[COREREFERENCENO],[APPLICANT_NAME],[EDMSREFERENCENO],[BILL_NUMBER],[CIF_NUMBER],[BUYER_NAME],[CURRENCY],[HOST_ID],[LC_NUMBER],[CONTRACT_EXPIRY_DATE]" +
              " FROM EVENT_FT_TRADEFINANCE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`PRODUCT_TYPE`,`BENEFICIARY_NAME`,`AMOUNT`,`SELLER_NAME`,`BILL_OF_LADING`,`COREREFERENCENO`,`APPLICANT_NAME`,`EDMSREFERENCENO`,`BILL_NUMBER`,`CIF_NUMBER`,`BUYER_NAME`,`CURRENCY`,`HOST_ID`,`LC_NUMBER`,`CONTRACT_EXPIRY_DATE`" +
              " FROM EVENT_FT_TRADEFINANCE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_TRADEFINANCE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"PRODUCT_TYPE\",\"BENEFICIARY_NAME\",\"AMOUNT\",\"SELLER_NAME\",\"BILL_OF_LADING\",\"COREREFERENCENO\",\"APPLICANT_NAME\",\"EDMSREFERENCENO\",\"BILL_NUMBER\",\"CIF_NUMBER\",\"BUYER_NAME\",\"CURRENCY\",\"HOST_ID\",\"LC_NUMBER\",\"CONTRACT_EXPIRY_DATE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[PRODUCT_TYPE],[BENEFICIARY_NAME],[AMOUNT],[SELLER_NAME],[BILL_OF_LADING],[COREREFERENCENO],[APPLICANT_NAME],[EDMSREFERENCENO],[BILL_NUMBER],[CIF_NUMBER],[BUYER_NAME],[CURRENCY],[HOST_ID],[LC_NUMBER],[CONTRACT_EXPIRY_DATE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`PRODUCT_TYPE`,`BENEFICIARY_NAME`,`AMOUNT`,`SELLER_NAME`,`BILL_OF_LADING`,`COREREFERENCENO`,`APPLICANT_NAME`,`EDMSREFERENCENO`,`BILL_NUMBER`,`CIF_NUMBER`,`BUYER_NAME`,`CURRENCY`,`HOST_ID`,`LC_NUMBER`,`CONTRACT_EXPIRY_DATE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

