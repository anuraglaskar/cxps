// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCT_OPENED_SAME_DETAILS", Schema="rice")
public class NFT_AcctOpenedSameDetailsEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp newAcctcloseddate;
       @Field(size=50) public String newCustdob;
       @Field(size=20) public String matchedTradelicense;
       @Field public java.sql.Timestamp oldAcctcloseddate;
       @Field(size=20) public String oldCompmis2;
       @Field(size=20) public String oldEmail;
       @Field(size=50) public String oldCompmis1;
       @Field(size=50) public String newMobile;
       @Field(size=50) public String oldAccountno;
       @Field(size=20) public String matchedEid;
       @Field(size=50) public String oldBranchid;
       @Field(size=20) public String newCompmis1;
       @Field(size=20) public String matchedPassport;
       @Field(size=50) public String newCompmis2;
       @Field(size=20) public String newPassport;
       @Field(size=20) public String oldCustname;
       @Field public java.sql.Timestamp newAcctopendate;
       @Field(size=50) public String oldTradelicense;
       @Field(size=50) public String newTradelicense;
       @Field public java.sql.Timestamp oldAcctopendate;
       @Field(size=20) public String oldMobile;
       @Field(size=20) public String oldCif;
       @Field(size=20) public String oldEid;
       @Field(size=50) public String oldCustdob;
       @Field(size=20) public String newCustname;
       @Field(size=50) public String newAccountno;
       @Field(size=20) public String oldPassport;
       @Field(size=20) public String matchedMobile;
       @Field(size=20) public String newCif;
       @Field(size=20) public String newEid;
       @Field(size=50) public String newBranchid;
       @Field(size=20) public String newEmail;


    @JsonIgnore
    public ITable<NFT_AcctOpenedSameDetailsEvent> t = AEF.getITable(this);

	public NFT_AcctOpenedSameDetailsEvent(){}

    public NFT_AcctOpenedSameDetailsEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AcctOpenedSameDetails");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setNewAcctcloseddate(EventHelper.toTimestamp(json.getString("new_acctcloseddate")));
            setNewCustdob(json.getString("new_custdob"));
            setMatchedTradelicense(json.getString("matched_tradelicense"));
            setOldAcctcloseddate(EventHelper.toTimestamp(json.getString("old_acctcloseddate")));
            setOldCompmis2(json.getString("old_compmis2"));
            setOldEmail(json.getString("old_email"));
            setOldCompmis1(json.getString("old_compmis1"));
            setNewMobile(json.getString("new_mobile"));
            setOldAccountno(json.getString("old_accountno"));
            setMatchedEid(json.getString("matched_eid"));
            setOldBranchid(json.getString("old_branchid"));
            setNewCompmis1(json.getString("new_compmis1"));
            setMatchedPassport(json.getString("matched_passport"));
            setNewCompmis2(json.getString("new_compmis2"));
            setNewPassport(json.getString("new_passport"));
            setOldCustname(json.getString("old_custname"));
            setNewAcctopendate(EventHelper.toTimestamp(json.getString("new_acctopendate")));
            setOldTradelicense(json.getString("old_tradelicense"));
            setNewTradelicense(json.getString("new_tradelicense"));
            setOldAcctopendate(EventHelper.toTimestamp(json.getString("old_acctopendate")));
            setOldMobile(json.getString("old_mobile"));
            setOldCif(json.getString("old_cif"));
            setOldEid(json.getString("old_eid"));
            setOldCustdob(json.getString("old_custdob"));
            setNewCustname(json.getString("new_custname"));
            setNewAccountno(json.getString("new_accountno"));
            setOldPassport(json.getString("old_passport"));
            setMatchedMobile(json.getString("matched_mobile"));
            setNewCif(json.getString("new_cif"));
            setNewEid(json.getString("new_eid"));
            setNewBranchid(json.getString("new_branchid"));
            setNewEmail(json.getString("new_email"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "ASD"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getNewAcctcloseddate(){ return newAcctcloseddate; }

    public String getNewCustdob(){ return newCustdob; }

    public String getMatchedTradelicense(){ return matchedTradelicense; }

    public java.sql.Timestamp getOldAcctcloseddate(){ return oldAcctcloseddate; }

    public String getOldCompmis2(){ return oldCompmis2; }

    public String getOldEmail(){ return oldEmail; }

    public String getOldCompmis1(){ return oldCompmis1; }

    public String getNewMobile(){ return newMobile; }

    public String getOldAccountno(){ return oldAccountno; }

    public String getMatchedEid(){ return matchedEid; }

    public String getOldBranchid(){ return oldBranchid; }

    public String getNewCompmis1(){ return newCompmis1; }

    public String getMatchedPassport(){ return matchedPassport; }

    public String getNewCompmis2(){ return newCompmis2; }

    public String getNewPassport(){ return newPassport; }

    public String getOldCustname(){ return oldCustname; }

    public java.sql.Timestamp getNewAcctopendate(){ return newAcctopendate; }

    public String getOldTradelicense(){ return oldTradelicense; }

    public String getNewTradelicense(){ return newTradelicense; }

    public java.sql.Timestamp getOldAcctopendate(){ return oldAcctopendate; }

    public String getOldMobile(){ return oldMobile; }

    public String getOldCif(){ return oldCif; }

    public String getOldEid(){ return oldEid; }

    public String getOldCustdob(){ return oldCustdob; }

    public String getNewCustname(){ return newCustname; }

    public String getNewAccountno(){ return newAccountno; }

    public String getOldPassport(){ return oldPassport; }

    public String getMatchedMobile(){ return matchedMobile; }

    public String getNewCif(){ return newCif; }

    public String getNewEid(){ return newEid; }

    public String getNewBranchid(){ return newBranchid; }

    public String getNewEmail(){ return newEmail; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setNewAcctcloseddate(java.sql.Timestamp val){ this.newAcctcloseddate = val; }
    public void setNewCustdob(String val){ this.newCustdob = val; }
    public void setMatchedTradelicense(String val){ this.matchedTradelicense = val; }
    public void setOldAcctcloseddate(java.sql.Timestamp val){ this.oldAcctcloseddate = val; }
    public void setOldCompmis2(String val){ this.oldCompmis2 = val; }
    public void setOldEmail(String val){ this.oldEmail = val; }
    public void setOldCompmis1(String val){ this.oldCompmis1 = val; }
    public void setNewMobile(String val){ this.newMobile = val; }
    public void setOldAccountno(String val){ this.oldAccountno = val; }
    public void setMatchedEid(String val){ this.matchedEid = val; }
    public void setOldBranchid(String val){ this.oldBranchid = val; }
    public void setNewCompmis1(String val){ this.newCompmis1 = val; }
    public void setMatchedPassport(String val){ this.matchedPassport = val; }
    public void setNewCompmis2(String val){ this.newCompmis2 = val; }
    public void setNewPassport(String val){ this.newPassport = val; }
    public void setOldCustname(String val){ this.oldCustname = val; }
    public void setNewAcctopendate(java.sql.Timestamp val){ this.newAcctopendate = val; }
    public void setOldTradelicense(String val){ this.oldTradelicense = val; }
    public void setNewTradelicense(String val){ this.newTradelicense = val; }
    public void setOldAcctopendate(java.sql.Timestamp val){ this.oldAcctopendate = val; }
    public void setOldMobile(String val){ this.oldMobile = val; }
    public void setOldCif(String val){ this.oldCif = val; }
    public void setOldEid(String val){ this.oldEid = val; }
    public void setOldCustdob(String val){ this.oldCustdob = val; }
    public void setNewCustname(String val){ this.newCustname = val; }
    public void setNewAccountno(String val){ this.newAccountno = val; }
    public void setOldPassport(String val){ this.oldPassport = val; }
    public void setMatchedMobile(String val){ this.matchedMobile = val; }
    public void setNewCif(String val){ this.newCif = val; }
    public void setNewEid(String val){ this.newEid = val; }
    public void setNewBranchid(String val){ this.newBranchid = val; }
    public void setNewEmail(String val){ this.newEmail = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.newCif);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AcctOpenedSameDetails");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AcctOpenedSameDetails");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}