cxps.events.event.nft-staticinfochange{
  table-name : EVENT_NFT_STATICINFOCHANGE
  event-mnemonic: NS
  workspaces : {
    ACCOUNT: "account-id"
    CUSTOMER: "cust-id"
  }
  event-attributes : {
        tran-date: {db : true ,raw_name : tran_date ,type : timestamp}
        branch-id: {db : true ,raw_name : branch_id ,type : "string:200"}
        entity-type: {db : true ,raw_name : entity_type ,type : "string:200"}
        initsubentityval: {db : true ,raw_name : initsubentityval ,type : "string:200"}
        finalsubentityval: {db : true ,raw_name : finalsubentityval ,type : "string:200"}
        account-id: {db : true ,raw_name : account_id ,type : "string:200"}
        user-id: {db:true ,raw_name : user_id ,type:"string:50"}
        cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
        channel: {db : true ,raw_name : channel ,type : "string:200"}
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        host-id: {db : true ,raw_name : host_id ,type : "string:20"}
	    ref-id: {db : true ,raw_name : ref_id ,type : "string:20"}
	    static-info-change-derivation : { db : true, raw_name : static_info_change_derivation,type:"string:50",derivation:"""cxps.events.CustomDerivator.getUserIdForCustomerId(this)"""}
        flag : {db : true,raw_name : flag,type:"String:10"}
        }
}
