package clari5.custom.freezeUnfreeze.devFreeze;

import clari5.aml.cms.newjira.JiraInstance;
import clari5.jiraclient.JiraClient;
import clari5.platform.jira.JiraClientException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class JiraUpdate {
    private static CxpsLogger logger = CxpsLogger.getLogger(JiraUpdate.class);
    public static String customField="";
    public static String dburl="";
    public static String user="";
    public static String password="";
    public static String drivername="";
    public static String schemaname="";
    public static String freezeFieldId="";

    public static String freezefield="";

    static {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("jiradb-module.conf");
        customField = hocon.getString("customField");
        dburl=hocon.getString("dburl");
        user=hocon.getString("user");
        password=hocon.getString("password");
        drivername=hocon.getString("drivername");
        schemaname=hocon.getString("schemaname");
        freezeFieldId=hocon.getString("freezeId");
        freezefield=hocon.getString("freezefield");

    }
    String table_name=schemaname+"CUSTOMFIELDVALUE";
    Connection conn = null;
    Statement stmt = null;
    public  boolean updatejiraForCust(String issueID, String reqString) {
        String query="";
        if (reqString.equalsIgnoreCase("Freeze")) {
            query = "update  "+table_name+" set stringvalue ='Un Freeze' where  issue ='"+issueID+"' and customfield ='"+freezeFieldId+"'";
        }else if (reqString.equalsIgnoreCase("UnFreeze")){
            query = "update  "+table_name+" set stringvalue ='Freeze' where  issue ='"+issueID+"' and customfield ='"+freezeFieldId+"'";
        }
        logger.info("query to update jira table for freeze unfreeze ::"+query);
        try {
            Class.forName(drivername);
            conn = DriverManager.getConnection(dburl, user, password);
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        logger.info("JIRA Table updation is failed for Freeze Unfreeze");
        return false;
    }

    public  boolean callingJiraApi(String issuekey,String issueID,String reqString) {
        String finaldata=jsonForm(reqString);
        boolean flag=updateCUSTOMFIELDVALUE(finaldata,issueID,reqString,issuekey );
        return flag;
    }
    public  boolean updateCUSTOMFIELDVALUE(String finaldata,String issueID,String reqString,String issuekey){
            boolean finalflag=false;
        try {

            JiraInstance instance = new JiraInstance("jira-instance");
            JiraClient jiraClient=new JiraClient(instance.getInstanceParams());
            String BaseUrl=  jiraClient.getBaseURL();
            System.out.println("base url is :: "+BaseUrl);
            String path="/rest/api/2/issue/";
            String finalurl=BaseUrl+path+issuekey;
            String Method="PUT";
           //jiraClient.updateIssueRequest()
            System.out.println("final url --"+finalurl);
            SupportJiiraCLient suppjiraclient=new SupportJiiraCLient(instance.getInstanceParams());
            CxJson cxJson=suppjiraclient.updateIssueRequest(finaldata,issuekey);
            logger.info("cxJson response code---"+cxJson);
            finalflag=true;

        }catch (Exception e){
            logger.info("update through API is failed");
            if(reqString.equalsIgnoreCase("Freeze") || reqString.equalsIgnoreCase("UnFreeze"))
                finalflag=updatejiraForCust(issueID,reqString);
            logger.info("customFieldvalue table is updated successfully without jira-api");
            e.printStackTrace();
        }
       return  finalflag;
    }
    public  String jsonForm(String reqString){
        String jsondata="";
        try {
            if(reqString.equalsIgnoreCase("Freeze")){
                jsondata = "{\"update\":{\""+freezefield+"\":[{\"set\":\"Un Freeze\"}]}}";
            }else if(reqString.equalsIgnoreCase("UnFreeze")){
                jsondata = "{\"update\":{\""+freezefield+"\":[{\"set\":\"Freeze\"}]}}";
            }else{
                logger.info("Invalid Request"+reqString);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsondata;
    }


  /*  public static void main(String[] args) {
        CustomCmsServlet customCmsServlet= new CustomCmsServlet();
        JiraUpdate jiraUpdate = new JiraUpdate();

        boolean data=jiraUpdate.callingJiraApi("EFM-641","42505","freeze");
       // boolean flag=jiraUpdate.updatejiraForCust("42505","Freeze");
        //boolean flag=jiraUpdate.updatejiraForCust("42505","UnFreeze");
        //boolean flag=jiraUpdate.updatejiraForCard("42505","cardblock");
        //boolean flag=jiraUpdate.updatejiraForCard("42505","cardUnblock");
        System.out.println("flag----"+data);


    }*/
}
