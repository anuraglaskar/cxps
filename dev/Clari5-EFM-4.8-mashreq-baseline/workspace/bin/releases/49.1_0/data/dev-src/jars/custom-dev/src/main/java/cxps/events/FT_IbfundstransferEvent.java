// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;
import cxps.apex.utils.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;
import cxps.noesis.constants.Constants;

@Table(Name="EVENT_FT_IBFUNDS_TRANSFER", Schema="rice")
public class FT_IbfundstransferEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String payeeAccountId;
       @Field(size=20) public String paymentType;
       @Field(size=20) public String remType;
       @Field(size=200) public String errorDesc;
       @Field(size=20) public String channel;
       @Field(size=20) public String succFailFlg;
       @Field(size=20) public String benefType;
       @Field(size=20) public String bankName;
       @Field(size=200) public String addrNetworkDerived;
       @Field(size=20) public String payeeMashreq;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String custIdDerived;
       @Field(size=20) public String payeeBankName;
       @Field(size=20) public String payeeCustId;
       @Field public java.sql.Timestamp acctopendate;
       @Field(size=200) public String addrNetwork;
       @Field(size=20) public Double avlBalance;
       @Field(size=20) public String tranId;
       @Field(size=20) public String hostId;
       @Field(size=20) public String custType;
       @Field(size=200) public String txnCrncy;
       @Field(size=20) public Double tranAmt;
       @Field(size=5) public String isBenefAdded;
       @Field(size=20) public String tranMode;
       @Field(size=20) public String bankId;
       @Field(size=20) public String txnRefNo;
       @Field(size=20) public String schmCode;
       @Field(size=20) public String userId;
       @Field(size=20) public String rchrgeMobNo;
       @Field(size=20) public String domesticTrsnfrFlag;
       @Field(size=20) public String payeeName;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=20) public String mccCode;
       @Field(size=20) public String networkType;
       @Field(size=20) public Double avgTxnAmt;
       @Field(size=20) public String tranType;
       @Field(size=20) public Double avlBal;
       @Field public java.sql.Timestamp benefAdditionDate;
       @Field(size=20) public String schmType;
       @Field(size=20) public String acctOwnership;
       @Field(size=20) public String accountNo;
       @Field(size=20) public Double dailyLimit;


    @JsonIgnore
    public ITable<FT_IbfundstransferEvent> t = AEF.getITable(this);

	public FT_IbfundstransferEvent(){}

    public FT_IbfundstransferEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Ibfundstransfer");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setPayeeAccountId(json.getString("payee_account_id"));
            setPaymentType(json.getString("payment_type"));
            setRemType(json.getString("rem_type"));
            setErrorDesc(json.getString("error_desc"));
            setChannel(json.getString("channel"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setBenefType(json.getString("benef_type"));
            setBankName(json.getString("bank_name"));
            setPayeeMashreq(json.getString("payee_mashreq"));
            setDeviceId(json.getString("device_id"));
            setPayeeBankName(json.getString("payee_bank_name"));
            setPayeeCustId(json.getString("payee_cust_id"));
            setAcctopendate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setAddrNetwork(json.getString("addr_network"));
            setTranId(json.getString("tran_id"));
            setHostId(json.getString("host_id"));
            setCustType(json.getString("cust_type"));
            setTxnCrncy(json.getString("txn_crncy"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setTranMode(json.getString("tran_mode"));
            setBankId(json.getString("bank_id"));
            setTxnRefNo(json.getString("txn_ref_no"));
            setSchmCode(json.getString("schm_code"));
            setUserId(json.getString("user_id"));
            setRchrgeMobNo(json.getString("rchrge_mob_no"));
            setDomesticTrsnfrFlag(json.getString("domestic_trsnfr_flag"));
            setPayeeName(json.getString("payee_name"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setMccCode(json.getString("mcc_code"));
            setNetworkType(json.getString("network_type"));
            setAvgTxnAmt(EventHelper.toDouble(json.getString("avg_txn_amt")));
            setTranType(json.getString("tran_type"));
            setBenefAdditionDate(EventHelper.toTimestamp(json.getString("benef_addition_date")));
            setSchmType(json.getString("schm_type"));
            setAcctOwnership(json.getString("acct_ownership"));
            setAccountNo(json.getString("account_no"));
            setDailyLimit(EventHelper.toDouble(json.getString("daily_limit")));

        setDerivedValues();

    }


    private void setDerivedValues() {
/* always put setCustIdDerived before any other setter to use cust-id for other derivations*/ 
setCustIdDerived(cxps.events.CustomDerivator.getCustIdDerived(this));setAvlBalance(cxps.events.CustomDerivator.getAvlBalance(this));setIsBenefAdded(cxps.events.CustomDerivator.getIsBenefAdded(this));setAvlBal(cxps.events.CustomDerivator.getAvlBal(this));setAddrNetworkDerived(cxps.events.CustomDerivator.getAddrNetworkderived(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "IFT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getPayeeAccountId(){ return payeeAccountId; }

    public String getPaymentType(){ return paymentType; }

    public String getRemType(){ return remType; }

    public String getErrorDesc(){ return errorDesc; }

    public String getChannel(){ return channel; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getBenefType(){ return benefType; }

    public String getBankName(){ return bankName; }

    public String getPayeeMashreq(){ return payeeMashreq; }

    public String getDeviceId(){ return deviceId; }

    public String getPayeeBankName(){ return payeeBankName; }

    public String getPayeeCustId(){ return payeeCustId; }

    public java.sql.Timestamp getAcctopendate(){ return acctopendate; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getTranId(){ return tranId; }

    public String getHostId(){ return hostId; }

    public String getCustType(){ return custType; }

    public String getTxnCrncy(){ return txnCrncy; }

    public Double getTranAmt(){ return tranAmt; }

    public String getTranMode(){ return tranMode; }

    public String getBankId(){ return bankId; }

    public String getTxnRefNo(){ return txnRefNo; }

    public String getSchmCode(){ return schmCode; }

    public String getUserId(){ return userId; }

    public String getRchrgeMobNo(){ return rchrgeMobNo; }

    public String getDomesticTrsnfrFlag(){ return domesticTrsnfrFlag; }

    public String getPayeeName(){ return payeeName; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getMccCode(){ return mccCode; }

    public String getNetworkType(){ return networkType; }

    public Double getAvgTxnAmt(){ return avgTxnAmt; }

    public String getTranType(){ return tranType; }

    public java.sql.Timestamp getBenefAdditionDate(){ return benefAdditionDate; }

    public String getSchmType(){ return schmType; }

    public String getAcctOwnership(){ return acctOwnership; }

    public String getAccountNo(){ return accountNo; }

    public Double getDailyLimit(){ return dailyLimit; }
    public String getAddrNetworkDerived(){ return addrNetworkDerived; }

    public String getCustIdDerived(){ return custIdDerived; }

    public Double getAvlBalance(){ return avlBalance; }

    public String getIsBenefAdded(){ return isBenefAdded; }

    public Double getAvlBal(){ return avlBal; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setPayeeAccountId(String val){ this.payeeAccountId = val; }
    public void setPaymentType(String val){ this.paymentType = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setBenefType(String val){ this.benefType = val; }
    public void setBankName(String val){ this.bankName = val; }
    public void setPayeeMashreq(String val){ this.payeeMashreq = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setPayeeBankName(String val){ this.payeeBankName = val; }
    public void setPayeeCustId(String val){ this.payeeCustId = val; }
    public void setAcctopendate(java.sql.Timestamp val){ this.acctopendate = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCustType(String val){ this.custType = val; }
    public void setTxnCrncy(String val){ this.txnCrncy = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setTranMode(String val){ this.tranMode = val; }
    public void setBankId(String val){ this.bankId = val; }
    public void setTxnRefNo(String val){ this.txnRefNo = val; }
    public void setSchmCode(String val){ this.schmCode = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setRchrgeMobNo(String val){ this.rchrgeMobNo = val; }
    public void setDomesticTrsnfrFlag(String val){ this.domesticTrsnfrFlag = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setMccCode(String val){ this.mccCode = val; }
    public void setNetworkType(String val){ this.networkType = val; }
    public void setAvgTxnAmt(Double val){ this.avgTxnAmt = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setBenefAdditionDate(java.sql.Timestamp val){ this.benefAdditionDate = val; }
    public void setSchmType(String val){ this.schmType = val; }
    public void setAcctOwnership(String val){ this.acctOwnership = val; }
    public void setAccountNo(String val){ this.accountNo = val; }
    public void setDailyLimit(Double val){ this.dailyLimit = val; }
    public void setAddrNetworkDerived(String val){ this.addrNetworkDerived = val; }
    public void setCustIdDerived(String val){ this.custIdDerived = val; }
    public void setAvlBalance(Double val){ this.avlBalance = val; }
    public void setIsBenefAdded(String val){ this.isBenefAdded = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountNo);
        //wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
	if (!StringUtils.isNullOrEmpty(payeeAccountId) && !StringUtils.isNullOrEmpty(addrNetwork))
        {
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.addrNetwork + "-" + this.payeeAccountId);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        }
        else
        {
        String noncustomerKey1= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.rchrgeMobNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey1));
        }
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
	
	//Added code for import scemanticevent collection functionality.
	//Started from here
	String cxCifId;
	String hostCustKey = getCustId();
	if (null != hostCustKey && hostCustKey.trim().length() > 0) {
	cxCifId = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), hostCustKey);
	} else {
	cxCifId = h.getCxCifIdGivenCxAcctKey(session, accountKey);
	}
	WorkspaceInfo wi = new WorkspaceInfo(Constants.KEY_REF_TYP_ACCT, accountKey);
	if (h.isValidCxKey(WorkspaceName.CUSTOMER, cxCifId)) {
	wi.addParam("cxCifID", cxCifId);
	}
	wsInfoSet.add(wi);
	//Ended here
	
	return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Ibfundstransfer");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Ibfundstransfer");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
