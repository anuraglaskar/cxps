package clari5.custom.customScn.scenario30;
import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Date;

public class MultiCustDaemon extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(MultiCustDaemon.class);

    @Override
    protected Object getData() throws RuntimeFatalException {
        logger.info("inside process data into get Data for Multiple customer--"+new Date());
        LocalDateTime currentTime = LocalDateTime.now();
        Hocon hocon= new Hocon();
        hocon.loadFromContext("execution_time.conf");
        int hrsToExecute =hocon.getInt("scn30hrs");
        int minToExecute =hocon.getInt("scn30hrsmin");
        int secToExecute =hocon.getInt("scn30hrssec");
        String selectQuery=hocon.getString("selectQuery");
        String insertQuery=hocon.getString("insertQuery");
        String updateQuery=hocon.getString("updateQuery");
        System.out.println("[MultiCustDaemon] getData() -> Thread In Process Name"+Thread.currentThread().getName()+" Id "+Thread.currentThread().getId());
        synchronized (this) {
            String threadName=Thread.currentThread().getName();
            long threadId=Thread.currentThread().getId();
            Date utilDate=new Date();
            java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());// current date
            logger.info("[MultiCustDaemon] Accquired Lock"+threadName +" with id"+threadId);
            try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
                Connection connection=rdbmsSession.getCxConnection();
                PreparedStatement selectQueryStatement=connection.prepareStatement(selectQuery)){
                selectQueryStatement.setString(1,threadName);
                selectQueryStatement.setDate(2,sqlDate);
                ResultSet resultSet=selectQueryStatement.executeQuery();
               if(resultSet.next()){
                    logger.info("[MultiCustDaemon] One of the thread has already processed.Hence quiting");
                }
                else{
                    logger.info("[MultiCustDaemon] No entry Found for this Daemon! Hence Inserting...");
                    CustEodReminder.callonEODForCust(hrsToExecute, minToExecute, secToExecute,threadName,threadId,sqlDate,insertQuery,updateQuery,selectQuery);
                }

            } catch (SQLException e) {
                logger.error(e.getCause() + " "+e.getMessage());
            }
        }
        return null;
    }
    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside the Process Data for Count Daemon");
    }
    @Override
    public void configure(Hocon h) {
        logger.debug("[MultiCustDaemon] configure()-> this is hocon configuration");
    }

}

