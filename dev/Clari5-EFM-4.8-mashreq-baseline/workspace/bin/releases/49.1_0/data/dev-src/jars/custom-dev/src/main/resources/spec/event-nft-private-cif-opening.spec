cxps.events.event.nft-private-cif-opening{
  table-name : EVENT_NFT_PRIVATE_CIF_OPENING
     event-mnemonic: PVT
     workspaces : {
     NONCUSTOMER: curr-mon-name
                  }
 event-attributes : {

curr-mon-count: {db : true ,raw_name : curr_mon_count ,type : "number:20,3"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
date: {db : true ,raw_name : date ,type : timestamp}
last-mon-count: {db : true ,raw_name : last_mon_count ,type : "number:20,3"}
avg-six-mon-count: {db : true ,raw_name : avg_six_mon_count ,type : "number:20,3"}
curr-mon-name: {db : true ,raw_name : curr_mon_name ,type : "string:200"}
last-mon-name: {db : true ,raw_name : last_mon_name ,type : "string:200"}
}
}
