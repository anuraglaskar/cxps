cxps.events.event.ft-tradenet{
  table-name : EVENT_FT_TRADENET
  event-mnemonic: TN
  workspaces : {
   NONCUSTOMER: "security-id"
   USER : "emp-id"
}
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
acct-id: {db : true ,raw_name : acct_id ,type : "string:20"}
trader-id: {db : true ,raw_name : trader_id ,type : "string:20"}
cust-id: {db : true ,raw_name : cust_id,type : "string:20"}
security-id: {db : true ,raw_name : security_id,type: "string:20" }
security-class: {db : true ,raw_name : security_class ,type : "string:20"}
channel: {db : true ,raw_name : channel ,type : "string:20"}
amount: {db : true ,raw_name : amount ,type : "number:20,4"}
tran-type: {db : true ,raw_name : tran_type ,type : "string:50"}
staff-flg: {db : true ,raw_name : staff_flg ,type : "string:20"}
emp-id: {db : true ,raw_name : emp_id ,type : "string:50"}
is-trader-bank-staff: {db : true ,raw_name : is_trader_bank_staff ,type : "string:50"}
is-self-txn: {db : true ,raw_name : is_self_txn ,type : "string:50"}
leave-type: {db : true ,raw_name : leave_type ,type : "string:50" , derivation :"""cxps.events.CustomDerivator.getLeaveType(this)"""}
leave-start-date: {db : true ,raw_name : leave_start_date ,type : timestamp}
leave-end-date: {db : true ,raw_name : leave_end_date ,type : timestamp}
}
}
