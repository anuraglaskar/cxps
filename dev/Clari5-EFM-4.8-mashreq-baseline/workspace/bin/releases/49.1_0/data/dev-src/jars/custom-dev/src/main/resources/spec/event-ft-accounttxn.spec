cxps.events.event.ft-accounttxn{  
  table-name : EVENT_FT_ACCOUNTTXN
  event-mnemonic: FA
  workspaces : {
    CUSTOMER: "cust-id"
    ACCOUNT: "account-id"
    NONCUSTOMER : "terminal-id+bin,id-number,tran-id"
    PAYMENTCARD : "cust-card-id"
    TRANSACTION : "tran-id"
  }
  event-attributes : {
        tran-category: {db:true ,raw_name : tran_category ,type:"string:20"}
        host-id: {db:true ,raw_name : host_id ,type:"string:2"}
        channel: {db:true ,raw_name : channel ,type:"string:20"}
        account-id: {db:true ,raw_name : account_id ,type:"string:20"}
        schm-type: {db : true ,raw_name : schm_type ,type : "string:20"}
	    source: {db:true ,raw_name : source ,type:"string:20"}
        schm-code: {db : true ,raw_name : schm_code ,type : "string:20"}
        place-holder: {db : true ,raw_name : place_holder ,type : "string:20"}
        acct-name: {db : true ,raw_name : acct_name ,type : "string:20"}
        acct-sol-id: {db : true ,raw_name : acct_sol_id ,type : "string:20"}
        acct-ownership: {db : true ,raw_name : acct_ownership ,type : "string:20"}
        acctopendate: {db : true ,raw_name : acctopendate ,type : timestamp}
        avl-bal: {db :true ,raw_name : avl_bal ,type : "number:20,3" ,derivation:"""cxps.events.CustomDerivator.getAvlBal(this)""" }
        clr-bal-amt: {db :true ,raw_name : clr_bal_amt ,type : "number:20,3"}
        un-clr-bal-amt: {db : true ,raw_name : un_clr_bal_amt ,type : "number:20,3"}
        eff-avl-bal: {db : true ,raw_name : eff_avl_bal ,type : "number:20,3"}
        tran-type: {db : true ,raw_name : tran_type ,type : "string:20"}
        tran-sub-type: {db : true ,raw_name : tran_sub_type ,type : "string:20"}
        part-tran-type: {db : true ,raw_name : part_tran_type ,type : "string:20"}
        tran-date: {db : true ,raw_name : tran_date ,type : timestamp}
        pstd-date: {db : true ,raw_name : pstd_date ,type : timestamp}
	    tran-id: {db : true ,raw_name : tran_id ,type : "string:20"}
        tran-amt: {db :true ,raw_name : tran_amt ,type : "number:20,3"}
        tran-crncy-code: {db :true ,raw_name : tran_crncy_code ,type : "string:20"}
        tran-particular: {db : true ,raw_name : tran_particular ,type : "string:20"}
        tran-rmks: {db : true ,raw_name : tran_rmks ,type: "string:20" }
        ref-num: {db : true ,raw_name : ref_num ,type: "string:20" }
        ip-address: {db : true ,raw_name : ip_address ,type: "string:20" }
        sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
        acct-br-code: {db : true ,raw_name : acct_br_code ,type: "string:20" }
        user-id: {db : true ,raw_name : user_id ,type: "string:20" }
        entry-user: {db : true ,raw_name : entry_user ,type: "string:20" }
	    acct-occp-code:{db : true ,raw_name : acct_occp_code ,type: "string:20" }
	    txn-br-code: {db : true ,raw_name : txn_br_code ,type: "string:20" }
	    cust-card-id: {db : true ,raw_name : cust_card_id ,type: "string:20" }
	    dcc-id: {db : true ,raw_name : dcc_id ,type: "string:20" }
	    device-id: {db : true ,raw_name : device_id ,type: "string:20" }
	    card-acceptor-name: {db : true ,raw_name : card_acceptor_name ,type: "string:20" }
	    dev-type: {db : true ,raw_name : dev_type ,type: "string:20" }
	    dev-owner-id: {db : true ,raw_name : dev_owner_id ,type: "string:20" }
	    payee-code: {db : true ,raw_name : payee_code ,type: "string:20" }
	    branch-id:{db : true ,raw_name :branch_id ,type : "string:20"}
	    city: {db : true ,raw_name :city ,type : "string:20"}
	    country-code: {db : true ,raw_name :country_code ,type : "string:20"}
	    cust-const: {db : true ,raw_name :cust_const ,type : "string:20"}
	    payee-id: {db : true ,raw_name :payee_id ,type : "string:20"}
	    txn-br-city: {db : true ,raw_name :txn_br_city ,type : "string:20"}
	    home-br-city: {db : true ,raw_name :home_br_city ,type : "string:20"}
	    status: {db : true ,raw_name :status ,type : "string:20"}
	    transaction-ref-no: {db : true ,raw_name :transaction_ref_no ,type : "string:20"}
	    hdrmkrs: {db : true ,raw_name :hdrmkrs ,type : "string:20"}
	    cust-induced-date: {db : true ,raw_name :cust_induced_date ,type : timestamp}
	    manul-debit-flag: {db : true ,raw_name :manul-debit-flag ,type : "string:20"}
	    ecom-limit: {db :true ,raw_name : ecom_limit ,type : "number:20,3"}
	    pos-limit: {db :true ,raw_name : pos_limit ,type : "number:20,3"}
	    atm-limit: {db :true ,raw_name : atm_limit ,type : "number:20,3"}
	    atm-limitderive: {db :true ,raw_name : atm_limitderive ,type : "number:20,3",derivation:"""cxps.events.CustomDerivator.getAtmLim(this)"""}
	    cardless-cash-limit: {db :true ,raw_name : cardless_cash_limit ,type : "number:20,3"}
	    cust-id: {db : true ,type : "string:20" , raw_num : cust_id }
            cust-idderive:{db : true ,type : "string:20" , raw_num : cust_idderive , derivation:"""cxps.events.CustomDerivator.getCustDerive(this)"""}
	    avl-balance: {db :true ,raw_name : avl_balance ,type : "number:20,3" ,derivation:"""cxps.events.CustomDerivator.getAvlBalance(this)""" }
  	    succ-fail-flag: {db:true ,raw_name:succ_fail_flag, type:"string:20"}
	    succ-fail-flag-derivd: {db:true ,raw_name:succ_fail_flag_derivd, type:"string:20", derivation:"""cxps.events.CustomDerivator.getSuccFailFlagDerivd(this)"""}
	   response-code: {db:true ,raw_name:response_code, type:"string:20"}
	   ins-time: {db:true ,raw_name:ins_time, type:timestamp}
	   id-number: {db:true ,raw_name:id_number, type:"string:50"}
	   swift-uaefts-ref-num: {db:true ,raw_name:swift_uaefts_ref_num, type:"string:50"}
	   swift-uaefts-msg-type: {db:true ,raw_name:swift_uaefts_msg_type, type:"string:50"}
	   itm-with-self-acct: {db:true ,raw_name:itm_with_self_acct, type:"string:50"}
	   mid-rate: {db:true ,raw_name:mid_rate, type:"number:20,3", derivation:"""cxps.events.CustomDerivator.getmidRate(this)"""}
	   ref-curr: {db:true ,raw_name:ref_curr, type:"string:50"}
	   conv-rate: {db:true ,raw_name:conv_rate, type:"number:20,3"}
	   value-date: {db:true ,raw_name:value_date, type:timestamp}
	   instrumt-date: {db:true ,raw_name:instrumt_date, type:timestamp}
	   wallet-type: {db:true ,raw_name:wallet_type, type:"string:50"}
	   coins-denomination: {db:true ,raw_name:coins_denomination, type:"string:50"}
	   bin: {db:true ,raw_name:bin, type:"string:20" ,derivation:"""cxps.events.CustomDerivator.getBin(this)""" }
	   terminal-id: {db:true ,raw_name:terminal_id, type:"string:50"}
	   merchant-id: {db:true ,raw_name:merchant_id, type:"string:50"}
	   cardserno: {db:true ,raw_name:cardserno, type:"string:50"}
	   designated-flg: {db:true ,raw_name:designated_flg, type:"string:5" , derivation:"""cxps.events.CustomDerivator.getdesignatedFlg(this)"""}
	   fcy-curr: {db:true ,raw_name:fcy_curr, type:"string:50"}
	   lcy-curr: {db:true ,raw_name:lcy_curr, type:"string:50"}
	  is26trancode-exists: {db:true ,type:"string:5" , derivation:"""cxps.events.CustomDerivator.gettrancode(this)"""}
	  is27trancode-exists: {db:true ,raw_name : is27trancode_exists , type:"string:5"}
	   fcy_tranamt: {db:true ,raw_name:fcy_tranamt, type:"number:20,3"}
	  compmis1: {db:true ,raw_name:compmis1, type:"string:50", derivation:"""cxps.events.CustomDerivator.getcompmis(this)"""}
	  compmis2: {db:true ,raw_name:compmis2, type:"string:50"}
}
}

