# Generated Code
cxps.noesis.glossary.entity.Frez_Unfrez_Details {
        db-name = FREZ_UNFREZ_DETAILS
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
        { name= customer-no, column =customerNo, type ="string:20",key=false}
        { name= frz-status, column =frzStatus, type ="string:10",key=false}
        { name= status, column =status, type ="string:10",key=false}
        { name= code, column =code, type ="string:20",key=false}
        { name= description, column =description, type ="string:255",key=false}
        { name = frz-date, column = frzDate, type = "date", key=false }
          ]
        indexes {
        NCK_CUSTOMER_pe = [ customerNo ]
    }
}
