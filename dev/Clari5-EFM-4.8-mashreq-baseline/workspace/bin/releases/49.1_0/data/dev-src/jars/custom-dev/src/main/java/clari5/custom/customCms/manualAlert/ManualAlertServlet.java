package clari5.custom.customCms.manualAlert;

import clari5.platform.logger.CxpsLogger;
import org.json.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.Random;

public class ManualAlertServlet extends HttpServlet {
    private static CxpsLogger logger = CxpsLogger.getLogger(ManualAlertServlet.class);


    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        doEither(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        doEither(request, response);
    }

    public void doEither(HttpServletRequest request, HttpServletResponse response) {
        logger.info("coming inside doGet method of ManualAlertServlet");
        String resp = "";
        PrintWriter out=null;
        String  line="";
        StringBuffer jb = new StringBuffer();
        try {
            response.setContentType("text/html");
            out=response.getWriter();
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    jb.append(line);

                String data=jb.toString();
                String str="\"";

                data= data.replaceAll("=","\":\"");
                 data=data.replaceAll("&","\",\"");
                  String finaldata=str+data+str;
                  finaldata="{"+finaldata+"}";

            logger.info("final data ----"+finaldata);
            JSONObject jsonObject= new JSONObject(finaldata);
            String parrentEntityType = jsonObject.getString("ParentEntityType");
            String parrentEntityId = jsonObject.getString("ParentEntityId");
            String childEntityType = jsonObject.getString("childEntitytype");
            String childEntityId = jsonObject.getString("ChildEntityId");
            String scnName = jsonObject.getString("scnerioName");
            String riskLevel = jsonObject.getString("riskLevel");
            String riskScore = jsonObject.getString("riskScore");
            String message = jsonObject.getString("AlertMessage");
            message=message.replaceAll("%20"," ");
            String casedisplaykey=parrentEntityId+" "+"| Mashreq";
            String num=generateRandomNum();
            String  date=getdate();
            String evidence= "{\"evidenceData\":[{\"keyValues\":{},\"evidenceMap\":{},\"eventIds\":[\""+parrentEntityId+"\"],\"addOnScore\":0.0,\"baseScore\":0.0,\"incidentSource\":\"MSTR\",\"modified\":false,\"scnName\":\""+scnName+"\"}]}";
            String event_id="FA_TRAN_"+num;


            DBOperation dbOperation = new DBOperation();
            boolean flag=dbOperation.insert(parrentEntityType, parrentEntityId, childEntityType, childEntityId, scnName, riskLevel, riskScore, message,casedisplaykey,event_id,date,evidence);
            if(flag==false){
                resp="success";
            }
            else {
                resp="fail";
            }
            out.write(resp);
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public String generateRandomNum(){
        Random random = new Random();
        int d= random.nextInt();
        String  number=String.valueOf(d);
        return number;
    }
    public String getdate(){
        Date date= new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String strDate= formatter.format(date);
        return strDate;
    }
}

