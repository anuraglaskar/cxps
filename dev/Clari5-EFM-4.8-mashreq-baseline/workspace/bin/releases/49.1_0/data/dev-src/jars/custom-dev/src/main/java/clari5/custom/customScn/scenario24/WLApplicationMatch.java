package clari5.custom.customScn.scenario24;

import clari5.aml.web.wl.search.WlQuery;
import clari5.aml.web.wl.search.WlRecordDetails;
import clari5.aml.web.wl.search.WlResult;
import clari5.aml.web.wl.search.WlSearcher;
import clari5.aml.wle.MatchedRecord;
import clari5.aml.wle.MatchedResults;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.List;

public class WLApplicationMatch {
    private static final CxpsLogger logger = CxpsLogger.getLogger(WLApplicationMatch.class);

    public static void getdata() {
        List<ApplicationField> datalist = ApplicationData.getAppFieldstoMacth();
        if(datalist.size()>0){
            logger.info("[WLApplicationMatch] Data Fetched from Event_NFT_AcctOpening"+datalist.size());
        }
        String ruleName = "MASHREQAPP";
        for (ApplicationField applicationField : datalist)
        {
            JSONObject obj = new JSONObject();
            JSONArray array = new JSONArray();
            obj.put("ruleName", ruleName);
            JSONObject jo1 = new JSONObject();
            jo1.put("name", "email_id");
            jo1.put("value", applicationField.getNewemail());
            array.put(jo1);
            JSONObject jo2 = new JSONObject();
            jo2.put("name", "ph_no");
            jo2.put("value", applicationField.getNewmobile());
            array.put(jo2);
            JSONObject jo3 = new JSONObject();
            jo3.put("name", "trade_license");
            jo3.put("value", applicationField.getNewtradelincense());
            array.put(jo3);
            JSONObject jo4 = new JSONObject();
            jo4.put("name", "eida");
            jo4.put("value", applicationField.getNeweid());
            array.put(jo4);
            obj.put("fields", array);
            String resp = WLApplicationMatch.getResponse(obj);
            logger.info("[WLApplicationMatch] Match Response for"+obj+" is"+resp);
            AppMqWriter.resposeFormatter(resp,applicationField.getNewapprefno());
        }

    }
    private static String getResponse(Object obj) {
        String response = "";
        try {
            WlQuery query = new WlQuery();
            query.from(obj.toString());
            logger.info("Query object created from json : " + obj.toString());

            WlSearcher searcher = new WlSearcher();
            WlResult result = searcher.search(query);
            MatchedResults matched = result.getMatchedResults();
            Collection<MatchedRecord> recordList = matched.getMatchedRecords();
            if (recordList != null) {
                for (MatchedRecord record : recordList) {
                    WlRecordDetails wlDetails = new WlRecordDetails();
                    String outputDetails = wlDetails.getRecordDetails(record.getDocId());
                    record.setDetails(outputDetails);
                }
                response = result.toJson();
                logger.info("[WLApplicationMatch] Response for Application opened: " + response);
            }
        } catch (Exception e) {
            logger.error("[WLApplicationMatch] Unable to fetch response from WL for URL: ---> " + obj.toString());
            e.getCause();
            e.printStackTrace();
        }
        return response;
    }
}
