package clari5.custom.customScn.scenario34;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cxps.apex.utils.StringUtils;
import jdk.nashorn.internal.runtime.RecompilableScriptFunctionData;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class AcctMqWriter {
    public static CxpsLogger logger = CxpsLogger.getLogger(AcctMqWriter.class);
    private static ThreadDump threadDump=AcctEodReminder.getThreadDump();
    static {
        ECClient.configure(null);
    }
    public static void writeJson(){
        logger.info("MqWriter called ");
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();){
            PreparedStatement preparedStatement=connection.prepareStatement(threadDump.getSelectQry());
            preparedStatement.setString(1,  threadDump.getCurrentThreadName());
            preparedStatement.setDate(2,threadDump.getThreadDate());
            ResultSet resultSet=preparedStatement.executeQuery();
            if(resultSet.next()){
                logger.info("[AcctMqWriter] Data Already Exist on today's Date");
            }
            else{
                AcctEodReminder.insert(threadDump.getCurrentThreadName(),threadDump.getThrdId(),threadDump.getThreadDate(),threadDump.getInsrtQry());
                WLAccountMatch wlaccountmatch= new WLAccountMatch();
                wlaccountmatch.getMatchedResult();
                //threadDump=null;
            }

        }
        catch (Exception e){
                logger.info("[AccMqWriter] Error while Fetching Data from thread audit"+e.getCause()+""+e.getMessage());
        }

        //System.out.println("wl Response :: "+wlresponse);
    }
    public static  void  resposeFormatter(String response, String newcif, String newaccount) {
        String custid="";
        String matched_nationalId = "NA";
        String matched_mobile = "NA";
        String matched_custTradeNo = "NA";
        String matched_custPassportNo = "NA";
        try {
            JsonParser parser = new JsonParser();
            if (!StringUtils.isNullOrEmpty(response)) {
                JsonObject jsonObject = parser.parse(response).getAsJsonObject();
            if (jsonObject.has("matchedResults")) {
                jsonObject = jsonObject.getAsJsonObject("matchedResults");
                if (jsonObject.has("matchedRecords")) {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("matchedRecords");
                    for (int j = 0; j < jsonArray.size(); j++) {
                        jsonObject = (JsonObject) jsonArray.get(j);
                        custid = jsonObject.get("id").toString().replaceAll("\"", "");
                        /*if (custid.equalsIgnoreCase(newcif)){
                            logger.info("both are same customer");
                        }else {*/
                            JsonArray fieldjsonArray = (JsonArray) jsonObject.get("fields");
                            int size = fieldjsonArray.size();
                            List<AccountPojo> oldDataList = AccountData.getAccountOldData(custid);

                            for (int i = 0; i < size; i++) {

                                JsonObject fieldjsonObject = (JsonObject) fieldjsonArray.get(i);
                                String score = fieldjsonObject.get("score").toString().replaceAll("\"", "");
                                Double finalscore = Double.parseDouble(score);
                                if (finalscore == 1.0) {
                                    String name = fieldjsonObject.get("name").toString().replaceAll("\"", "");
                                    String value = fieldjsonObject.get("value").toString().replaceAll("\"", "");
                                    String queryVal = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    if (name.equalsIgnoreCase("nationalId")) {
                                        matched_nationalId = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    } else if (name.equalsIgnoreCase("custMobile1")) {
                                        matched_mobile = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    } else if (name.equalsIgnoreCase("custTradeNo")) {
                                        matched_custTradeNo = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    } else if (name.equalsIgnoreCase("custPassportNo")) {
                                        matched_custPassportNo = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                    }
                                }
                            }

                                AccountPojo newdata = AccountData.getAccountNewdata(newcif, newaccount);

                                //AccountPojo newdata = AccountData.getAccountNewdata(matched_nationalId, matched_mobile, matched_custTradeNo, matched_custPassportNo);
                                logger.info("matched_nationalId--" + matched_nationalId + "mached mobile --" + matched_mobile + "matched_custTradeNo--" + matched_custTradeNo + "matched_custPassportNo--" + matched_custPassportNo);
                                AccountPojo finaldata = new AccountPojo();
                                //new data
                                finaldata.setNewcif(newdata.getNewcif());
                                finaldata.setNewcustname(newdata.getNewcustname());
                                finaldata.setNeweid(newdata.getNeweid());
                                finaldata.setNewpassport(newdata.getNewpassport());
                                finaldata.setNewaccountno(newdata.getNewaccountno());
                                finaldata.setNewtradelicense(newdata.getNewtradelicense());
                                finaldata.setNewcompmis1(newdata.getNewcompmis1());
                                finaldata.setNewcompmis2(newdata.getNewcompmis2());
                                finaldata.setNewemail(newdata.getNewemail());
                                finaldata.setNewbranchid(newdata.getNewbranchid());
                                finaldata.setNewmobile(newdata.getNewmobile());
                                finaldata.setNewcustdob(newdata.getNewcustdob());
                                finaldata.setNewacctopendate(newdata.getNewacctopendate());
                                finaldata.setNewacctcloseddate(newdata.getNewacctcloseddate());
                                //olddata
                                for (AccountPojo olddata : oldDataList) {
                                    finaldata.setOldcif(olddata.getOldcif());
                                    finaldata.setOldcustname(olddata.getOldcustname());
                                    finaldata.setOldeid(olddata.getOldeid());
                                    finaldata.setOldpassport(olddata.getOldpassport());
                                    finaldata.setOldaccountno(olddata.getOldaccountno());
                                    finaldata.setOldtradelicense(olddata.getOldtradelicense());
                                    finaldata.setOldcompmis1(olddata.getOldcompmis1());
                                    finaldata.setOldcompmis2(olddata.getOldcompmis2());
                                    finaldata.setOldemail(olddata.getOldemail());
                                    finaldata.setOldbranchid(olddata.getOldbranchid());
                                    finaldata.setOldmobile(olddata.getOldmobile());
                                    finaldata.setOldcustdob(olddata.getOldcustdob());
                                    finaldata.setOldacctopendate(olddata.getOldacctopendate());
                                    finaldata.setOldacctcloseddate(olddata.getOldacctcloseddate());
                                    //matched data
                                    finaldata.setMatchedeid(matched_nationalId);
                                    finaldata.setMatchedmobile(matched_mobile);
                                    finaldata.setMatchedpassport(matched_custPassportNo);
                                    finaldata.setMatchedtradelicense(matched_custTradeNo);
                                    writeJosntoMq(finaldata);
                                }
                                matched_nationalId = "NA";
                                matched_mobile = "NA";
                                matched_custPassportNo = "NA";
                                matched_custTradeNo = "NA";

                            //}
                          }
                        }
                    }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  static  boolean writeJosntoMq(AccountPojo finaldata){
        JSONObject json = new JSONObject();
        JSONObject msg = new JSONObject();
        String entity_id = "acctopenedsamedetails";
        String event_id=getRandomNum();
        json.put("EventType", "nft");
        json.put("EventSubType", "acctopenedsamedetails");
        json.put("event_name", "nft_acctopenedsamedetails");
        msg.put("sys_time", getSysTime());
        msg.put("host_id", "F");
        msg.put("event_id", event_id.replaceAll("-",""));
        msg.put("new_cif", finaldata.getNewcif().replaceAll("C_F_",""));
        msg.put("new_eid", finaldata.getNeweid() !=null ? finaldata.getNeweid(): "");
        msg.put("new_branchid", finaldata.getNewbranchid() != null ? finaldata.getNewbranchid() : "");
        msg.put("new_mobile", finaldata.getNewmobile() != null ? finaldata.getNewmobile() : "");
        msg.put("new_compmis1", finaldata.getNewcompmis1() != null ? finaldata.getNewcompmis1() : "");
        msg.put("new_compmis2", finaldata.getNewcompmis2() !=null ? finaldata.getNewcompmis2() : "");
        msg.put("new_passport", finaldata.getNewpassport() != null ? finaldata.getNewpassport() : "");
        msg.put("new_email", finaldata.getNewemail() != null ? finaldata.getNewemail() : "");
        msg.put("new_accountno", finaldata.getNewaccountno() != null ? finaldata.getNewaccountno() : "");
        msg.put("new_custdob", finaldata.getNewcustdob() != null ? finaldata.getNewcustdob() : "");
        msg.put("new_tradelicense", finaldata.getNewtradelicense() != null ? finaldata.getNewtradelicense() : "");
        msg.put("new_acctopendate", finaldata.getNewacctopendate() != null ? finaldata.getNewacctopendate() : "");
        msg.put("new_acctcloseddate", finaldata.getNewacctcloseddate() != null ? finaldata.getNewacctcloseddate() : "");
        msg.put("old_mobile", finaldata.getOldmobile() != null ? finaldata.getOldmobile() : "");
        msg.put("old_cif", finaldata.getOldcif().replaceAll("C_F_",""));
        msg.put("old_eid", finaldata.getOldeid() !=null ? finaldata.getOldeid() : "");
        msg.put("old_passport", finaldata.getOldpassport() != null ? finaldata.getOldpassport() : "");
        msg.put("old_compmis2", finaldata.getOldcompmis1() != null ? finaldata.getOldcompmis1() : "");
        msg.put("old_compmis1", finaldata.getOldcompmis2() != null ? finaldata.getOldcompmis2() : "");
        msg.put("old_email", finaldata.getOldemail() != null ? finaldata.getOldemail() : "");
        msg.put("old_custname", finaldata.getOldcustname() != null ? finaldata.getOldcustname() : "");
        msg.put("old_custdob", finaldata.getOldcustdob() != null ? finaldata.getOldcustdob() : "");
        msg.put("old_branchid", finaldata.getOldbranchid() != null ? finaldata.getOldbranchid() : "");
        msg.put("old_accountno", finaldata.getOldaccountno() !=null ? finaldata.getOldaccountno() : "");
        msg.put("old_tradelicense", finaldata.getOldtradelicense() != null ? finaldata.getOldtradelicense() : "");
        msg.put("old_acctopendate", finaldata.getOldacctopendate() != null ? finaldata.getOldacctopendate() : "");
        msg.put("old_acctcloseddate", finaldata.getOldacctcloseddate() != null ? finaldata.getOldacctcloseddate() : "");
        msg.put("matched_passport", finaldata.getMatchedpassport() != null ? finaldata.getMatchedpassport() : "");
        msg.put("matched_mobile", finaldata.getMatchedmobile() != null ? finaldata.getMatchedmobile() : "");
        msg.put("matched_eid", finaldata.getMatchedeid() != null ? finaldata.getMatchedeid() : "");
        msg.put("matched_tradelicense", finaldata.getMatchedtradelicense() !=null ? finaldata.getMatchedtradelicense() : "");
        json.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

        logger.info("Final JSON: " + json.toString());
        boolean status = ECClient.enqueue("HOST", entity_id, event_id, json.toString());
        logger.info("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        return false;

    }
    public  static  String getRandomNum(){
        Random rand = new Random();
        int randomnum=rand.nextInt();

        return String.valueOf(randomnum);
    }
    public  static String getSysTime(){
        String systime="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= new Date();
            systime=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return systime;

    }
}
