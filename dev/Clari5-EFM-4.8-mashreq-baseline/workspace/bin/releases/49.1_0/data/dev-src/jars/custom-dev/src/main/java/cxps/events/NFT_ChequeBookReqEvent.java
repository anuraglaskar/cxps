// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_CHEQUE_BOOK_REQUEST", Schema="rice")
public class NFT_ChequeBookReqEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String accountName;
       @Field(size=20) public String branchIdDesc;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field(size=20) public String channel;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String branchId;
       @Field(size=200) public String custId;
       @Field(size=20) public String empId;
       @Field(size=20) public String unusedlvs;
       @Field(size=20) public String issuedlvs;
       @Field(size=20) public String beginchqnum;
       @Field(size=20) public String beginalpha;
       @Field(size=20) public String hostId;
       @Field(size=20) public String endchqnum;


    @JsonIgnore
    public ITable<NFT_ChequeBookReqEvent> t = AEF.getITable(this);

	public NFT_ChequeBookReqEvent(){}

    public NFT_ChequeBookReqEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("ChequeBookReq");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAccountName(json.getString("account_name"));
            setBranchIdDesc(json.getString("branch_id_desc"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setChannel(json.getString("channel"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setEmpId(json.getString("emp_id"));
            setUnusedlvs(json.getString("unusedlvs"));
            setIssuedlvs(json.getString("issuedlvs"));
            setBeginchqnum(json.getString("beginchqnum"));
            setBeginalpha(json.getString("beginalpha"));
            setHostId(json.getString("host_id"));
            setEndchqnum(json.getString("endchqnum"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setCustId(cxps.events.CustomDerivator.getCustIdFromAccountChequeBookReq(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "CB"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAccountName(){ return accountName; }

    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public String getChannel(){ return channel; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getEmpId(){ return empId; }

    public String getUnusedlvs(){ return unusedlvs; }

    public String getIssuedlvs(){ return issuedlvs; }

    public String getBeginchqnum(){ return beginchqnum; }

    public String getBeginalpha(){ return beginalpha; }

    public String getHostId(){ return hostId; }

    public String getEndchqnum(){ return endchqnum; }
    public String getCustId(){ return custId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAccountName(String val){ this.accountName = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setEmpId(String val){ this.empId = val; }
    public void setUnusedlvs(String val){ this.unusedlvs = val; }
    public void setIssuedlvs(String val){ this.issuedlvs = val; }
    public void setBeginchqnum(String val){ this.beginchqnum = val; }
    public void setBeginalpha(String val){ this.beginalpha = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setEndchqnum(String val){ this.endchqnum = val; }
    public void setCustId(String val){ this.custId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_ChequeBookReq");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "ChequeBookReq");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}