// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_MultiplecustomerEventMapper extends EventMapper<NFT_MultiplecustomerEvent> {

public NFT_MultiplecustomerEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_MultiplecustomerEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_MultiplecustomerEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_MultiplecustomerEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_MultiplecustomerEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_MultiplecustomerEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_MultiplecustomerEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getNewCusttype());
            preparedStatement.setString(i++, obj.getNewCxcif());
            preparedStatement.setString(i++, obj.getOldMobile());
            preparedStatement.setString(i++, obj.getOldCif());
            preparedStatement.setString(i++, obj.getOldEid());
            preparedStatement.setString(i++, obj.getOldCusttype());
            preparedStatement.setString(i++, obj.getOldCompmis2());
            preparedStatement.setString(i++, obj.getNewCustname());
            preparedStatement.setString(i++, obj.getOldCompmis1());
            preparedStatement.setString(i++, obj.getOldEmail());
            preparedStatement.setString(i++, obj.getNewMobile());
            preparedStatement.setString(i++, obj.getMatchedEmail());
            preparedStatement.setString(i++, obj.getNewCompmis1());
            preparedStatement.setString(i++, obj.getNewCompmis2());
            preparedStatement.setString(i++, obj.getOldPassport());
            preparedStatement.setString(i++, obj.getMatchedMobile());
            preparedStatement.setString(i++, obj.getNewEid());
            preparedStatement.setString(i++, obj.getNewPassport());
            preparedStatement.setString(i++, obj.getOldCustname());
            preparedStatement.setString(i++, obj.getNewEmail());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_MULTIPLECUSTOMER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_MultiplecustomerEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_MultiplecustomerEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_MULTIPLECUSTOMER"));
        putList = new ArrayList<>();

        for (NFT_MultiplecustomerEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "NEW_CUSTTYPE",  obj.getNewCusttype());
            p = this.insert(p, "NEW_CXCIF",  obj.getNewCxcif());
            p = this.insert(p, "OLD_MOBILE",  obj.getOldMobile());
            p = this.insert(p, "OLD_CIF",  obj.getOldCif());
            p = this.insert(p, "OLD_EID",  obj.getOldEid());
            p = this.insert(p, "OLD_CUSTTYPE",  obj.getOldCusttype());
            p = this.insert(p, "OLD_COMPMIS2",  obj.getOldCompmis2());
            p = this.insert(p, "NEW_CUSTNAME",  obj.getNewCustname());
            p = this.insert(p, "OLD_COMPMIS1",  obj.getOldCompmis1());
            p = this.insert(p, "OLD_EMAIL",  obj.getOldEmail());
            p = this.insert(p, "NEW_MOBILE",  obj.getNewMobile());
            p = this.insert(p, "MATCHED_EMAIL",  obj.getMatchedEmail());
            p = this.insert(p, "NEW_COMPMIS1",  obj.getNewCompmis1());
            p = this.insert(p, "NEW_COMPMIS2",  obj.getNewCompmis2());
            p = this.insert(p, "OLD_PASSPORT",  obj.getOldPassport());
            p = this.insert(p, "MATCHED_MOBILE",  obj.getMatchedMobile());
            p = this.insert(p, "NEW_EID",  obj.getNewEid());
            p = this.insert(p, "NEW_PASSPORT",  obj.getNewPassport());
            p = this.insert(p, "OLD_CUSTNAME",  obj.getOldCustname());
            p = this.insert(p, "NEW_EMAIL",  obj.getNewEmail());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_MULTIPLECUSTOMER"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_MULTIPLECUSTOMER]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_MULTIPLECUSTOMER]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_MultiplecustomerEvent obj = new NFT_MultiplecustomerEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setNewCusttype(rs.getString("NEW_CUSTTYPE"));
    obj.setNewCxcif(rs.getString("NEW_CXCIF"));
    obj.setOldMobile(rs.getString("OLD_MOBILE"));
    obj.setOldCif(rs.getString("OLD_CIF"));
    obj.setOldEid(rs.getString("OLD_EID"));
    obj.setOldCusttype(rs.getString("OLD_CUSTTYPE"));
    obj.setOldCompmis2(rs.getString("OLD_COMPMIS2"));
    obj.setNewCustname(rs.getString("NEW_CUSTNAME"));
    obj.setOldCompmis1(rs.getString("OLD_COMPMIS1"));
    obj.setOldEmail(rs.getString("OLD_EMAIL"));
    obj.setNewMobile(rs.getString("NEW_MOBILE"));
    obj.setMatchedEmail(rs.getString("MATCHED_EMAIL"));
    obj.setNewCompmis1(rs.getString("NEW_COMPMIS1"));
    obj.setNewCompmis2(rs.getString("NEW_COMPMIS2"));
    obj.setOldPassport(rs.getString("OLD_PASSPORT"));
    obj.setMatchedMobile(rs.getString("MATCHED_MOBILE"));
    obj.setNewEid(rs.getString("NEW_EID"));
    obj.setNewPassport(rs.getString("NEW_PASSPORT"));
    obj.setOldCustname(rs.getString("OLD_CUSTNAME"));
    obj.setNewEmail(rs.getString("NEW_EMAIL"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_MULTIPLECUSTOMER]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_MultiplecustomerEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_MultiplecustomerEvent> events;
 NFT_MultiplecustomerEvent obj = new NFT_MultiplecustomerEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_MULTIPLECUSTOMER"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_MultiplecustomerEvent obj = new NFT_MultiplecustomerEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setNewCusttype(getColumnValue(rs, "NEW_CUSTTYPE"));
            obj.setNewCxcif(getColumnValue(rs, "NEW_CXCIF"));
            obj.setOldMobile(getColumnValue(rs, "OLD_MOBILE"));
            obj.setOldCif(getColumnValue(rs, "OLD_CIF"));
            obj.setOldEid(getColumnValue(rs, "OLD_EID"));
            obj.setOldCusttype(getColumnValue(rs, "OLD_CUSTTYPE"));
            obj.setOldCompmis2(getColumnValue(rs, "OLD_COMPMIS2"));
            obj.setNewCustname(getColumnValue(rs, "NEW_CUSTNAME"));
            obj.setOldCompmis1(getColumnValue(rs, "OLD_COMPMIS1"));
            obj.setOldEmail(getColumnValue(rs, "OLD_EMAIL"));
            obj.setNewMobile(getColumnValue(rs, "NEW_MOBILE"));
            obj.setMatchedEmail(getColumnValue(rs, "MATCHED_EMAIL"));
            obj.setNewCompmis1(getColumnValue(rs, "NEW_COMPMIS1"));
            obj.setNewCompmis2(getColumnValue(rs, "NEW_COMPMIS2"));
            obj.setOldPassport(getColumnValue(rs, "OLD_PASSPORT"));
            obj.setMatchedMobile(getColumnValue(rs, "MATCHED_MOBILE"));
            obj.setNewEid(getColumnValue(rs, "NEW_EID"));
            obj.setNewPassport(getColumnValue(rs, "NEW_PASSPORT"));
            obj.setOldCustname(getColumnValue(rs, "OLD_CUSTNAME"));
            obj.setNewEmail(getColumnValue(rs, "NEW_EMAIL"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_MULTIPLECUSTOMER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_MULTIPLECUSTOMER]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"NEW_CUSTTYPE\",\"NEW_CXCIF\",\"OLD_MOBILE\",\"OLD_CIF\",\"OLD_EID\",\"OLD_CUSTTYPE\",\"OLD_COMPMIS2\",\"NEW_CUSTNAME\",\"OLD_COMPMIS1\",\"OLD_EMAIL\",\"NEW_MOBILE\",\"MATCHED_EMAIL\",\"NEW_COMPMIS1\",\"NEW_COMPMIS2\",\"OLD_PASSPORT\",\"MATCHED_MOBILE\",\"NEW_EID\",\"NEW_PASSPORT\",\"OLD_CUSTNAME\",\"NEW_EMAIL\"" +
              " FROM EVENT_NFT_MULTIPLECUSTOMER";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [NEW_CUSTTYPE],[NEW_CXCIF],[OLD_MOBILE],[OLD_CIF],[OLD_EID],[OLD_CUSTTYPE],[OLD_COMPMIS2],[NEW_CUSTNAME],[OLD_COMPMIS1],[OLD_EMAIL],[NEW_MOBILE],[MATCHED_EMAIL],[NEW_COMPMIS1],[NEW_COMPMIS2],[OLD_PASSPORT],[MATCHED_MOBILE],[NEW_EID],[NEW_PASSPORT],[OLD_CUSTNAME],[NEW_EMAIL]" +
              " FROM EVENT_NFT_MULTIPLECUSTOMER";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`NEW_CUSTTYPE`,`NEW_CXCIF`,`OLD_MOBILE`,`OLD_CIF`,`OLD_EID`,`OLD_CUSTTYPE`,`OLD_COMPMIS2`,`NEW_CUSTNAME`,`OLD_COMPMIS1`,`OLD_EMAIL`,`NEW_MOBILE`,`MATCHED_EMAIL`,`NEW_COMPMIS1`,`NEW_COMPMIS2`,`OLD_PASSPORT`,`MATCHED_MOBILE`,`NEW_EID`,`NEW_PASSPORT`,`OLD_CUSTNAME`,`NEW_EMAIL`" +
              " FROM EVENT_NFT_MULTIPLECUSTOMER";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_MULTIPLECUSTOMER (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"NEW_CUSTTYPE\",\"NEW_CXCIF\",\"OLD_MOBILE\",\"OLD_CIF\",\"OLD_EID\",\"OLD_CUSTTYPE\",\"OLD_COMPMIS2\",\"NEW_CUSTNAME\",\"OLD_COMPMIS1\",\"OLD_EMAIL\",\"NEW_MOBILE\",\"MATCHED_EMAIL\",\"NEW_COMPMIS1\",\"NEW_COMPMIS2\",\"OLD_PASSPORT\",\"MATCHED_MOBILE\",\"NEW_EID\",\"NEW_PASSPORT\",\"OLD_CUSTNAME\",\"NEW_EMAIL\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[NEW_CUSTTYPE],[NEW_CXCIF],[OLD_MOBILE],[OLD_CIF],[OLD_EID],[OLD_CUSTTYPE],[OLD_COMPMIS2],[NEW_CUSTNAME],[OLD_COMPMIS1],[OLD_EMAIL],[NEW_MOBILE],[MATCHED_EMAIL],[NEW_COMPMIS1],[NEW_COMPMIS2],[OLD_PASSPORT],[MATCHED_MOBILE],[NEW_EID],[NEW_PASSPORT],[OLD_CUSTNAME],[NEW_EMAIL]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`NEW_CUSTTYPE`,`NEW_CXCIF`,`OLD_MOBILE`,`OLD_CIF`,`OLD_EID`,`OLD_CUSTTYPE`,`OLD_COMPMIS2`,`NEW_CUSTNAME`,`OLD_COMPMIS1`,`OLD_EMAIL`,`NEW_MOBILE`,`MATCHED_EMAIL`,`NEW_COMPMIS1`,`NEW_COMPMIS2`,`OLD_PASSPORT`,`MATCHED_MOBILE`,`NEW_EID`,`NEW_PASSPORT`,`OLD_CUSTNAME`,`NEW_EMAIL`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

