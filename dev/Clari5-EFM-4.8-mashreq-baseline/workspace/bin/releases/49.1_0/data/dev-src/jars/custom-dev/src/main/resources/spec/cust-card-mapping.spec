cxps.noesis.glossary.entity.CUST_CARD_MAPPING {
        db-name = CUST_CARD_MAPPING
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
        { name= jira-parent-id, column =JIRA_PARENT_ID, type ="string:20",key=false}
        { name= cust-id, column =CUST_ID, type ="string:20",key=false}
        { name= card-id, column =CARD_ID, type ="string:100",key=false}
        { name= caseentity-id, column =CASE_ENTITY_ID, type ="string:20",key=false}
                  ]
        indexes {
        CaseEntityId = [ CASE_ENTITY_ID ]
    }
}
