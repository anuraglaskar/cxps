package clari5.custom.customCms.api;

import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.*;
import java.util.Set;

public class GetCustIdCardId_Case_Level {

    private static final CxpsLogger logger = CxpsLogger.getLogger(GetCustIdCardId_Case_Level.class);


    public static String Cust_CustomField_Case_Level="";
    public static String Card_CustomField_Case_Level="";

    public static String dburl="";
    public static String user="";
    public static String password="";
    public static String drivername="";
    public static String schemaname="";

    static {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("jiradb-module.conf");

        Cust_CustomField_Case_Level = hocon.getString("Cust_CustomField_Case_Level");
        Card_CustomField_Case_Level = hocon.getString("Card_CustomField_Case_Level");
        dburl=hocon.getString("dburl");
        user=hocon.getString("user");
        password=hocon.getString("password");
        drivername=hocon.getString("drivername");
        schemaname=hocon.getString("schemaname");

        logger.info("CUst_customfieldId "+Cust_CustomField_Case_Level);
        logger.info("Card_customfieldId "+Card_CustomField_Case_Level);
    }


    public JSONObject getCust_Card_Details(String issuekey)  {
        Connection con =null;


        JSONObject json = new JSONObject();
        String table_name=schemaname+"CUSTOMFIELDVALUE";

        logger.info("Table name is : "+table_name);

        try {

            Class.forName(drivername);
            con= DriverManager.getConnection(dburl,user,password) ;

            logger.info("Connected to DB");

            json=getCust(con,table_name,issuekey,json);
            json=getCard(con,table_name,issuekey,json);

            JSONArray custstatus = new JSONArray();
            JSONObject cust_obj = new JSONObject();

            JSONArray jsonArray = (JSONArray) json.get("custId");

               for(int i=0;i < jsonArray.length();i++){

                   String cust_Id = jsonArray.get(i).toString();

                String status = getCustStatus(cust_Id);
                cust_obj.put(cust_Id,status);
               }

               custstatus.put(cust_obj);
               json = json.put("custstatus",custstatus);


            JSONArray jsonarray = (JSONArray) json.get("cardNumber");
            JSONObject innerObject = jsonarray.getJSONObject(0);
            Set<String> keys = innerObject.keySet();
            JSONObject cust_cardStatus_Array = new JSONObject();
            JSONObject cust_cardStatus=null;
            JSONArray A2 ;
            JSONArray A1 = new JSONArray();

            for(String s : keys)
            {
                JSONArray jsonArray1 =(JSONArray) innerObject.get(s);

                A2 = new JSONArray();

                for(int i=0;i<jsonArray1.length();i++) {

                    String str = jsonArray1.get(i).toString();

                    String status  = getCardStatus(str);

                    cust_cardStatus = new JSONObject();
                    cust_cardStatus.put("cardno",str);
                    cust_cardStatus.put("status",status);
                    A2.put(cust_cardStatus);
                }
                cust_cardStatus_Array.put(s,A2);
            }

            A1.put(cust_cardStatus_Array);

            json = json.put("cardstatus",A1);

       } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (con!=null){
                try {
                    con.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        }
        return json;
    }

    public static JSONObject getCust(Connection con,String tablename,String issuekey,JSONObject json){

        String op = null;

        String sql = "select TEXTVALUE from "+tablename+" where issue ='" + issuekey + "' and CUSTOMFIELD ='" + Cust_CustomField_Case_Level +"'";

        logger.info("GetCust query is "+sql);

        try(Statement stmt = con.createStatement()){

            try(ResultSet rs = stmt.executeQuery(sql)){

                while (rs.next())
                {
                    op = rs.getString("TEXTVALUE");
                    logger.info("Output customer number is "+op);
                }

                if (op == null || op == "")
                        op="No Data Found";

                if(!op.equalsIgnoreCase("No Data Found"))
                {
                    op=op.replace("[","").replace("]","");
                    String[] s = op.split(",");

                    JSONArray obj = new JSONArray();
                    for(String s1 : s)
                    {
                        obj.put(s1.trim());
                    }

                    json.put("custId",obj);
                    logger.info("JSONObject is "+json);
               }
                else
                    json.put("custId",op);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return json;
     }

    public static JSONObject getCard(Connection con,String tablename,String issuekey,JSONObject json)
    {
        String op = null;

        String sql = "select TEXTVALUE from "+tablename+" where issue='" + issuekey + "' and CUSTOMFIELD ='" + Card_CustomField_Case_Level +"'";

        logger.info("Sql query for card is : "+sql);
        try(Statement stmt = con.createStatement()){

            try(ResultSet rs = stmt.executeQuery(sql)){

                while (rs.next())
                {
                    op = rs.getString("TEXTVALUE");
                    logger.info("Resultset for card "+op);
                }

                if (op == null || op == "")
                    op="No Data Found";


                if(!op.equalsIgnoreCase("No Data Found"))
                {
                    JSONObject jsonObj = new JSONObject();
                    op = op.replace("{","").replace("}","");
                    String[] s1 = op.split("],");

                    for(String s : s1)
                    {
                        String[] s2 = s.split(":");
                        s2[1] = s2[1].replace("[","").replace("]","");
                        String[] s3 = s2[1].split(",");

                        JSONArray jsonArray = new JSONArray();

                        for(String str : s3)
                            jsonArray.put(str.trim());

                        jsonObj.put(s2[0].trim(),jsonArray);
                    }
                    JSONArray jsa = new JSONArray();
                    jsa.put(jsonObj);
                    json.put("cardNumber",jsa);
                    logger.info("JSONObject for cardNunber is "+json);
                }
                else
                    json.put("cardNumber",op);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String getCustStatus(String cust_id)
    {
        if(cust_id.equalsIgnoreCase("Not Available"))
        {
            return "NA";
        }
        String sql = "SELECT frzStatus from FREZ_UNFREZ_DETAILS WHERE customerNo = ?";

        logger.info("Freeze status query : "+sql);

        try(Connection con = Rdbms.getAppConnection()){

            try(PreparedStatement ps = con.prepareStatement(sql))
            {
                ps.setString(1,cust_id);

                try(ResultSet rs = ps.executeQuery())
                {
                    if(rs.next())
                    {
                        String status = rs.getString("frzStatus");
                        logger.info("Status is "+status);
                        if(status.equalsIgnoreCase("Y"))
                            return "Applied";
                        else
                            return "Not Applied";
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Not Applied";
    }

    public static String getCardStatus(String card_no)
    {
        if(card_no.equalsIgnoreCase("Not Available"))
        {
            return "NA";
        }
        String sql = "SELECT blkStatus from CRDBLK_UNBLK_DETAILS WHERE cardNo = ?";
        logger.info("Block query "+sql);

        try(Connection con = Rdbms.getAppConnection()){

            try(PreparedStatement ps = con.prepareStatement(sql))
            {
                ps.setString(1,card_no);

                try(ResultSet rs = ps.executeQuery())
                {
                    if(rs.next())
                    {
                        String status = rs.getString("blkStatus");
                        logger.info("Block status "+status);
                        if(status.equalsIgnoreCase("Y"))
                            return "Blocked";
                        else
                            return "UnBlocked";
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "UnBlocked";
    }
}
