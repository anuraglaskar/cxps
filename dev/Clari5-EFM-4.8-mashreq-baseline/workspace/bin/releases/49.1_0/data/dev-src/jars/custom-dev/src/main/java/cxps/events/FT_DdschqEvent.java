// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_DDSCHQ", Schema="rice")
public class FT_DdschqEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String chequeDdNo;
       @Field(size=50) public String tradeNameAr;
       @Field(size=20) public String beneName;
       @Field(size=50) public String dor;
       @Field(size=20) public Double amt;
       @Field(size=50) public String tradeNameEn;
       @Field(size=50) public String sourceName;
       @Field(size=50) public String reasonCode;
       @Field(size=50) public String regPlace;
       @Field(size=50) public String emiratesId;
       @Field(size=50) public String recordId;
       @Field(size=50) public String typePayOrd;
       @Field(size=50) public String bnkIdentifier;
       @Field(size=50) public String fullNameAr;
       @Field(size=50) public String primaryMobileNo;
       @Field(size=50) public String cisNo;
       @Field(size=50) public String recordType;
       @Field(size=50) public String fullNameEn;
       @Field(size=50) public String subjectType;
       @Field(size=50) public String emirate;
       @Field(size=50) public String nationality;
       @Field(size=50) public String dob;
       @Field(size=50) public String iban;
       @Field(size=50) public String passportNo;
       @Field public java.sql.Timestamp refDate;
       @Field(size=50) public String tradeLinNo;
       @Field(size=50) public String descCode;
       @Field public java.sql.Timestamp dataDate;


    @JsonIgnore
    public ITable<FT_DdschqEvent> t = AEF.getITable(this);

	public FT_DdschqEvent(){}

    public FT_DdschqEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Ddschq");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setChequeDdNo(json.getString("cheque_dd_no"));
            setTradeNameAr(json.getString("trade_name_ar"));
            setBeneName(json.getString("bene_name"));
            setDor(json.getString("dor"));
            setAmt(EventHelper.toDouble(json.getString("amt")));
            setTradeNameEn(json.getString("trade_name_en"));
            setSourceName(json.getString("source_name"));
            setReasonCode(json.getString("reason_code"));
            setRegPlace(json.getString("reg_place"));
            setEmiratesId(json.getString("emirates_id"));
            setRecordId(json.getString("record_id"));
            setTypePayOrd(json.getString("type_pay_ord"));
            setBnkIdentifier(json.getString("bnk_identifier"));
            setFullNameAr(json.getString("full_name_ar"));
            setPrimaryMobileNo(json.getString("primary_mobile_no"));
            setCisNo(json.getString("cis_no"));
            setRecordType(json.getString("record_type"));
            setFullNameEn(json.getString("full_name_en"));
            setSubjectType(json.getString("subject_type"));
            setEmirate(json.getString("emirate"));
            setNationality(json.getString("nationality"));
            setDob(json.getString("dob"));
            setIban(json.getString("iban"));
            setPassportNo(json.getString("passport_no"));
            setRefDate(EventHelper.toTimestamp(json.getString("ref_date")));
            setTradeLinNo(json.getString("trade_lin_no"));
            setDescCode(json.getString("desc_code"));
            setDataDate(EventHelper.toTimestamp(json.getString("data_date")));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "DDS"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getChequeDdNo(){ return chequeDdNo; }

    public String getTradeNameAr(){ return tradeNameAr; }

    public String getBeneName(){ return beneName; }

    public String getDor(){ return dor; }

    public Double getAmt(){ return amt; }

    public String getTradeNameEn(){ return tradeNameEn; }

    public String getSourceName(){ return sourceName; }

    public String getReasonCode(){ return reasonCode; }

    public String getRegPlace(){ return regPlace; }

    public String getEmiratesId(){ return emiratesId; }

    public String getRecordId(){ return recordId; }

    public String getTypePayOrd(){ return typePayOrd; }

    public String getBnkIdentifier(){ return bnkIdentifier; }

    public String getFullNameAr(){ return fullNameAr; }

    public String getPrimaryMobileNo(){ return primaryMobileNo; }

    public String getCisNo(){ return cisNo; }

    public String getRecordType(){ return recordType; }

    public String getFullNameEn(){ return fullNameEn; }

    public String getSubjectType(){ return subjectType; }

    public String getEmirate(){ return emirate; }

    public String getNationality(){ return nationality; }

    public String getDob(){ return dob; }

    public String getIban(){ return iban; }

    public String getPassportNo(){ return passportNo; }

    public java.sql.Timestamp getRefDate(){ return refDate; }

    public String getTradeLinNo(){ return tradeLinNo; }

    public String getDescCode(){ return descCode; }

    public java.sql.Timestamp getDataDate(){ return dataDate; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setChequeDdNo(String val){ this.chequeDdNo = val; }
    public void setTradeNameAr(String val){ this.tradeNameAr = val; }
    public void setBeneName(String val){ this.beneName = val; }
    public void setDor(String val){ this.dor = val; }
    public void setAmt(Double val){ this.amt = val; }
    public void setTradeNameEn(String val){ this.tradeNameEn = val; }
    public void setSourceName(String val){ this.sourceName = val; }
    public void setReasonCode(String val){ this.reasonCode = val; }
    public void setRegPlace(String val){ this.regPlace = val; }
    public void setEmiratesId(String val){ this.emiratesId = val; }
    public void setRecordId(String val){ this.recordId = val; }
    public void setTypePayOrd(String val){ this.typePayOrd = val; }
    public void setBnkIdentifier(String val){ this.bnkIdentifier = val; }
    public void setFullNameAr(String val){ this.fullNameAr = val; }
    public void setPrimaryMobileNo(String val){ this.primaryMobileNo = val; }
    public void setCisNo(String val){ this.cisNo = val; }
    public void setRecordType(String val){ this.recordType = val; }
    public void setFullNameEn(String val){ this.fullNameEn = val; }
    public void setSubjectType(String val){ this.subjectType = val; }
    public void setEmirate(String val){ this.emirate = val; }
    public void setNationality(String val){ this.nationality = val; }
    public void setDob(String val){ this.dob = val; }
    public void setIban(String val){ this.iban = val; }
    public void setPassportNo(String val){ this.passportNo = val; }
    public void setRefDate(java.sql.Timestamp val){ this.refDate = val; }
    public void setTradeLinNo(String val){ this.tradeLinNo = val; }
    public void setDescCode(String val){ this.descCode = val; }
    public void setDataDate(java.sql.Timestamp val){ this.dataDate = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.cisNo);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Ddschq");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Ddschq");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}