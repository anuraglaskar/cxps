// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_TRADEFINANCE", Schema="rice")
public class FT_TradefinanceEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String productType;
       @Field(size=200) public String beneficiaryName;
       @Field(size=20) public Double amount;
       @Field(size=200) public String sellerName;
       @Field(size=200) public String billOfLading;
       @Field(size=20) public String corereferenceno;
       @Field(size=200) public String applicantName;
       @Field(size=20) public String edmsreferenceno;
       @Field(size=200) public String billNumber;
       @Field(size=20) public String cifNumber;
       @Field(size=200) public String buyerName;
       @Field(size=20) public String currency;
       @Field(size=20) public String hostId;
       @Field(size=200) public String lcNumber;
       @Field public java.sql.Timestamp contractExpiryDate;


    @JsonIgnore
    public ITable<FT_TradefinanceEvent> t = AEF.getITable(this);

	public FT_TradefinanceEvent(){}

    public FT_TradefinanceEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Tradefinance");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setProductType(json.getString("product_type"));
            setBeneficiaryName(json.getString("beneficiary_name"));
            setAmount(EventHelper.toDouble(json.getString("amount")));
            setSellerName(json.getString("seller_name"));
            setBillOfLading(json.getString("bill_of_lading"));
            setCorereferenceno(json.getString("corereferenceno"));
            setApplicantName(json.getString("applicant_name"));
            setEdmsreferenceno(json.getString("edmsreferenceno"));
            setBillNumber(json.getString("bill_number"));
            setCifNumber(json.getString("cif_number"));
            setBuyerName(json.getString("buyer_name"));
            setCurrency(json.getString("currency"));
            setHostId(json.getString("host_id"));
            setLcNumber(json.getString("lc_number"));
            setContractExpiryDate(EventHelper.toTimestamp(json.getString("contract_expiry_date")));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "TF"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getProductType(){ return productType; }

    public String getBeneficiaryName(){ return beneficiaryName; }

    public Double getAmount(){ return amount; }

    public String getSellerName(){ return sellerName; }

    public String getBillOfLading(){ return billOfLading; }

    public String getCorereferenceno(){ return corereferenceno; }

    public String getApplicantName(){ return applicantName; }

    public String getEdmsreferenceno(){ return edmsreferenceno; }

    public String getBillNumber(){ return billNumber; }

    public String getCifNumber(){ return cifNumber; }

    public String getBuyerName(){ return buyerName; }

    public String getCurrency(){ return currency; }

    public String getHostId(){ return hostId; }

    public String getLcNumber(){ return lcNumber; }

    public java.sql.Timestamp getContractExpiryDate(){ return contractExpiryDate; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setProductType(String val){ this.productType = val; }
    public void setBeneficiaryName(String val){ this.beneficiaryName = val; }
    public void setAmount(Double val){ this.amount = val; }
    public void setSellerName(String val){ this.sellerName = val; }
    public void setBillOfLading(String val){ this.billOfLading = val; }
    public void setCorereferenceno(String val){ this.corereferenceno = val; }
    public void setApplicantName(String val){ this.applicantName = val; }
    public void setEdmsreferenceno(String val){ this.edmsreferenceno = val; }
    public void setBillNumber(String val){ this.billNumber = val; }
    public void setCifNumber(String val){ this.cifNumber = val; }
    public void setBuyerName(String val){ this.buyerName = val; }
    public void setCurrency(String val){ this.currency = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setLcNumber(String val){ this.lcNumber = val; }
    public void setContractExpiryDate(java.sql.Timestamp val){ this.contractExpiryDate = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.billOfLading);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Tradefinance");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Tradefinance");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}