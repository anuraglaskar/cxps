// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_TRADENET", Schema="rice")
public class FT_TradenetEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String acctId;
       @Field(size=20) public Double amount;
       @Field(size=50) public String isSelfTxn;
       @Field(size=50) public String leaveType;
       @Field(size=20) public String channel;
       @Field public java.sql.Timestamp leaveStartDate;
       @Field(size=20) public String securityId;
       @Field(size=20) public String custId;
       @Field(size=50) public String empId;
       @Field(size=20) public String securityClass;
       @Field(size=20) public String staffFlg;
       @Field(size=50) public String isTraderBankStaff;
       @Field(size=50) public String tranType;
       @Field(size=20) public String traderId;
       @Field public java.sql.Timestamp leaveEndDate;
       @Field(size=20) public String hostId;


    @JsonIgnore
    public ITable<FT_TradenetEvent> t = AEF.getITable(this);

	public FT_TradenetEvent(){}

    public FT_TradenetEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Tradenet");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctId(json.getString("acct_id"));
            setAmount(EventHelper.toDouble(json.getString("amount")));
            setIsSelfTxn(json.getString("is_self_txn"));
            setChannel(json.getString("channel"));
            setLeaveStartDate(EventHelper.toTimestamp(json.getString("leave_start_date")));
            setSecurityId(json.getString("security_id"));
            setCustId(json.getString("cust_id"));
            setEmpId(json.getString("emp_id"));
            setSecurityClass(json.getString("security_class"));
            setStaffFlg(json.getString("staff_flg"));
            setIsTraderBankStaff(json.getString("is_trader_bank_staff"));
            setTranType(json.getString("tran_type"));
            setTraderId(json.getString("trader_id"));
            setLeaveEndDate(EventHelper.toTimestamp(json.getString("leave_end_date")));
            setHostId(json.getString("host_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setLeaveType(cxps.events.CustomDerivator.getLeaveType(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "TN"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAcctId(){ return acctId; }

    public Double getAmount(){ return amount; }

    public String getIsSelfTxn(){ return isSelfTxn; }

    public String getChannel(){ return channel; }

    public java.sql.Timestamp getLeaveStartDate(){ return leaveStartDate; }

    public String getSecurityId(){ return securityId; }

    public String getCustId(){ return custId; }

    public String getEmpId(){ return empId; }

    public String getSecurityClass(){ return securityClass; }

    public String getStaffFlg(){ return staffFlg; }

    public String getIsTraderBankStaff(){ return isTraderBankStaff; }

    public String getTranType(){ return tranType; }

    public String getTraderId(){ return traderId; }

    public java.sql.Timestamp getLeaveEndDate(){ return leaveEndDate; }

    public String getHostId(){ return hostId; }
    public String getLeaveType(){ return leaveType; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setAmount(Double val){ this.amount = val; }
    public void setIsSelfTxn(String val){ this.isSelfTxn = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setLeaveStartDate(java.sql.Timestamp val){ this.leaveStartDate = val; }
    public void setSecurityId(String val){ this.securityId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setEmpId(String val){ this.empId = val; }
    public void setSecurityClass(String val){ this.securityClass = val; }
    public void setStaffFlg(String val){ this.staffFlg = val; }
    public void setIsTraderBankStaff(String val){ this.isTraderBankStaff = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setTraderId(String val){ this.traderId = val; }
    public void setLeaveEndDate(java.sql.Timestamp val){ this.leaveEndDate = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setLeaveType(String val){ this.leaveType = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.securityId);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.empId);
        wsInfoSet.add(new WorkspaceInfo("User", userKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Tradenet");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Tradenet");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}