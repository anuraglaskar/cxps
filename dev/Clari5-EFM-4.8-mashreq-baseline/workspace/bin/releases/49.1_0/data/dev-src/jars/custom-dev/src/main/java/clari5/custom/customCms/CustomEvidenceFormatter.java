package clari5.custom.customCms;

/*import clari5.aml.cms.CmsException;
import clari5.aml.cms.jira.PPrintRisk;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.HostEvent;
import clari5.hfdb.HostEventHelper;
import clari5.hfdb.scenario.ScenarioHelper;
import clari5.hfdb.scenario.ScnMetaInfo;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.trace.TracerUtil;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.*;*/

import clari5.aml.cms.CmsException;
import clari5.aml.cms.jira.PPrintRisk;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.HostEvent;
import clari5.hfdb.HostEventHelper;
import clari5.hfdb.scenario.ScenarioHelper;
import clari5.hfdb.scenario.ScnMetaInfo;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import clari5.trace.TracerException;
import clari5.trace.TracerUtil;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import clari5.aml.cms.jira.display.DefaultEvidenceFormatter;
import clari5.aml.cms.jira.display.IEvidenceFormatter;
import clari5.aml.cms.jira.display.Formatter;
import java.sql.Connection;

/**
 * Created by alok on 11/12/14.
 * refactoring
 */
public class CustomEvidenceFormatter extends DefaultEvidenceFormatter  implements IEvidenceFormatter {

    private static CxpsLogger logger = CxpsLogger.getLogger(CustomEvidenceFormatter.class);
    private static final int evidenceMaxLength = 32000;

    private class EventDetail {
        public String eventId;
        public Hocon eventHocon;

        public EventDetail(String eventId, Hocon eventHocon) {
            this.eventId = eventId;
            this.eventHocon = eventHocon;
        }
    }

 
    @Override
    @ExceptionMetered
    @Timed
    @Metered
    public String formatEvidenceRisk(CxJson eventElement, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        if (eventElement == null) {
            return "There is no evidence for this fact. This can happen if the fact evaluation led to no matching conditions. " +
                    "If you wish to avoid seeing details of such facts, please contact the application administrators so " +
                    "that they can modify the Risk Level Assessment and Action Maintenance for this fact.";
        }

        String evidStr = eventElement.toString();
        Hocon h = new Hocon(evidStr);
        StringBuilder resEvid = new StringBuilder();
        if (h.getString("risk1", null) != null) {
            String ev = PPrintRisk.PPrint(h.getString("risk1").replaceAll("`", "\""));
            if (ev.length() > 0) {
                resEvid.append(ev);
            }
        }
        if (h.getString("risk2", null) != null) {
            String remitterevd = PPrintRisk.PPrint(h.getString("risk2").replaceAll("`", "\""));
            if (remitterevd.length() > 0) {
                resEvid.append(remitterevd);
            }
        }
        return resEvid.toString();
    }

    @Override
    @Metered
    @ExceptionMetered
    @Timed
    public String formatEvidenceSam(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws Exception {
        return formatEvidenceSam(e, hfdbEventDetailsMap, -1, 50, (Incident)null);
    }

    //@Override
    @Metered
    @ExceptionMetered
    @Timed
    public String formatEvidenceSam(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap, int maxEventCount, int eventFetchSize, Incident event) throws Exception {
        if (e == null) {
            logger.error("Unable to find freeText element in SAM evidence.");
            return "Unable to parse evidence details for SAM.";
        }
        String evidStr = e.toString();
        StringBuilder resEvid = new StringBuilder();
        StringBuilder evidenceList = new StringBuilder();
        CxJson evidJson = CxJson.parse(evidStr);

        Set<String> hfdbEventIds = new HashSet<>(getUniqueEventIds(e));
        List<String> eventIdList = new ArrayList<>(hfdbEventIds);
        RDBMS rdbms = (RDBMS)Clari5.rdbms();

        try {
            CxConnection con = rdbms.get();
            Throwable var14 = null;

            try {
                Map<String, HostEvent> hfdbEventDetails = Hfdb.getHostEventHelper().getEventMap(con, eventIdList, eventFetchSize);
                hfdbEventDetailsMap.putAll(hfdbEventDetails);
            } catch (Throwable var38) {
                var14 = var38;
                throw var38;
            } finally {
                if (con != null) {
                    if (var14 != null) {
                        try {
                            con.close();
                        } catch (Throwable var37) {
                            var14.addSuppressed(var37);
                        }
                    } else {
                        con.close();
                    }
                }

            }
        } catch (Exception var40) {
            throw new CmsException.Retry("Event table Query Timeout Exception : " + var40.getLocalizedMessage());
        }
        Map<String, Map<String, List<String>>> evidenceMap = new HashMap<>();
        ScnMetaInfo scnMetaInfo = null;
        if (!(evidJson.getString("addOnName") != null && "".equals(evidJson.getString("addOnName")))) {
            String scnName = evidJson.getString("scnName");
            String content = evidJson.getString("content");
            String[] fields_delimiter = content.split("#/#");
            ScenarioHelper scenarioHelper = new ScenarioHelper();
            String ws = CxKeyHelper.getWorkspaceNameGivenCxKey(fields_delimiter[0].split(":")[1]);
            scnMetaInfo = scenarioHelper.getScnMetaInfo(scnName, ws);
            Map<String, List<String>> headerMap = new HashMap<>();
            populateHeaderMap(headerMap, evidJson, hfdbEventDetailsMap, maxEventCount);
            if (headerMap.size() > 0) evidenceMap.put(evidJson.getString("addOnName"), headerMap);
        } else {
            Map<String, List<String>> headerMap = new HashMap<>();
            populateHeaderMap(headerMap, evidJson, hfdbEventDetailsMap, maxEventCount);
            if (headerMap.size() > 0) evidenceMap.put("opinion", headerMap);
        }

        for (Map.Entry<String, Map<String, List<String>>> evid : evidenceMap.entrySet()) {
            evidenceList.append("Evidence for ");
            String blockName = evid.getKey();
            Map<String, List<String>> details = evid.getValue();
            if (blockName.equals("opinion")) {
                evidenceList.append("trigger");
            } else {
                if(scnMetaInfo != null && scnMetaInfo.getAddOnDetails().get(blockName) != null) {
                    evidenceList.append("push up ").append(scnMetaInfo.getAddOnDetails().get(blockName));
                }
                else{
                    evidenceList.append("push up ").append(blockName);
                }
            }

            evidenceList.append("\r\n");

            Map<String, String> mnemonicToHeaderMap = new HashMap<>();
            Map<String, Long> eventTypeToShortestTimeMap = new HashMap<>();
            Map<String, Map<Long, EventDetail>> eventTypeToEventHoconMap = new HashMap<>();

            for (Map.Entry<String, List<String>> entry : details.entrySet()) {
                eventTypeToShortestTimeMap.putIfAbsent(entry.getKey(), Long.MAX_VALUE);
                eventTypeToEventHoconMap.putIfAbsent(entry.getKey(), new TreeMap<>());
                List<String> evidValues = entry.getValue();

                for (String evStr : evidValues) {
                    if (!hfdbEventIds.contains(evStr)) {
                        // let the header print here... we will print body while
                        String mnemonic = evidValues.get(1).split("_")[0];
                        mnemonicToHeaderMap.put(mnemonic, evStr);
                        continue;
                    }

                    HostEvent ev = hfdbEventDetailsMap.get(evStr);
                    Hocon msgBodyHocon = Formatter.getMsgBodyHocon(ev);
                    Timestamp sys_time = TracerUtil.extractTimestamp(new JSONObject(msgBodyHocon.getJSON()));

                    eventTypeToEventHoconMap.get(entry.getKey()).put(sys_time.getTime(), new EventDetail(evStr, msgBodyHocon));
                    if (eventTypeToShortestTimeMap.get(entry.getKey()) > sys_time.getTime())
                        eventTypeToShortestTimeMap.put(entry.getKey(), sys_time.getTime());
                }
            }

            List<Map.Entry<String, Long>> entryList = new LinkedList<>(eventTypeToShortestTimeMap.entrySet());
            entryList.sort(Comparator.comparing(Map.Entry::getValue));
            Map<String, Long> sortedEventTypeToShortestTimeMap = entryList.stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> b, LinkedHashMap::new));

            Map<String,Integer> mnemonicToIsHeaderMap = new HashMap<>();

            /* print events with header in order */
            for (String eventType : sortedEventTypeToShortestTimeMap.keySet()) {
                Map<Long, EventDetail> eventDetailMap = eventTypeToEventHoconMap.get(eventType);
                for (Map.Entry<Long, EventDetail> entry : eventDetailMap.entrySet()) {
                    EventDetail detail = entry.getValue();
                    HostEvent ev = hfdbEventDetailsMap.get(detail.eventId);
                    String mnemonic = detail.eventId.split("_")[0];
                    if (mnemonicToIsHeaderMap.get(mnemonic) == null) {
                        evidenceList.append(mnemonicToHeaderMap.get(mnemonic));
                        evidenceList.append("\r\n");
                        mnemonicToIsHeaderMap.put(mnemonic, 1);
                    }

                    if (hfdbEventIds.contains(detail.eventId)) {
                        String english = CustomFormatter.getEnglish(ev, detail.eventHocon);
                        if (english != null) {
                            evidenceList.append(english);
                            evidenceList.append("\r\n");
                        }
                    }
                }
            }

            evidenceList.append("\r\n");
        }

        resEvid.append(evidenceList.toString());
        return resEvid.toString();
    }

    @Override
    @Metered
    @ExceptionMetered
    @Timed
    public String formatNewEvidenceSam(CxJson evidenceJson, int maxEventCount, int eventFetchSize, Incident event,
                                       int opinionLimit, int addonLimit) throws Exception {
        System.out.println("Custome Evidence Formatter is working");
        if (evidenceJson == null) {
            logger.error("Unable to find freeText element in SAM evidence.");
            return "Unable to parse evidence details for SAM.";
        }

        RDBMS rdbms = Clari5.rdbms();
        boolean isLimitExceeded = false;
        Map<String, Map<String, HostEvent>> opnAddonNameToEventIdTOHostEventMap = new HashMap<>();
        StringBuilder evidenceStr = new StringBuilder();
        if (evidenceJson.isArray()) {

            Iterator<CxJson> iterator = evidenceJson.iterator();
            while (iterator.hasNext()) {

                CxJson evidenceElement = iterator.next();

                Set<String> hfdbEventIds = new HashSet<>(getUniqueEventIds(evidenceElement));
                List<String> eventIdList = new ArrayList<>(hfdbEventIds);

                Map<String, HostEvent> hfdbEventDetails = new HashMap<>();

                String addonName = evidenceElement.getString("addOnName");
                if ((addonName == null || addonName.isEmpty()) && !opnAddonNameToEventIdTOHostEventMap.containsKey("opinion")) {
                    //it means this is opinion one
                    try (CxConnection con = rdbms.get()) {
                        hfdbEventDetails = Hfdb.getHostEventHelper().getEventMap(con, eventIdList, eventFetchSize);
                    } catch (Exception ex) {
                        throw new CmsException.Retry("Event table Query Timeout Exception : " + ex.getLocalizedMessage());
                    }
                    opnAddonNameToEventIdTOHostEventMap.put("opinion", hfdbEventDetails);
                } else if (addonName != null && !addonName.isEmpty() && !opnAddonNameToEventIdTOHostEventMap.containsKey(addonName)) {
                    // this is the addon so adding this one also , this is before checking the size ...
                    try (CxConnection con = rdbms.get()) {
                        hfdbEventDetails = Hfdb.getHostEventHelper().getEventMap(con, eventIdList, eventFetchSize);
                    } catch (Exception ex) {
                        throw new CmsException.Retry("Event table Query Timeout Exception : " + ex.getLocalizedMessage());
                    }
                    opnAddonNameToEventIdTOHostEventMap.put(addonName, hfdbEventDetails);
                }

                addingHeaderAndEvidenceDetails(addonName, evidenceElement, hfdbEventDetails,
                        maxEventCount, evidenceStr, hfdbEventIds, isLimitExceeded, opinionLimit, addonLimit, false);

            }
        }

        if (evidenceStr.length() > evidenceMaxLength) {

            evidenceStr = new StringBuilder();
            isLimitExceeded = true;
            //then actual logic starts to remove the eventIds and take top x eventIds which have to read from conf file ...
            if (evidenceJson.isArray()) {
                Iterator<CxJson> iterator = evidenceJson.iterator();
                while (iterator.hasNext()) {

                    CxJson evidenceElement = iterator.next();
                    Set<String> hfdbEventIds = new HashSet<>(getUniqueEventIds(evidenceElement));
                    Map<String, HostEvent> hfdbEventDetails;

                    String addonName = evidenceElement.getString("addOnName");

                    // this will be used when length of string exceeds 32K ...
                    if (addonName == null || addonName.isEmpty()) {
                        hfdbEventDetails = opnAddonNameToEventIdTOHostEventMap.get("opinion");
                    } else {
                        hfdbEventDetails = opnAddonNameToEventIdTOHostEventMap.get(addonName);
                    }

                    addingHeaderAndEvidenceDetails(addonName, evidenceElement, hfdbEventDetails,
                            maxEventCount, evidenceStr, hfdbEventIds, isLimitExceeded, opinionLimit, addonLimit, false);

                }
            }

            //TODO eventlist is crossed 32K then show only the push up description don't show any push up evidence, show
            //TODO only opinion evidence that too with opnLimit.
            if(evidenceStr.length() > evidenceMaxLength){

                evidenceStr = new StringBuilder();
                isLimitExceeded = true;
                //show only the pushup description and only opinion evidence with opinionLimit...
                if (evidenceJson.isArray()) {
                    Iterator<CxJson> iterator = evidenceJson.iterator();
                    while (iterator.hasNext()) {

                        CxJson evidenceElement = iterator.next();
                        Set<String> hfdbEventIds = new HashSet<>(getUniqueEventIds(evidenceElement));
                        Map<String, HostEvent> hfdbEventDetails;

                        String addonName = evidenceElement.getString("addOnName");

                        // this will be used when length of string exceeds 32K ...
                        if (addonName == null || addonName.isEmpty()) {
                            hfdbEventDetails = opnAddonNameToEventIdTOHostEventMap.get("opinion");
                        } else {
                            hfdbEventDetails = opnAddonNameToEventIdTOHostEventMap.get(addonName);
                        }

                        addingHeaderAndEvidenceDetails(addonName, evidenceElement, hfdbEventDetails,
                                maxEventCount, evidenceStr, hfdbEventIds, isLimitExceeded, opinionLimit, addonLimit, true);

                    }
                }
            }
        }
        return evidenceStr.toString();

    }

    public String addingHeaderAndEvidenceDetails(String addonName, CxJson evidenceElement, Map<String,
            HostEvent> hfdbEventDetails, int maxEventCount, StringBuilder evidenceStr, Set<String> hfdbEventIds,
                                                 boolean islimitExceeded, int maxLimitEventOpn, int maxEventLimitAddon,
                                                 boolean crossedSecondTime)
            throws TracerException.UnableToExtractTimestamp {

        Map<String, Map<String, List<String>>> evidenceMap = new HashMap<>();
        ScnMetaInfo scnMetaInfo = null;
        if (addonName != null && !addonName.isEmpty()) {
            String scnName = evidenceElement.getString("scnName");
            String content = evidenceElement.getString("content");
            String[] fields_delimiter = content.split("#/#");
            ScenarioHelper scenarioHelper = new ScenarioHelper();
            String ws = CxKeyHelper.getWorkspaceNameGivenCxKey(fields_delimiter[0].split(":")[1]);
            scnMetaInfo = scenarioHelper.getScnMetaInfo(scnName, ws);
            Map<String, List<String>> headerMap = new HashMap<>();
            populateHeaderMap(headerMap, evidenceElement, hfdbEventDetails, maxEventCount);
            if (headerMap.size() > 0) evidenceMap.put(evidenceElement.getString("addOnName"), headerMap);
        } else {
            Map<String, List<String>> headerMap = new HashMap<>();
            populateHeaderMap(headerMap, evidenceElement, hfdbEventDetails, maxEventCount);
            if (headerMap.size() > 0) evidenceMap.put("opinion", headerMap);
        }

        if (evidenceElement.getString("addOnName") != null && !(evidenceElement.getString("addOnName").isEmpty()) && evidenceElement.getString("addOnName").startsWith("_a")) {
            CxJson evIdEle = evidenceElement.get("eventIds");
            Iterator<CxJson> eventIds = evIdEle.iterator();

            if (!eventIds.hasNext()) {
                String blockName = evidenceElement.getString("addOnName");
                if ((blockName != null) && !blockName.isEmpty()) {
                    evidenceStr.append("Evidence for ");
                    if (scnMetaInfo != null && scnMetaInfo.getAddOnDetails().get(blockName) != null) {
                        evidenceStr.append("push up").append("[View] ").append(scnMetaInfo.getAddOnDetails().get(blockName));
                    } else {
                        evidenceStr.append("push up").append("[View] ").append(blockName);
                    }
                }
            }
            evidenceStr.append("\r\n");
        }
        for (Map.Entry<String, Map<String, List<String>>> evid : evidenceMap.entrySet()) {
            evidenceStr.append("Evidence for ");
            String blockName = evid.getKey();
            Map<String, List<String>> details = evid.getValue();
            if (blockName.equals("opinion")) {
                evidenceStr.append("trigger");
            } else {
                if (scnMetaInfo != null && scnMetaInfo.getAddOnDetails().get(blockName) != null) {
                    evidenceStr.append("push up ").append(scnMetaInfo.getAddOnDetails().get(blockName));
                } else {
                    evidenceStr.append("push up ").append(blockName);
                }
            }
            evidenceStr.append("\r\n");

            if(crossedSecondTime && !blockName.equals("opinion")) continue;

            int maxLimit = -1;
            if (blockName.equals("opinion")) {
                maxLimit = maxLimitEventOpn;
            } else {
                maxLimit = maxEventLimitAddon;
            }

            int counter = 0;
            for (Map.Entry<String, List<String>> entry : details.entrySet()) {

                List<String> evidValues = entry.getValue();
                Map<Timestamp, Map<String, Hocon>> sortedMapBySysTime = new TreeMap<>();

                if (!islimitExceeded || maxLimit == -1) {
                    for (String evStr : evidValues) {
                        if (hfdbEventIds.contains(evStr)) {
                            HostEvent ev = hfdbEventDetails.get(evStr);
                            Hocon msgBodyHocon = Formatter.getMsgBodyHocon(ev);
                            Timestamp sys_time = TracerUtil.extractTimestamp(new JSONObject(msgBodyHocon.getJSON()));
                            Map<String, Hocon> temp = new HashMap<>();
                            temp.put(evStr, msgBodyHocon);
                            if (sortedMapBySysTime.containsKey(sys_time)) {
                                Map<String, Hocon> eventIdMap = sortedMapBySysTime.get(sys_time);
                                eventIdMap.put(evStr, msgBodyHocon);
                            } else {
                                sortedMapBySysTime.putIfAbsent(sys_time, temp);
                            }
                        } else {
                            // let the header print here... we will print body while
                            evidenceStr.append(evStr);
                            evidenceStr.append("\r\n");
                        }
                    }
                } else {

                    for (String evStr : evidValues) {

                        // breaking after reaching the counter ...
                        if(counter >= maxLimit) break;

                        if (hfdbEventIds.contains(evStr)) {
                            HostEvent ev = hfdbEventDetails.get(evStr);
                            Hocon msgBodyHocon = Formatter.getMsgBodyHocon(ev);
                            Timestamp sys_time = TracerUtil.extractTimestamp(new JSONObject(msgBodyHocon.getJSON()));
                            Map<String, Hocon> temp = new HashMap<>();
                            temp.put(evStr, msgBodyHocon);
                            if (sortedMapBySysTime.containsKey(sys_time)) {
                                Map<String, Hocon> eventIdMap = sortedMapBySysTime.get(sys_time);
                                eventIdMap.put(evStr, msgBodyHocon);
                            } else {
                                sortedMapBySysTime.putIfAbsent(sys_time, temp);
                            }
                            counter++;
                        } else {
                            // let the header print here... we will print body while
                            evidenceStr.append(evStr);
                            evidenceStr.append("\r\n");
                        }
                    }
                }

                /* priting the body and not the header ...*/
                for (Map.Entry<Timestamp, Map<String, Hocon>> timestampStringEntry : sortedMapBySysTime.entrySet()) {
                    Map<String, Hocon> timestampStringMap = timestampStringEntry.getValue();
                    for (Map.Entry<String, Hocon> tempMap : timestampStringMap.entrySet()) {
                        String evStr = tempMap.getKey();
                        Hocon msgBodyHocon = tempMap.getValue();

                        HostEvent ev = hfdbEventDetails.get(evStr);
                        if (hfdbEventIds.contains(evStr)) {
                            String english = CustomFormatter.getEnglish(ev, msgBodyHocon);
                            if (english != null) {
                                evidenceStr.append(english);
                                evidenceStr.append("\r\n");
                            }
                        } else {
                            evidenceStr.append(evStr);
                            evidenceStr.append("\r\n");
                        }
                    }
                }
            }
            evidenceStr.append("\r\n");
        }
        return evidenceStr.toString();
    }

    @Override
    @Metered
    @ExceptionMetered
    @Timed
    public String formatSwiftMessage(CxJson eventElement, Map<String, HostEvent> hfdbEventDetailsMap) throws JSONException {
        String freeTextEle = (eventElement.toString()).replaceAll("`", "\"");
        freeTextEle = freeTextEle.replaceAll("#curlystart#", "\\\\(").replaceAll("#curlyend#", "\\\\)").replaceAll("#eol#", " \\\\\\\\ ");
        return freeTextEle;
    }

    @Override
    @Metered
    @ExceptionMetered
    @Timed
    public String formatEvidenceDefault(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        StringBuilder resEvid = new StringBuilder();
        logger.error("Error : Evidence source is not identified. It has to be one of the following : - RDE, SAM, WL.");
        resEvid.append(e.toString());
        return resEvid.toString();
    }

    /**
     * Accepts tranDetail in the sequence ||Txn Amount||Txn Type|| Txn Time
     *
     * @param e the evidence json element
     * @return
     * @throws JSONException {
     *                       "addOnName":null,
     *                       "content":"WSKEY:A_F_0100001234512|tranDetail:11111.0 pipe T pipe 1464087871001#/#rdetesting#/#null#/#11111",
     *                       "eventIds":["11111"],
     *                       "evidenceMap":{},
     *                       "incidentSource":"RDE",
     *                       "modified":true,
     *                       "risk1":null,
     *                       "risk2":null,
     *                       "scnName":"rdetesting",
     *                       "swiftMessage":null
     *                       }
     */
    public String formatEvidenceRde(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {

        StringBuilder sb = new StringBuilder();
        ScnMetaInfo scnMetaInfo;

        if (e.getString("addOnName").isEmpty()) {
            sb.append("Evidence For Trigger").append("\r\n");
        } else if (!e.getString("addOnName").isEmpty()) {
            String scnName = e.getString("scnName");
            String content = e.getString("content");
            String[] fields_delimiter = content.split("\\|");
            ScenarioHelper scenarioHelper = new ScenarioHelper();
            String ws = CxKeyHelper.getWorkspaceNameGivenCxKey(fields_delimiter[0].split(":")[1]);
            scnMetaInfo = scenarioHelper.getScnMetaInfo(scnName, ws);
            if (scnMetaInfo != null && scnMetaInfo.getAddOnDetails().get(e.getString("addOnName")) != null) {
                sb.append("\r\n Evidence for push up ").append(scnMetaInfo.getAddOnDetails().get(e.getString("addOnName"))).append("\r\n");
            } else {
                sb.append("\r\n Evidence for push up ").append(e.getString("addOnName")).append("\r\n");
            }
        }

        String evidStr = e.getString("content", "");
        if (evidStr.isEmpty()) {
            logger.error("content is null in evidenceDetails...");
            return "||||\r\n||";
        }

        String lhs = evidStr.split("#/#")[0];
        String tranDet = lhs.split("\\|").length > 1 ? lhs.split("\\|")[1] : "";
        tranDet = tranDet.replaceAll("tranDetail", "")
                .replaceAll(":", "")
                .replace("{", "")
                .replace("}", "").trim();

        List<String> headers = new ArrayList<>(), params = new ArrayList<>();
        for (String anEviHeader : tranDet.split(";")) {
            String header = anEviHeader.split("-")[0];
            header = header.substring(1, header.length() - 1);
            headers.add(header);
        }

        for (String param : tranDet.split(";")) {
            String firstParam = param.split("-")[1];
            firstParam = firstParam.substring(1, firstParam.length() - 1);
            String paramToAppend = firstParam.isEmpty() ? "NA" : firstParam;
            params.add(paramToAppend);
        }

        sb.append("||").append(String.join("||", headers)).append("||");
        sb.append("\r\n|").append(String.join("|", params)).append("|");
        return sb.toString();
    }

    @Override
    @Metered
    @ExceptionMetered
    @Timed
    public String formatMSTREvidence(CxJson e, Map<String, HostEvent> hfdbEventDetailsMap) throws org.json.JSONException {
        CxJson responseJson = new CxJson();
        responseJson.put("className", "EvidenceData");
        responseJson.put("incidentSource", e.get("incidentSource"));
        responseJson.put("eventIds", e.get("eventIds"));

        StringBuilder resEvid = new StringBuilder();
        logger.error("Error : Evidence source is not identified. It has to be one of the following : - RDE, SAM, WL.");
        resEvid.append(responseJson.toString());
        return resEvid.toString();
    }

    protected Set<String> getUniqueEventIds(CxJson addOnJson) {

        Set<String> out = new HashSet<>();
        CxJson evIdEle = addOnJson.get("eventIds");
        if (evIdEle == null) return out;
        Iterator<CxJson> eventIds = evIdEle.iterator();
        for (Iterator<CxJson> it = eventIds; it.hasNext(); ) {
            out.add(it.next().toString());
        }
        return out;
    }

    protected void populateHeaderMap(Map<String, List<String>> headerMap, CxJson addOnJson, Map<String, HostEvent> hfdbEventDetailsMap) {
        populateHeaderMap(headerMap, addOnJson, hfdbEventDetailsMap, -1);
    }

    protected void populateHeaderMap(Map<String, List<String>> headerMap, CxJson addOnJson, Map<String, HostEvent> hfdbEventDetailsMap, int maxEventCount) {
        CxJson evIdEle = addOnJson.get("eventIds");
        if (evIdEle == null) return;
        Iterator<CxJson> eventIds = evIdEle.iterator();
        List<String> eventIdList = new ArrayList<>();
        for (Iterator<CxJson> it = eventIds; it.hasNext(); ) {
            eventIdList.add(it.next().toString());
        }

        /* TODO avoid creation of sorted list in every call by creating the sorted master list once in the caller*/
        List<String> sortedEventIdList = sortByTime(eventIdList, hfdbEventDetailsMap);
        if (maxEventCount > 0 && eventIdList.size() > maxEventCount) {
            sortedEventIdList = sortedEventIdList.subList(0, maxEventCount);
        }
        for (String eventId : sortedEventIdList) {
            String eventName = hfdbEventDetailsMap.get(eventId).getEventName();
            if (!headerMap.containsKey(eventName)) {
                List<String> addOnValue = new ArrayList<>();
                headerMap.put(eventName, addOnValue);
                String header = getEventHeader(eventName);
                addOnValue.add(header);
            }
            headerMap.get(eventName).add(eventId);
        }
    }

    protected List<String> sortByTime(List<String> eventIdList, Map<String, HostEvent> hfdbEventDetailsMap) {
        List<String> tsEvId = new ArrayList<>();
        for (HostEvent h : hfdbEventDetailsMap.values()) {
            /* assumes all timestamp long values are the same length */
            if(h.getEventTs() != null) {
                tsEvId.add(h.getEventTs().getTime() + ":" + h.getEventId());
            }
        }
        Collections.sort(tsEvId);

        List<String> out = new ArrayList<>();
        for (String s : tsEvId) {
            out.add(s.substring(s.indexOf(":") + 1));
        }
        return out;
    }

    protected static String getEventHeader(String eventName) {

        try {
            String header = HostEventHelper.getHeaderFormat(eventName);
            if (header != null)
                return header;
        } catch (Exception ex) {
            logger.error("Error : Couldn't form header for evidence details :" + ex.getMessage());
        }
        String fallbackHeader = Hfdb.getRefCodeVal("HOST_EVENT_NAME_TO_DESC", eventName);
        return fallbackHeader == null ? eventName : fallbackHeader;

    }
}
