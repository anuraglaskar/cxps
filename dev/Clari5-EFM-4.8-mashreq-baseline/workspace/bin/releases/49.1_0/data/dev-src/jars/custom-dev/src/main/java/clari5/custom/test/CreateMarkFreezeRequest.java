package clari5.custom.test;

import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name = "CreateMarkFreezeRequest")
public class CreateMarkFreezeRequest{
    String CifId;
    String FraudFreeze;

    public String getCifId() {
        return CifId;
    }

    public void setCifId(String cifId) {
        CifId = cifId;
    }

    public String getFraudFreeze() {
        return FraudFreeze;
    }

    public void setFraudFreeze(String fraudFreeze) {
        FraudFreeze = fraudFreeze;
    }


}