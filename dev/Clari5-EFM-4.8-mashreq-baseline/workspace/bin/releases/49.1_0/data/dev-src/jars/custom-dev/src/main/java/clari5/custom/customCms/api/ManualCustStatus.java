package clari5.custom.customCms.api;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static clari5.custom.customCms.api.GetCustIdCardId_Case_Level.getCardStatus;
import static clari5.custom.customCms.api.GetCustIdCardId_Case_Level.getCustStatus;

public class ManualCustStatus {

    private static final CxpsLogger logger = CxpsLogger.getLogger(ManualCustStatus.class);


    public JSONObject getManualCustStatus(String cust_id)
    {
        JSONObject json = new JSONObject();
        try {

            String sql = "SELECT * from CUSTOMER where hostCustId = ?";

            try(Connection con = Rdbms.getAppConnection()){

                try(PreparedStatement ps = con.prepareStatement(sql))
                {
                    ps.setString(1,cust_id);

                    try(ResultSet rs = ps.executeQuery())
                    {
                        if(rs.next())
                        {
                            String status = getCustStatus(cust_id);
                            json = json.put("status", status);
                        }
                        else json = json.put("status","NA");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }


        }catch (Exception e)
        {
            logger.info("Exception in ManualCustStatus ====> "+e.getMessage());
        }

     return json;
    }

    public JSONObject getManualCardStatus(String card_no)
    {
        JSONObject json = new JSONObject();

        try{
            String status = getCardStatus(card_no);
            json = json.put("status",status);
        }catch (Exception e)
        {
            logger.info("Exception in ManualCardStatus =====> "+e.getMessage());
        }

        return json;
    }
}
