// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_UAEFTSREQUEST", Schema="rice")
public class NFT_UaeftsrequestEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String eid;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=50) public String appId;
       @Field(size=50) public String instanceId;
       @Field(size=50) public String ibanNum;
       @Field public java.sql.Timestamp statToDate;
       @Field public java.sql.Timestamp statFromDate;
       @Field(size=50) public String passportNo;
       @Field(size=50) public String appRef;
       @Field(size=50) public String bankCode;


    @JsonIgnore
    public ITable<NFT_UaeftsrequestEvent> t = AEF.getITable(this);

	public NFT_UaeftsrequestEvent(){}

    public NFT_UaeftsrequestEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Uaeftsrequest");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setEid(json.getString("eid"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setAppId(json.getString("app_id"));
            setInstanceId(json.getString("instance_id"));
            setIbanNum(json.getString("iban_num"));
            setStatToDate(EventHelper.toTimestamp(json.getString("stat_to_date")));
            setStatFromDate(EventHelper.toTimestamp(json.getString("stat_from_date")));
            setPassportNo(json.getString("passport_no"));
            setAppRef(json.getString("app_ref"));
            setBankCode(json.getString("bank_code"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NUAE"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getEid(){ return eid; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getAppId(){ return appId; }

    public String getInstanceId(){ return instanceId; }

    public String getIbanNum(){ return ibanNum; }

    public java.sql.Timestamp getStatToDate(){ return statToDate; }

    public java.sql.Timestamp getStatFromDate(){ return statFromDate; }

    public String getPassportNo(){ return passportNo; }

    public String getAppRef(){ return appRef; }

    public String getBankCode(){ return bankCode; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setEid(String val){ this.eid = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setAppId(String val){ this.appId = val; }
    public void setInstanceId(String val){ this.instanceId = val; }
    public void setIbanNum(String val){ this.ibanNum = val; }
    public void setStatToDate(java.sql.Timestamp val){ this.statToDate = val; }
    public void setStatFromDate(java.sql.Timestamp val){ this.statFromDate = val; }
    public void setPassportNo(String val){ this.passportNo = val; }
    public void setAppRef(String val){ this.appRef = val; }
    public void setBankCode(String val){ this.bankCode = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.ibanNum);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Uaeftsrequest");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Uaeftsrequest");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}