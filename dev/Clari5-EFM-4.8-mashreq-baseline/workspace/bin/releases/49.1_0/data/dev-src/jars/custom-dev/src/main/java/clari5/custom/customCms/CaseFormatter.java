package clari5.custom.customCms;

import clari5.aml.cms.newjira.JiraInstance;
import clari5.jiraclient.JiraClient;
import clari5.platform.jira.JiraClientException;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CaseFormatter {

    private static final CxpsLogger logger = CxpsLogger.getLogger(CaseFormatter.class);


    public static void insertIntodb(String JiraParentId, String CustId, String CardId,String caseentityId) throws JiraClientException {

        String sql = "INSERT INTO CUST_CARD_MAPPING (JIRA_PARENT_ID, CUST_ID, CARD_ID,CASE_ENTITY_ID) VALUES(?, ?, ?, ?)";

        try (Connection con = Rdbms.getAppConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setString(1, JiraParentId);
                stmt.setString(2, CustId);
                stmt.setString(3, CardId);
                stmt.setString(4, caseentityId);

                stmt.executeUpdate();
                con.commit();

            } catch (SQLException e) {
                logger.info("Exception occured while inserting data in CUST_CARD_MAPPING Table----> " + e.getMessage());
            }
        } catch (SQLException e) {
            logger.info("Exception occured while inserting data in CUST_CARD_MAPPING Table----> " + e.getMessage());
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Hocon hocon = new Hocon();
        hocon.loadFromContext("custom_field.conf");
        String cust_customfield = hocon.getString("cust_id_caseLevel");
        String card_customfield = hocon.getString("card_id_caseLevel");

        getData(JiraParentId,caseentityId,cust_customfield,card_customfield);
    }

    public static void getData(String JiraParentId,String CaseEntityId,String custCustomField , String cardCustomField) throws JiraClientException {

        Map<String, String> fieldsData = new HashMap<>();

        fieldsData = getFieldData(CaseEntityId, custCustomField, cardCustomField);

        updateCase(JiraParentId,custCustomField,fieldsData.get(custCustomField));

        updateCase(JiraParentId,cardCustomField,fieldsData.get(cardCustomField));

    }

    public static void updateCase(String JiraParentId,String customfieldId,String fielddata) throws JiraClientException {

        try {
            JiraInstance instance = new JiraInstance("jira-instance");
            JiraClient jiraClient = new JiraClient(instance.getInstanceParams());

            String BaseUrl = jiraClient.getBaseURL();

            String path = "/rest/api/2/issue/";
            String finalurl = BaseUrl + path + JiraParentId;
            String Method = "PUT";

            SupportJiiraCLient suppjiraclient = new SupportJiiraCLient(instance.getInstanceParams());
            String finaldata = jsonForm(customfieldId, fielddata);

            CxJson cxJson = suppjiraclient.updateIssueRequest(finaldata, JiraParentId);

        }catch (Exception e)
        {
            logger.info("Exception while updating the case---> "+JiraParentId+"==="+e.getMessage());
        }
    }

    public static String jsonForm(String customfieldId,String data){

        String jsondata="";
        try {


            jsondata = "{\"update\":{\""+customfieldId+"\":[{\"set\":\""+data+"\"}]}}";

        }catch (Exception e){
            e.printStackTrace();
        }
        return jsondata;
    }


    public static Map<String, String> getFieldData(String CaseEntityId, String custCustomField , String cardCustomField)
    {
        String sql = "SELECT CUST_ID,CARD_ID FROM CUST_CARD_MAPPING where CASE_ENTITY_ID = ?";

        Set<String> cust_id = new HashSet<>();
        Map<String, List<String>> cust_card_map = new HashMap<>();


        Map<String, String> fieldsData = new HashMap<>();

        try(Connection con = Rdbms.getAppConnection()){

            try(PreparedStatement stmt = con.prepareStatement(sql)){

                stmt.setString(1,CaseEntityId);

                try(ResultSet rs = stmt.executeQuery()) {

                    while (rs.next())
                    {
                        String cust_id1 = rs.getString("CUST_ID");
                        String card_id1 = rs.getString("CARD_ID");

                        cust_id.add(cust_id1);

                        if(cust_card_map.containsKey(cust_id1)) {
                            if(!cust_card_map.get(cust_id1).contains(card_id1))
                                 cust_card_map.get(cust_id1).add(card_id1);

                        }
                        else
                        {
                            List<String> card_id = new ArrayList<>();
                            card_id.add(card_id1);
                            cust_card_map.put(cust_id1, card_id);
                        }
                    }
                }

                fieldsData.put(custCustomField,cust_id.toString());
                fieldsData.put(cardCustomField,cust_card_map.toString().replaceAll("=",":"));

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return fieldsData;
    }




}
