// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_DdschqEventMapper extends EventMapper<FT_DdschqEvent> {

public FT_DdschqEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_DdschqEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_DdschqEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_DdschqEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_DdschqEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_DdschqEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_DdschqEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getChequeDdNo());
            preparedStatement.setString(i++, obj.getTradeNameAr());
            preparedStatement.setString(i++, obj.getBeneName());
            preparedStatement.setString(i++, obj.getDor());
            preparedStatement.setDouble(i++, obj.getAmt());
            preparedStatement.setString(i++, obj.getTradeNameEn());
            preparedStatement.setString(i++, obj.getSourceName());
            preparedStatement.setString(i++, obj.getReasonCode());
            preparedStatement.setString(i++, obj.getRegPlace());
            preparedStatement.setString(i++, obj.getEmiratesId());
            preparedStatement.setString(i++, obj.getRecordId());
            preparedStatement.setString(i++, obj.getTypePayOrd());
            preparedStatement.setString(i++, obj.getBnkIdentifier());
            preparedStatement.setString(i++, obj.getFullNameAr());
            preparedStatement.setString(i++, obj.getPrimaryMobileNo());
            preparedStatement.setString(i++, obj.getCisNo());
            preparedStatement.setString(i++, obj.getRecordType());
            preparedStatement.setString(i++, obj.getFullNameEn());
            preparedStatement.setString(i++, obj.getSubjectType());
            preparedStatement.setString(i++, obj.getEmirate());
            preparedStatement.setString(i++, obj.getNationality());
            preparedStatement.setString(i++, obj.getDob());
            preparedStatement.setString(i++, obj.getIban());
            preparedStatement.setString(i++, obj.getPassportNo());
            preparedStatement.setTimestamp(i++, obj.getRefDate());
            preparedStatement.setString(i++, obj.getTradeLinNo());
            preparedStatement.setString(i++, obj.getDescCode());
            preparedStatement.setTimestamp(i++, obj.getDataDate());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_DDSCHQ]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_DdschqEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_DdschqEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_DDSCHQ"));
        putList = new ArrayList<>();

        for (FT_DdschqEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "CHEQUE_DD_NO",  obj.getChequeDdNo());
            p = this.insert(p, "TRADE_NAME_AR",  obj.getTradeNameAr());
            p = this.insert(p, "BENE_NAME",  obj.getBeneName());
            p = this.insert(p, "DOR",  obj.getDor());
            p = this.insert(p, "AMT", String.valueOf(obj.getAmt()));
            p = this.insert(p, "TRADE_NAME_EN",  obj.getTradeNameEn());
            p = this.insert(p, "SOURCE_NAME",  obj.getSourceName());
            p = this.insert(p, "REASON_CODE",  obj.getReasonCode());
            p = this.insert(p, "REG_PLACE",  obj.getRegPlace());
            p = this.insert(p, "EMIRATES_ID",  obj.getEmiratesId());
            p = this.insert(p, "RECORD_ID",  obj.getRecordId());
            p = this.insert(p, "TYPE_PAY_ORD",  obj.getTypePayOrd());
            p = this.insert(p, "BNK_IDENTIFIER",  obj.getBnkIdentifier());
            p = this.insert(p, "FULL_NAME_AR",  obj.getFullNameAr());
            p = this.insert(p, "PRIMARY_MOBILE_NO",  obj.getPrimaryMobileNo());
            p = this.insert(p, "CIS_NO",  obj.getCisNo());
            p = this.insert(p, "RECORD_TYPE",  obj.getRecordType());
            p = this.insert(p, "FULL_NAME_EN",  obj.getFullNameEn());
            p = this.insert(p, "SUBJECT_TYPE",  obj.getSubjectType());
            p = this.insert(p, "EMIRATE",  obj.getEmirate());
            p = this.insert(p, "NATIONALITY",  obj.getNationality());
            p = this.insert(p, "DOB",  obj.getDob());
            p = this.insert(p, "IBAN",  obj.getIban());
            p = this.insert(p, "PASSPORT_NO",  obj.getPassportNo());
            p = this.insert(p, "REF_DATE", String.valueOf(obj.getRefDate()));
            p = this.insert(p, "TRADE_LIN_NO",  obj.getTradeLinNo());
            p = this.insert(p, "DESC_CODE",  obj.getDescCode());
            p = this.insert(p, "DATA_DATE", String.valueOf(obj.getDataDate()));
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_DDSCHQ"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_DDSCHQ]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_DDSCHQ]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_DdschqEvent obj = new FT_DdschqEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setChequeDdNo(rs.getString("CHEQUE_DD_NO"));
    obj.setTradeNameAr(rs.getString("TRADE_NAME_AR"));
    obj.setBeneName(rs.getString("BENE_NAME"));
    obj.setDor(rs.getString("DOR"));
    obj.setAmt(rs.getDouble("AMT"));
    obj.setTradeNameEn(rs.getString("TRADE_NAME_EN"));
    obj.setSourceName(rs.getString("SOURCE_NAME"));
    obj.setReasonCode(rs.getString("REASON_CODE"));
    obj.setRegPlace(rs.getString("REG_PLACE"));
    obj.setEmiratesId(rs.getString("EMIRATES_ID"));
    obj.setRecordId(rs.getString("RECORD_ID"));
    obj.setTypePayOrd(rs.getString("TYPE_PAY_ORD"));
    obj.setBnkIdentifier(rs.getString("BNK_IDENTIFIER"));
    obj.setFullNameAr(rs.getString("FULL_NAME_AR"));
    obj.setPrimaryMobileNo(rs.getString("PRIMARY_MOBILE_NO"));
    obj.setCisNo(rs.getString("CIS_NO"));
    obj.setRecordType(rs.getString("RECORD_TYPE"));
    obj.setFullNameEn(rs.getString("FULL_NAME_EN"));
    obj.setSubjectType(rs.getString("SUBJECT_TYPE"));
    obj.setEmirate(rs.getString("EMIRATE"));
    obj.setNationality(rs.getString("NATIONALITY"));
    obj.setDob(rs.getString("DOB"));
    obj.setIban(rs.getString("IBAN"));
    obj.setPassportNo(rs.getString("PASSPORT_NO"));
    obj.setRefDate(rs.getTimestamp("REF_DATE"));
    obj.setTradeLinNo(rs.getString("TRADE_LIN_NO"));
    obj.setDescCode(rs.getString("DESC_CODE"));
    obj.setDataDate(rs.getTimestamp("DATA_DATE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_DDSCHQ]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_DdschqEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_DdschqEvent> events;
 FT_DdschqEvent obj = new FT_DdschqEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_DDSCHQ"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_DdschqEvent obj = new FT_DdschqEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setChequeDdNo(getColumnValue(rs, "CHEQUE_DD_NO"));
            obj.setTradeNameAr(getColumnValue(rs, "TRADE_NAME_AR"));
            obj.setBeneName(getColumnValue(rs, "BENE_NAME"));
            obj.setDor(getColumnValue(rs, "DOR"));
            obj.setAmt(EventHelper.toDouble(getColumnValue(rs, "AMT")));
            obj.setTradeNameEn(getColumnValue(rs, "TRADE_NAME_EN"));
            obj.setSourceName(getColumnValue(rs, "SOURCE_NAME"));
            obj.setReasonCode(getColumnValue(rs, "REASON_CODE"));
            obj.setRegPlace(getColumnValue(rs, "REG_PLACE"));
            obj.setEmiratesId(getColumnValue(rs, "EMIRATES_ID"));
            obj.setRecordId(getColumnValue(rs, "RECORD_ID"));
            obj.setTypePayOrd(getColumnValue(rs, "TYPE_PAY_ORD"));
            obj.setBnkIdentifier(getColumnValue(rs, "BNK_IDENTIFIER"));
            obj.setFullNameAr(getColumnValue(rs, "FULL_NAME_AR"));
            obj.setPrimaryMobileNo(getColumnValue(rs, "PRIMARY_MOBILE_NO"));
            obj.setCisNo(getColumnValue(rs, "CIS_NO"));
            obj.setRecordType(getColumnValue(rs, "RECORD_TYPE"));
            obj.setFullNameEn(getColumnValue(rs, "FULL_NAME_EN"));
            obj.setSubjectType(getColumnValue(rs, "SUBJECT_TYPE"));
            obj.setEmirate(getColumnValue(rs, "EMIRATE"));
            obj.setNationality(getColumnValue(rs, "NATIONALITY"));
            obj.setDob(getColumnValue(rs, "DOB"));
            obj.setIban(getColumnValue(rs, "IBAN"));
            obj.setPassportNo(getColumnValue(rs, "PASSPORT_NO"));
            obj.setRefDate(EventHelper.toTimestamp(getColumnValue(rs, "REF_DATE")));
            obj.setTradeLinNo(getColumnValue(rs, "TRADE_LIN_NO"));
            obj.setDescCode(getColumnValue(rs, "DESC_CODE"));
            obj.setDataDate(EventHelper.toTimestamp(getColumnValue(rs, "DATA_DATE")));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_DDSCHQ]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_DDSCHQ]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"CHEQUE_DD_NO\",\"TRADE_NAME_AR\",\"BENE_NAME\",\"DOR\",\"AMT\",\"TRADE_NAME_EN\",\"SOURCE_NAME\",\"REASON_CODE\",\"REG_PLACE\",\"EMIRATES_ID\",\"RECORD_ID\",\"TYPE_PAY_ORD\",\"BNK_IDENTIFIER\",\"FULL_NAME_AR\",\"PRIMARY_MOBILE_NO\",\"CIS_NO\",\"RECORD_TYPE\",\"FULL_NAME_EN\",\"SUBJECT_TYPE\",\"EMIRATE\",\"NATIONALITY\",\"DOB\",\"IBAN\",\"PASSPORT_NO\",\"REF_DATE\",\"TRADE_LIN_NO\",\"DESC_CODE\",\"DATA_DATE\"" +
              " FROM EVENT_FT_DDSCHQ";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [CHEQUE_DD_NO],[TRADE_NAME_AR],[BENE_NAME],[DOR],[AMT],[TRADE_NAME_EN],[SOURCE_NAME],[REASON_CODE],[REG_PLACE],[EMIRATES_ID],[RECORD_ID],[TYPE_PAY_ORD],[BNK_IDENTIFIER],[FULL_NAME_AR],[PRIMARY_MOBILE_NO],[CIS_NO],[RECORD_TYPE],[FULL_NAME_EN],[SUBJECT_TYPE],[EMIRATE],[NATIONALITY],[DOB],[IBAN],[PASSPORT_NO],[REF_DATE],[TRADE_LIN_NO],[DESC_CODE],[DATA_DATE]" +
              " FROM EVENT_FT_DDSCHQ";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`CHEQUE_DD_NO`,`TRADE_NAME_AR`,`BENE_NAME`,`DOR`,`AMT`,`TRADE_NAME_EN`,`SOURCE_NAME`,`REASON_CODE`,`REG_PLACE`,`EMIRATES_ID`,`RECORD_ID`,`TYPE_PAY_ORD`,`BNK_IDENTIFIER`,`FULL_NAME_AR`,`PRIMARY_MOBILE_NO`,`CIS_NO`,`RECORD_TYPE`,`FULL_NAME_EN`,`SUBJECT_TYPE`,`EMIRATE`,`NATIONALITY`,`DOB`,`IBAN`,`PASSPORT_NO`,`REF_DATE`,`TRADE_LIN_NO`,`DESC_CODE`,`DATA_DATE`" +
              " FROM EVENT_FT_DDSCHQ";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_DDSCHQ (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"CHEQUE_DD_NO\",\"TRADE_NAME_AR\",\"BENE_NAME\",\"DOR\",\"AMT\",\"TRADE_NAME_EN\",\"SOURCE_NAME\",\"REASON_CODE\",\"REG_PLACE\",\"EMIRATES_ID\",\"RECORD_ID\",\"TYPE_PAY_ORD\",\"BNK_IDENTIFIER\",\"FULL_NAME_AR\",\"PRIMARY_MOBILE_NO\",\"CIS_NO\",\"RECORD_TYPE\",\"FULL_NAME_EN\",\"SUBJECT_TYPE\",\"EMIRATE\",\"NATIONALITY\",\"DOB\",\"IBAN\",\"PASSPORT_NO\",\"REF_DATE\",\"TRADE_LIN_NO\",\"DESC_CODE\",\"DATA_DATE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[CHEQUE_DD_NO],[TRADE_NAME_AR],[BENE_NAME],[DOR],[AMT],[TRADE_NAME_EN],[SOURCE_NAME],[REASON_CODE],[REG_PLACE],[EMIRATES_ID],[RECORD_ID],[TYPE_PAY_ORD],[BNK_IDENTIFIER],[FULL_NAME_AR],[PRIMARY_MOBILE_NO],[CIS_NO],[RECORD_TYPE],[FULL_NAME_EN],[SUBJECT_TYPE],[EMIRATE],[NATIONALITY],[DOB],[IBAN],[PASSPORT_NO],[REF_DATE],[TRADE_LIN_NO],[DESC_CODE],[DATA_DATE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`CHEQUE_DD_NO`,`TRADE_NAME_AR`,`BENE_NAME`,`DOR`,`AMT`,`TRADE_NAME_EN`,`SOURCE_NAME`,`REASON_CODE`,`REG_PLACE`,`EMIRATES_ID`,`RECORD_ID`,`TYPE_PAY_ORD`,`BNK_IDENTIFIER`,`FULL_NAME_AR`,`PRIMARY_MOBILE_NO`,`CIS_NO`,`RECORD_TYPE`,`FULL_NAME_EN`,`SUBJECT_TYPE`,`EMIRATE`,`NATIONALITY`,`DOB`,`IBAN`,`PASSPORT_NO`,`REF_DATE`,`TRADE_LIN_NO`,`DESC_CODE`,`DATA_DATE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

