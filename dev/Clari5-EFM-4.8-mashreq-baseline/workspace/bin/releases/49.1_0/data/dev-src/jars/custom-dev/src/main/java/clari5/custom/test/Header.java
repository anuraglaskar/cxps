package clari5.custom.test;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Header")
public class Header{
    String SrcAppId;
    int SrcMsgId;
    String SrvCode;
    String OrgId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    String UserId;


    public String getSrcAppId() {
        return SrcAppId;
    }

    public void setSrcAppId(String srcAppId) {
        SrcAppId = srcAppId;
    }

    public int getSrcMsgId() {
        return SrcMsgId;
    }

    public void setSrcMsgId(int srcMsgId) {
        SrcMsgId = srcMsgId;
    }

    public String getSrvCode() {
        return SrvCode;
    }

    public void setSrvCode(String srvCode) {
        SrvCode = srvCode;
    }

    public String getOrgId() {
        return OrgId;
    }

    public void setOrgId(String orgId) {
        OrgId = orgId;
    }


}