cxps.noesis.glossary.entity.bidrate {
       db-name = BIDRATE
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
                { name = ccy1, column = ccy1, type = "string:50"}
		{ name = ccy2, column = ccy2, type = "string:50"}
		{ name = date, column = date, type = timestamp}
		{ name = quotient, column = quotient, type = "string:50"}
		{ name = quotation_method, column = quotation_method, type = "string:50"}
		{ name = buy_rate, column = buy_rate, type = "number:20,4", key=true }
		{ name = sell_rate, column = sell_rate, type = "number:20,4"}
               ]
       }
