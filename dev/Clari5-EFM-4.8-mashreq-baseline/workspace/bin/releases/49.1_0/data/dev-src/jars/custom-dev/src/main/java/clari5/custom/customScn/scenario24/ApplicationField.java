package clari5.custom.customScn.scenario24;

import java.util.Date;

public class ApplicationField {
    private  String newapprefno;
    private  String newproduct;
    private  String newcif;
    private  String neweid;
    private  String newpassport;
    private  String newemail;
    private  String newmobile;
    private  Date    newapplopendate;
    private  String newcustname;
    private  String newdeviceid;
    private  String newipaddress;
    private  String newtradelincense;
    private  String oldapprefno;
    private  String oldproduct;
    private  String oldcif;
    private  String oldeid;
    private  String oldpassport;
    private  String oldemail;
    private  String oldmobile;
    private  Date oldapplopendate;
    private  String oldcustname;
    private  String olddeviceid;
    private  String oldipaddress;
    private  String oldtradelincense;
    private  String matchedeid;
    private  String matchedtradelicense;
    private  String matchedemail;
    private  String matchedmobile;
    private  String matchedScoreeid;
    private  String newChannel;
    private  String oldChannel;

    public String getNewChannel() {
        return newChannel;
    }

    public void setNewChannel(String newChannel) {
        this.newChannel = newChannel;
    }

    public String getOldChannel() {
        return oldChannel;
    }

    public void setOldChannel(String oldChannel) {
        this.oldChannel = oldChannel;
    }


    public Date getNewapplopendate() {
        return newapplopendate;
    }

    public void setNewapplopendate(Date newapplopendate) {
        this.newapplopendate = newapplopendate;
    }

    public Date getOldapplopendate() {
        return oldapplopendate;
    }

    public void setOldapplopendate(Date oldapplopendate) {
        this.oldapplopendate = oldapplopendate;
    }

    public String getNewtradelincense() {
        return newtradelincense;
    }

    public void setNewtradelincense(String newtradelincense) {
        this.newtradelincense = newtradelincense;
    }

    public String getOldtradelincense() {
        return oldtradelincense;
    }

    public void setOldtradelincense(String oldtradelincense) {
        this.oldtradelincense = oldtradelincense;
    }

    public String getNewapprefno() {
        return newapprefno;
    }

    public void setNewapprefno(String newapprefno) {
        this.newapprefno = newapprefno;
    }

    public String getNewproduct() {
        return newproduct;
    }

    public void setNewproduct(String newproduct) {
        this.newproduct = newproduct;
    }

    public String getNewcif() {
        return newcif;
    }

    public void setNewcif(String newcif) {
        this.newcif = newcif;
    }

    public String getNeweid() {
        return neweid;
    }

    public void setNeweid(String neweid) {
        this.neweid = neweid;
    }

    public String getNewpassport() {
        return newpassport;
    }

    public void setNewpassport(String newpassport) {
        this.newpassport = newpassport;
    }

    public String getNewemail() {
        return newemail;
    }

    public void setNewemail(String newemail) {
        this.newemail = newemail;
    }

    public String getNewmobile() {
        return newmobile;
    }

    public void setNewmobile(String newmobile) {
        this.newmobile = newmobile;
    }

    public String getNewcustname() {
        return newcustname;
    }

    public void setNewcustname(String newcustname) {
        this.newcustname = newcustname;
    }

    public String getNewdeviceid() {
        return newdeviceid;
    }

    public void setNewdeviceid(String newdeviceid) {
        this.newdeviceid = newdeviceid;
    }

    public String getNewipaddress() {
        return newipaddress;
    }

    public void setNewipaddress(String newipaddress) {
        this.newipaddress = newipaddress;
    }

    public String getOldapprefno() {
        return oldapprefno;
    }

    public void setOldapprefno(String oldapprefno) {
        this.oldapprefno = oldapprefno;
    }

    public String getOldproduct() {
        return oldproduct;
    }

    public void setOldproduct(String oldproduct) {
        this.oldproduct = oldproduct;
    }

    public String getOldcif() {
        return oldcif;
    }

    public void setOldcif(String oldcif) {
        this.oldcif = oldcif;
    }

    public String getOldeid() {
        return oldeid;
    }

    public void setOldeid(String oldeid) {
        this.oldeid = oldeid;
    }

    public String getOldpassport() {
        return oldpassport;
    }

    public void setOldpassport(String oldpassport) {
        this.oldpassport = oldpassport;
    }

    public String getOldemail() {
        return oldemail;
    }

    public void setOldemail(String oldemail) {
        this.oldemail = oldemail;
    }

    public String getOldmobile() {
        return oldmobile;
    }

    public void setOldmobile(String oldmobile) {
        this.oldmobile = oldmobile;
    }

    public String getOldcustname() {
        return oldcustname;
    }

    public void setOldcustname(String oldcustname) {
        this.oldcustname = oldcustname;
    }

    public String getOlddeviceid() {
        return olddeviceid;
    }

    public void setOlddeviceid(String olddeviceid) {
        this.olddeviceid = olddeviceid;
    }

    public String getOldipaddress() {
        return oldipaddress;
    }

    public void setOldipaddress(String oldipaddress) {
        this.oldipaddress = oldipaddress;
    }

    public String getMatchedeid() {
        return matchedeid;
    }

    public void setMatchedeid(String matchedeid) {
        this.matchedeid = matchedeid;
    }

    public String getMatchedtradelicense() {
        return matchedtradelicense;
    }

    public void setMatchedtradelicense(String matchedtradelicense) {
        this.matchedtradelicense = matchedtradelicense;
    }

    public String getMatchedemail() {
        return matchedemail;
    }

    public void setMatchedemail(String matchedemail) {
        this.matchedemail = matchedemail;
    }

    public String getMatchedmobile() {
        return matchedmobile;
    }

    public void setMatchedmobile(String matchedmobile) {
        this.matchedmobile = matchedmobile;
    }
    public String getMatchedScoreeid() {
        return matchedScoreeid;
    }

    public void setMatchedScoreeid(String matchedScoreeid) {
        this.matchedScoreeid = matchedScoreeid;
    }
}