package clari5.custom.customScn.scenario24;

import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AppEodReminder extends TimerTask {
    private static CxpsLogger logger=CxpsLogger.getLogger(AppEodReminder.class);
    private static ThreadPojo threadPojo=new ThreadPojo();

    public static ThreadPojo getThreadPojo() {
        return threadPojo;
    }

    public static void setThreadPojo(ThreadPojo threadPojo) {
        AppEodReminder.threadPojo = threadPojo;
    }

    public  void run(){
        try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();) {
                logger.info("[AppEodReminder] Thread has been scheduled" + threadPojo.getCurrentThreadName());
                    //AppMqWriter appMqWriter = new AppMqWriter();
                    //appMqWriter.writeJson();
                    AppMqWriter.writeJson();
                    logger.info("[AppEodReminder] Writing JSON has been completed!! Hence updating status for thread" + threadPojo.getCurrentThreadName());
                    PreparedStatement preparedStatement = connection.prepareStatement(threadPojo.getCurrentUpdateQuery());
                    preparedStatement.setString(1, "C");
                    preparedStatement.setTimestamp(2,new java.sql.Timestamp(new java.util.Date().getTime()));
                    preparedStatement.setString(3, threadPojo.getCurrentThreadName());
                    preparedStatement.setDate(4, threadPojo.getThreadDate());
                    preparedStatement.executeUpdate();
                    connection.commit();
                    threadPojo=null;
            }catch (Exception e){
                e.printStackTrace();
            }
    }
    public static void insert(String threadName, long threadId, java.sql.Date date,String insertQuery){
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();
            PreparedStatement insertQueryStatement=connection.prepareStatement(insertQuery);){
            insertQueryStatement.setString(1,threadName);// thread name
            insertQueryStatement.setLong(2,threadId); // id
            insertQueryStatement.setDate(3,date);
            insertQueryStatement.setString(4,"NEW");
            insertQueryStatement.setString(5,threadName+"-"+threadId+"-"+date);
            insertQueryStatement.setTimestamp(6,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.setTimestamp(7,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.executeUpdate();
            connection.commit();
            logger.info("[AppEodReminder] Insertion Completed");
        } catch (SQLException e) {
            logger.error("[AppEodReminder] insert()-> insertion Failed"+ e.getCause() + " "+e.getMessage());
        }
    }
    public static void callonEOD(int hrsToExecute,int minToExecute,int secToExecute,String threadName,long threadId,java.sql.Date newDate,String insertQuery,String updateQuery,String selectQuery){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH,0);
            calendar.add(Calendar.YEAR,0);
            // comment this line
            //calendar.set(Calendar.DATE,26);
            calendar.set(Calendar.HOUR_OF_DAY,hrsToExecute);
            calendar.set(Calendar.MINUTE,minToExecute);
            calendar.set(Calendar.SECOND,secToExecute);
            Date date= calendar.getTime();
            threadPojo.setCurrentThreadName(threadName);
            threadPojo.setThrdId(threadId);
            threadPojo.setThreadDate(newDate);
            threadPojo.setInsrtQry(insertQuery);
            threadPojo.setCurrentUpdateQuery(updateQuery);
            threadPojo.setSelectQry(selectQuery);
            Timer timer = new Timer();
            AppEodReminder.setThreadPojo(threadPojo);
            // added a condition for running at the same time
            while (true){
                Date currentDate=new Date();
                if(date.compareTo(currentDate)>0){
                    timer.schedule(new AppEodReminder(), date);
                    logger.info("[AppEodReminder] Call on EOD..Time has been scheduled"+date);
                    int timetosleeep = (24 * 60 * 60 * 1000);
                    Thread.sleep(timetosleeep);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
