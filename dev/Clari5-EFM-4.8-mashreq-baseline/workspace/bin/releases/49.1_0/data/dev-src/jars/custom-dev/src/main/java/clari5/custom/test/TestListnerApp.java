package clari5.custom.test;


import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class TestListnerApp extends Application {
    private Set<Object> singletons = new HashSet<>();

    public TestListnerApp(){

        singletons.add(new TestListner());
    }

//    @Override
//    public Set<Class<?>> getClasses() {
//        return null;
//    }


    @Override
    public Set<Object> getSingletons() {
        return this.singletons;
    }
}