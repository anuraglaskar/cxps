package clari5.custom.customScn11;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.type.TIMESTAMP;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonObject;
import cxps.events.NFT_AcctStatusChangeEvent;
import cxps.events.NFT_AcctStatusChangeEventMapper;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/****
 * @author Suryakant
 * @since 17/06/2020
 *
 */

public class DataFetchCreateEvent extends Thread {

    private static final CxpsLogger logger = CxpsLogger.getLogger(DataFetchCreateEvent.class);
    static ConcurrentLinkedQueue<JSONObject> queue = new ConcurrentLinkedQueue<>();
    static Hocon hocon;
    static String dataFetchFromUserMapTable;
    static String insertDataInAuditTable;
    static int dDays;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("snew11.conf");
        dataFetchFromUserMapTable = hocon.getString("selectQueryFromUserMappingTable");
        insertDataInAuditTable = hocon.getString("customScnDetailsTracker");
        dDays = hocon.getInt("limitDaysForPushUp1");

    }


    public void run() {

        logger.info("Inside the process data for event creation");
        ConcurrentLinkedQueue<JSONObject> DataQueue = queue;

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(dataFetchFromUserMapTable)) {

                    while (!DataQueue.isEmpty()) {

                        JSONObject jsonObject = new JSONObject();
                        jsonObject = DataQueue.poll();

                        String accountId = jsonObject.getString("accountId");
                        NFT_AcctStatusChangeEvent event = (NFT_AcctStatusChangeEvent) jsonObject.get("eventJson");
                        /**
                         * Adding Hashmap for distinct userId
                         */
                        HashSet distinctUserId = new HashSet();
                        statement.setString(1, accountId);
                        Calendar cal = Calendar.getInstance();
                        Date cur_Date = new Date();
                        cal.setTime(cur_Date);
                        cal.add(Calendar.DATE, -dDays);
                        java.util.Date daysCheck = cal.getTime();
                        String initialAccountStatus = event.intialAcctStatus;

                        try (ResultSet resultSet = statement.executeQuery()) {

                            while (resultSet.next()) {

                                String userId = resultSet.getString("UserID");
                                String customerId = resultSet.getString("CUST_ID");
                                java.sql.Timestamp rcreTime = resultSet.getTimestamp("RCRE_TIME");
                                Date rcreDate = new Date(rcreTime.getTime());


                                /****
                                 * Checking the Account enquiry happened with in 15 days
                                 */

                                if (rcreDate.after(daysCheck) && rcreDate.before(cur_Date) && !distinctUserId.contains(userId) ) {
                                    /**
                                     * Method using to insert the custom table to track
                                     */
                                    dataInsertInAuditTable(connection, accountId, customerId, userId, initialAccountStatus);
                                    distinctUserId.add(userId);
                                    
                                }

                            }

                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dataInsertInAuditTable(CxConnection connection, String acctId, String custId, String userId, String initialAccountStatus ) {

        logger.info("Method is to insert the data in SCN11_DETAILS_TRACK table");

        try (PreparedStatement statement = connection.prepareStatement(insertDataInAuditTable)) {

            statement.setString(1, acctId);
            statement.setString(2, custId);
            statement.setString(3, userId);
            statement.setString(4, "NEW");
            statement.setString(5,initialAccountStatus);
            statement.setString(6,"NA");

            statement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }


    public static void generateEventForDistinctUser(String accountId, NFT_AcctStatusChangeEvent event) {

        /*****
         * This method is to create to add the event and accountId
         * and Daemon will pick the data
         *
         */

        logger.info("Inside the method which will return queue");

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("eventJson", event);
            jsonObject.put("accountId", accountId);
            queue.add(jsonObject);

            DataFetchCreateEvent dataFetchCreateEvent = new DataFetchCreateEvent();
            dataFetchCreateEvent.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
