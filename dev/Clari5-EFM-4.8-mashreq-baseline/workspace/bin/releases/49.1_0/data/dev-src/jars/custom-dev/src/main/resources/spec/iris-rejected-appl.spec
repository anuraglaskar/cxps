cxps.noesis.glossary.entity.iris-rejected-appl {
       db-name = IRIS_REJECTED_APPL
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = application-id, column = application_id, type = "string:50", key=true }
	       { name = application_date, column = applicationDate, type = date }
	       { name = cif, column = CIF, type = "string:50"}
 	       { name = mobile_number, column = MobileNumber, type = "string:50" }
	       { name = passport_number, column = PassportNumber, type = "string:50"}
 	       { name = emirates_id, column = EmiratesID, type = "string:50" }
     	       { name = date_Of_birth, column = DateOfBirth, type = date}
	       { name = product_code, column = productCode, type = "string:50" }
	       { name = decision_details, column = DecisionDetails, type = "string:50" }
   	       { name = decline_reason, column = DeclineReason, type = "string:50" }
 	       { name = rejection_date, column = RejectionDate, type = date}
	       { name = Residence_Phone_Number, column = Residence_Phone_Number, type = "string:50"}
 	       { name = Home_Country_Phone_Number, column = Home_Country_Phone_Number, type = "string:50" }
	       { name = Office_Phone_Number, column = Office_Phone_Number, type = "string:50"}
 	       { name = Communication_Phone_Number, column = Communication_Phone_Number, type = "string:50" }

               ]
       }
