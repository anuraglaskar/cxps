// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_RemittanceEventMapper extends EventMapper<FT_RemittanceEvent> {

public FT_RemittanceEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_RemittanceEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_RemittanceEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_RemittanceEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_RemittanceEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_RemittanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_RemittanceEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getRemAdd1());
            preparedStatement.setString(i++, obj.getCif());
            preparedStatement.setString(i++, obj.getRemType());
            preparedStatement.setString(i++, obj.getRemAdd3());
            preparedStatement.setString(i++, obj.getRemAdd2());
            preparedStatement.setTimestamp(i++, obj.getTrnDate());
            preparedStatement.setString(i++, obj.getBenCity());
            preparedStatement.setString(i++, obj.getRemBank());
            preparedStatement.setString(i++, obj.getBranch());
            preparedStatement.setString(i++, obj.getBenCntryCode());
            preparedStatement.setString(i++, obj.getBenAdd3());
            preparedStatement.setString(i++, obj.getBenAdd2());
            preparedStatement.setString(i++, obj.getRemAcctNo());
            preparedStatement.setString(i++, obj.getRemName());
            preparedStatement.setString(i++, obj.getRemIdNo());
            preparedStatement.setString(i++, obj.getBenAdd1());
            preparedStatement.setString(i++, obj.getBenBic());
            preparedStatement.setString(i++, obj.getTranRefNo());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getBenfBank());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getBenName());
            preparedStatement.setString(i++, obj.getRemInformation1());
            preparedStatement.setString(i++, obj.getRemInformation2());
            preparedStatement.setString(i++, obj.getBenAcctNo());
            preparedStatement.setString(i++, obj.getRemInformation3());
            preparedStatement.setString(i++, obj.getRemInformation4());
            preparedStatement.setDouble(i++, obj.getLcyAmount());
            preparedStatement.setString(i++, obj.getRemCntryCode());
            preparedStatement.setString(i++, obj.getTranCurr());
            preparedStatement.setString(i++, obj.getBenfIdNo());
            preparedStatement.setString(i++, obj.getRemCity());
            preparedStatement.setString(i++, obj.getRemBic());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_REMITTANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_RemittanceEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_RemittanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_REMITTANCE"));
        putList = new ArrayList<>();

        for (FT_RemittanceEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "REM_ADD1",  obj.getRemAdd1());
            p = this.insert(p, "CIF",  obj.getCif());
            p = this.insert(p, "REM_TYPE",  obj.getRemType());
            p = this.insert(p, "REM_ADD3",  obj.getRemAdd3());
            p = this.insert(p, "REM_ADD2",  obj.getRemAdd2());
            p = this.insert(p, "TRN_DATE", String.valueOf(obj.getTrnDate()));
            p = this.insert(p, "BEN_CITY",  obj.getBenCity());
            p = this.insert(p, "REM_BANK",  obj.getRemBank());
            p = this.insert(p, "BRANCH",  obj.getBranch());
            p = this.insert(p, "BEN_CNTRY_CODE",  obj.getBenCntryCode());
            p = this.insert(p, "BEN_ADD3",  obj.getBenAdd3());
            p = this.insert(p, "BEN_ADD2",  obj.getBenAdd2());
            p = this.insert(p, "REM_ACCT_NO",  obj.getRemAcctNo());
            p = this.insert(p, "REM_NAME",  obj.getRemName());
            p = this.insert(p, "REM_ID_NO",  obj.getRemIdNo());
            p = this.insert(p, "BEN_ADD1",  obj.getBenAdd1());
            p = this.insert(p, "BEN_BIC",  obj.getBenBic());
            p = this.insert(p, "TRAN_REF_NO",  obj.getTranRefNo());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "BENF_BANK",  obj.getBenfBank());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "BEN_NAME",  obj.getBenName());
            p = this.insert(p, "REM_INFORMATION1",  obj.getRemInformation1());
            p = this.insert(p, "REM_INFORMATION2",  obj.getRemInformation2());
            p = this.insert(p, "BEN_ACCT_NO",  obj.getBenAcctNo());
            p = this.insert(p, "REM_INFORMATION3",  obj.getRemInformation3());
            p = this.insert(p, "REM_INFORMATION4",  obj.getRemInformation4());
            p = this.insert(p, "LCY_AMOUNT", String.valueOf(obj.getLcyAmount()));
            p = this.insert(p, "REM_CNTRY_CODE",  obj.getRemCntryCode());
            p = this.insert(p, "TRAN_CURR",  obj.getTranCurr());
            p = this.insert(p, "BENF_ID_NO",  obj.getBenfIdNo());
            p = this.insert(p, "REM_CITY",  obj.getRemCity());
            p = this.insert(p, "REM_BIC",  obj.getRemBic());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_REMITTANCE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_REMITTANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_REMITTANCE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_RemittanceEvent obj = new FT_RemittanceEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setRemAdd1(rs.getString("REM_ADD1"));
    obj.setCif(rs.getString("CIF"));
    obj.setRemType(rs.getString("REM_TYPE"));
    obj.setRemAdd3(rs.getString("REM_ADD3"));
    obj.setRemAdd2(rs.getString("REM_ADD2"));
    obj.setTrnDate(rs.getTimestamp("TRN_DATE"));
    obj.setBenCity(rs.getString("BEN_CITY"));
    obj.setRemBank(rs.getString("REM_BANK"));
    obj.setBranch(rs.getString("BRANCH"));
    obj.setBenCntryCode(rs.getString("BEN_CNTRY_CODE"));
    obj.setBenAdd3(rs.getString("BEN_ADD3"));
    obj.setBenAdd2(rs.getString("BEN_ADD2"));
    obj.setRemAcctNo(rs.getString("REM_ACCT_NO"));
    obj.setRemName(rs.getString("REM_NAME"));
    obj.setRemIdNo(rs.getString("REM_ID_NO"));
    obj.setBenAdd1(rs.getString("BEN_ADD1"));
    obj.setBenBic(rs.getString("BEN_BIC"));
    obj.setTranRefNo(rs.getString("TRAN_REF_NO"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setBenfBank(rs.getString("BENF_BANK"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setBenName(rs.getString("BEN_NAME"));
    obj.setRemInformation1(rs.getString("REM_INFORMATION1"));
    obj.setRemInformation2(rs.getString("REM_INFORMATION2"));
    obj.setBenAcctNo(rs.getString("BEN_ACCT_NO"));
    obj.setRemInformation3(rs.getString("REM_INFORMATION3"));
    obj.setRemInformation4(rs.getString("REM_INFORMATION4"));
    obj.setLcyAmount(rs.getDouble("LCY_AMOUNT"));
    obj.setRemCntryCode(rs.getString("REM_CNTRY_CODE"));
    obj.setTranCurr(rs.getString("TRAN_CURR"));
    obj.setBenfIdNo(rs.getString("BENF_ID_NO"));
    obj.setRemCity(rs.getString("REM_CITY"));
    obj.setRemBic(rs.getString("REM_BIC"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_REMITTANCE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_RemittanceEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_RemittanceEvent> events;
 FT_RemittanceEvent obj = new FT_RemittanceEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_REMITTANCE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_RemittanceEvent obj = new FT_RemittanceEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setRemAdd1(getColumnValue(rs, "REM_ADD1"));
            obj.setCif(getColumnValue(rs, "CIF"));
            obj.setRemType(getColumnValue(rs, "REM_TYPE"));
            obj.setRemAdd3(getColumnValue(rs, "REM_ADD3"));
            obj.setRemAdd2(getColumnValue(rs, "REM_ADD2"));
            obj.setTrnDate(EventHelper.toTimestamp(getColumnValue(rs, "TRN_DATE")));
            obj.setBenCity(getColumnValue(rs, "BEN_CITY"));
            obj.setRemBank(getColumnValue(rs, "REM_BANK"));
            obj.setBranch(getColumnValue(rs, "BRANCH"));
            obj.setBenCntryCode(getColumnValue(rs, "BEN_CNTRY_CODE"));
            obj.setBenAdd3(getColumnValue(rs, "BEN_ADD3"));
            obj.setBenAdd2(getColumnValue(rs, "BEN_ADD2"));
            obj.setRemAcctNo(getColumnValue(rs, "REM_ACCT_NO"));
            obj.setRemName(getColumnValue(rs, "REM_NAME"));
            obj.setRemIdNo(getColumnValue(rs, "REM_ID_NO"));
            obj.setBenAdd1(getColumnValue(rs, "BEN_ADD1"));
            obj.setBenBic(getColumnValue(rs, "BEN_BIC"));
            obj.setTranRefNo(getColumnValue(rs, "TRAN_REF_NO"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setBenfBank(getColumnValue(rs, "BENF_BANK"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setBenName(getColumnValue(rs, "BEN_NAME"));
            obj.setRemInformation1(getColumnValue(rs, "REM_INFORMATION1"));
            obj.setRemInformation2(getColumnValue(rs, "REM_INFORMATION2"));
            obj.setBenAcctNo(getColumnValue(rs, "BEN_ACCT_NO"));
            obj.setRemInformation3(getColumnValue(rs, "REM_INFORMATION3"));
            obj.setRemInformation4(getColumnValue(rs, "REM_INFORMATION4"));
            obj.setLcyAmount(EventHelper.toDouble(getColumnValue(rs, "LCY_AMOUNT")));
            obj.setRemCntryCode(getColumnValue(rs, "REM_CNTRY_CODE"));
            obj.setTranCurr(getColumnValue(rs, "TRAN_CURR"));
            obj.setBenfIdNo(getColumnValue(rs, "BENF_ID_NO"));
            obj.setRemCity(getColumnValue(rs, "REM_CITY"));
            obj.setRemBic(getColumnValue(rs, "REM_BIC"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_REMITTANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_REMITTANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"REM_ADD1\",\"CIF\",\"REM_TYPE\",\"REM_ADD3\",\"REM_ADD2\",\"TRN_DATE\",\"BEN_CITY\",\"REM_BANK\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"BEN_ADD3\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"REM_NAME\",\"REM_ID_NO\",\"BEN_ADD1\",\"BEN_BIC\",\"TRAN_REF_NO\",\"TRAN_AMT\",\"BENF_BANK\",\"ACCOUNT_ID\",\"BEN_NAME\",\"REM_INFORMATION1\",\"REM_INFORMATION2\",\"BEN_ACCT_NO\",\"REM_INFORMATION3\",\"REM_INFORMATION4\",\"LCY_AMOUNT\",\"REM_CNTRY_CODE\",\"TRAN_CURR\",\"BENF_ID_NO\",\"REM_CITY\",\"REM_BIC\"" +
              " FROM EVENT_FT_REMITTANCE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [REM_ADD1],[CIF],[REM_TYPE],[REM_ADD3],[REM_ADD2],[TRN_DATE],[BEN_CITY],[REM_BANK],[BRANCH],[BEN_CNTRY_CODE],[BEN_ADD3],[BEN_ADD2],[REM_ACCT_NO],[REM_NAME],[REM_ID_NO],[BEN_ADD1],[BEN_BIC],[TRAN_REF_NO],[TRAN_AMT],[BENF_BANK],[ACCOUNT_ID],[BEN_NAME],[REM_INFORMATION1],[REM_INFORMATION2],[BEN_ACCT_NO],[REM_INFORMATION3],[REM_INFORMATION4],[LCY_AMOUNT],[REM_CNTRY_CODE],[TRAN_CURR],[BENF_ID_NO],[REM_CITY],[REM_BIC]" +
              " FROM EVENT_FT_REMITTANCE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`REM_ADD1`,`CIF`,`REM_TYPE`,`REM_ADD3`,`REM_ADD2`,`TRN_DATE`,`BEN_CITY`,`REM_BANK`,`BRANCH`,`BEN_CNTRY_CODE`,`BEN_ADD3`,`BEN_ADD2`,`REM_ACCT_NO`,`REM_NAME`,`REM_ID_NO`,`BEN_ADD1`,`BEN_BIC`,`TRAN_REF_NO`,`TRAN_AMT`,`BENF_BANK`,`ACCOUNT_ID`,`BEN_NAME`,`REM_INFORMATION1`,`REM_INFORMATION2`,`BEN_ACCT_NO`,`REM_INFORMATION3`,`REM_INFORMATION4`,`LCY_AMOUNT`,`REM_CNTRY_CODE`,`TRAN_CURR`,`BENF_ID_NO`,`REM_CITY`,`REM_BIC`" +
              " FROM EVENT_FT_REMITTANCE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_REMITTANCE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"REM_ADD1\",\"CIF\",\"REM_TYPE\",\"REM_ADD3\",\"REM_ADD2\",\"TRN_DATE\",\"BEN_CITY\",\"REM_BANK\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"BEN_ADD3\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"REM_NAME\",\"REM_ID_NO\",\"BEN_ADD1\",\"BEN_BIC\",\"TRAN_REF_NO\",\"TRAN_AMT\",\"BENF_BANK\",\"ACCOUNT_ID\",\"BEN_NAME\",\"REM_INFORMATION1\",\"REM_INFORMATION2\",\"BEN_ACCT_NO\",\"REM_INFORMATION3\",\"REM_INFORMATION4\",\"LCY_AMOUNT\",\"REM_CNTRY_CODE\",\"TRAN_CURR\",\"BENF_ID_NO\",\"REM_CITY\",\"REM_BIC\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[REM_ADD1],[CIF],[REM_TYPE],[REM_ADD3],[REM_ADD2],[TRN_DATE],[BEN_CITY],[REM_BANK],[BRANCH],[BEN_CNTRY_CODE],[BEN_ADD3],[BEN_ADD2],[REM_ACCT_NO],[REM_NAME],[REM_ID_NO],[BEN_ADD1],[BEN_BIC],[TRAN_REF_NO],[TRAN_AMT],[BENF_BANK],[ACCOUNT_ID],[BEN_NAME],[REM_INFORMATION1],[REM_INFORMATION2],[BEN_ACCT_NO],[REM_INFORMATION3],[REM_INFORMATION4],[LCY_AMOUNT],[REM_CNTRY_CODE],[TRAN_CURR],[BENF_ID_NO],[REM_CITY],[REM_BIC]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`REM_ADD1`,`CIF`,`REM_TYPE`,`REM_ADD3`,`REM_ADD2`,`TRN_DATE`,`BEN_CITY`,`REM_BANK`,`BRANCH`,`BEN_CNTRY_CODE`,`BEN_ADD3`,`BEN_ADD2`,`REM_ACCT_NO`,`REM_NAME`,`REM_ID_NO`,`BEN_ADD1`,`BEN_BIC`,`TRAN_REF_NO`,`TRAN_AMT`,`BENF_BANK`,`ACCOUNT_ID`,`BEN_NAME`,`REM_INFORMATION1`,`REM_INFORMATION2`,`BEN_ACCT_NO`,`REM_INFORMATION3`,`REM_INFORMATION4`,`LCY_AMOUNT`,`REM_CNTRY_CODE`,`TRAN_CURR`,`BENF_ID_NO`,`REM_CITY`,`REM_BIC`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

