package clari5.custom.customScn.scenarioNew1;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class ApplMqwriter {
    public static CxpsLogger logger = CxpsLogger.getLogger(ApplMqwriter.class);
    private static ThreadData threadData=EodReminder.getThreadData();

    static {
        ECClient.configure(null);
    }

    public  static void writeJson() {
        logger.info("[ApplMqWriter]MqWriter called ");
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();){
            PreparedStatement preparedStatement=connection.prepareStatement(threadData.getSelectQry());
            preparedStatement.setString(1,threadData.getCurrentThreadName());
            preparedStatement.setDate(2,threadData.getThreadDate());
            ResultSet resultSet=preparedStatement.executeQuery();
            if(resultSet.next()){
                logger.info("[ApplMqWriter] Data already exists in Thread Audit Table");
            }else{
                logger.info("[ApplMqWriter] Thread Data Details"+threadData);
                EodReminder.insert(threadData.getCurrentThreadName(),threadData.getThrdId(),threadData.getThreadDate(),threadData.getInsrtQry());
                RejectedMatches rejectedMatches = new RejectedMatches();
                //rejectedMatches.getdata();
                rejectedMatches.getdata();
                //threadData=null;
            }
        }catch (Exception e){
            logger.info("[ApplMqWriter] Exception While Selecting Data from Thread Audit Table"+e.getMessage() + " "+e.getCause());
        }

    }


    public static boolean writeJosntoMq(ApplFields finaldata){
        JSONObject json = new JSONObject();
        JSONObject msg = new JSONObject();
        String entity_id = "applmatchedrejappl";
        String event_id=getRandomNum();
        json.put("EventType", "nft");
        json.put("EventSubType", "applmatchedrejappl");
        json.put("event_name", "nft_applmatchedrejappl");
        msg.put("sys_time", getSysTime());
        msg.put("host_id", "F");
        msg.put("event_id", event_id.replaceAll("-",""));

        //olddata
        msg.put("current_date", finaldata.getCurrentdate() != null ? finaldata.getCurrentdate() : "");
        msg.put("app_ref_no", finaldata.getApprefno() != null ? finaldata.getApprefno() : "");
        msg.put("product", finaldata.getProduct() != null ? finaldata.getProduct() : "");
        msg.put("eida", finaldata.getEida() != null ? finaldata.getEida() : "");
        msg.put("cust_id", finaldata.getCustid() != null ? finaldata.getCustid() : "");
        msg.put("ph_no", finaldata.getPhno() !=null ? finaldata.getPhno() : "");
        msg.put("email_id", finaldata.getEmailid() != null ? finaldata.getEmailid() : "");
        msg.put("channel", finaldata.getChannel() != null ? finaldata.getChannel() : "");
        msg.put("dob", finaldata.getDob() != null ? finaldata.getDob() : "");
        msg.put("passport_no", finaldata.getPassportno() != null ? finaldata.getPassportno() : "");
        msg.put("driving_lnc", finaldata.getDrivinglnc() != null ? finaldata.getDrivinglnc() : "");
        msg.put("trade_license", finaldata.getTradelicense() != null ? finaldata.getTradelicense() : "");
        msg.put("vat", finaldata.getVat() != null ? finaldata.getVat() : "");
        msg.put("is_employer_nonsfa", finaldata.getIsemployernonsfa() != null ? finaldata.getIsemployernonsfa() : "");
        msg.put("employee_id", finaldata.getEmployeeid() != null ? finaldata.getEmployeeid() : "");
        msg.put("account_id", finaldata.getAccountid() !=null ? finaldata.getAccountid() : "");
        msg.put("appl_date", finaldata.getAppldate() !=null ? finaldata.getAppldate() : "");
        msg.put("iban_no", finaldata.getIbanno() != null ? finaldata.getIbanno() : "");
        msg.put("appl_name", finaldata.getApplname() != null ? finaldata.getApplname() : "");
        msg.put("branch_id", finaldata.getBranchid() !=null ? finaldata.getBranchid() : "");
        msg.put("employer_id", finaldata.getEmployerid() !=null ? finaldata.getEmployerid() : "");
        //matcheddata
        msg.put("matched_mobileno", finaldata.getMatchedmobileno() != null ? finaldata.getMatchedmobileno() : "");
        msg.put("matched_passportno", finaldata.getMatchedpassportno() != null ? finaldata.getMatchedpassportno() : "");
        msg.put("matched_eid", finaldata.getMatchedeid() != null ? finaldata.getMatchedeid() : "");
        msg.put("matched_dateofbirth", finaldata.getMatcheddateofbirth() != null ? finaldata.getMatcheddateofbirth() : "");
        //rejtabledata
        msg.put("rej_appl_id", finaldata.getRejapplid() != null ? finaldata.getRejapplid() : "");
        msg.put("rej_appl_date", finaldata.getRejappldate() != null ? finaldata.getRejappldate() : "");
        msg.put("rej_cif", finaldata.getRejcif() != null ? finaldata.getRejcif() : "");
        msg.put("rej_mobileno", finaldata.getRejmobileno() != null ? finaldata.getRejmobileno() : "");
        msg.put("rej_passportno", finaldata.getRejpassportno() != null ? finaldata.getRejmobileno() : "");
        msg.put("rej_eid", finaldata.getRejeid() != null ? finaldata.getRejeid() : "");
        msg.put("rej_dateofbirth", finaldata.getRejdateofbirth() !=null ? finaldata.getRejdateofbirth() : "");
        msg.put("rejection_date", finaldata.getRejectiondate() != null ? finaldata.getRejectiondate() : "");
        msg.put("rej_product_code", finaldata.getRejproductcode() !=null ? finaldata.getRejproductcode() : "");
        msg.put("decline_reason", finaldata.getDeclinereason() !=null ? finaldata.getDeclinereason() : "");
        json.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

        logger.info("[ApplMqWriter] Final JSON: " + json.toString());
        boolean status = ECClient.enqueue("HOST", entity_id, event_id, json.toString());
        logger.info("[ApplMqWriter] The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        return false;
    }
    public  static  String getRandomNum(){
        Random rand = new Random();
        int randomnum=rand.nextInt();

        return String.valueOf(randomnum);
    }
    public  static String getSysTime(){
        String systime="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= new Date();
            systime=simpleDateFormat.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return systime;
    }

    public static void main(String[] args) {
        logger.info("inside main method for deriving NFT_ApplmatchedrejapplEvent");
    }
}

