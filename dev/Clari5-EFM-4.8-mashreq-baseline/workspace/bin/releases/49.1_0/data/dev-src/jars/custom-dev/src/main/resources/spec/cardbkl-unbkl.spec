# Generated Code
cxps.noesis.glossary.entity.CRDBLK_UNBLK_DETAILS {
        db-name = CRDBLK_UNBLK_DETAILS
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
        { name= card-No, column =cardNo, type ="string:225",key=false}
        { name= blk-Status, column =blkStatus, type ="string:225",key=false}
        { name= status, column =status, type ="string:225",key=false}
        { name= code, column =code, type ="string:225",key=false}
        { name= description, column =description, type ="string:255",key=false}
          ]
        indexes {
        NCK_CUSTOMER_pe = [cardNo ]
    }
}

