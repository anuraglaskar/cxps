cxps.noesis.glossary.entity.scenario2-tran-code {
       db-name = scenario2_TRAN_CODE
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = tran-code, column = TRAN_CODE, type = "string:20", key=true }
               ]
       }
