package clari5.custom.customScn.scenario14;

import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Timer;
import java.util.TimerTask;

public class RemindTask extends TimerTask {
    Timer timer = new Timer();
    private static CxpsLogger cxpsLogger=CxpsLogger.getLogger(RemindTask.class);
    private static ThreadInfo threadInfo=CallOnEndOfMonth.getThreadInfp();

    public void run() {
        synchronized (this){

            try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
                Connection connection=rdbmsSession.getCxConnection();) {
                JsonWriter jwriter = new JsonWriter();
                jwriter.send();

                PreparedStatement preparedStatement=connection.prepareStatement(threadInfo.getUpdateQry()); // update query
                preparedStatement.setString(1,"C");
                preparedStatement.setTimestamp(2,new java.sql.Timestamp(new java.util.Date().getTime()));
                preparedStatement.setString(3,threadInfo.getCurrentThreadName());
                preparedStatement.setDate(4,threadInfo.getThreadDate());
                preparedStatement.executeUpdate();
                connection.commit();
                cxpsLogger.info("[RemindTask] Updated the status");
                // update the status inside the thread audit table

            } catch (Exception e) {
                e.printStackTrace();
            }
//            timer.cancel(); //Terminate the timer thread
        }
    }
}
