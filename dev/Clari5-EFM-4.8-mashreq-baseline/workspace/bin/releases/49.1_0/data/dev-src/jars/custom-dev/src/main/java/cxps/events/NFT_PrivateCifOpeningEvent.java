// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_PRIVATE_CIF_OPENING", Schema="rice")
public class NFT_PrivateCifOpeningEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp date;
       @Field(size=200) public String currMonName;
       @Field(size=20) public Double currMonCount;
       @Field(size=200) public String lastMonName;
       @Field(size=20) public Double lastMonCount;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public Double avgSixMonCount;


    @JsonIgnore
    public ITable<NFT_PrivateCifOpeningEvent> t = AEF.getITable(this);

	public NFT_PrivateCifOpeningEvent(){}

    public NFT_PrivateCifOpeningEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("PrivateCifOpening");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setDate(EventHelper.toTimestamp(json.getString("date")));
            setCurrMonName(json.getString("curr_mon_name"));
            setCurrMonCount(EventHelper.toDouble(json.getString("curr_mon_count")));
            setLastMonName(json.getString("last_mon_name"));
            setLastMonCount(EventHelper.toDouble(json.getString("last_mon_count")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setAvgSixMonCount(EventHelper.toDouble(json.getString("avg_six_mon_count")));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "PVT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getDate(){ return date; }

    public String getCurrMonName(){ return currMonName; }

    public Double getCurrMonCount(){ return currMonCount; }

    public String getLastMonName(){ return lastMonName; }

    public Double getLastMonCount(){ return lastMonCount; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public Double getAvgSixMonCount(){ return avgSixMonCount; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setDate(java.sql.Timestamp val){ this.date = val; }
    public void setCurrMonName(String val){ this.currMonName = val; }
    public void setCurrMonCount(Double val){ this.currMonCount = val; }
    public void setLastMonName(String val){ this.lastMonName = val; }
    public void setLastMonCount(Double val){ this.lastMonCount = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setAvgSixMonCount(Double val){ this.avgSixMonCount = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.currMonName);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_PrivateCifOpening");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "PrivateCifOpening");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}