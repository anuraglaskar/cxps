cxps.events.event.nft-walletactivation{  
  table-name : EVENT_NFT_WALLETACTIVATION
  event-mnemonic: WA
  workspaces : {
    CUSTOMER: "cust-id"
    NONCUSTOMER: "device-id"
  }
 event-attributes : {

host-id: {db:true ,raw_name : host_id ,type:"string:2"}
account-id: {db:true ,raw_name : account_id ,type:"string:20"}
wallet-type: {db :true ,raw_name : wallet_type ,type : "string:50"}
cust-id: {db:true ,raw_name : cust_id ,type:"string:20",derivation :"""cxps.events.CustomDerivator.getCustId(this)"""}
Channel: {db:true ,raw_name : channel ,type:"string:20"}
device-id: {db:true ,raw_name : device_id ,type:"string:50"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
cardserno: {db:true ,raw_name : cardserno ,type:"string:50"}
}
}
