// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_IRIS_APPL_SAL", Schema="rice")
public class NFT_IrisApplSalEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String isEmployerNonsfa;
       @Field(size=50) public String commLandline;
       @Field(size=100) public String npaWrittenoff;
       @Field(size=50) public String offPoBox;
       @Field(size=50) public String sellerName;
       @Field(size=20) public String channel;
       @Field(size=50) public String deviceId;
       @Field(size=20) public String eida;
       @Field(size=50) public String tradeLicense;
       @Field(size=50) public String offAddress2;
       @Field(size=50) public String resLandline;
       @Field(size=50) public String offAddress1;
       @Field public java.sql.Timestamp acctopendate;
       @Field(size=20) public String isIbanExternal;
       @Field(size=50) public String ibanNumber;
       @Field(size=50) public String perPoBox;
       @Field(size=50) public String applStage;
       @Field(size=20) public String accountId;
       @Field(size=50) public String vat;
       @Field(size=20) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public Double salaryAmt;
       @Field(size=50) public String poBox;
       @Field(size=20) public String emailId;
       @Field(size=50) public String nationality;
       @Field public java.sql.Timestamp dob;
       @Field(size=50) public String passportNo;
       @Field public java.sql.Timestamp salaryDate;
       @Field(size=5) public String salaryTranType;
       @Field public java.sql.Timestamp createdOn;
       @Field(size=50) public String commPoBox;
       @Field(size=20) public String ipCountry;
       @Field(size=5) public String matchedDob;
       @Field(size=50) public String drivingLnc;
       @Field(size=20) public Double debitAmount;
       @Field public java.sql.Timestamp updatedOn;
       @Field(size=5) public String matchedEid;
       @Field(size=50) public String landlineNo;
       @Field(size=50) public String perLandline;
       @Field(size=20) public String appRefNo;
       @Field(size=50) public String employeeId;
       @Field(size=20) public String product;
       @Field(size=50) public String perAddress2;
       @Field(size=50) public String commAddress1;
       @Field(size=200) public String address2;
       @Field(size=50) public String commAddress2;
       @Field(size=200) public String address1;
       @Field(size=5) public String matchedPpno;
       @Field(size=50) public String perAddress1;
       @Field(size=50) public String offLandline;
       @Field(size=20) public String custId;
       @Field(size=20) public String phNo;
       @Field(size=50) public String employerId;
       @Field(size=50) public String sellerId;
       @Field(size=50) public String resPoBox;
       @Field(size=5) public String matchedPhno;
       @Field(size=5) public String salaryNarative;
       @Field(size=20) public String ipAddress;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=50) public String npaProduct;
       @Field public java.sql.Timestamp npaDate;
       @Field(size=20) public String succFailFlag;
       @Field(size=50) public String resAddress2;
       @Field(size=50) public String resAddress1;


    @JsonIgnore
    public ITable<NFT_IrisApplSalEvent> t = AEF.getITable(this);

	public NFT_IrisApplSalEvent(){}

    public NFT_IrisApplSalEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("IrisApplSal");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setIsEmployerNonsfa(json.getString("is_employer_nonsfa"));
            setCommLandline(json.getString("comm_landline"));
            setNpaWrittenoff(json.getString("npa_writtenoff"));
            setOffPoBox(json.getString("off_po_box"));
            setSellerName(json.getString("seller_name"));
            setChannel(json.getString("channel"));
            setDeviceId(json.getString("device_id"));
            setEida(json.getString("eida"));
            setTradeLicense(json.getString("trade_license"));
            setOffAddress2(json.getString("off_address2"));
            setResLandline(json.getString("res_landline"));
            setOffAddress1(json.getString("off_address1"));
            setAcctopendate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setIsIbanExternal(json.getString("is_iban_external"));
            setIbanNumber(json.getString("iban_number"));
            setPerPoBox(json.getString("per_po_box"));
            setApplStage(json.getString("appl_stage"));
            setAccountId(json.getString("account_id"));
            setVat(json.getString("vat"));
            setBranchId(json.getString("branch_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setSalaryAmt(EventHelper.toDouble(json.getString("salary_amt")));
            setPoBox(json.getString("po_box"));
            setEmailId(json.getString("email_id"));
            setNationality(json.getString("nationality"));
            setDob(EventHelper.toTimestamp(json.getString("dob")));
            setPassportNo(json.getString("passport_no"));
            setSalaryDate(EventHelper.toTimestamp(json.getString("salary_date")));
            setSalaryTranType(json.getString("salary_tran_type"));
            setCreatedOn(EventHelper.toTimestamp(json.getString("created_on")));
            setCommPoBox(json.getString("comm_po_box"));
            setIpCountry(json.getString("ip_country"));
            setMatchedDob(json.getString("matched_dob"));
            setDrivingLnc(json.getString("driving_lnc"));
            setDebitAmount(EventHelper.toDouble(json.getString("debit_amount")));
            setUpdatedOn(EventHelper.toTimestamp(json.getString("updated_on")));
            setMatchedEid(json.getString("matched_eid"));
            setLandlineNo(json.getString("landline_no"));
            setPerLandline(json.getString("per_landline"));
            setAppRefNo(json.getString("app_ref_no"));
            setEmployeeId(json.getString("employee_id"));
            setProduct(json.getString("product"));
            setPerAddress2(json.getString("per_address2"));
            setCommAddress1(json.getString("comm_address1"));
            setAddress2(json.getString("address2"));
            setCommAddress2(json.getString("comm_address2"));
            setAddress1(json.getString("address1"));
            setMatchedPpno(json.getString("matched_ppno"));
            setPerAddress1(json.getString("per_address1"));
            setOffLandline(json.getString("off_landline"));
            setCustId(json.getString("cust_id"));
            setPhNo(json.getString("ph_no"));
            setEmployerId(json.getString("employer_id"));
            setSellerId(json.getString("seller_id"));
            setResPoBox(json.getString("res_po_box"));
            setMatchedPhno(json.getString("matched_phno"));
            setSalaryNarative(json.getString("salary_narative"));
            setIpAddress(json.getString("ip_address"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setNpaProduct(json.getString("npa_product"));
            setNpaDate(EventHelper.toTimestamp(json.getString("npa_date")));
            setSuccFailFlag(json.getString("succ_fail_flg"));
            setResAddress2(json.getString("res_address2"));
            setResAddress1(json.getString("res_address1"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "IAS"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getIsEmployerNonsfa(){ return isEmployerNonsfa; }

    public String getCommLandline(){ return commLandline; }

    public String getNpaWrittenoff(){ return npaWrittenoff; }

    public String getOffPoBox(){ return offPoBox; }

    public String getSellerName(){ return sellerName; }

    public String getChannel(){ return channel; }

    public String getDeviceId(){ return deviceId; }

    public String getEida(){ return eida; }

    public String getTradeLicense(){ return tradeLicense; }

    public String getOffAddress2(){ return offAddress2; }

    public String getResLandline(){ return resLandline; }

    public String getOffAddress1(){ return offAddress1; }

    public java.sql.Timestamp getAcctopendate(){ return acctopendate; }

    public String getIsIbanExternal(){ return isIbanExternal; }

    public String getIbanNumber(){ return ibanNumber; }

    public String getPerPoBox(){ return perPoBox; }

    public String getApplStage(){ return applStage; }

    public String getAccountId(){ return accountId; }

    public String getVat(){ return vat; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public Double getSalaryAmt(){ return salaryAmt; }

    public String getPoBox(){ return poBox; }

    public String getEmailId(){ return emailId; }

    public String getNationality(){ return nationality; }

    public java.sql.Timestamp getDob(){ return dob; }

    public String getPassportNo(){ return passportNo; }

    public java.sql.Timestamp getSalaryDate(){ return salaryDate; }

    public String getSalaryTranType(){ return salaryTranType; }

    public java.sql.Timestamp getCreatedOn(){ return createdOn; }

    public String getCommPoBox(){ return commPoBox; }

    public String getIpCountry(){ return ipCountry; }

    public String getMatchedDob(){ return matchedDob; }

    public String getDrivingLnc(){ return drivingLnc; }

    public Double getDebitAmount(){ return debitAmount; }

    public java.sql.Timestamp getUpdatedOn(){ return updatedOn; }

    public String getMatchedEid(){ return matchedEid; }

    public String getLandlineNo(){ return landlineNo; }

    public String getPerLandline(){ return perLandline; }

    public String getAppRefNo(){ return appRefNo; }

    public String getEmployeeId(){ return employeeId; }

    public String getProduct(){ return product; }

    public String getPerAddress2(){ return perAddress2; }

    public String getCommAddress1(){ return commAddress1; }

    public String getAddress2(){ return address2; }

    public String getCommAddress2(){ return commAddress2; }

    public String getAddress1(){ return address1; }

    public String getMatchedPpno(){ return matchedPpno; }

    public String getPerAddress1(){ return perAddress1; }

    public String getOffLandline(){ return offLandline; }

    public String getCustId(){ return custId; }

    public String getPhNo(){ return phNo; }

    public String getEmployerId(){ return employerId; }

    public String getSellerId(){ return sellerId; }

    public String getResPoBox(){ return resPoBox; }

    public String getMatchedPhno(){ return matchedPhno; }

    public String getSalaryNarative(){ return salaryNarative; }

    public String getIpAddress(){ return ipAddress; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getNpaProduct(){ return npaProduct; }

    public java.sql.Timestamp getNpaDate(){ return npaDate; }

    public String getSuccFailFlag(){ return succFailFlag; }

    public String getResAddress2(){ return resAddress2; }

    public String getResAddress1(){ return resAddress1; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setIsEmployerNonsfa(String val){ this.isEmployerNonsfa = val; }
    public void setCommLandline(String val){ this.commLandline = val; }
    public void setNpaWrittenoff(String val){ this.npaWrittenoff = val; }
    public void setOffPoBox(String val){ this.offPoBox = val; }
    public void setSellerName(String val){ this.sellerName = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setEida(String val){ this.eida = val; }
    public void setTradeLicense(String val){ this.tradeLicense = val; }
    public void setOffAddress2(String val){ this.offAddress2 = val; }
    public void setResLandline(String val){ this.resLandline = val; }
    public void setOffAddress1(String val){ this.offAddress1 = val; }
    public void setAcctopendate(java.sql.Timestamp val){ this.acctopendate = val; }
    public void setIsIbanExternal(String val){ this.isIbanExternal = val; }
    public void setIbanNumber(String val){ this.ibanNumber = val; }
    public void setPerPoBox(String val){ this.perPoBox = val; }
    public void setApplStage(String val){ this.applStage = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setVat(String val){ this.vat = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setSalaryAmt(Double val){ this.salaryAmt = val; }
    public void setPoBox(String val){ this.poBox = val; }
    public void setEmailId(String val){ this.emailId = val; }
    public void setNationality(String val){ this.nationality = val; }
    public void setDob(java.sql.Timestamp val){ this.dob = val; }
    public void setPassportNo(String val){ this.passportNo = val; }
    public void setSalaryDate(java.sql.Timestamp val){ this.salaryDate = val; }
    public void setSalaryTranType(String val){ this.salaryTranType = val; }
    public void setCreatedOn(java.sql.Timestamp val){ this.createdOn = val; }
    public void setCommPoBox(String val){ this.commPoBox = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setMatchedDob(String val){ this.matchedDob = val; }
    public void setDrivingLnc(String val){ this.drivingLnc = val; }
    public void setDebitAmount(Double val){ this.debitAmount = val; }
    public void setUpdatedOn(java.sql.Timestamp val){ this.updatedOn = val; }
    public void setMatchedEid(String val){ this.matchedEid = val; }
    public void setLandlineNo(String val){ this.landlineNo = val; }
    public void setPerLandline(String val){ this.perLandline = val; }
    public void setAppRefNo(String val){ this.appRefNo = val; }
    public void setEmployeeId(String val){ this.employeeId = val; }
    public void setProduct(String val){ this.product = val; }
    public void setPerAddress2(String val){ this.perAddress2 = val; }
    public void setCommAddress1(String val){ this.commAddress1 = val; }
    public void setAddress2(String val){ this.address2 = val; }
    public void setCommAddress2(String val){ this.commAddress2 = val; }
    public void setAddress1(String val){ this.address1 = val; }
    public void setMatchedPpno(String val){ this.matchedPpno = val; }
    public void setPerAddress1(String val){ this.perAddress1 = val; }
    public void setOffLandline(String val){ this.offLandline = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setPhNo(String val){ this.phNo = val; }
    public void setEmployerId(String val){ this.employerId = val; }
    public void setSellerId(String val){ this.sellerId = val; }
    public void setResPoBox(String val){ this.resPoBox = val; }
    public void setMatchedPhno(String val){ this.matchedPhno = val; }
    public void setSalaryNarative(String val){ this.salaryNarative = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setNpaProduct(String val){ this.npaProduct = val; }
    public void setNpaDate(java.sql.Timestamp val){ this.npaDate = val; }
    public void setSuccFailFlag(String val){ this.succFailFlag = val; }
    public void setResAddress2(String val){ this.resAddress2 = val; }
    public void setResAddress1(String val){ this.resAddress1 = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.appRefNo + this.ibanNumber);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_IrisApplSal");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "IrisApplSal");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
