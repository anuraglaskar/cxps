// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_MULTIPLECUSTOMER", Schema="rice")
public class NFT_MultiplecustomerEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String newCusttype;
       @Field(size=20) public String newCxcif;
       @Field(size=20) public String oldMobile;
       @Field(size=20) public String oldCif;
       @Field(size=20) public String oldEid;
       @Field(size=50) public String oldCusttype;
       @Field(size=20) public String oldCompmis2;
       @Field(size=20) public String newCustname;
       @Field(size=50) public String oldCompmis1;
       @Field(size=20) public String oldEmail;
       @Field(size=50) public String newMobile;
       @Field(size=20) public String matchedEmail;
       @Field(size=20) public String newCompmis1;
       @Field(size=50) public String newCompmis2;
       @Field(size=20) public String oldPassport;
       @Field(size=20) public String matchedMobile;
       @Field(size=20) public String newEid;
       @Field(size=20) public String newPassport;
       @Field(size=20) public String oldCustname;
       @Field(size=20) public String newEmail;


    @JsonIgnore
    public ITable<NFT_MultiplecustomerEvent> t = AEF.getITable(this);

	public NFT_MultiplecustomerEvent(){}

    public NFT_MultiplecustomerEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Multiplecustomer");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setNewCusttype(json.getString("new_custType"));
            setNewCxcif(json.getString("new_cxcif"));
            setOldMobile(json.getString("old_mobile"));
            setOldCif(json.getString("old_cif"));
            setOldEid(json.getString("old_eid"));
            setOldCusttype(json.getString("old_custType"));
            setOldCompmis2(json.getString("old_compmis2"));
            setNewCustname(json.getString("new_custName"));
            setOldCompmis1(json.getString("old_compmis1"));
            setOldEmail(json.getString("old_email"));
            setNewMobile(json.getString("new_mobile"));
            setMatchedEmail(json.getString("matched_email"));
            setNewCompmis1(json.getString("new_compmis1"));
            setNewCompmis2(json.getString("new_compmis2"));
            setOldPassport(json.getString("old_passportNo"));
            setMatchedMobile(json.getString("matched_mobile"));
            setNewEid(json.getString("new_eid"));
            setNewPassport(json.getString("new_passport"));
            setOldCustname(json.getString("old_cust_name"));
            setNewEmail(json.getString("new_email"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "MCE"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getNewCusttype(){ return newCusttype; }

    public String getNewCxcif(){ return newCxcif; }

    public String getOldMobile(){ return oldMobile; }

    public String getOldCif(){ return oldCif; }

    public String getOldEid(){ return oldEid; }

    public String getOldCusttype(){ return oldCusttype; }

    public String getOldCompmis2(){ return oldCompmis2; }

    public String getNewCustname(){ return newCustname; }

    public String getOldCompmis1(){ return oldCompmis1; }

    public String getOldEmail(){ return oldEmail; }

    public String getNewMobile(){ return newMobile; }

    public String getMatchedEmail(){ return matchedEmail; }

    public String getNewCompmis1(){ return newCompmis1; }

    public String getNewCompmis2(){ return newCompmis2; }

    public String getOldPassport(){ return oldPassport; }

    public String getMatchedMobile(){ return matchedMobile; }

    public String getNewEid(){ return newEid; }

    public String getNewPassport(){ return newPassport; }

    public String getOldCustname(){ return oldCustname; }

    public String getNewEmail(){ return newEmail; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setNewCusttype(String val){ this.newCusttype = val; }
    public void setNewCxcif(String val){ this.newCxcif = val; }
    public void setOldMobile(String val){ this.oldMobile = val; }
    public void setOldCif(String val){ this.oldCif = val; }
    public void setOldEid(String val){ this.oldEid = val; }
    public void setOldCusttype(String val){ this.oldCusttype = val; }
    public void setOldCompmis2(String val){ this.oldCompmis2 = val; }
    public void setNewCustname(String val){ this.newCustname = val; }
    public void setOldCompmis1(String val){ this.oldCompmis1 = val; }
    public void setOldEmail(String val){ this.oldEmail = val; }
    public void setNewMobile(String val){ this.newMobile = val; }
    public void setMatchedEmail(String val){ this.matchedEmail = val; }
    public void setNewCompmis1(String val){ this.newCompmis1 = val; }
    public void setNewCompmis2(String val){ this.newCompmis2 = val; }
    public void setOldPassport(String val){ this.oldPassport = val; }
    public void setMatchedMobile(String val){ this.matchedMobile = val; }
    public void setNewEid(String val){ this.newEid = val; }
    public void setNewPassport(String val){ this.newPassport = val; }
    public void setOldCustname(String val){ this.oldCustname = val; }
    public void setNewEmail(String val){ this.newEmail = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.newCxcif);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Multiplecustomer");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Multiplecustomer");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}