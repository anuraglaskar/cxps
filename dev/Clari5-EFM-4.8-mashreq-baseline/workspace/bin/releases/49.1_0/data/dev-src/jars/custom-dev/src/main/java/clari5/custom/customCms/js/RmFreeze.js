<script type="text/javascript">
AJS.$(document).ready(function() {
    AJS.$('#customfield_12003').css({ 'display': 'none' });
    AJS.$('#rowForcustomfield_12003').css({ 'display': 'none' });
    AJS.$('label[for="customfield_12003"]').hide();
    AJS.$('#customfield_12002').css({ 'display': 'none' });
    AJS.$('#rowForcustomfield_12002').css({ 'display': 'none' });
    AJS.$('label[for="customfield_12002"]').hide()
    AJS.$('#customfield_12001').css({ 'display': 'none' });
    AJS.$('#rowForcustomfield_12001').css({ 'display': 'none' });
    AJS.$('label[for="customfield_12001"]').hide();
    AJS.$('#customfield_12000').css({ 'display': 'none' });
    AJS.$('#rowForcustomfield_12000').css({ 'display': 'none' });
    AJS.$('label[for="customfield_12000"]').hide();
    var alertType = AJS.$('#type-val').text();
    if (alertType.trim() == "Incident") {
        const metas = document.getElementsByTagName('meta');
        var issuekey;
        for (let i = 0; i < metas.length; i++) {
            if (metas[i].getAttribute('name') === 'ajs-issue-key') {
                issuekey = metas[i].getAttribute('content');
            }
        }
        var curl = "https://efm2ozulapvmsrv.mashreqdev.com:2504/efm/customcmsservlet?";
        var str = "";
        var url = "";
        var editButton = AJS.$('#opsbar-comment-issue_container a');
        var issueId = "";
        if (editButton) {
            var href = editButton.attr("href");
            if (href) {
                if (href.indexOf('?id=') > -1) { issueId = href.substring(href.indexOf('?id=') + 4);  } } }
        AJS.$("</br><button class='aui-button' type='button' id='F' style='margin-left: 10px;'>R.M. Freeze</button><button class='aui-button' id='C' type='button'>Card Block</button>").insertAfter(AJS.$(".ops-cont ").first());
        if (AJS.$('#customfield_12003-val').text()) {
            AJS.$('#F').text(AJS.$('#customfield_12003-val').text());
        }
        if (AJS.$('#customfield_12002-val').text()) {
            AJS.$('#C').text(AJS.$('#customfield_12002-val').text());
        }
        AJS.$('#F').click(function() {
            if (AJS.$('#customfield_12000-val').text().trim() !== "Not Available") {
                if (AJS.$('#F').text().trim() == 'R.M. Freeze') {
                    str = "action=F&customerNumber=";
                    url = (curl.concat(str)).concat(AJS.$('#customfield_12000-val').text().trim()).concat("&issuekey=").concat(issuekey).concat("&issueId=").concat(issueId);
                } else {
                    str = "action=UZ&customerNumber=";
                    url = (curl.concat(str)).concat(AJS.$('#customfield_12000-val').text().trim()).concat("&issuekey=").concat(issuekey).concat("&issueId=").concat(issueId);
                }
                getAjax();
            } else {
                alert("No information of customer in this scenario");
            }
        });
        AJS.$('#C').click(function() {
            if (AJS.$('#customfield_12001-val').text().trim() !== "Not Available") {
                if (AJS.$('#C').text().trim() == 'Card Block') {
                    str = "action=B&customerNumber=";
                    url = (curl.concat(str)).concat(AJS.$('#customfield_12001-val').text().trim()).concat("&issuekey=").concat(issuekey).concat("&issueId=").concat(issueId);
                } else {
                    str = "action=UB&customerNumber=";
                    url = (curl.concat(str)).concat(AJS.$('#customfield_12001-val').text().trim()).concat("&issuekey=").concat(issuekey).concat("&issueId=").concat(issueId);
                }
                getAjax();
            } else {
                alert("No information of cards in this scenario");
            }
        });

        function getAjax() {
            AJS.$.ajax({
                type: 'GET',
                url: url,
                crossDomain: true,
                contentType: "application/javascript; charset=utf-8",
                dataType: 'jsonp',
                jsonp: "callback",
                xhrFields: { ithCredentials: true },
                success: function(data) {
                    if (data.resp == "FS") {
                        if (AJS.$('#F').text().trim() == 'R.M. Freeze') {
                            AJS.$('#F').text("R.M. Unfreeze");
                        } else {
                            AJS.$('#F').text("R.M. Freeze");
                        }
                    } else if (data.resp == "CS") {
                        if (AJS.$('#C').text().trim() == 'Card Block') {
                            AJS.$('#C').text("Card UnBlock");
                        } else {
                            AJS.$('#C').text("Card Block");
                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log(ajaxOptions);
                }
            });
        }
    }
});
</script>
