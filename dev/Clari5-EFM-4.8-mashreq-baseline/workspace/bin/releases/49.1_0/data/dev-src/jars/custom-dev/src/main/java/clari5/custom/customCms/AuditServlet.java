package clari5.custom.customCms;
import clari5.platform.applayer.Clari5;
import clari5.platform.util.Hocon;
import clari5.utils.upload.FileUploader;
import clari5.utils.upload.IFileUpload;
import clari5.utils.upload.RequestParser;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import org.json.*;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AuditServlet extends HttpServlet {

    CxpsLogger logger = CxpsLogger.getLogger(AuditServlet.class);

    @Override
    public void init(ServletConfig config){
        try {
            String appName = config.getServletContext().getContextPath().substring(1);
            Clari5.bootstrap(appName, appName.toLowerCase() + "-clari5.conf");
        }
        catch(Exception ex){
            ex.printStackTrace();
            logger.error("Unable to bootstrap", ex);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    public boolean doEither(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String json=request.getParameter("jsondata");
        JSONObject jsonObject = new JSONObject(json);
        String cmd = jsonObject.getString("cmd");
        JSONObject pay=jsonObject.getJSONObject("payload");
        String payload=pay.getString("custId");
        

        if(cmd == null || "".equals(cmd)){
            return error(response, "{ \"error\":\"No command found\" }");
        }

        if(payload == null || "".equals(payload)){
            return error(response, "{ \"error\":\""+cmd+" requires input and passed null\" }");
        }
        String responseStr=null;
        try {
            switch(cmd){
                case "getAudit_Log":
                    responseStr=getAudit_Log(payload);
                    break;
                case "getCardAudit_Log":
                    responseStr=getCardAudit_Log(payload);
                    break;
                default:
                    System.out.println("Unknown command");
            }
        }
        catch(Exception ex){
            logger.error("Invalid payload:["+payload+"]");
            return error(response, "{ \"error\":\"Unable to get Data\" }");
        }

        if(responseStr == null){
            return error(response, "{ \"error\":\""+cmd+" returned error\" }");
        }
        return writeOut(response, responseStr);

    }


    /*
     * Function for freez unfreez.
     */
    protected String getAudit_Log(String payload) throws ClassNotFoundException {
        logger.debug("in getCustDetails");
        JSONObject obj = null;
        String date="";
        String date1="";
        String custsince="";
        boolean flag=true;
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        String memberId = payload;
        Export export=new Export();
        export.export(memberId);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select * from Audit_Log where Cust_Id='"+memberId+"'order by Freez_Date DESC ";
            logger.debug("query to get CustDetails : "+sql);
            rs = stmt.executeQuery(sql);
            logger.debug("after exec : "+rs);
            while( rs.next()){
                flag=false;
                obj = new JSONObject();
                obj.put("CustId", rs.getString("Cust_Id")!= null ? rs.getString("Cust_Id") : "");
               // obj.put("Card Number", rs.getString("Card_Number")!= null ? rs.getString("Card_number") : "");
                obj.put("Freez status", rs.getString("Freez_status")!= null ? rs.getString("Freez_status") : "");
                String dd=(rs.getString("Freez_Date")!= null ? rs.getString("Freez_Date") : "");
                String date4=dd.substring(0,19);
                obj.put("Freez Date", date4);
                obj.put("Username", rs.getString("User_Name")!= null ? rs.getString("User_Name") : "");
                jarray.put(obj);

            }

            if(flag==true){
                obj1.put("msg","N");
            }else {
                obj1.put("msg", "S");
                obj1.put("customerDetails", jarray);
            }
            logger.debug("getCustDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            obj1.put("msg","F");
            logger.debug("exception in getting cust details from db");
            e.printStackTrace();
        }finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";
    }
    ///for card block unblock audit
    protected String getCardAudit_Log(String payload) throws ClassNotFoundException {
        logger.debug("in getCustDetails");
        JSONObject obj = null;
        String date="";
        boolean flag=true;
        String date1="";
        String custsince="";
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        String memberId = payload;
        Export export=new Export();
        export.exportCard(memberId);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select * from Audit_Log where Card_number='"+memberId+"'order by Freez_Date DESC ";
            logger.debug("query to get CustDetails : "+sql);
            rs = stmt.executeQuery(sql);
            logger.debug("after exec : "+rs);
            while( rs.next()){
                flag=false;
                obj = new JSONObject();
              //  obj.put("CustId", rs.getString("Cust_Id")!= null ? rs.getString("Cust_Id") : "");
                obj.put("Card Number", rs.getString("Card_Number")!= null ? rs.getString("Card_number") : "");
                obj.put("Freez status", rs.getString("Freez_status")!= null ? rs.getString("Freez_status") : "");
                String dd=(rs.getString("Freez_Date")!= null ? rs.getString("Freez_Date") : "");
                String date4=dd.substring(0,19);
                obj.put("Freez Date", date4);
                obj.put("Username", rs.getString("User_Name")!= null ? rs.getString("User_Name") : "");
                jarray.put(obj);
            }
            if(flag==true){
                obj1.put("msg","N");
            }else {
                obj1.put("msg", "S");
                obj1.put("customerDetails", jarray);
            }
            logger.debug("getCustDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            obj1.put("msg","F");
            logger.debug("exception in getting cust details from db");
            e.printStackTrace();
        }finally {

            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }

            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";
    }

    protected boolean writeOut(HttpServletResponse response, String message) throws IOException {
        logger.debug("Writing out["+message+"]");
        try {
            Writer writer = response.getWriter();
            if(writer != null){
                writer.write(message);
            }
            else
                logger.error("Found null http writer");
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return false;
    }
    protected boolean error(HttpServletResponse response, String message) throws IOException {
        logger.error("Error: Processing Request ["+message+"]");
        return writeOut(response, message);
    }



}
