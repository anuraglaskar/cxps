package cxps.events;

import clari5.custom.customCms.CustomEvidenceFormatter;
import clari5.custom.customScn11.DataFetchCreateEvent;
import clari5.custom.customScn11.StaticInfoChangeEventCreation;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.maxmind.geolite.MaxmindUtil;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.typesafe.config.ConfigException;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;
import org.json.JSONObject;
import sun.reflect.generics.scope.ClassScope;


public class CustomDerivator {

    private static CxpsLogger logger = CxpsLogger.getLogger(CustomDerivator.class);

    public static String getIpCountry(NFT_LoginEvent nft) {

        String ip = nft.getAddrNetwork();
        if (!StringUtils.isNullOrEmpty(ip)) {
            String maxMindCountryCode = MaxmindUtil.getCountryCode(ip);
            if (maxMindCountryCode != null) {
                nft.ipCountry = maxMindCountryCode;
            }
        }
        return nft.ipCountry;
    }

    public static String IpCountry(NFT_AcctopeningEvent nft) {

        String ip = nft.getIpAddress();
        if (!StringUtils.isNullOrEmpty(ip)) {
            String maxMindCountryCode = MaxmindUtil.getCountryCode(ip);
            if (maxMindCountryCode != null) {
                nft.ipCountry = maxMindCountryCode;
            }
        }
        return nft.ipCountry;
    }

    public static Double getAvlBal(FT_IbfundstransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        Double avlbal = 0.0;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT CURR_BAL FROM DDA WHERE hostAcctId = '" + event.getAccountNo() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                avlbal = result.getDouble("CURR_BAL");
            }

            return avlbal;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return avlbal;
    }

    public static Double getAvlBalance(FT_IbfundstransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        Double avlbal = 0.0;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT CURR_BAL FROM DDA WHERE hostAcctId = '" + event.getAccountNo() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                avlbal = result.getDouble("CURR_BAL");
                if (avlbal <= 0.0) {
                    avlbal = 1.0;
                }
            }
            return avlbal;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return avlbal;
    }

    public static String getAddrNetworkderived(FT_IbfundstransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String ipadd = "";
        ipadd = event.getAddrNetwork();
        if (StringUtils.isNullOrEmpty(event.getAddrNetwork())) {
            try {
                con = Rdbms.getAppConnection();
                String query = "select TOP 1 ADDR_NETWORK from EVENT_NFT_LOGIN where CUST_ID = '" + event.getCustId() + "' ORDER BY EVENT_DATE DESC";
                psmt = con.prepareStatement(query);
                result = psmt.executeQuery();

                while (result.next()) {
                    ipadd = result.getString("ADDR_NETWORK");
                }
                event.setAddrNetwork(ipadd);
                return ipadd;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (psmt != null) {
                        psmt.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                    if (result != null) {
                        result.close();
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        } else {
            event.setAddrNetwork(ipadd);
        }
        return ipadd;
    }

    public static String getIsBenefAdded(FT_IbfundstransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;

        try {
            con = Rdbms.getAppConnection();
            String query = "select MAX(EVENT_DATE) as EVENT_DATE from EVENT_NFT_BENEFICIARY_ADDITION where cust_id = '" + event.getCustId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                Timestamp bentime = result.getTimestamp("EVENT_DATE");
                event.benefAdditionDate = result.getTimestamp("EVENT_DATE");
                if (bentime != null && bentime.before(event.getEventDate())) {
                    long millis = event.getEventDate().getTime() - bentime.getTime();
                    int seconds = (int) millis / 1000;
                    int hours = seconds / 3600;
                    if (hours < 48) {
                        event.isBenefAdded = "Y";
                    }
                }
                return event.isBenefAdded;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.isBenefAdded;
    }

    public static String getCustDerive(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String customerId = "";
        customerId = event.getCustId();
        if (StringUtils.isNullOrEmpty(event.getCustId())) {
            try {
                if (event.getSource().equalsIgnoreCase("CC")) {
                    try {
                        con = Rdbms.getAppConnection();
                        String query = "select hostCustId from CUSTOMER WHERE primecardserno = '" + event.getCustCardId() + "'";
                        psmt = con.prepareStatement(query);
                        result = psmt.executeQuery();
                        while (result.next()) {
                            // System.out.println("Cust_id when source is cc : " + result.getString("hostCustId"));
                            customerId = result.getString("hostCustId");
                            if (customerId.startsWith("C_F_")) {
                                customerId = customerId.substring(4);
                                event.setCustId(customerId);
                            }
                            event.setCustId(customerId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (psmt != null) {
                                psmt.close();
                            }

                            if (con != null) {
                                con.close();
                            }
                            if (result != null) {
                                result.close();
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }


                } else if (!event.getSource().equalsIgnoreCase("CC") && !event.getSource().equalsIgnoreCase("CORE")) {
                    try {
                        con = Rdbms.getAppConnection();
                        String query = "select acctCustID from DDA where hostAcctID = '" + event.getAccountId() + "'";
                        psmt = con.prepareStatement(query);
                        result = psmt.executeQuery();
                        while (result.next()) {
                            //System.out.println("Cust_id when source is DC : " + result.getString("acctCustID"));
                            customerId = result.getString("acctCustID");
                            if (customerId.startsWith("C_F_")) {
                                customerId = customerId.substring(4);
                                event.setCustId(customerId);
                            }
                            event.setCustId(customerId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (psmt != null) {
                                psmt.close();
                            }

                            if (con != null) {
                                con.close();
                            }
                            if (result != null) {
                                result.close();
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            event.setCustId(customerId);
        }
        return customerId;
    }

    public static String getSuccFailFlagDerivd(FT_AccounttxnEvent event) {

        String succfail = event.getSuccFailFlag();
        if (event.channel.equalsIgnoreCase("DC")) {
            if (event.getResponseCode().equalsIgnoreCase("00")) {
                event.setSuccFailFlag("Authorized");
                event.succFailFlagDerivd = "Authorized";
            } else
                event.setSuccFailFlag("Declined");
        } else
            event.setSuccFailFlag(succfail);

        return event.succFailFlag;
    }

    public static Double getAvlBal(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        Double avlbal = 0.0;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT CURR_BAL FROM DDA WHERE hostAcctId = '" + event.getAccountId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                avlbal = result.getDouble("CURR_BAL");
            }

            return avlbal;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return avlbal;
    }

    public static Double getAvlBalance(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        Double avlbal = 0.0;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT CURR_BAL FROM DDA WHERE hostAcctId = '" + event.getAccountId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                avlbal = result.getDouble("CURR_BAL");
                if (avlbal <= 0.0) {
                    avlbal = 1.0;
                }
            }
            return avlbal;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return avlbal;
    }

    public static String getCustIdDerived(FT_IbfundstransferEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String customerId = "";
        customerId = event.getCustId();
        if (StringUtils.isNullOrEmpty(event.getCustId())) {
            try {
                con = Rdbms.getAppConnection();
                String query = "SELECT acctCustID FROM DDA WHERE hostAcctId = '" + event.getAccountNo() + "'";
                psmt = con.prepareStatement(query);
                result = psmt.executeQuery();

                while (result.next()) {
                    customerId = result.getString("acctCustID");

                    if (customerId.startsWith("C_F_")) {
                        customerId = customerId.substring(4);
                        event.setCustId(customerId);
                    }
                }
                return customerId;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (psmt != null) {
                        psmt.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                    if (result != null) {
                        result.close();
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            event.setCustId(customerId);
        }
        return customerId;
    }

    public static double getAtmLim(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        double limit = 0.0;
        String segmentID = "";
        limit = event.getAtmLimit();
        Hocon h = new Hocon();
        try {
            if (!StringUtils.isNullOrEmpty(event.custCardId) && event.custCardId.length() >= 6) {
                if (!StringUtils.isNullOrEmpty(event.getSource()) && event.getSource().equalsIgnoreCase("DC")) {

                    h.loadFromContext("daily-limit-derived.conf");
                    List<String> keys = h.getKeysAsList();
                    String cardnum = event.getCustCardId();

                    cardnum = cardnum.substring(0, 6);
                    if (keys.contains(cardnum)) {
                        limit = Double.parseDouble(h.getString(cardnum));
                        //System.out.println(limit);
                    }
                    event.setAtmLimit(limit);
                }
                 /*    String cardnum1 = cardnum.substring(0, 7);
                    String cardnum2 = cardnum.substring(7);
                    ArrayList<String> list = new ArrayList<>();
                    if (keys.contains(cardnum1)) {
                        Hocon customHocon = h.get(cardnum1);
                        List<String> keys1 = customHocon.getKeysAsList();
                        for (String s : keys1) {
                            list.add(s);
                        }
                        if (list.size() == 1) limit = Double.parseDouble(customHocon.getString(list.get(0)));
                        else {
                            for (int j = 0; j < list.size(); j++) {
                                Integer compare = Integer.parseInt(cardnum2) - Integer.parseInt(list.get(j));
                                if (compare >= 0) {
                                    int index = 0;
                                    for (int i = 0; i < list.size(); i++) {
                                        Integer min = Integer.parseInt(cardnum2) - Integer.parseInt(list.get(i));
                                        if (min >= 0 && min <= compare) {
                                            index = i;
                                            compare = min;
                                        }
                                    }
                                    limit = Double.parseDouble(customHocon.getString(list.get(index)));
                                    break;
                                }
                            }
                            event.setAtmLimit(limit);
                        }
                        event.setAtmLimit(limit);
                    }else
                        event.setAtmLimit(limit);
                }*/
                else {
                    event.setAtmLimit(limit);
                }
            } else if (!StringUtils.isNullOrEmpty(event.getSchmType())) {
                try {
                    con = Rdbms.getAppConnection();
                    String query = "SELECT custCategory FROM CUSTOMER WHERE hostCustId = '" + event.getCustId() + "'";
                    psmt = con.prepareStatement(query);
                    result = psmt.executeQuery();

                    while (result.next()) {
                        segmentID = result.getString("custCategory");
                    }
                    if (event.getSchmType().equalsIgnoreCase("ITWL")) {
                        h.loadFromContext("itm-itwl-derived.conf");
                        List<String> keys = h.getKeysAsList();

                        if (keys.contains(segmentID)) {
                            limit = Double.parseDouble(h.getString(segmentID));
                            // System.out.println(limit);
                        } else {
                            limit = 100000.0;
                        }
                        event.setAtmLimit(limit);
                    } else if (event.getSchmType().equalsIgnoreCase("ITCW")) {
                        h.loadFromContext("itm-itcw-derived.conf");
                        List<String> keys = h.getKeysAsList();

                        if (keys.contains(segmentID)) {
                            limit = Double.parseDouble(h.getString(segmentID));
                            //System.out.println(limit);
                        } else {
                            limit = 100000.0;
                        }
                        event.setAtmLimit(limit);
                    } else
                        event.setAtmLimit(limit);
                } catch (SQLException e) {
                    //System.out.println("custCategory not found");
                    e.printStackTrace();
                }
            } else
                event.setAtmLimit(limit);
        } catch (ConfigException ex) {
            System.out.println("cust_card_id value is unavailable or not correct ");
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            System.out.println("cust_card_id value is unavailable or not correct ");
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return limit;
    }

    public static double getCardlessCashLimit(FT_ElectronicPaymentReqEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        event.cardlessCashLimit = 0.0;
        try {
            con = Rdbms.getAppConnection();
            String query = "select constitution from customer where hostCustId = '" + event.getCustId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                String constitution = result.getString("constitution");

                if (!StringUtils.isNullOrEmpty(constitution)) {
                    if (constitution.equalsIgnoreCase("I")) {
                        event.cardlessCashLimit = 5000.0;
                    } else if (constitution.equalsIgnoreCase("C")) {
                        event.cardlessCashLimit = 20000.0;
                    } else
                        System.out.println("Constitution is not C or I");
                } else System.out.println("Constitution is empty");
            }
            return event.cardlessCashLimit;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.cardlessCashLimit;
    }

    public static String getCustIdFromAccountEPay(FT_ElectronicPaymentReqEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String custId = "";
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT acctCustID FROM DDA WHERE hostAcctId = '" + event.getAccountId() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                String customerId = result.getString("acctCustID");

                if (customerId.startsWith("C_F_")) {
                    custId = customerId.substring(4);
                }
            }
            return custId;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return custId;
    }

    public static String getCustIdFromAccountChequeBookReq(NFT_ChequeBookReqEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String custId = "";
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT acctCustID FROM DDA WHERE hostAcctId = '" + event.getAccountId() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                String customerId = result.getString("acctCustID");

                if (customerId.startsWith("C_F_")) {
                    custId = customerId.substring(4);
                }
            }
            return custId;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return custId;
    }

    public static String getBin(FT_AccounttxnEvent event) {

        String cardnum = event.getCustCardId();
        try {
            if (!StringUtils.isNullOrEmpty(event.custCardId) && event.custCardId.length() >= 6)
                cardnum = cardnum.substring(0, 6);
        } catch (Exception e) {
            System.out.println("cust_card_id value is unavailable or not correct ");
        }
        return cardnum;
    }

    public static String getIsInfoChngd(FT_EcbtEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;

        try {
            con = Rdbms.getAppConnection();
            String query = "select event_date , entity_type from EVENT_NFT_STATICINFOCHANGE ens where ens.event_date = (select MAX(event_date) from EVENT_NFT_STATICINFOCHANGE ens2 where ens2.cust_id = '" + event.getCustId() + "')";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                Timestamp chngtime = result.getTimestamp("event_date");
                String entityType = result.getString("entity_type");
                event.infoChngdTime = result.getTimestamp("event_date");
                if (chngtime != null && chngtime.before(event.getEventDate())) {
                    if (!StringUtils.isNullOrEmpty(entityType) && entityType.contains("MOBILE_NUMBER")) {
                        long millis = event.getEventDate().getTime() - chngtime.getTime();
                        int seconds = (int) (millis / 1000);
                        int hours = seconds / 3600;
                        if (hours < 120) {
                            event.isInfoChngd = "Y";
                        } else event.isInfoChngd = "N";
                    } else event.isInfoChngd = "N";
                } else event.isInfoChngd = "N";
            }
            return event.isInfoChngd;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.isInfoChngd;
    }

    public static String getCustId(NFT_WalletactivationEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String customerId = "";
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT cif_id FROM CARDMASTER WHERE card_ser_no = '" + event.getCardserno() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                customerId = result.getString("cif_id");

                if (customerId.startsWith("C_F_")) {
                    customerId = customerId.substring(4);
                }
            }
            return customerId;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customerId;
    }

    public static String getdesignatedFlg(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;

        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT emirates_id FROM DESIGNATED_PERSON WHERE cif_id = '" + event.getCustId() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                String EID = result.getString("emirates_id");

                if (EID.equalsIgnoreCase(event.idNumber)) {
                    event.designatedFlg = "Y";
                } else event.designatedFlg = "N";
            }
            return event.designatedFlg;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.designatedFlg;
    }


    public static double getmidRate(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        event.midRate = 0.0;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT buy_rate FROM BIDRATE WHERE ccy1 = '" + event.getFcyCurr() + "' and ccy2 = '" + event.getLcyCurr() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                double bidrate = Double.parseDouble(result.getString("buy_rate"));
                event.midRate = bidrate;
            }
        } catch (Exception e) {
            System.out.println("buy rate not exits");
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.midRate;
    }

    public static String getLeaveType(FT_TradenetEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        event.leaveType = null;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT leave_type,leave_start_date,leave_end_date from tradenettable where emp_id = '" + event.getEmpId() + "' and leave_start_date <= '" + event.eventDate + "' and leave_end_date >= '" + event.eventDate + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                event.leaveType = result.getString("leave_type");
                event.leaveStartDate = result.getTimestamp("leave_start_date");
                event.leaveEndDate = result.getTimestamp("leave_end_date");
            }

        } catch (SQLException e) {
            System.out.println("leave details are null");
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.leaveType;
    }

    public static String getnpaWrittenoff(NFT_AcctopeningEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        event.npaWrittenoff = null;
        String customerID = "C_F_" + event.getCustId();

        try {
            con = Rdbms.getAppConnection();
            String query = "select acctSchemeType , acctStatus , hostAcctId from dda where acctCustID = '" + customerID + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            ArrayList<String> accountID = new ArrayList<String>();

            while (result.next()) {
                if (!StringUtils.isNullOrEmpty(result.getString("acctStatus")) && result.getString("acctSchemeType").equalsIgnoreCase("LAA")) {
                    if (!StringUtils.isNullOrEmpty(result.getString("acctStatus")) && !result.getString("acctStatus").equalsIgnoreCase("1") && !result.getString("acctStatus").equalsIgnoreCase("10")) {
                        String acctid = result.getString("hostAcctId");
                        accountID.add(acctid);
                    }
                }
            }
            event.npaWrittenoff = String.join(",", accountID);

            return event.npaWrittenoff;
        } catch (SQLException e) {
            System.out.println("npa details in DDA table are not correct");
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.npaWrittenoff;
    }

    public static String gettrancode(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        PreparedStatement psmt1 = null;
        ResultSet result1 = null;
        event.is26trancodeExists = "N";
        event.is27trancodeExists = "N";
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT TRAN_CODE FROM SCENARIO26_TRAN_CODE WHERE TRAN_CODE = '" + event.getTranType() + "'";
            String query1 = "SELECT TRAN_CODE FROM SCENARIO27_TRAN_CODE WHERE TRAN_CODE = '" + event.getTranType() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                event.is26trancodeExists = "Y";
            }
            psmt1 = con.prepareStatement(query1);
            result1 = psmt1.executeQuery();

            while (result1.next()) {
                event.is27trancodeExists = "Y";
            }

        } catch (SQLException e) {
            event.is26trancodeExists = "N";
            event.is27trancodeExists = "N";
            System.out.println("trancode does not exists in SCENARIO26_TRAN_CODE or  SCENARIO27_TRAN_CODE table");
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (psmt1 != null) {
                    psmt1.close();
                }
                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }
                if (result1 != null) {
                    result1.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.is26trancodeExists;
    }

    public static String getMatchedeid(NFT_AcctopeningEvent event) {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        event.matchedEid = "N";
        try {
            String query = "select MobileNumber, PassportNumber, EmiratesID, Office_Phone_Number, Communication_Phone_Number, Home_Country_Phone_Number, Residence_Phone_Number, FORMAT(DateOfBirth, 'dd-MM-yyyy') DateOfBirth from IRIS_REJECTED_APPL where MobileNumber = ? or PassportNumber = ? or EmiratesID = ? or Office_Phone_Number = ? or Communication_Phone_Number = ? or Home_Country_Phone_Number = ? or Residence_Phone_Number = ? or FORMAT(DateOfBirth, 'dd-MM-yyyy') = ?";
            connection = Rdbms.getAppConnection();
            ps = connection.prepareStatement(query);
            ps.setString(1, event.phNo);
            ps.setString(2, event.passportNo);
            ps.setString(3, event.eida);
            ps.setString(4, event.offLandline);
            ps.setString(5, event.commLandline);
            ps.setString(6, event.perLandline);
            ps.setString(7, event.resLandline);
            ps.setString(8, datetostring(event.dob));
            rs = ps.executeQuery();

            while (rs.next()) {
                if (!StringUtils.isNullOrEmpty(rs.getString("MobileNumber")) && rs.getString("MobileNumber").equalsIgnoreCase(event.phNo)) {
                    event.setMatchedPhno("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("PassportNumber")) && rs.getString("PassportNumber").equalsIgnoreCase(event.passportNo)) {
                    event.setMatchedPpno("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("EmiratesID")) && rs.getString("EmiratesID").equalsIgnoreCase(event.eida)) {
                    event.setMatchedEid("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("Office_Phone_Number")) && rs.getString("Office_Phone_Number").equalsIgnoreCase(event.offLandline)) {
                    event.setMatchedOffno("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("Communication_Phone_Number")) && rs.getString("Communication_Phone_Number").equalsIgnoreCase(event.commLandline)) {
                    event.setMatchedCommno("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("Home_Country_Phone_Number")) && rs.getString("Home_Country_Phone_Number").equalsIgnoreCase(event.perLandline)) {
                    event.setMatchedPerno("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("Residence_Phone_Number")) && rs.getString("Residence_Phone_Number").equalsIgnoreCase(event.resLandline)) {
                    event.setMatchedResno("Y");
                }
                if (!StringUtils.isNullOrEmpty(rs.getString("DateOfBirth")) && rs.getString("DateOfBirth").equalsIgnoreCase(datetostring(event.dob))) {
                    event.setMatchedDob("Y");
                }
            }
            return event.matchedEid;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.matchedEid;
    }

    public static String datetostring(java.sql.Timestamp date) {
        String stringdate = null;
        if (date != null) {
            stringdate = new SimpleDateFormat("dd-MM-YYYY").format(date);
        }
        return stringdate;
    }

    public static void getInsertData(List<String> eventIdList) {
        /*****
         * This method is to get the all eventList of Acctinq event
         * for which scn new 11 is triggering
         *
         */
        String user_id = "";
        String acct_id = "";
        String cust_id = "";
        Timestamp rcre_time = null;

        Hocon hocon = new Hocon();
        hocon.loadFromContext("snew11.conf");

        String selectQueryFromEventTable = hocon.getString("selectQueryFromEventTable");
        String insertQueryIntoUserMapping = hocon.getString("insertQueryIntoUserMapping");
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {


                try (PreparedStatement select_stmt = connection.prepareStatement(selectQueryFromEventTable);
                     PreparedStatement insert_stmt = connection.prepareStatement(insertQueryIntoUserMapping)) {

                    for (int i = 0; i < eventIdList.size(); i++) {

                        select_stmt.setString(1, eventIdList.get(i));

                        try (ResultSet resultSet = select_stmt.executeQuery()) {

                            if (resultSet.next()) {
                                user_id = resultSet.getString("user_id");
                                acct_id = resultSet.getString("account_id");
                                cust_id = resultSet.getString("cust_id");
                                rcre_time = resultSet.getTimestamp("sys_time");
                            }

                            insert_stmt.setString(1, user_id);
                            insert_stmt.setString(2, acct_id);
                            insert_stmt.setString(3, cust_id);
                            insert_stmt.setTimestamp(4, rcre_time);

                            try {
                                insert_stmt.executeUpdate();
                                connection.commit();
                            } catch (Exception e) {
                                logger.info("Exception occured while inserting data in UserMapping Table----> " + e.getMessage());
                            }

                        }
                    }

                }
                connection.close();
            }
        } catch (Exception e) {
            logger.info("Exception occured while inserting data in UserMapping Table----> " + e.getMessage());
        }
    }

    public static String getUserIdForAccountId(NFT_AcctStatusChangeEvent event) {
        /****
         * This method is to get the User_Id for the specific Account_Id, it will check from custom table
         *
         */
        if (!event.getFlag().equals("N")) {

            DataFetchCreateEvent.generateEventForDistinctUser(event.accountId, event);
        }
        return "Success";
    }

    public static String getUserIdForCustomerId(NFT_StaticinfochangeEvent event) {
        /****
         * This method is to get the User_Id for the specific Customer_Id, it will check from custom table
         *
         */
        if (!event.getFlag().equals("N")) {
            StaticInfoChangeEventCreation.generateEventForDistinctUser(event.custId, event);
        }
        return "Success";
    }


    public static String getcompmis(FT_AccounttxnEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        event.compmis1 = null;
        event.compmis2 = null;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT COMPMIS1, COMPMIS2 FROM CUSTOMER WHERE hostCustId = '" + event.getCustId() + "'";

            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                if (!StringUtils.isNullOrEmpty(result.getString("COMPMIS1"))) {
                    event.compmis1 = result.getString("COMPMIS1");
                }
                if (!StringUtils.isNullOrEmpty(result.getString("COMPMIS2"))) {
                    event.compmis2 = result.getString("COMPMIS2");
                }
            }
        } catch (SQLException e) {
            System.out.println("compmis1 or compmis2 value does not exists in CUSTOMER table");
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.compmis1;
    }

    public static Timestamp createdNupdatedOn(NFT_AcctopeningEvent event) {
        event.createdOn = event.getEventDate();
        event.updatedOn = event.getEventDate();
        return event.createdOn;
    }

    public static Double getdenomMulInflow(FT_VltDenomcoretxnEvent event) {
        String denom = event.getDenomcode();
        denom = denom.substring(3);
        Double denominflow = Double.parseDouble(denom) * event.getInflow();
        event.setDenomMulInflow(denominflow);
        return denominflow;
    }

    public static String getIsstaff(NFT_AcctInqEvent event) {
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        PreparedStatement psmt1 = null;
        ResultSet result1 = null;
        PreparedStatement psmt2 = null;
        ResultSet result2 = null;
        PreparedStatement psmt3 = null;
        ResultSet result3 = null;
        event.isStaff = null;
        event.acctStatus = null;
        event.compmis1 =  null;
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT STAFF , COMPMIS1 FROM CUSTOMER WHERE hostCustId = '" + event.getCustId() + "'";
            String queryA = "SELECT acctStatus FROM DDA WHERE hostAcctId = '" + event.getAccountId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                if (!StringUtils.isNullOrEmpty(result.getString("STAFF")) || !StringUtils.isNullOrEmpty(result.getString("COMPMIS1"))) {
                    event.isStaff = result.getString("STAFF");
                    event.compmis1  = result.getString("COMPMIS1");
                }else {
                    event.isStaff = "N";
                    event.compmis1  = "NUL";}
            }

            psmt1 = con.prepareStatement(queryA);
            result1 = psmt1.executeQuery();

            while (result1.next()) {
                if (!StringUtils.isNullOrEmpty(result1.getString("acctStatus"))) {
                    event.acctStatus = result1.getString("acctStatus");
                }
                else event.acctStatus = "NULL";
            }
	
           if(event.isStaff.equalsIgnoreCase("Y") || (!event.acctStatus.equalsIgnoreCase("1") && !event.acctStatus.equalsIgnoreCase("10"))  && event.compmis1.equalsIgnoreCase("RBG")){
               String query2 = "SELECT DISTINCT TOP 9  account_id , event_date , is_eligible FROM\n" +
                        "(   SELECT account_id, event_date, is_staff, acct_status , user_id , is_eligible, ROW_NUMBER() OVER(PARTITION BY account_id ORDER BY event_date DESC) events\n" +
                        "    FROM EVENT_NFT_ACCT_INQ where acct_status != '10' and acct_status != '1' and compmis1 = 'RBG' and user_id = '" + event.userId + "' or is_staff = 'Y' and compmis1 = 'RBG' and user_id = '" + event.userId + "'\n" +
                        ") A\n" +
                        "WHERE A.events = 1\n" +
                        "ORDER BY event_date DESC";
                psmt2 = con.prepareStatement(query2);
                result2 = psmt2.executeQuery();
                int count = 0;
                String isLastY = "N";
                Timestamp firsteventts = null;
                Timestamp lastevntts = event.eventDate;
                while (result2.next()) {
                    if(result2.getString("is_eligible").equalsIgnoreCase("Y")){
                        firsteventts = result2.getTimestamp("event_date");
                        isLastY = "Y";
                        break;
                    } else if(!result2.getString("account_id").equalsIgnoreCase(event.accountId)){
                        count++;
                        if (count == 1) {
                            isLastY = result2.getString("is_eligible");
                        }
                        if (count == 9) {
                            firsteventts = result2.getTimestamp("event_date");
                        }
                    }else break;
                }
                String query3 = "select RCRE_TIME from USER_ACCT_MAPPING where UserID = '" +event.userId + "' ORDER BY RCRE_TIME ASC";
                psmt3 = con.prepareStatement(query3);
                result3 = psmt3.executeQuery();
                Timestamp firsttsfromUAM = null;
                while (result3.next()){
                    firsttsfromUAM = result3.getTimestamp("RCRE_TIME");
                    break;
                }

                if (lastevntts != null && firsteventts != null) {
                    if (firsteventts.before(lastevntts)) {
                        long millis = lastevntts.getTime() - firsteventts.getTime();
                        int hours = (int) (millis / (1000 * 3600));
                        if (hours < 48 && isLastY.equalsIgnoreCase("N")) {
                            event.setIsEligible("Y");
                        } else if (firsttsfromUAM != null) {
                            if (firsttsfromUAM.before(lastevntts)) {
                                long millisec = lastevntts.getTime() - firsttsfromUAM.getTime();
                                int hour = (int) (millisec / (1000 * 3600));
                                if (isLastY.equalsIgnoreCase("Y") && hour < 360) {
                                    event.setIsEligible("Y");
                                }
                            } else event.setIsEligible("N");
                        } else event.setIsEligible("N");
                    }else event.setIsEligible("N");
                }else event.setIsEligible("N");
            }else event.setIsEligible("N");
        } catch (SQLException e) {
            System.out.println("staff and acctStatus are not derived");
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (psmt != null) {
                    psmt.close();
                }
                if (result != null) {
                    result.close();
                }
                if (psmt1 != null) {
                    psmt1.close();
                }
                if (result1 != null) {
                    result1.close();
                }
                if (psmt2 != null) {
                    psmt2.close();
                }
                if (result2 != null) {
                    result2.close();
                }
                if (psmt3 != null) {
                    psmt3.close();
                }
                if (result3 != null) {
                    result3.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return event.isStaff;
    }
}
