package clari5.custom.customScn.scenario30;

public class CustomerResultPojo {
    private String new_email;
    private String new_mobile;
    private String new_cxcif;
    private String new_eid;
    private String new_passport;
    private String new_custName;
    private String new_custType;
    private String new_compmis1;
    private String new_compmis2;
    private String eventid;
    private String matched_email;
    private String matched_mobile;
    private String old_cif;
    private String old_compmis1;
    private String old_compmis2;
    private String old_cust_name;
    private String old_passportNo;
    private String old_eid;
    private String old_email;
    private String old_mobile;
    private String old_custType;

    public String getNew_email() {
        return new_email;
    }

    public void setNew_email(String new_email) {
        this.new_email = new_email;
    }

    public String getNew_mobile() {
        return new_mobile;
    }

    public void setNew_mobile(String new_mobile) {
        this.new_mobile = new_mobile;
    }

    public String getNew_cxcif() {
        return new_cxcif;
    }

    public void setNew_cxcif(String new_cxcif) {
        this.new_cxcif = new_cxcif;
    }

    public String getNew_eid() {
        return new_eid;
    }

    public void setNew_eid(String new_eid) {
        this.new_eid = new_eid;
    }

    public String getNew_passport() {
        return new_passport;
    }

    public void setNew_passport(String new_passport) {
        this.new_passport = new_passport;
    }

    public String getNew_custName() {
        return new_custName;
    }

    public void setNew_custName(String new_custName) {
        this.new_custName = new_custName;
    }

    public String getNew_custType() {
        return new_custType;
    }

    public void setNew_custType(String new_custType) {
        this.new_custType = new_custType;
    }

    public String getNew_compmis1() {
        return new_compmis1;
    }

    public void setNew_compmis1(String new_compmis1) {
        this.new_compmis1 = new_compmis1;
    }

    public String getNew_compmis2() {
        return new_compmis2;
    }

    public void setNew_compmis2(String new_compmis2) {
        this.new_compmis2 = new_compmis2;
    }

    public String getOld_eid() {
        return old_eid;
    }

    public void setOld_eid(String old_eid) {
        this.old_eid = old_eid;
    }


    public String getOld_custType() {
        return old_custType;
    }

    public void setOld_custType(String old_custType) {
        this.old_custType = old_custType;
    }

    public String getMatched_email() {
        return matched_email;
    }

    public void setMatched_email(String matched_email) {
        this.matched_email = matched_email;
    }

    public String getMatched_mobile() {
        return matched_mobile;
    }

    public void setMatched_mobile(String matched_mobile) {
        this.matched_mobile = matched_mobile;
    }

    public String getOld_cif() {
        return old_cif;
    }

    public void setOld_cif(String old_cif) {
        this.old_cif = old_cif;
    }

    public String getOld_compmis1() {
        return old_compmis1;
    }

    public void setOld_compmis1(String old_compmis1) {
        this.old_compmis1 = old_compmis1;
    }

    public String getOld_compmis2() {
        return old_compmis2;
    }

    public void setOld_compmis2(String old_compmis2) {
        this.old_compmis2 = old_compmis2;
    }

    public String getOld_cust_name() {
        return old_cust_name;
    }

    public void setOld_cust_name(String old_cust_name) {
        this.old_cust_name = old_cust_name;
    }

    public String getOld_passportNo() {
        return old_passportNo;
    }

    public void setOld_passportNo(String old_passportNo) {
        this.old_passportNo = old_passportNo;
    }

    public String getOld_email() {
        return old_email;
    }

    public void setOld_email(String old_email) {
        this.old_email = old_email;
    }

    public String getOld_mobile() {
        return old_mobile;
    }

    public void setOld_mobile(String old_mobile) {
        this.old_mobile = old_mobile;
    }
    public String getEventid() {
        return eventid;
    }

    public void setEventid(String eventid) {
        this.eventid = eventid;
    }


}