// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_FULFILLMENT_APPL", Schema="rice")
public class NFT_FulfillmentApplEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String mob;
       @Field(size=10) public String bmMismatch;
       @Field public java.sql.Timestamp dob;
       @Field(size=10) public String ppMismatch;
       @Field(size=50) public String applRefNum;
       @Field(size=20) public String email;
       @Field(size=10) public String eidMismatch;


    @JsonIgnore
    public ITable<NFT_FulfillmentApplEvent> t = AEF.getITable(this);

	public NFT_FulfillmentApplEvent(){}

    public NFT_FulfillmentApplEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("FulfillmentAppl");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setMob(json.getString("mob"));
            setBmMismatch(json.getString("bm_mismatch"));
            setDob(EventHelper.toTimestamp(json.getString("dob")));
            setPpMismatch(json.getString("pp_mismatch"));
            setApplRefNum(json.getString("appl_ref_num"));
            setEmail(json.getString("email"));
            setEidMismatch(json.getString("eid_mismatch"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NFA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getMob(){ return mob; }

    public String getBmMismatch(){ return bmMismatch; }

    public java.sql.Timestamp getDob(){ return dob; }

    public String getPpMismatch(){ return ppMismatch; }

    public String getApplRefNum(){ return applRefNum; }

    public String getEmail(){ return email; }

    public String getEidMismatch(){ return eidMismatch; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setMob(String val){ this.mob = val; }
    public void setBmMismatch(String val){ this.bmMismatch = val; }
    public void setDob(java.sql.Timestamp val){ this.dob = val; }
    public void setPpMismatch(String val){ this.ppMismatch = val; }
    public void setApplRefNum(String val){ this.applRefNum = val; }
    public void setEmail(String val){ this.email = val; }
    public void setEidMismatch(String val){ this.eidMismatch = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String terminalKey= h.getCxKeyGivenHostKey(WorkspaceName.TERMINAL, getHostId(), this.applRefNum);
        wsInfoSet.add(new WorkspaceInfo("Terminal", terminalKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_FulfillmentAppl");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "FulfillmentAppl");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}