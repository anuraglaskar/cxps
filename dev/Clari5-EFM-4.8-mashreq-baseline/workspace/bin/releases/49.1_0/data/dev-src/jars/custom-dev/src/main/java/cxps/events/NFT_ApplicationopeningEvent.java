// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_APPLICATIONOPENING", Schema="rice")
public class NFT_ApplicationopeningEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field public java.sql.Timestamp newApplopendate;
       @Field(size=20) public String oldDeviceid;
       @Field public java.sql.Timestamp oldApplopendate;
       @Field(size=20) public String newIpaddress;
       @Field(size=20) public String oldProduct;
       @Field(size=20) public String matchedTradelicense;
       @Field(size=20) public String oldEmail;
       @Field(size=20) public String newAppRefno;
       @Field(size=20) public String oldTradelincense;
       @Field(size=20) public String newMobile;
       @Field(size=20) public String matchedEid;
       @Field(size=20) public String oldChannel;
       @Field(size=20) public String newPassport;
       @Field(size=20) public String oldCustname;
       @Field(size=20) public String oldMobile;
       @Field(size=20) public String newDeviceid;
       @Field(size=20) public String oldIpaddress;
       @Field(size=20) public String oldCif;
       @Field(size=20) public String oldEid;
       @Field(size=20) public String oldAppRefno;
       @Field(size=20) public String newCustname;
       @Field(size=20) public String matchedScoreEid;
       @Field(size=20) public String newProduct;
       @Field(size=20) public String matchedEmail;
       @Field(size=20) public String newTradelincense;
       @Field(size=20) public String oldPassport;
       @Field(size=20) public String newChannel;
       @Field(size=20) public String matchedMobile;
       @Field(size=20) public String newCif;
       @Field(size=20) public String newEid;
       @Field(size=20) public String newEmail;


    @JsonIgnore
    public ITable<NFT_ApplicationopeningEvent> t = AEF.getITable(this);

	public NFT_ApplicationopeningEvent(){}

    public NFT_ApplicationopeningEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Applicationopening");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setNewApplopendate(EventHelper.toTimestamp(json.getString("new_applopendate")));
            setOldDeviceid(json.getString("old_deviceid"));
            setOldApplopendate(EventHelper.toTimestamp(json.getString("old_applopendate")));
            setNewIpaddress(json.getString("new_ipaddress"));
            setOldProduct(json.getString("old_product"));
            setMatchedTradelicense(json.getString("matched_tradelicense"));
            setOldEmail(json.getString("old_email"));
            setNewAppRefno(json.getString("new_app_refno"));
            setOldTradelincense(json.getString("old_tradelincense"));
            setNewMobile(json.getString("new_mobile"));
            setMatchedEid(json.getString("matched_eid"));
            setOldChannel(json.getString("old_channel"));
            setNewPassport(json.getString("new_passport"));
            setOldCustname(json.getString("old_custname"));
            setOldMobile(json.getString("old_mobile"));
            setNewDeviceid(json.getString("new_deviceid"));
            setOldIpaddress(json.getString("old_ipaddress"));
            setOldCif(json.getString("old_cif"));
            setOldEid(json.getString("old_eid"));
            setOldAppRefno(json.getString("old_app_refno"));
            setNewCustname(json.getString("new_custname"));
            setMatchedScoreEid(json.getString("matched_score_eid"));
            setNewProduct(json.getString("new_product"));
            setMatchedEmail(json.getString("matched_email"));
            setNewTradelincense(json.getString("new_tradelincense"));
            setOldPassport(json.getString("old_passport"));
            setNewChannel(json.getString("new_channel"));
            setMatchedMobile(json.getString("matched_mobile"));
            setNewCif(json.getString("new_cif"));
            setNewEid(json.getString("new_eid"));
            setNewEmail(json.getString("new_email"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "APP"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public java.sql.Timestamp getNewApplopendate(){ return newApplopendate; }

    public String getOldDeviceid(){ return oldDeviceid; }

    public java.sql.Timestamp getOldApplopendate(){ return oldApplopendate; }

    public String getNewIpaddress(){ return newIpaddress; }

    public String getOldProduct(){ return oldProduct; }

    public String getMatchedTradelicense(){ return matchedTradelicense; }

    public String getOldEmail(){ return oldEmail; }

    public String getNewAppRefno(){ return newAppRefno; }

    public String getOldTradelincense(){ return oldTradelincense; }

    public String getNewMobile(){ return newMobile; }

    public String getMatchedEid(){ return matchedEid; }

    public String getOldChannel(){ return oldChannel; }

    public String getNewPassport(){ return newPassport; }

    public String getOldCustname(){ return oldCustname; }

    public String getOldMobile(){ return oldMobile; }

    public String getNewDeviceid(){ return newDeviceid; }

    public String getOldIpaddress(){ return oldIpaddress; }

    public String getOldCif(){ return oldCif; }

    public String getOldEid(){ return oldEid; }

    public String getOldAppRefno(){ return oldAppRefno; }

    public String getNewCustname(){ return newCustname; }

    public String getMatchedScoreEid(){ return matchedScoreEid; }

    public String getNewProduct(){ return newProduct; }

    public String getMatchedEmail(){ return matchedEmail; }

    public String getNewTradelincense(){ return newTradelincense; }

    public String getOldPassport(){ return oldPassport; }

    public String getNewChannel(){ return newChannel; }

    public String getMatchedMobile(){ return matchedMobile; }

    public String getNewCif(){ return newCif; }

    public String getNewEid(){ return newEid; }

    public String getNewEmail(){ return newEmail; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setNewApplopendate(java.sql.Timestamp val){ this.newApplopendate = val; }
    public void setOldDeviceid(String val){ this.oldDeviceid = val; }
    public void setOldApplopendate(java.sql.Timestamp val){ this.oldApplopendate = val; }
    public void setNewIpaddress(String val){ this.newIpaddress = val; }
    public void setOldProduct(String val){ this.oldProduct = val; }
    public void setMatchedTradelicense(String val){ this.matchedTradelicense = val; }
    public void setOldEmail(String val){ this.oldEmail = val; }
    public void setNewAppRefno(String val){ this.newAppRefno = val; }
    public void setOldTradelincense(String val){ this.oldTradelincense = val; }
    public void setNewMobile(String val){ this.newMobile = val; }
    public void setMatchedEid(String val){ this.matchedEid = val; }
    public void setOldChannel(String val){ this.oldChannel = val; }
    public void setNewPassport(String val){ this.newPassport = val; }
    public void setOldCustname(String val){ this.oldCustname = val; }
    public void setOldMobile(String val){ this.oldMobile = val; }
    public void setNewDeviceid(String val){ this.newDeviceid = val; }
    public void setOldIpaddress(String val){ this.oldIpaddress = val; }
    public void setOldCif(String val){ this.oldCif = val; }
    public void setOldEid(String val){ this.oldEid = val; }
    public void setOldAppRefno(String val){ this.oldAppRefno = val; }
    public void setNewCustname(String val){ this.newCustname = val; }
    public void setMatchedScoreEid(String val){ this.matchedScoreEid = val; }
    public void setNewProduct(String val){ this.newProduct = val; }
    public void setMatchedEmail(String val){ this.matchedEmail = val; }
    public void setNewTradelincense(String val){ this.newTradelincense = val; }
    public void setOldPassport(String val){ this.oldPassport = val; }
    public void setNewChannel(String val){ this.newChannel = val; }
    public void setMatchedMobile(String val){ this.matchedMobile = val; }
    public void setNewCif(String val){ this.newCif = val; }
    public void setNewEid(String val){ this.newEid = val; }
    public void setNewEmail(String val){ this.newEmail = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.newCif);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Applicationopening");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Applicationopening");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
