package clari5.custom.customScn.scenario30;

import cxps.apex.utils.CxpsLogger;
import clari5.rdbms.Rdbms;
import clari5.platform.rdbms.RDBMSSession;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class CustEodReminder extends TimerTask {

    private static final CxpsLogger cxpslogger=CxpsLogger.getLogger(CustEodReminder.class);
    private static ThreadDetails threadDetails=new ThreadDetails();

    public static ThreadDetails getThreadDetails() {
        return threadDetails;
    }

    public static void setThreadDetails(ThreadDetails threadDetails) {
        CustEodReminder.threadDetails = threadDetails;
    }

    public void run() {
        try (RDBMSSession rdbmsSession = Rdbms.getAppSession();
             Connection connection = rdbmsSession.getCxConnection();) {
                cxpslogger.info("[CustEodReminder] Task has been scheduled..");

                MqWriter mqWriter = new MqWriter();
                mqWriter.writeJson();
                cxpslogger.info("[CustEodReminder] Writing JSON is completed.. Hence Updating the thread status" + threadDetails.getThrdId() + " " + threadDetails.getCurrentThreadName());
                PreparedStatement updateStatement = connection.prepareStatement(threadDetails.getCurrentUpdateQuery());
                updateStatement.setString(1, "C");
                updateStatement.setTimestamp(2,new java.sql.Timestamp(new java.util.Date().getTime()));
                updateStatement.setString(3, threadDetails.getCurrentThreadName());
                updateStatement.setDate(4, threadDetails.getThreadDate());
                updateStatement.executeUpdate();
                connection.commit();
                threadDetails=null;
        }catch(Exception e) {
            e.printStackTrace();
        }

    }
    public static void insert(String threadName, long threadId, java.sql.Date date,String insertQuery){
        try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();
            PreparedStatement insertQueryStatement=connection.prepareStatement(insertQuery);){
            insertQueryStatement.setString(1,threadName);// thread name
            insertQueryStatement.setLong(2,threadId); // id
            insertQueryStatement.setDate(3,date);
            insertQueryStatement.setString(4,"NEW");
            insertQueryStatement.setString(5,threadName+"-"+threadId+"-"+date);
            insertQueryStatement.setTimestamp(6,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.setTimestamp(7,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.executeUpdate();
            connection.commit();
            cxpslogger.info("[MultiCustDaemon] Insertion Completed");
        } catch (SQLException e) {
            cxpslogger.error("[MultiCustDaemon] insert()-> insertion Failed"+ e.getCause() + " "+e.getMessage());
        }
    }
    public static  void callonEODForCust(int hrsToExecute, int minToExecute, int secToExecute, String threadName, long threadId, java.sql.Date date1, String insertQuery, String updateQuery,String selectQuery){
        try {
            cxpslogger.info("[CustEodReminder] Calling EODForCust...");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH,0);
            calendar.add(Calendar.YEAR,0);
            //comment this one line
            //calendar.set(Calendar.DATE,26);
            calendar.set(Calendar.HOUR_OF_DAY,hrsToExecute);
            calendar.set(Calendar.MINUTE,minToExecute);
            calendar.set(Calendar.SECOND,secToExecute);
            Date date= calendar.getTime();
            Timer timer = new Timer();
            threadDetails.setThreadDate(date1);
            threadDetails.setCurrentThreadName(threadName);
            threadDetails.setCurrentUpdateQuery(updateQuery);
            threadDetails.setThrdId(threadId);
            threadDetails.setInsrtQry(insertQuery);
            threadDetails.setSelectQuery(selectQuery);
            CustEodReminder.setThreadDetails(threadDetails);

            while (true){
                Date currentDate=new Date();
                if(date.compareTo(currentDate)>0){
                    timer.schedule(new CustEodReminder(), date);
                    cxpslogger.info("[AcctEodReminder] Call on EOD..Time has been scheduled");
                    int timetosleeep = (24 * 60 * 60 * 1000);
                    System.out.println("[CustEodReminder] Going to sleep...");
                    Thread.sleep(timetosleeep);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
