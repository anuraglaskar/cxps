cxps.events.event.nft-fulfillment-appl{  
  table-name : EVENT_NFT_FULFILLMENT_APPL
  event-mnemonic: NFA
  workspaces : {
    TERMINAL: "appl-ref-num"
  }
event-attributes : {
appl-ref-num: {db:true ,raw_name : appl_ref_num ,type:"string:50"}
eid-mismatch: {db:true ,raw_name : eid_mismatch ,type:"string:10"}
pp-mismatch: {db :true ,raw_name : pp_mismatch ,type : "string:10"}
bm-mismatch: {db:true ,raw_name : bm_mismatch ,type:"string:10"}
dob: {db:true ,raw_name : dob ,type:timestamp}
email: {db:true ,raw_name : email ,type:"string:20"}
mob: {db:true ,raw_name : mob ,type:"string:20"}
}
}
