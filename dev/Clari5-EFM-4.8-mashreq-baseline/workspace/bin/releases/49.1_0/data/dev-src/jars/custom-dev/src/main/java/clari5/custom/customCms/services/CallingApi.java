package clari5.custom.customCms.services;

import clari5.custom.customCms.CustomCmsServlet;
import clari5.platform.logger.CxpsLogger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class CallingApi {


    private static CxpsLogger logger = CxpsLogger.getLogger(CallingApi.class);

    public String callingMashreqApi(String urlFromConfgFile, String req, int timeOutExc) {

        System.out.println("inside the callingMashreqApi method and this is the url=-- "+urlFromConfgFile+"and request is --"+req);

        /*logger.debug("inside the callingMashreqApi method ");
        logger.debug("this is api url",urlFromConfgFile);*/


        String s= null;

        try {

            URL url = new URL(urlFromConfgFile);
            System.out.println("this is URL in --"+url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            System.out.println("HTTP URLL CONNECTION =="+conn);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/xml");
            conn.setDoOutput(true);
            conn.setReadTimeout(timeOutExc);
            conn.setConnectTimeout(timeOutExc);
            System.out.println("connection output stream --"+conn.getOutputStream());
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            System.out.println("Dataoutput stream ---"+wr);

            wr.writeBytes(req);
            wr.flush();
            wr.close();
            System.out.println("DataInput stream ---"+conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            System.out.println("Buffer Reader ----"+br);
            s = br.readLine();
            System.out.println("final string SSSS---"+s);
            logger.debug("this is resonse from mashreq api------------------------",s);


        } catch (SocketTimeoutException e) {

            s="time out Exception";
            logger.error("time out exception");
            return s;


        }catch (ProtocolException e)
        {

            logger.error("protocal excetion");
            e.printStackTrace();
        }
        catch (IOException e)
        {

             logger.error("IOException");
             e.printStackTrace();
        }

        return s;

        //below is the expected response



//        return "<SOAPMessage>\n" +
//                "<Context>\n" +
//                "\t\t<Namespace xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"/>\n" +
//                "\t</Context>\n" +
//                "\t<NS1:Header xmlns:NS1=\"http://schemas.xmlsoap.org/soap/envelope/\"/>\n" +
//                "\t<Body>\n" +
//                "\t\t<NS2:EAIServices xmlns:NS2=\"http://www.mbcdm.creditcardservices.com/cardBlocking\">\n" +
//                "\t\t\t<NS2:Header>\n" +
//                "\t\t\t\t<NS3:MsgVersion xmlns:NS3=\"http://www.mbcdm.header.com\">1.0</NS3:MsgVersion>\n" +
//                "\t\t\t\t<NS4:SrcAppType xmlns:NS4=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS5:SrcAppId xmlns:NS5=\"http://www.mbcdm.header.com\">PRDAPPS</NS5:SrcAppId>\n" +
//                "\t\t\t\t<NS6:SrcAppReqId xmlns:NS6=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS7:SrcAppHostId xmlns:NS7=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS8:SrcMsgId xmlns:NS8=\"http://www.mbcdm.header.com\">B190904195550003</NS8:SrcMsgId>\n" +
//                "\t\t\t\t<NS9:SrcAppTimestamp xmlns:NS9=\"http://www.mbcdm.header.com\">2019-09-04T19:55:50.621+04:00</NS9:SrcAppTimestamp>\n" +
//                "\t\t\t\t<NS10:SrcAppSessionId xmlns:NS10=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS11:SrvCode xmlns:NS11=\"http://www.mbcdm.header.com\">TDBBLK</NS11:SrvCode>\n" +
//                "\t\t\t\t<NS12:SrvName xmlns:NS12=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS13:SrvRunCode xmlns:NS13=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS14:SrvOpCode xmlns:NS14=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS15:TargetApp xmlns:NS15=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS16:EAITimestamp xmlns:NS16=\"http://www.mbcdm.header.com\">0001-01-01T00:00:00</NS16:EAITimestamp>\n" +
//                "\t\t\t\t<NS17:TrackingId xmlns:NS17=\"http://www.mbcdm.header.com\">7785ec3e-cf2c-11e9-85f3-ac1856060000</NS17:TrackingId>\n" +
//                "\t\t\t\t<NS18:OrgId xmlns:NS18=\"http://www.mbcdm.header.com\">AE</NS18:OrgId>\n" +
//                "\t\t\t\t<NS19:InstanceId xmlns:NS19=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS20:Status xmlns:NS20=\"http://www.mbcdm.header.com\">S</NS20:Status>\n" +
//                "\t\t\t\t<NS21:UserId xmlns:NS21=\"http://www.mbcdm.header.com\">EFMUSER</NS21:UserId>\n" +
//                "\t\t\t\t<NS22:SecurityInfo xmlns:NS22=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS23:Language xmlns:NS23=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS24:AddlData1 xmlns:NS24=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t\t<NS25:AddlData2 xmlns:NS25=\"http://www.mbcdm.header.com\"/>\n" +
//                "\t\t\t</NS2:Header>\n" +
//                "\t\t\t<NS2:Body>\n" +
//                "\t\t\t\t<NS2:ExceptionDetails>\n" +
//                "\t\t\t\t\t<NS26:ErrorCode xmlns:NS26=\"http://www.mbcdm.header.com\">EAI-SEL-BRK-000</NS26:ErrorCode>\n" +
//                "\t\t\t\t\t<NS27:ErrorDescription xmlns:NS27=\"http://www.mbcdm.header.com\">Success</NS27:ErrorDescription>\n" +
//                "\t\t\t\t</NS2:ExceptionDetails>\n" +
//                "\t\t\t</NS2:Body>\n" +
//                "\t\t</NS2:EAIServices>\n" +
//                "\t</Body>\n" +
//                "</SOAPMessage>\n";

    }
}