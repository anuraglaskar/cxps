package clari5.custom.customCms.freezUnfreez;

public class Response {


    private String status;
    private String ErrorCode;
    private String ErrorDescription;


    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getErrorCode() {
        return ErrorCode;
    }
    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }
    public String getErrorDescription() {
        return ErrorDescription;
    }
    public void setErrorDescription(String errorDescription) {
        ErrorDescription = errorDescription;
    }

}
