cxps.noesis.glossary.entity.tradenettable {
       db-name = tradenettable
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
                { name = emp_id, column = emp_id, type = "string:50"}
		{ name = leave_type, column = leave_type, type = "string:50"}
		{ name = leave_start_date, column = leave_start_date, type = timestamp}
		{ name = leave_end_date, column = leave_end_date, type = timestamp}
               ]
       }
