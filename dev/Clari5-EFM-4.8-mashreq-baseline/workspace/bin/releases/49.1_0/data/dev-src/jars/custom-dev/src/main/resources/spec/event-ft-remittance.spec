cxps.events.event.ft-remittance{  
  table-name : EVENT_FT_REMITTANCE
  event-mnemonic: FREM
  workspaces : {
    ACCOUNT : "account-id"
    NONCUSTOMER: "tran-ref-no"
  }
 event-attributes : {
account-id: {db:true ,raw_name : account_id ,type:"string:50"}
cif: {db:true ,raw_name : cif ,type:"string:50"}
rem-type: {db :true ,raw_name : rem_type ,type : "string:50"}
branch: {db:true ,raw_name : branch ,type:"string:50"}
tran-ref-no: {db:true ,raw_name : tran_ref_no ,type:"string:50"}
tran-curr: {db:true ,raw_name : tran_curr ,type:"string:50"}
tran-amt: {db:true ,raw_name : tran_amt ,type:"number:20,4"}
lcy-amount: {db:true ,raw_name : lcy_amount ,type:"number:20,4"}
rem-name: {db:true ,raw_name : rem_name ,type:"string:50"}
rem-add1: {db:true ,raw_name : rem_add1 ,type:"string:50"}
rem-add2: {db :true ,raw_name : rem_add2 ,type : "string:50"}
rem-add3: {db:true ,raw_name : rem_add3 ,type:"string:50"}
rem-city: {db:true ,raw_name : rem_city ,type:"string:50"}
rem-cntry-code: {db:true ,raw_name : rem_cntry_code ,type:"string:50"}
rem-id-no: {db:true ,raw_name : rem_id_no ,type:"string:50"}
rem-bank: {db:true ,raw_name : rem_bank ,type:"string:20"}
ben-name: {db:true ,raw_name : ben_name ,type:"string:50"}
ben-add1: {db:true ,raw_name : ben_add1 ,type:"string:50"}
ben-add2: {db :true ,raw_name : ben_add2 ,type : "string:50"}
ben-add3: {db:true ,raw_name : ben_add3 ,type:"string:50"}
ben-city: {db:true ,raw_name : ben_city ,type:"string:50"}
ben-cntry-code: {db:true ,raw_name : ben_cntry_code ,type:"string:50"}
benf-id-no: {db:true ,raw_name : benf_id_no ,type:"string:50"}
benf-bank: {db:true ,raw_name : benf_bank ,type:"string:50"}
ben-acct-no: {db:true ,raw_name : ben_acct_no ,type:"string:50"}
rem-acct-no: {db :true ,raw_name : rem_acct_no ,type : "string:50"}
ben-bic: {db:true ,raw_name : ben_bic ,type:"string:50"}
rem-bic: {db:true ,raw_name : rem_bic ,type:"string:50"}
trn-date: {db:true ,raw_name : trn_date ,type:timestamp}
rem-information1: {db:true ,raw_name : rem_information1 ,type:"string:50"}
rem-information2: {db:true ,raw_name : rem_information2 ,type:"string:50"}
rem-information3: {db:true ,raw_name : rem_information3 ,type:"string:50"}
rem-information4: {db:true ,raw_name : rem_information4 ,type:"string:50"}
}
}
