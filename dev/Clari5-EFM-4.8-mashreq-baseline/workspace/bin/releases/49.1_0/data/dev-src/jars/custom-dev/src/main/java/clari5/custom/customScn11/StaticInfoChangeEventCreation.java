package clari5.custom.customScn11;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.events.NFT_StaticinfochangeEvent;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

public class StaticInfoChangeEventCreation extends Thread {
    private static final CxpsLogger logger = CxpsLogger.getLogger(DataFetchCreateEvent.class);
    static ConcurrentLinkedQueue<JSONObject> queue = new ConcurrentLinkedQueue<>();
    static Hocon hocon;
    static String dataFetchFromUserMapTable;
    static int dDays;
    static String insertDataInAuditTable;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("snew11.conf");
        dataFetchFromUserMapTable = hocon.getString("selectQueryCustRelUser");
        insertDataInAuditTable = hocon.getString("customScnDetailsTracker");
        dDays = hocon.getInt("limitDaysForPushUp3");
    }

    public void run() {

        logger.info("Inside the process data for event creation");
        ConcurrentLinkedQueue<JSONObject> DataQueue = queue;

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(dataFetchFromUserMapTable)) {

                    while (!DataQueue.isEmpty()) {

                        JSONObject jsonObject = new JSONObject();
                        jsonObject = DataQueue.poll();

                        String custId = jsonObject.getString("custId");
                        NFT_StaticinfochangeEvent event = (NFT_StaticinfochangeEvent) jsonObject.get("eventJson");
                        HashSet distinctUserId = new HashSet();
                        statement.setString(1, custId);
                        Calendar cal = Calendar.getInstance();
                        Date cur_Date = new Date();
                        cal.setTime(cur_Date);
                        cal.add(Calendar.DATE, -dDays);
                        java.util.Date daysCheck = cal.getTime();

                        try (ResultSet resultSet = statement.executeQuery()) {
                            while (resultSet.next()) {

                                String acctId = resultSet.getString("Account");
                                String userId = resultSet.getString("UserID");
                                String customerId = resultSet.getString("CUST_ID");
                                java.sql.Timestamp rcreTime = resultSet.getTimestamp("RCRE_TIME");
                                Date rcreDate = new Date(rcreTime.getTime());
                                String entity_type = event.entityType;

                                /****
                                 * Checking the static info change  happened with in 15 days
                                 */

                                if (rcreDate.after(daysCheck) && rcreDate.before(cur_Date) && !distinctUserId.contains(userId)) {

                                    dataInsertInAuditTable(connection,acctId,customerId,userId,entity_type);
                                    distinctUserId.add(userId);
                                    //JSONObject eventJSON = getJSONFormat(event, userId);
                                    // eventJSON.put("user_id",userId);
                                   // String eventId = eventJSON.getString("event_id");
                                    //String jsonString = eventJSON.toString();
                                    //ECClient.enqueue("HOST", eventId, jsonString);

                                }

                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dataInsertInAuditTable(CxConnection connection, String acctId, String custId, String userId,String entity_type) {

        logger.info("Method is to insert the data in SCN11_DETAILS_TRACK table for static info change");

        try (PreparedStatement statement = connection.prepareStatement(insertDataInAuditTable)) {

            /**
             * inserting data from SCN11_DETAILS_TRACK but status will New_Info.
             */

            statement.setString(1, acctId);
            statement.setString(2, custId);
            statement.setString(3, userId);
            statement.setString(4, "New_Info");
            statement.setString(5,"NA");
            statement.setString(6,entity_type);

            statement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }


    public static void generateEventForDistinctUser(String custId, NFT_StaticinfochangeEvent event) {

        /*****
         * This method is to create to add the event and accountId
         * and Thread will pick the data
         *
         */

        logger.info("Inside the method which will return queue");

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("eventJson", event);
            jsonObject.put("custId", custId);
            queue.add(jsonObject);

            StaticInfoChangeEventCreation dataFetchCreateEvent = new StaticInfoChangeEventCreation();
            dataFetchCreateEvent.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
