package clari5.custom.customCms;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.jiraclient.JiraClient;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;

import clari5.platform.jira.JiraClientException;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import cxps.events.CustomDerivator;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


public class CustomCmsFormatter extends DefaultCmsFormatter {

    private static final CxpsLogger logger = CxpsLogger.getLogger(CustomCmsFormatter.class);

    static Hocon query;

    static {
        query = new Hocon();
        query.loadFromContext("query.conf");

        //TODO get the data from DB
        //TODO decide to get the list of USER in what format

	//  groupUserCasesMap = getUserDetails();
    }


    public CustomCmsFormatter() {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }


    protected String getCustId(String tableName, String eventId) {
        String custId = "";

        String sql = "SELECT cust_id FROM " + tableName + " WHERE event_id = ?";
        try (CxConnection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setString(i, eventId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    custId = rs.getString("cust_id") != null ? rs.getString("cust_id") : "Not Available";
                }
            }
        } catch (SQLException e) {
            String msg = e.getMessage();
            String[] searchFor= {"Invalid", "column", "name"};
           if(Arrays.stream(searchFor).allMatch(msg::contains)) {
               System.out.println("There is no cust_id for this event, returning Not Available");
               return "Not Available";
           }else
               e.getMessage();
        }
        return custId;
    }


    protected String getCardNo(String tableName, String eventId) {
        String cust_card_id = "";

        String sql = "SELECT cust_card_id FROM " + tableName + " WHERE event_id = ?";
        try (CxConnection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setString(i, eventId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    cust_card_id = rs.getString("cust_card_id") != null ? rs.getString("cust_card_id") : "Not Available";
                }
            }
        } catch (SQLException e) {
            String msg = e.getMessage();
            String[] searchFor= {"Invalid", "column", "name"};
            if(Arrays.stream(searchFor).allMatch(msg::contains)) {
                System.out.println("There is no card_no for this event, returning Not Available");
                return "Not Available";
            }else
                e.getMessage();
        }
        return cust_card_id;
    }


    protected static CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }


    public String getClassnameFromMnemonic(String mnemonic) {

        String className = "";
        try (CxConnection con = getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT class_name from WS_MNEMONIC WHERE mnemonic = ?");
            ps.setString(1, mnemonic);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                className = rs.getString("class_name") != null ? rs.getString("class_name")
                        : "";
            }
            return className.endsWith("Event") ? className : className + "Event";
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return className.endsWith("Event") ? className : className + "Event";
    }


    public static String getTableNamefromClassName(String className) {
        String tableName = "";
        try {
            for (Annotation annotation : Class.forName("cxps.events." + className).getAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();
                Method method = type.getMethod("Name");
                Object value = method.invoke(annotation, (Object[]) null);
                tableName = (String) value;
                System.out.println("table name from Event Name: " + tableName);
            }
            return tableName != null ? tableName : "";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return tableName != null ? tableName : "";
    }

    public CxJson populateCaseJson(Incident event) throws CmsException {

        String cust_card_id = "";
        String cust_id = "";


        Hocon hocon = new Hocon();
        hocon.loadFromContext("custom_field.conf");
        String cust_customfield = hocon.getString("cust_id_caseLevel");
        String card_customfield = hocon.getString("card_id_caseLevel");

        CxJson json = null;

        json = CMSUtility.getInstance(event.moduleId).populateCaseJson(event);

        String mnemonic = event.eventId.substring(0, event.eventId.indexOf("_"));
        String eventClassName = getClassnameFromMnemonic(mnemonic);
        String tableName = getTableNamefromClassName(eventClassName);

        if (event.factName.contains("MANUAL_ALERT") && event.caseentityName.equalsIgnoreCase("CUSTOMER")) {
            cust_id = event.caseentityId;
            logger.info("inside manual customer Id----" + cust_id);
        } else {

            cust_id = getCustId(tableName, event.eventId);

        }
        if (tableName.equalsIgnoreCase("EVENT_FT_ACCOUNTTXN") || tableName.equalsIgnoreCase("EVENT_NFT_BALANCE_ENQUIRY") || tableName.equalsIgnoreCase("EVENT_NFT_TIN_RESET")) {

            cust_card_id = getCardNo(tableName, event.eventId);

        }

        logger.info("final custid --" + cust_id + "and card no --" + cust_card_id);

        if (json.get("fields").hasKey(cust_customfield) || json.get("fields").hasKey(card_customfield)) {
            if (cust_card_id == null || cust_card_id == "") {
                cust_card_id = "Not Available";
            }
            if (cust_id == null || cust_id == "") {
                cust_id = "Not Available";
            }

            json.get("fields").put(cust_customfield, cust_id);
            json.get("fields").put(card_customfield, cust_card_id);

        } else {
            if (cust_card_id == null || cust_card_id == "") {
                cust_card_id = "Not Available";
            }
            if (cust_id == null || cust_id == "") {
                cust_id = "Not Available";
            }

            json.get("fields").put(cust_customfield, cust_id);
            json.get("fields").put(card_customfield, cust_card_id);

        }
        System.out.println("json data from CMS utility is: " + json);
        return json;
    }

    public CxJson populateIncidentJson(Incident event, String jiraParentId, boolean isupdate) throws IOException, CmsException {
        String cust_card_id = "";
        String cust_id = "";

        Hocon hocon = new Hocon();
        hocon.loadFromContext("custom_field.conf");
        String customer_field = hocon.getString("customer_field");
        String card_field = hocon.getString("card_field");

        String scnName = "SNEW11_INQUIRIES_DORMANT_STAFF_ACC_INTU";

        
        if (event.factName.equals(scnName)) {
            List<String> list = new ArrayList();
            try {

                JsonParser parser = new JsonParser();
                JsonObject object = (JsonObject) parser.parse(event.evidence);
                JsonArray arr = (JsonArray) object.get("evidenceData");
                JsonObject obj = (JsonObject) arr.iterator().next();

                JsonArray Ids = (JsonArray) obj.get("eventIds");
                // JsonArray Ids = (JsonArray) arr.get(1);
                Iterator iterator = Ids.iterator();
                while (iterator.hasNext()) {
                    list.add(iterator.next().toString().replaceAll("\"", ""));
                }

                //Set<String> hfdbEventIds = new HashSet<>(format.getUniqueEventIds(evidJson));
                CustomDerivator.getInsertData(list);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
  
        if (isupdate) {
            CxJson json = CMSUtility.getInstance(event.moduleId).populateUpdateJson(event);
            return json;
        }
        CxJson json = null;


        json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
        //if(!event.entityId.startsWith("C_F_") && !event.entityId.startsWith("A_F_")) {
        String mnemonic = event.eventId.substring(0, event.eventId.indexOf("_"));
        String caseentityId = event.caseentityId;
        String eventClassName = getClassnameFromMnemonic(mnemonic);
        String tableName = getTableNamefromClassName(eventClassName);
        if (event.factName.contains("MANUAL_ALERT") && event.caseentityName.equalsIgnoreCase("CUSTOMER")) {
            cust_id = event.caseentityId;
            logger.info("inside manual customer Id----" + cust_id);
        } else {
            cust_id = getCustId(tableName, event.eventId);
        }
        if (tableName.equalsIgnoreCase("EVENT_FT_ACCOUNTTXN") || tableName.equalsIgnoreCase("EVENT_NFT_BALANCE_ENQUIRY") || tableName.equalsIgnoreCase("EVENT_NFT_TIN_RESET")) {
            cust_card_id = getCardNo(tableName, event.eventId);
        }
         /*cust_id = getCustId(tableName, event.eventId);
        if(tableName.equalsIgnoreCase("EVENT_FT_ACCOUNTTXN") || tableName.equalsIgnoreCase("EVENT_NFT_BALANCE_ENQUIRY")  || tableName.equalsIgnoreCase("EVENT_NFT_TIN_RESET")){
            cust_card_id = getCardNo(tableName, event.eventId);
        }*/
        logger.info("final custid --" + cust_id + "and card no --" + cust_card_id);

        if (json.get("fields").hasKey(customer_field) || json.get("fields").hasKey(card_field)) {
            if (cust_card_id == null || cust_card_id == "") {
                cust_card_id = "Not Available";
            }
            if (cust_id == null || cust_id == "") {
                cust_id = "Not Available";
            }
            json.get("fields").put(customer_field, cust_id);
            json.get("fields").put(card_field, cust_card_id);
        } else {
            if (cust_card_id == null || cust_card_id == "") {
                cust_card_id = "Not Available";
            }
            if (cust_id == null || cust_id == "") {
                cust_id = "Not Available";
            }
      //    json.get("fields").put(customer_field, cust_id);
      //    json.get("fields").put(card_field, cust_card_id);
        }

        System.out.println("json data from CMS utility is: " + json);

      try {
           CaseFormatter.insertIntodb(jiraParentId,cust_id,cust_card_id,caseentityId);
        } catch (JiraClientException e) {
            e.printStackTrace();
        }

        return json;
    }


    // Below method is for Case Assignment logic for Jira/CMS - MASHREQ-491


/*  public  static HashMap<String, Map<String, Integer>> getUserDetails(){
        String sql_query = "SELECT * from CMS_USERS_DETAILS";
        PreparedStatement ps = null;
        HashMap<String, Map<String, Integer>> opUserMap= new HashMap<>();
        CxConnection con = null;

        try{
            //TODO get the query for list of user
            con = getConnection();
            ps = con.prepareStatement(sql_query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //TODO keep the user in 'opUserList'
                String grp = rs.getString("UserGroup");
                String usrName = rs.getString("UserName");
                int tolCase = rs.getInt("TotalCase");

                logger.info(" getUserDetails grp: "+grp+" || tolCase: "+tolCase+" || usrName: "+usrName);

                Map<String, Integer> mapOfUserAndTCase = new HashMap<>();

                if(grp.equalsIgnoreCase("admin")){
                    if(opUserMap.containsKey(grp)){
                        mapOfUserAndTCase = opUserMap.get(grp);
                        mapOfUserAndTCase.put(usrName, tolCase);
                        opUserMap.replace(grp, mapOfUserAndTCase);
                    } else {
                        mapOfUserAndTCase.put(usrName, tolCase);
                        opUserMap.put(grp, mapOfUserAndTCase);
                    }
                } else if(grp.equalsIgnoreCase("other")){
                    if(opUserMap.containsKey(grp)){
                        mapOfUserAndTCase = opUserMap.get(grp);
                        mapOfUserAndTCase.put(usrName, tolCase);
                        opUserMap.replace(grp, mapOfUserAndTCase);
                    } else {
                        mapOfUserAndTCase.put(usrName, tolCase);
                        opUserMap.put(grp, mapOfUserAndTCase);
                    }
                }
            }
            logger.info("1. getUserDetails -> Result : "+opUserMap);
            return opUserMap;

        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
		    //rs.close();
		    ps.close();
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        logger.info("2. getUserDetails -> Result : "+opUserMap);
        return opUserMap;
    }


    public String getCaseAssignee(Incident incident) throws CmsException {

        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }

        String userGroup = "admin";

        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        synchronized(userGroup.intern()) {
            Map<String, Integer> userCasesMap =  groupUserCasesMap.get(userGroup.toLowerCase());
            for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
                if (entry.getValue() < noOfCases) {
                    noOfCases = entry.getValue();
                    resNextUser = entry.getKey();
                }
            }

            userCasesMap.put(resNextUser, ++noOfCases);
            groupUserCasesMap.put(userGroup, userCasesMap);
            logger.info("getCaseAssignee -> resNextUser : "+resNextUser+" || noOfCases : "+noOfCases);
            updateNoOfCasesInDB(resNextUser, noOfCases);        //  to update the noOfCases in DB
        }

        return resNextUser;
    }

    public String getIncidentAssignee(Incident incident) throws CmsException {

        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }

        String userGroup = "other";

        int noOfCases = Integer.MAX_VALUE;
        String resNextUser = null;
        synchronized(userGroup.intern()) {
            Map<String, Integer> userCasesMap = groupUserCasesMap.get(userGroup.toLowerCase());

            for (Map.Entry<String, Integer> entry : userCasesMap.entrySet()) {
                if (entry.getValue() < noOfCases) {
                    noOfCases = entry.getValue();
                    resNextUser = entry.getKey();
                }
            }

            userCasesMap.put(resNextUser, ++noOfCases);
            groupUserCasesMap.put(userGroup, userCasesMap);
            logger.info("getIncidentAssignee -> resNextUser : "+resNextUser+" || noOfCases : "+noOfCases);
            //TODO update the DB table for noOfCases
            updateNoOfCasesInDB(resNextUser, noOfCases);        //  to update the noOfCases in DB
        }

        return resNextUser;
    }


    public void updateNoOfCasesInDB(String resNextUser, int noOfCases){
        //TODO update the noOfCases for specific user
        String sql_query = "Update CMS_USERS_DETAILS SET TotalCase = ? WHERE UserName = ?";
        CxConnection con = null;
        PreparedStatement ps = null;


        try {
            con = getConnection();
            ps = con.prepareStatement(sql_query);
            ps.setInt(1, noOfCases);
            ps.setString(2, resNextUser);
            int rs = ps.executeUpdate();
            logger.info("updateNoOfCasesInDB -> DONE  resNextUser "+resNextUser+" || noOfCases : "+noOfCases+"|| update row : "+rs);
            con.commit();
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
		    ps.close();
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    } */
}
