cxps.events.event.ft-kfactoring{  
  table-name : EVENT_FT_KFACTORING
  event-mnemonic: KFA
  workspaces : {
    NONCUSTOMER : "invoiceno-discounted-cheque-no"
  }
 event-attributes : {
factoring-contract-number: {db:true ,raw_name : factoring_contract_number ,type:"string:50"}
cif-id: {db:true ,raw_name : cif_id ,type:"string:50"}
customer-account-number: {db :true ,raw_name : customer_account_number ,type : "string:50"}
invoiceno-discounted-cheque-no: {db:true ,raw_name : invoiceno_discounted_cheque_no ,type:"string:50"}
contract-end-date-validity: {db:true ,raw_name : contract_end_date_validity ,type:timestamp}
invoice-cheque-date: {db:true ,raw_name : invoice_cheque_date ,type:timestamp}
invoice-cheque-amount: {db:true ,raw_name : invoice_cheque_amount ,type:"number:20,4"}
invoice-cheque-currency: {db:true ,raw_name : invoice_cheque_currency ,type:"string:20"}
}
}
