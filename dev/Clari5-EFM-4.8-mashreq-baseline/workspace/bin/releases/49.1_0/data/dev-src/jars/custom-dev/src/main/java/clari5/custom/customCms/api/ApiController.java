package clari5.custom.customCms.api;

import clari5.custom.customCms.CustomCmsServlet;
import clari5.custom.customCms.Export;
import clari5.custom.customCms.cardblockUnblock.CrdBlk_UnBlk_Controller;
import clari5.custom.customCms.freezUnfreez.FreezeUnfreezeController;
import clari5.platform.util.Hocon;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * Created by pallavi on 16/6/20.
 */


@Path("file")
public class ApiController {

    private FreezeUnfreezeController freezeUnfreezeController = new FreezeUnfreezeController();
    private CrdBlk_UnBlk_Controller crdBlk_unBlk_controller= new CrdBlk_UnBlk_Controller();
    private static CxpsLogger logger = CxpsLogger.getLogger(ApiController.class);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/CustomCmsApi")
    public Response CustomCms(String data)
    {

        String sql = "";

        JSONObject result = new JSONObject();
        JSONObject json = new JSONObject(data);

        logger.debug("coming inside CustomCmsAPI");

        Hocon h = new Hocon();
        h.loadFromContext("FreezUnfreezBlockApiDetails.conf");
        String fApi=h.getString("freez_UnFrz_api");
        String cApi=h.getString("cardblock_UnBlk_api");
        String envType=h.getString("env_type");

        int time=h.getInt("timeToWaitForRes");

        logger.info("this is freez api"+fApi+"block api --"+cApi+"time ---"+time);

        String act = json.get("action").toString();
        String custNo = json.get("customerNumber").toString();
        String username = json.get("username").toString();

        logger.info("action --"+act+"custNo --"+custNo);

        try {

            if (act.equalsIgnoreCase("F")) {
                System.out.println("inside freeze in api");

                boolean flag=freezeUnfreezeController.callingToFreez(custNo,fApi, time,envType);

                if(flag==true) {

                    sql = "SELECT frzStatus from FREZ_UNFREZ_DETAILS WHERE customerNo = ?";

                    try(Connection con = Rdbms.getAppConnection()) {

                        try (PreparedStatement ps = con.prepareStatement(sql)) {
                            ps.setString(1, custNo);

                            try (ResultSet rs = ps.executeQuery()) {
                                if (rs.next()) {
                                    String status = rs.getString("frzStatus");
                                    if (status.equalsIgnoreCase("P")) {
                                        result.put("resp", "FF");
                                        return Response.status(Response.Status.OK).entity(result.toString()).build();
                                    }
                                }
                            }
                        }
                    }

                    result.put("resp","FS");
                    String data1="{\n" +
                            "  \"custId\": "+custNo+",\n" +
                            "  \"cardno\": "+""+"null"+",\n" +
                            "  \"status\": "+"Applied"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                }
                else
                    result.put("resp","FF");
            }
            else if(act.equalsIgnoreCase("UZ"))
            {
                logger.debug(" un freez method");

                boolean flag=freezeUnfreezeController.callingToUnFreez(custNo,fApi,time,envType);

                if (flag==true){

                    sql = "SELECT frzStatus from FREZ_UNFREZ_DETAILS WHERE customerNo = ?";

                    try(Connection con = Rdbms.getAppConnection()) {
                        try (PreparedStatement ps = con.prepareStatement(sql)) {
                            ps.setString(1, custNo);

                            try (ResultSet rs = ps.executeQuery()) {
                                if (rs.next()) {
                                    String status = rs.getString("frzStatus");
                                    if (status.equalsIgnoreCase("P")) {
                                        result.put("resp", "FF");
                                        return Response.status(Response.Status.OK).entity(result.toString()).build();
                                    }
                                }
                            }
                        }
                    }
                    result.put("resp","FS");
                    String data1="{\n" +
                            "  \"custId\": "+custNo+",\n" +
                            "  \"cardno\": "+""+"null"+",\n" +
                            "  \"status\": "+"Not Applied"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                }
                else
                    result.put("resp","FF");

            }
            else if(act.equalsIgnoreCase("B"))
            {
                logger.debug("this is block method");

                boolean flag=crdBlk_unBlk_controller.callingToBlkCard(custNo,cApi, time,envType);

                if(flag==true){

                    sql = "SELECT frzStatus from FREZ_UNFREZ_DETAILS WHERE customerNo = ?";

                    try(Connection con = Rdbms.getAppConnection()) {

                        try (PreparedStatement ps = con.prepareStatement(sql)) {
                            ps.setString(1, custNo);

                            try (ResultSet rs = ps.executeQuery()) {
                                if (rs.next()) {
                                    String status = rs.getString("frzStatus");
                                    if (status.equalsIgnoreCase("P")) {
                                        result.put("resp", "CF");
                                        return Response.status(Response.Status.OK).entity(result.toString()).build();
                                    }
                                }
                            }
                        }
                    }
                    result.put("resp","CS");
                    String data1="{\n" +
                            "  \"custId\": "+""+"null"+",\n" +
                            "  \"cardno\": "+custNo+",\n" +
                            "  \"status\": "+"CardBlocked"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                } else
                    result.put("resp","CF");
            }
            else if (act.equalsIgnoreCase("UB")) {

                logger.debug("this is unblock method");

                boolean flag=crdBlk_unBlk_controller.callingToUnBlock(custNo,cApi, time,envType);

                if(flag==true){

                    sql = "SELECT frzStatus from FREZ_UNFREZ_DETAILS WHERE customerNo = ?";

                    try(Connection con = Rdbms.getAppConnection()) {

                        try (PreparedStatement ps = con.prepareStatement(sql)) {
                            ps.setString(1, custNo);

                            try (ResultSet rs = ps.executeQuery()) {
                                if (rs.next()) {
                                    String status = rs.getString("frzStatus");
                                    if (status.equalsIgnoreCase("P")) {
                                        result.put("resp", "FF");
                                        return Response.status(Response.Status.OK).entity(result.toString()).build();
                                    }
                                }
                            }
                        }
                    }
                    result.put("resp","CS");
                    String data1="{\n" +
                            "  \"custId\": "+""+"null"+",\n" +
                            "  \"cardno\": "+custNo+",\n" +
                            "  \"status\": "+"CardUnBlocked"+",\n" +
                            "  \"username\": "+username+"\n" +
                            "\n" +
                            "}";
                    CustomCmsServlet.saveaudit(data1);
                } else
                    result.put("resp","CF");
            }
            else {
                //if status is not proper print on log file
                logger.debug(" Status getting from jira is not correct");
                logger.debug(" Expected status is F or UF or B or UB");
                logger.debug(" Status we are getting" + act);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return Response.status(Response.Status.OK).entity(result.toString()).build();

    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/fetchCustIdCardDetails")
    public Response getCustCardDetails(String data) {

        String cmd = null;
        JSONObject param = null;

        JSONObject object = new JSONObject(data);

        try {
            cmd = object.getString("cmd");
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject result = new JSONObject();
        try {
            param = (JSONObject) object.get("param");
        } catch (Exception e) {

            logger.info("Exception while reading parameter value from json===> "+e.getMessage());
        }

        try {
            switch (cmd) {
                case "getCustId_Card_Id":
                    GetCustIdCardId_Case_Level getCustIdCardId_case_level = new GetCustIdCardId_Case_Level();
                    String issueKey = param.getString("issueKey");
                    result = getCustIdCardId_case_level.getCust_Card_Details(issueKey);
                    break;
                default:
                    logger.info("Invalid Command");
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.OK).entity(result.toString()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/manualcust_cardstatus")
    public Response getManualCustStatus(String data) {

        JSONObject result = new JSONObject();

        try {
            JSONObject obj = new JSONObject(data);

            String cmd = obj.getString("cmd");
            String type = obj.getString("type");



            switch (cmd){

                case "getstatus" :
                    if(type.equalsIgnoreCase("cust")) {

                        String custid = obj.getString("custid");
                        ManualCustStatus manualCustStatus = new ManualCustStatus();
                        result = manualCustStatus.getManualCustStatus(custid);

                    }else if(type.equalsIgnoreCase("card"))
                    {
                        String cardNo = obj.getString("cardno");
                        ManualCustStatus manualCustStatus = new ManualCustStatus();
                        result = manualCustStatus.getManualCardStatus(cardNo);
                    }
                    break;

                 default:
                        logger.info("Invalid command");
                        break;
            }

        }catch (Exception e)
        {
            logger.info("Exception ==> "+e.getMessage());
        }



        return Response.status(Response.Status.OK).entity(result.toString()).build();
    }

}

