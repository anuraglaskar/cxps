'use strict';
var HttpClient = function() {
    this.get = function(aUrl, hmethod, data, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState === 4 && anHttpRequest.status === 200)
                aCallback(anHttpRequest.responseText);
        };
        anHttpRequest.open(hmethod, aUrl, true);
        anHttpRequest.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        anHttpRequest.send(data);
    }
};
var client = new HttpClient();
var userName = "";

function getUserName(id) { //get logged in user
    var theurl = '/clari5-cc/rest/1.0/session/validate/' + id;
    var hmethod = 'GET';
    var data = null;
    client.get(theurl, hmethod, data, function(response) {
        userName = response;
    });
}
var dataArr = {};
var cardDataTable = [];
var errMsg = '<div class="alert alert-danger" ><strong>Error!</strong> ' +
    'Some problem occured. Please try after sometime</div>';
var errMsgModal = ' <div  class="alert alert-danger alert-dismissible fade show">' +
    '<strong>Error!</strong> Some problem occured. Please try after sometime' +
    '<button id = "E" onclick="errCardMsg(this.id);" type="button" class="close" data-dismiss="alert">&times;</button></div>';
var validNumMsg = "Please enter valid number";
var validOnlyNum = "Only numbers are allowed";
var replaceTag = "<option id = 'selectCust' value='0'>-- Select --</option>";

function getDataList(issueKey, callback) { //called Oninit - gets custId suggestions and loads cardDataTable
    var load = {
        "cmd": "getCustId_Card_Id",
        "param": {
            "issueKey": issueKey
        }
    };
    var loadListUrl = '/efm/details/file/fetchCustIdCardDetails';
    var hmethod = 'POST';
    $("#spin").modal('show');
    client.get(loadListUrl, hmethod, JSON.stringify(load), function(response) {
        $("#spin").modal('hide');
        dataArr = JSON.parse(response);
        if (callback) callback(dataArr);
        if (dataArr.custId != "No Data Found") {
            for (var i = 0; i < dataArr.custId.length; i++) {
                if (i == 0) {
                    replaceTag = '<option id = "selectCust" value="0">-- Select --</option>' +
                        '<option value="' + dataArr.custId[i] + '" selected>' + dataArr.custId[i] + '</option>';
                } else {
                    replaceTag += '<option value="' + dataArr.custId[i] + '">' + dataArr.custId[i] + '</option>';
                }
            }
            $("#selectCust").replaceWith(replaceTag);
            getCardDataTable(dataArr, dataArr.custId[0], "Y");
            getStatus("cust", "customerId", "custStatus", "I");
        }
    });
}
var cardNumber = "";
var getCardStatusUrl = '';
var load = "";
var isModal = ""; //to get status
function getStatus(type, id, statusId, flg) {
    isModal = "";
    var textValue = "",
        cmd = "",
        setStatus = "";
    if (type == "custModal") {
        isModal = "Y";
    }
    textValue = document.getElementById(id).value;
    if (id == "customerId") {
        if (dataArr.custId != "No Data Found") {
            if (dataArr.custId.includes(textValue)) {
                flg = "I";
                revertHighLighter('cust', id, '');
                setInitCustStatus(textValue, type, statusId);
                getCardDataTable(dataArr, textValue, "N");
            }
        }
    }
    if (flg == "N") { //manual get status
        if (textValue == "") {
            if ($("#" + statusId).is(":hidden")) {
                HighLighter(id, textValue, validNumMsg);
                return false;
            }
        } else {
            if (id == "cardNumber") { //card validation
                if (/[^0-9 ]/.test(textValue)) {
                    HighLighter(id, textValue, validOnlyNum);
                    return false;
                } else if (textValue.toString().length != 16) {
                    HighLighter("cardNumber", textValue, validNumMsg);
                    return false;
                } else {
                    $("#spin_card").removeAttr('hidden');
                    getCardStatusUrl = '/efm/details/file/manualcust_cardstatus';
                    load = {
                        "cmd": "getstatus",
                        "type": "card",
                        "cardno": textValue
                    };
                }
            }
            if (id == "customerId" || id == "custModal") {

                if (/[^0-9 ]/.test(textValue)) {
                    HighLighter(id, textValue, validOnlyNum);
                    return false;
                } else if (textValue.toString().length != 9) {
                    HighLighter(id, textValue, validNumMsg);
                    return false;
                } else {
                    if (isModal = "Y") {
                        $("#spin_custModal").removeAttr('hidden');
                    } else {
                        $("#spin").modal('show');
                    }
                    getCardStatusUrl = '/efm/details/file/manualcust_cardstatus';
                    load = {
                        "cmd": "getstatus",
                        "type": "cust",
                        "custid": textValue
                    };
                }
            }
        }
        var hmethod = 'POST'; //get status http request and response
        client.get(getCardStatusUrl, hmethod, JSON.stringify(load), function(resp) {
            var resp = JSON.parse(resp);
            revertHighLighter(type, id, '');
            $("#spin_card").attr('hidden', true);
            $("#spin_custModal").attr('hidden', true);
            $("#spin").modal('hide');
            if (resp.status != "" || resp.status != "undefined" || resp != {}) {
                if (resp.status == "Applied") {
                    document.getElementById(type + "Stat").value = "Applied";
                    chooseUnBlockBtn(statusId, type);
                } else if (resp.status == "Not Applied") {
                    document.getElementById(type + "Stat").value = "Not Applied";
                    chooseBlockBtn(statusId, type);
                } else if (resp["status"] == "NA") {
                    $('#' + id).attr("style", "border-color:#dc3545;width:306px;");
                    if (isModal = "Y") {
                        $("#" + id + "_text").append('<p id="errorText"><i class="fa fa-exclamation" aria-hidden="true" style="font-size:14px;margin-right: 4px;color:#dc3545"></i>CustomerId does not exist</p>');
                    } else {
                        $("#" + id + "_text").append('<p id="errorText"><i class="fa fa-exclamation" aria-hidden="true" style="font-size:14px;margin-right: 4px;color:#dc3545"></i>' + id.charAt(0).toUpperCase() + id.slice(1) + ' does not exist</p>');
                    }
                    $('#' + id + "_text").attr("hidden", false);
                    return false;
                } else if (resp.status == "Blocked") {
                    document.getElementById(type + "Stat").value = "Blocked";
                    chooseUnBlockBtn(statusId, type);
                } else if (resp.status == "UnBlocked") {
                    document.getElementById(type + "Stat").value = "UnBlocked";
                    chooseBlockBtn(statusId, type);
                } else {
                    $('#' + id).attr("style", "border-color:#dc3545;width:306px;");
                    $("#" + id + "_text").append('<p id="errorText"><i class="fa fa-exclamation" aria-hidden="true" style="font-size:14px;margin-right: 4px;color:#dc3545"></i>Please verify your value </p>');
                    $('#' + id + "_text").attr("hidden", false);
                    return false;
                }
            }
        });
    }
}
function doBlock(value, id) { //to do freeze/infreeze or block/unblock
    isModal = "";
    if (id == "custModal") {
        isModal = "Y";
    }
    var flag = 0;
    var blk_cmd = "",
        alertMsg = "",
        currentText = "",
        dataa = "",
        theurl = "/efm/details/file/CustomCmsApi",
        hmethod = "POST",
        data = "";
    var currentRow = [];
    if (value == null) { //set the payload
        flag = 1;
        $("#spin").modal('show');
        currentRow = $("#crd_status_" + id).text();
        currentText = $("#card_no_" + id).text();
        if (currentRow == "Blocked" || currentRow == "NA") {
            alertMsg = '<div class="alert alert-success" ><strong>Success!  </strong>' +
                'Card UnBlocked Successfully.</div>';
            dataa = getData("UB", currentText, userName);
        } else if (currentRow == "UnBlocked") {
            alertMsg = '<div class="alert alert-success" ><strong>Success!  </strong>' +
                'Card Blocked Successfully.</div>';
            dataa = getData("B", currentText, userName);
        }
    } else {
        currentText = document.getElementById(id).value;
        if (currentText == "") {
            HighLighter(id, currentText, validNumMsg);
            return false;
        } else {
            if (id == "cardNumber") { //card validation
                if (/[^0-9 ]/.test(currentText)) {
                    HighLighter(id, currentText, validOnlyNum);
                    return false;
                } else if (currentText.toString().length != 16) {
                    HighLighter("cardNumber", currentText, validNumMsg);
                    return false;
                } else {
                    $("#spin_card").removeAttr('hidden');
                    $("#cardForm :input").prop("disabled", true);
                    $("#getCardStatus").addClass("disabled");
                    if (value == "b") {
                        alertMsg = '<div class="alert alert-success" ><strong>Success!  </strong>' +
                            'Card Blocked Successfully.<button id = "S" onclick="errCardMsg(this.id);" type="button" class="close" data-dismiss="alert">&times;</button></div>';
                        dataa = getData("B", currentText, userName);
                    } else if (value == "u") {
                        alertMsg = '<div class="alert alert-success" ><strong>Success!  </strong>' +
                            'Card UnBlocked Successfully.<button id = "S" onclick="errCardMsg(this.id);" type="button" class="close" data-dismiss="alert">&times;</button></div>';
                        dataa = getData("UB", currentText, userName);
                    }
                }
            }
            if (id == "customerId" || id == "custModal") { //customer validation
                if (/[^0-9 ]/.test(currentText)) {
                    HighLighter(id, currentText, validOnlyNum);
                    return false;
                } else {
                    if (isModal == "Y") {
                        $("#spin_custModal").removeAttr('hidden');
                        $("#custModalForm :input").prop("disabled", true);
                        $("#getCustStatus").addClass("disabled");
                    } else {
                        $("#spin").modal('show');
                    }
                    if (value == "b") {
                        alertMsg = '<div class="alert alert-success" ><strong>Success!  </strong>Customer ID  Freezed Successfully.';
                        if (isModal == "Y") {
                            alertMsg += '<button id = "S" onclick="errCardMsg(this.id);" type="button" class="close" data-dismiss="alert">&times;</button>';
                        }
                        alertMsg += '</div>';
                        dataa = getData("F", currentText, userName);
                    } else if (value == "u") {
                        alertMsg = '<div class="alert alert-success" ><strong>Success!  </strong>Customer ID  UnFreezed Successfully.';
                        if (isModal == "Y") {
                            alertMsg += '<button id = "S" onclick="errCardMsg(this.id);" type="button" class="close" data-dismiss="alert">&times;</button>';
                        }
                        alertMsg += '</div>';
                        dataa = getData("UZ", currentText, userName);
                    }
                }
            }
        }
    }
    client.get(theurl, hmethod, JSON.stringify(dataa), function(responseText) {
        if (responseText != "{}") {
            var rs = responseText.split(":");
            var respText = rs[1].split('"');
            if (respText[1] == "FS") {
                $("#spin_custModal").attr('hidden', true);
                $("#spin").modal('hide');
                if (isModal != "Y") {
                    $('.respata').html(alertMsg);
                    $("#resp").modal('show');
                    $('.okbtn').click(function() { //modal close
                        location.reload();
                    });
                } else {
                    resetCardErrMsg('frzSuc_custModal', alertMsg);
                }
            } else if (respText[1] == "CS") {
                $("#crd_status_" + id).removeAttr('name');
                $("#spin_card").attr('hidden', true);
                $("#spin").modal('hide');
                if (flag == 1) {
                    $('.respata').html(alertMsg);
                    $("#resp").modal('show');
                    if (currentRow == "Blocked") {
                        $("#crd_status_" + id).text('UnBlocked');
                        $("#" + id).attr('name', "bTbl");
                        $("#" + id).text('Block');
                    }
                    if (currentRow == "UnBlocked") {
                        $("#crd_status_" + id).text('Blocked');
                        $("#" + id).attr('name', "uTbl");
                        $("#" + id).text('UnBlock');
                    }
                } else {
                    resetCardErrMsg('frzSuc_card', alertMsg);
                }
            } else if (respText[1] == "FF") {
                $("#spin_custModal").attr('hidden', true);
                $("#spin").modal('hide');
                if (isModal != "Y") {
                    $('.respata').html(errMsg);
                    $("#resp").modal('show');
                }
                resetCardErrMsg('frzSuc_custModal', errMsgModal);
            } else if (respText[1] == "CF") {
                $("#spin_card").attr('hidden', true);
                $("#spin").modal('hide');
                if (flag == 1) {
                    $('.respata').html(errMsg);
                    $("#resp").modal('show');
                }
                resetCardErrMsg('frzSuc_card', errMsgModal);
            }
        } else {
            if (id == "customerId" || flag == 1) {
                $("#spin").modal('hide');
                $('.respata').html(errMsg);
                $("#resp").modal('show');
            } else {
                $("#spin_card").attr('hidden', true);
                $("#spin_custModal").attr('hidden', true);
                if (id == "custModal") {
                    resetCardErrMsg('frzSuc_custModal', errMsgModal);
                } else {
                    resetCardErrMsg('frzSuc_card', errMsgModal);
                }
            }
        }
    });
}
var audit_type = "";var audit_value = ""; //manual audit validate
function auditManual() {
    audit_type = document.getElementById("audit_type").value;
    audit_value = document.getElementById("audit_value").value;
    if (audit_type == "") {
        var message = "Please select Type"
        ManualHighLgt("audit_type", message);
        return false;
    } else if (audit_value == "") {
        ManualHighLgt("audit_value", validNumMsg);
        return false;
    } else if (audit_type == "Card Number") {
        if (/[^0-9 ]/.test(audit_value)) {
            ManualHighLgt("audit_value", validOnlyNum);
            return false;
        } else if (audit_value.toString().length != 16) {
            ManualHighLgt("audit_value", validNumMsg);
            return false;
        } else {
            $("#spin").modal('show');
        }
    } else if (audit_type == "Customer Number") {
        if (/[^0-9 ]/.test(audit_value)) {
            ManualHighLgt("audit_value", validOnlyNum);
            return false;
        } else if (audit_value.toString().length != 9) {
            ManualHighLgt("audit_value", validNumMsg);
            return false;
        } else {
            $("#spin").modal('show');
        }
    }
    showAudit("manual");
}
var CustOrCard = "";var custId = ""; //get audit log
function showAudit(id) {
    if (id == "customerId") {
        CustOrCard = "Customer Number";
        custId = document.getElementById(id).value;
        if (custId == "") {
            HighLighter(id, custId, validNumMsg);
            return false;
        } else if (/[^0-9 ]/.test(custId)) {
            HighLighter("customerId", custId, validOnlyNum);
            return false;
        } else if (custId.toString().length != 9) {
            HighLighter("customerId", custId, validNumMsg);
            return false;
        } else {
            $("#spin").modal('show');
            var jsondata = getjsondata(custId, "getAudit_Log");
        }
    } else if (id == "manual") {
        CustOrCard = audit_type;
        custId = audit_value;
        if (audit_type == "Customer Number") {
            var jsondata = getjsondata(custId, "getAudit_Log");
        } else if (audit_type == "Card Number") {
            var jsondata = getjsondata(custId, "getCardAudit_Log");
        } else {
            $("#spin").modal('show');
        }
    } else {
        CustOrCard = "Card Number";
        custId = id;
        if (custId != "") {
            var jsondata = getjsondata(custId, "getCardAudit_Log");
            $("#spin").modal('show');
        } else {
            $('.respata').html(validNumMsg);
            $("#resp").modal('show');
            return false;
        }
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: { jsondata: JSON.stringify(jsondata) },
        url: '/efm/AuditServlet',
        timeout: 5000,
        success: function(data, textStatus) {
            $("#spin").modal('hide');
            var aRR = data;
            if (aRR["msg"] == "S") {
                getAuditLog(aRR["customerDetails"]);
                $("#fileDownld").removeAttr('hidden')
                $("#audit_value").removeAttr('hidden');
            }
            if (aRR["msg"] == "F" || aRR["msg"] == "N") {
                getAuditLog('');
                $("#audit_value").removeAttr('hidden');
            }

        },
        error: function(xhr, textStatus, errorThrown) {
            $("#spin").modal('hide');
            getAuditLog('');
            $("#audit_value").removeAttr('hidden');
        }
    });
}
function getAuditLog(aRR) {
    var btnHtml2 = "";
    var htm = "";
    htm += '<table id="auditDataTable" class="table table-striped table-bordered">';
    htm += ' <thead><tr><th>#</th><th>' + CustOrCard + '</th><th>Status</th><th>Date</th><th>User</th></tr></thead><tbody>';
    for (var i = 0; i < aRR.length; i++) {
        var innerhtm = '';
        innerhtm += '<tr>';
        innerhtm += '<td>' + (i + 1) + '</td>';
        if (CustOrCard == "Customer Number") { innerhtm += '<td>' + aRR[i]["CustId"] + '</td>'; }
        if (CustOrCard == "Card Number") { innerhtm += '<td>' + aRR[i]["Card Number"] + '</td>'; }
        innerhtm += '<td >' + aRR[i]["Freez status"] + '</td>';
        innerhtm += '<td>' + aRR[i]["Freez Date"] + '</td>';
        innerhtm += '<td>' + aRR[i]["Username"] + '</td>';
        innerhtm += '</tr>';
        htm += innerhtm;
    }
    htm += '</tbody></table></br></br>';
    $('#auditDt').html(htm);
    var auditDataTable = $('#auditDataTable').DataTable({
        columnDefs: [{
            orderable: false,
            defaultContent: ''
        }],
        order: [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "searching": false
    });
}
function getCardDataTable(dataArr, custId, isInit) {
    var cardDetails = "";
    if (isInit == "Y") {
        cardDetails = dataArr.cardstatus[0][dataArr.custId[0]];
    } else {
        cardDetails = dataArr.cardstatus[0][custId];
    }
    var btnHtml = "";
    var htm = "";
    htm += '<table id="resultDataTable" class="table table-striped table-bordered">';
    htm += ' <thead><tr><th>#</th><th>Card Number</th><th>Status</th><th>Action</th></tr></thead><tbody>';
    for (var i = 0; i < cardDetails.length; i++) {
        if (cardDetails[i]["cardno"] == "Not Available") { break; }
        var append = "'" + cardDetails[i]["cardno"] + "'";
        var innerhtm = '';
        innerhtm += '<tr>';
        innerhtm += '<td>' + (i + 1) + '</td>';
        innerhtm += '<td><a  href="#auditDt" id="card_no_' + i + '" title="Audit Log" onclick="showAudit(' + append + ')">' + cardDetails[i]["cardno"] + '</a></td>';
        innerhtm += '<td id="crd_status_' + i + '">' + cardDetails[i]["status"] + '</td>';
        if (cardDetails[i]["status"] == "Blocked" || cardDetails[i]["status"] == "NA") {
            btnHtml = '<button type="button" class="btn btn-warning" id=' + i + ' name="uTbl" onclick="doBlock(null,this.id)" >UnBlock</button>';
        }
        if (cardDetails[i]["status"] == "UnBlocked") {
            btnHtml = '<button type="button" class="btn btn-warning" id=' + i + ' name="bTbl" onclick="doBlock(null,this.id)" >Block</button>';
        }
        innerhtm += '<td>' + btnHtml + '</td>';
        innerhtm += '</tr>';
        htm += innerhtm;
    }
    htm += '</tbody></table></br></br>';
    $('#cardDT').html(htm);
    cardDataTable = $('#resultDataTable').DataTable({
        columnDefs: [{
            orderable: false,
            defaultContent: ''
        }],
        order: [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "searching": false
    });
}
function setInitCustStatus(textValue, type, statusId) { //sets customer's status
    if (dataArr.custstatus[0][textValue] == "Not Applied") {
        document.getElementById(type + "Stat").value = "Not Applied";
        chooseBlockBtn(statusId, type);
    } else if (dataArr.custstatus[0][textValue] == "Applied") {
        document.getElementById(type + "Stat").value = "Applied";
        chooseUnBlockBtn(statusId, type);
    }
}
function chooseBlockBtn(statusId, type) { //choose button type
    $('#' + statusId).attr("hidden", false);
    $('#block_' + type).attr("hidden", false);
}
function chooseUnBlockBtn(statusId, type) {
    $('#' + statusId).attr("hidden", false);
    $('#unblock_' + type).attr("hidden", false);
}
function getData(action, currentText, userName) { //json
    var dataa = {
        "action": action,
        "customerNumber": currentText,
        "username": userName
    };
    return dataa;
}
function getjsondata(custId, cmd) {
    var jsondata = {
        "cmd": cmd,
        "payload": { "custId": custId }
    };
    return jsondata;
}
function resetCardErrMsg(id, msg) { //reset msg in modal
    document.getElementById(id).innerHTML = "";
    document.getElementById(id).innerHTML = msg;
}
function HighLighter(id, value, msg) { //highlights input tag if error
    $('#' + id).removeAttr("style", "border-color:#dc3545;");
    if (id == "customerId") {
        $("#" + id + "_text").replaceWith('<div  id="' + id + '_text"  class = "textHighLight col-sm-5 roco" style="width:100%" ></div>');
        $('#' + id).attr("style", "border-color:#dc3545;width:306px;");
        $("#" + id + "_text").append('<p id="errorText"><i class="fa fa-exclamation text-right"  aria-hidden="true" style="font-size:14px;margin-right: 4px;"></i>' + msg + '</p>');
    } else {
        $("#" + id + "_text").replaceWith('<div  id="' + id + '_text"  class = "textHighLight col-sm-7 roco" style="width:100%" hidden></div>');
        $('#' + id).attr("style", "border-color:#dc3545;width:306px;");
        $("#" + id + "_text").append('<p id="errorText"><i class="fa fa-exclamation text-right"  aria-hidden="true" style="font-size:14px;margin-right: 4px;"></i>' + msg + '</p>');
        $("#" + id + "_text").attr("hidden", false);
    }
}
function revertHighLighter(type, id, flag) { //reverts highlights of input tag
    $('#frzSuc_custModal').html('<div class="frzSuc_custModal"></div>');
    $('#frzSuc_card').html('<div class="frzSuc_card"></div>');
    $("#spin_card").attr("hidden", true);
    $("#spin_custModal").attr("hidden", true);
    $("#cardForm :input").removeAttr('disabled');
    $("#getCustStatus").removeClass('disabled');
    $("#getCardStatus").removeClass('disabled');
    $("#custModalForm :input").removeAttr('disabled');
    if (id == "customerId") {
        if (flag != "N") {
            $("#" + id + "_text").replaceWith('<div  id="' + id + '_text" class = "textHighLight col-sm-5 roco" style="width:100%" ></div>');
        }
        if (dataArr.custId != "No Data Found") {
            cardDataTable.clear().draw();
        }
    } else if (id == "cardNumber" || id == "custModal") {
        $("#" + id + "_text").replaceWith('<div  id="' + id + '_text" class = "textHighLight col-sm-7 roco" style="width:100%" hidden></div>');
    }
    $('#' + id).removeAttr("style", "border-color:#dc3545;");
    document.getElementById(type + "Stat").value = "";
    $('#' + type + "Status").attr("hidden", true);
    $('#unblock_' + type).attr("hidden", true);
    $('#block_' + type).attr("hidden", true);
}
function ManualHighLgt(id, msg) {
    $("#spin_" + id).removeAttr('hidden');
    $("#cardForm :input").removeAttr('disabled');
    $("#custModalForm :input").removeAttr('disabled');
    $("#getCustStatus").removeClass("disabled");
    $("#getCardStatus").removeClass("disabled");
    $('#' + id).removeAttr("style", "border-color:#dc3545;");
    $("#" + id + "_text").replaceWith('<div  id="' + id + '_text"  class = "textHighLight col-sm-4 roco"  ></div>');
    $('#' + id).attr("style", "border-color:#dc3545;width:306px;");
    $("#" + id + "_text").append('<p id="errorText"><i class="fa fa-exclamation text-right"  aria-hidden="true" style="font-size:14px;margin-right: 4px;"></i>' + msg + '</p>');
    $("#" + id + "_text").attr("hidden", false);
}
function revertManual(id, type) {
    $('#audit_value').attr('maxlength', '16');
    if (id == "audit_type") {
        $('#audit_value_text').replaceWith('<div  id="audit_value_text"  class = "textHighLight col-sm-4 roco"  ></div>');
        $('#audit_value').removeAttr("style", "border-color:#dc3545;");
        $('#audit_value').attr("style", "width: 306px !important;");
    }
    $('#' + id).removeAttr("style", "border-color:#dc3545;");
    $('#' + id).attr("style", "width: 306px !important;");
    $('#auditDt').replaceWith("<div id='auditDt'> </div>");
    if (type == "type") {
        document.getElementById("audit_value").value = "";
    }
    if (document.getElementById("audit_type").value == "Customer Number") {
        $('#audit_value').attr('maxlength', '9');
    } else if (document.getElementById("audit_type").value == "Card Name") {
        $('#audit_value').attr('maxlength', '16');
    }
    $("#" + id + "_text").replaceWith('<div  id="' + id + '_text"  class = "textHighLight col-sm-4 roco"  ></div>');
}
function restForm(type) { //resets card and cust based on type
    if (type == "cust") {
        cardDataTable.clear().draw();
        document.getElementById("customerId").value = "";
    }
    $('#frzSuc_custModal').html('<div id="frzSuc_custModal"></div>');
    $('#frzSuc_card').html('<div id="frzSuc_card"></div>');
    document.getElementById('frzSuc_' + type).innerHTML = "";
    document.getElementById("cardNumber").value = "";
    document.getElementById("custModal").value = "";
    document.getElementById(type + "Stat").value = "";
    $('#' + type + "Status").attr("hidden", true);
    $('#unblock_' + type).attr("hidden", true);
    $('#block_' + type).attr("hidden", true);
}
function errCardMsg(id) {
    var element = document.getElementById("errCardMsg");
    if (typeof(element) == 'undefined' || element == null) {
        $("#cardForm :input").removeAttr('disabled');
        $("#getCustStatus").removeClass("disabled");
        $("#getCardStatus").removeClass("disabled");
        $("#custModalForm :input").removeAttr('disabled');
    }
    if (id == "S") {
        restForm("card");
        restForm("custModal");
    }
}
$('.InputModal').modal({
    backdrop: 'static',
    keyboard: false
});
function ManualModalLoad() { //modal close
    revertHighLighter('card', "cardNumber", '');
    revertHighLighter('custModal','custModal','');
     document.getElementById("custModal").value = "";
     document.getElementById("cardNumber").value = "";
}

