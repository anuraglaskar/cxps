package clari5.custom.customScn.scenario14;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;

public class CountDaemon extends CxpsRunnable implements ICxResource {
    private static final CxpsLogger logger = CxpsLogger.getLogger(CountDaemon.class);



    @Override
    protected Object getData() throws RuntimeFatalException {
        logger.info("inside process data into get Data --");
        CallOnEndOfMonth cem = new CallOnEndOfMonth();
        LocalDateTime currentTime = LocalDateTime.now();
        cem.year = currentTime.getYear();

        Month month = currentTime.getMonth();
        int currentMonthIdx = cem.getMonthInIntFormat(month.toString());
        Hocon hocon= new Hocon();
        hocon.loadFromContext("execution_time.conf");
        int dayOfMonWhenWantToExecute =hocon.getInt("month_day");
        int hrsToExecute =hocon.getInt("hrs");
        int minToExecute =hocon.getInt("min");
        int secToExecute =hocon.getInt("sec");
        String selectQuery=hocon.getString("selectQuery");
        String insertQuery=hocon.getString("insertQuery");
        String updateQuery=hocon.getString("updateQuery");
        synchronized (this){
            String threadName=Thread.currentThread().getName();
            long threadId=Thread.currentThread().getId();
            Date utilDate=new Date();
            java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());// current date
            logger.info("[CountDaemon] Accquired Lock"+threadName +" with id"+threadId);
            try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
                Connection connection=rdbmsSession.getCxConnection();
                PreparedStatement selectQueryStatement=connection.prepareStatement(selectQuery)){
                selectQueryStatement.setString(1,threadName);
                selectQueryStatement.setDate(2,sqlDate);
                ResultSet resultSet=selectQueryStatement.executeQuery();
                if(resultSet.next()){
                    logger.info("[CountDaemon] One of the thread has already processed.Hence quiting");
                }
                else{
                    //logger.info("[CountDaemon] No entry Found for this Daemon! Hence Inserting...");
                    //AppEodReminder appEodReminder=new AppEodReminder();
                    cem.getTimeStamp(cem.year, currentMonthIdx, dayOfMonWhenWantToExecute, hrsToExecute, minToExecute, secToExecute,threadName,threadId,sqlDate,insertQuery,updateQuery,selectQuery);
                   // AppEodReminder.callonEOD(hrsToExecute, minToExecute, secToExecute,threadName,threadId,sqlDate,insertQuery,updateQuery,selectQuery);
                }
            }catch (SQLException se){
                logger.error(se.getCause() + " "+se.getMessage());
            }
        }
        logger.info("monthday :"+dayOfMonWhenWantToExecute+"hrs :: "+hrsToExecute+"min :: "+minToExecute+" sec :: "+secToExecute);
        //cem.getTimeStamp(cem.year, currentMonthIdx, dayOfMonWhenWantToExecute, hrsToExecute, minToExecute, secToExecute);

        return null;
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.debug("Inside the Process Data for Count Daemon");
    }
    @Override
    public void configure(Hocon h) {
        logger.debug("this is hocon configuration");
    }
}
