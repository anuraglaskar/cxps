package clari5.custom.jiraevidencformator;

import clari5.platform.util.Hocon;
import org.json.JSONObject;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class FetchingData {
    public static String customField="";
    public static String dburl="";
    public static String user="";
    public static String password="";
    public static String drivername="";
    public static String schemaname="";

    static {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("jiradb-module.conf");
        customField = hocon.getString("customField");
        dburl=hocon.getString("dburl");
        user=hocon.getString("user");
        password=hocon.getString("password");
        drivername=hocon.getString("drivername");
        schemaname=hocon.getString("schemaname");
    }
    public  JSONObject fetch(String issue) throws IOException {
        Connection con =null;
        ResultSet rs=null;
        Statement stmt=null;
        String op = null;
        JSONObject json = new JSONObject();
        String table_name=schemaname+"CUSTOMFIELDVALUE";
        try {

            Class.forName(drivername);
            con= DriverManager.getConnection(dburl,user,password) ;
            stmt = con.createStatement();
            rs = stmt.executeQuery("select TEXTVALUE from "+table_name+" where issue=" + issue + " and CUSTOMFIELD =" + customField + "");
            while (rs.next()) {
                op = rs.getString("TEXTVALUE").toString();
            }
            if (op == null || op == ""){
                op="No Data Found";
            }
            json=json.put("evidence",op);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (con!=null){
                try {
                    con.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
        return json;
    }

   /* public static void main(String[] args) {
        FetchingData fetchingData = new FetchingData();
        try {
            JSONObject jsonObject = fetchingData.fetch("43227");
            System.out.println("===="+jsonObject);
        }catch (Exception e)
        {

        }

    }*/
}
