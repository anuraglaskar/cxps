// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AcctInqEventMapper extends EventMapper<NFT_AcctInqEvent> {

public NFT_AcctInqEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AcctInqEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AcctInqEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AcctInqEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AcctInqEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AcctInqEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AcctInqEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAcctBrCode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getIsEligible());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setString(i++, obj.getCompmis1());
            preparedStatement.setString(i++, obj.getEmpId());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getIsStaff());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getTranRmks());
            preparedStatement.setString(i++, obj.getTranCode());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setTimestamp(i++, obj.getPstdDate());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getTxnBrCode());
            preparedStatement.setString(i++, obj.getTransactionRefNo());
            preparedStatement.setString(i++, obj.getProductCode());
            preparedStatement.setString(i++, obj.getPartTranType());
            preparedStatement.setString(i++, obj.getAcctStatus());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_ACCT_INQ]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AcctInqEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AcctInqEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCT_INQ"));
        putList = new ArrayList<>();

        for (NFT_AcctInqEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCT_BR_CODE",  obj.getAcctBrCode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "IS_ELIGIBLE",  obj.getIsEligible());
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "COMPMIS1",  obj.getCompmis1());
            p = this.insert(p, "EMP_ID",  obj.getEmpId());
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "IS_STAFF",  obj.getIsStaff());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_RMKS",  obj.getTranRmks());
            p = this.insert(p, "TRAN_CODE",  obj.getTranCode());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "PSTD_DATE", String.valueOf(obj.getPstdDate()));
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "TXN_BR_CODE",  obj.getTxnBrCode());
            p = this.insert(p, "TRANSACTION_REF_NO",  obj.getTransactionRefNo());
            p = this.insert(p, "PRODUCT_CODE",  obj.getProductCode());
            p = this.insert(p, "PART_TRAN_TYPE",  obj.getPartTranType());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_ACCT_INQ"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_ACCT_INQ]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_ACCT_INQ]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AcctInqEvent obj = new NFT_AcctInqEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAcctBrCode(rs.getString("ACCT_BR_CODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setIsEligible(rs.getString("IS_ELIGIBLE"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setCompmis1(rs.getString("COMPMIS1"));
    obj.setEmpId(rs.getString("EMP_ID"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setIsStaff(rs.getString("IS_STAFF"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranRmks(rs.getString("TRAN_RMKS"));
    obj.setTranCode(rs.getString("TRAN_CODE"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setPstdDate(rs.getTimestamp("PSTD_DATE"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setTxnBrCode(rs.getString("TXN_BR_CODE"));
    obj.setTransactionRefNo(rs.getString("TRANSACTION_REF_NO"));
    obj.setProductCode(rs.getString("PRODUCT_CODE"));
    obj.setPartTranType(rs.getString("PART_TRAN_TYPE"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_ACCT_INQ]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AcctInqEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AcctInqEvent> events;
 NFT_AcctInqEvent obj = new NFT_AcctInqEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCT_INQ"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AcctInqEvent obj = new NFT_AcctInqEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAcctBrCode(getColumnValue(rs, "ACCT_BR_CODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setIsEligible(getColumnValue(rs, "IS_ELIGIBLE"));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setCompmis1(getColumnValue(rs, "COMPMIS1"));
            obj.setEmpId(getColumnValue(rs, "EMP_ID"));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setIsStaff(getColumnValue(rs, "IS_STAFF"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranRmks(getColumnValue(rs, "TRAN_RMKS"));
            obj.setTranCode(getColumnValue(rs, "TRAN_CODE"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setPstdDate(EventHelper.toTimestamp(getColumnValue(rs, "PSTD_DATE")));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setTxnBrCode(getColumnValue(rs, "TXN_BR_CODE"));
            obj.setTransactionRefNo(getColumnValue(rs, "TRANSACTION_REF_NO"));
            obj.setProductCode(getColumnValue(rs, "PRODUCT_CODE"));
            obj.setPartTranType(getColumnValue(rs, "PART_TRAN_TYPE"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCT_INQ]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCT_INQ]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCT_BR_CODE\",\"CHANNEL\",\"IS_ELIGIBLE\",\"SOURCE\",\"COMPMIS1\",\"EMP_ID\",\"TRAN_SUB_TYPE\",\"TRAN_ID\",\"HOST_ID\",\"IS_STAFF\",\"ACCOUNT_ID\",\"USER_ID\",\"SYS_TIME\",\"CUST_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TRAN_CODE\",\"ACCT_NAME\",\"PSTD_DATE\",\"TRAN_DATE\",\"TRAN_TYPE\",\"TXN_BR_CODE\",\"TRANSACTION_REF_NO\",\"PRODUCT_CODE\",\"PART_TRAN_TYPE\",\"ACCT_STATUS\"" +
              " FROM EVENT_NFT_ACCT_INQ";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCT_BR_CODE],[CHANNEL],[IS_ELIGIBLE],[SOURCE],[COMPMIS1],[EMP_ID],[TRAN_SUB_TYPE],[TRAN_ID],[HOST_ID],[IS_STAFF],[ACCOUNT_ID],[USER_ID],[SYS_TIME],[CUST_ID],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TRAN_CODE],[ACCT_NAME],[PSTD_DATE],[TRAN_DATE],[TRAN_TYPE],[TXN_BR_CODE],[TRANSACTION_REF_NO],[PRODUCT_CODE],[PART_TRAN_TYPE],[ACCT_STATUS]" +
              " FROM EVENT_NFT_ACCT_INQ";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCT_BR_CODE`,`CHANNEL`,`IS_ELIGIBLE`,`SOURCE`,`COMPMIS1`,`EMP_ID`,`TRAN_SUB_TYPE`,`TRAN_ID`,`HOST_ID`,`IS_STAFF`,`ACCOUNT_ID`,`USER_ID`,`SYS_TIME`,`CUST_ID`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TRAN_CODE`,`ACCT_NAME`,`PSTD_DATE`,`TRAN_DATE`,`TRAN_TYPE`,`TXN_BR_CODE`,`TRANSACTION_REF_NO`,`PRODUCT_CODE`,`PART_TRAN_TYPE`,`ACCT_STATUS`" +
              " FROM EVENT_NFT_ACCT_INQ";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_ACCT_INQ (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCT_BR_CODE\",\"CHANNEL\",\"IS_ELIGIBLE\",\"SOURCE\",\"COMPMIS1\",\"EMP_ID\",\"TRAN_SUB_TYPE\",\"TRAN_ID\",\"HOST_ID\",\"IS_STAFF\",\"ACCOUNT_ID\",\"USER_ID\",\"SYS_TIME\",\"CUST_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TRAN_CODE\",\"ACCT_NAME\",\"PSTD_DATE\",\"TRAN_DATE\",\"TRAN_TYPE\",\"TXN_BR_CODE\",\"TRANSACTION_REF_NO\",\"PRODUCT_CODE\",\"PART_TRAN_TYPE\",\"ACCT_STATUS\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCT_BR_CODE],[CHANNEL],[IS_ELIGIBLE],[SOURCE],[COMPMIS1],[EMP_ID],[TRAN_SUB_TYPE],[TRAN_ID],[HOST_ID],[IS_STAFF],[ACCOUNT_ID],[USER_ID],[SYS_TIME],[CUST_ID],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TRAN_CODE],[ACCT_NAME],[PSTD_DATE],[TRAN_DATE],[TRAN_TYPE],[TXN_BR_CODE],[TRANSACTION_REF_NO],[PRODUCT_CODE],[PART_TRAN_TYPE],[ACCT_STATUS]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCT_BR_CODE`,`CHANNEL`,`IS_ELIGIBLE`,`SOURCE`,`COMPMIS1`,`EMP_ID`,`TRAN_SUB_TYPE`,`TRAN_ID`,`HOST_ID`,`IS_STAFF`,`ACCOUNT_ID`,`USER_ID`,`SYS_TIME`,`CUST_ID`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TRAN_CODE`,`ACCT_NAME`,`PSTD_DATE`,`TRAN_DATE`,`TRAN_TYPE`,`TXN_BR_CODE`,`TRANSACTION_REF_NO`,`PRODUCT_CODE`,`PART_TRAN_TYPE`,`ACCT_STATUS`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

