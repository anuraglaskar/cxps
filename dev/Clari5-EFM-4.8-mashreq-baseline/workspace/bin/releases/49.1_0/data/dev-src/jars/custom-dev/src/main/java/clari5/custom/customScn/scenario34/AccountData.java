package clari5.custom.customScn.scenario34;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;


public class AccountData {
    public static clari5.platform.logger.CxpsLogger logger = CxpsLogger.getLogger(AccountData.class);

    public static CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }
    public static List<AccountPojo> getFieldstoMacth() {
        CxConnection connection = null;
        String todaydate = todayDate();
      //  String query = "select c.cxCifID, c.nationalId, c.custPassportNo, c.custTradeNo, c.custMobile1 ,d.acctID from CUSTOMER c,DDA d where c.cxCifID in (select acctcustID from DDA where CONVERT(varchar,acctOpenDt,23) ='"++"') and c.cxCifID = d.acctcustID";
        String query = "select c.cxCifID, c.nationalId, c.custPassportNo, c.custTradeNo, c.custMobile1 ,d.hostAcctId from CUSTOMER c,DDA d where c.cxCifID in (select acctcustID from DDA where CONVERT(varchar,acctOpenDt,23) ='"+todaydate+"') and c.cxCifID = d.acctcustID";
        logger.info("query to get data from customer table of today's opened accounts::"+query);
        List<AccountPojo> finaldatalist = new ArrayList<>();
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountPojo accountPojo = new AccountPojo();
                accountPojo.setNeweid(rs.getString("nationalId"));
                accountPojo.setNewpassport(rs.getString("custPassportNo"));
                accountPojo.setNewmobile(rs.getString("custMobile1"));
                accountPojo.setNewtradelicense(rs.getString("custTradeNo"));
                accountPojo.setNewcif(rs.getString("cxCifID"));
                accountPojo.setNewaccountno(rs.getString("hostAcctId"));
                finaldatalist.add(accountPojo);
                logger.info("inside getFieldstoMacth data become :" + finaldatalist.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return finaldatalist;
    }
    public static List<AccountPojo> getAccountOldData(String custid) {
        //String hostid=custid.replaceAll("C_F_","");
        CxConnection connection = null;
        String query = "select c.cxCifID,c.custName,c.custDOB,d.hostAcctId,c.custMobile1,c.custPassportNo,c.primarySOL,c.custEmail1,c.COMPMIS1,c.COMPMIS2,c.custTradeNo,c.nationalId,d.acctOpenDt ,d.acctCloseDt from CUSTOMER c,DDA d where c.cxCifID = '"+custid+"' and c.cxCifID =d.acctCustID ;";
        //String query = "select * from CUSTOMER where cxCifID = ?";
        logger.info("Query to get old data from customer for cust::" +custid+ " is --"+query);
        List<AccountPojo> finaldatalist = new ArrayList<>();
       // AccountPojo acctolddata = AccountData.getAccountTableOldData(custid);
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                AccountPojo accountPojo = new AccountPojo();
                accountPojo.setOldcif(rs.getString("cxCifID"));
                accountPojo.setOldcustname(rs.getString("custName"));
                accountPojo.setOldcustdob(rs.getDate("custDOB"));
                accountPojo.setOldmobile(rs.getString("custMobile1"));
                accountPojo.setOldpassport(rs.getString("custPassportNo"));
                accountPojo.setOldbranchid(rs.getString("primarySOL"));
                accountPojo.setOldemail(rs.getString("custEmail1"));
                accountPojo.setOldcompmis1(rs.getString("COMPMIS1"));
                accountPojo.setOldcompmis2(rs.getString("COMPMIS2"));
                accountPojo.setOldtradelicense(rs.getString("custTradeNo"));
                accountPojo.setOldeid(rs.getString("nationalId"));
                accountPojo.setOldaccountno(rs.getString("hostAcctId"));
                accountPojo.setOldacctopendate(rs.getDate("acctOpenDt"));
                accountPojo.setOldacctcloseddate(rs.getDate("acctCloseDt"));
                finaldatalist.add(accountPojo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return finaldatalist;
    }
    public static AccountPojo getAccountNewdata(String newcif,String newaccount) {
        CxConnection connection = null;
        String todaydate = todayDate();
        AccountPojo accountPojo = new AccountPojo();
        //select * from CUSTOMER where (nationalId =  '' or  custPassportNo = '' or custTradeNo = '' or custMobile1 = '' )
        //and (  CONVERT (varchar,created_on,23) =''  or CONVERT (varchar,updated_on,23) = '' );
        String query = "select * from CUSTOMER where cxCifID = ? ";
        logger.info("Query to get New data from customer for mactched data :: "+query);
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,newcif);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountPojo accountPojo1 = new AccountPojo();
                accountPojo1 = getAccountTableNewData(newaccount);
                accountPojo.setNewcif(rs.getString("cxCifID"));
                accountPojo.setNewcustname(rs.getString("custName"));
                accountPojo.setNewcustdob(rs.getDate("custDOB"));
                accountPojo.setNewmobile(rs.getString("custMobile1"));
                accountPojo.setNewpassport(rs.getString("custPassportNo"));
                accountPojo.setNewbranchid(rs.getString("primarySOL"));
                accountPojo.setNewemail(rs.getString("custEmail1"));
                accountPojo.setNewcompmis1(rs.getString("COMPMIS1"));
                accountPojo.setNewcompmis2(rs.getString("COMPMIS2"));
                accountPojo.setNewtradelicense(rs.getString("custTradeNo"));
                accountPojo.setNeweid(rs.getString("nationalId"));
                accountPojo.setNewacctopendate(accountPojo1.getNewacctopendate());
                accountPojo.setNewacctcloseddate(accountPojo1.getNewacctcloseddate());
                accountPojo.setNewaccountno(accountPojo1.getNewaccountno());
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return accountPojo;
    }
    public  static  AccountPojo getAccountTableOldData(String custid){
        CxConnection connection = null;
        String query = "select * from DDA where acctCustID = ? ";
        logger.info("query to get old data from DDA for cust id :: "+custid+" is --"+query);
        AccountPojo accountPojo = new AccountPojo();
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,custid);
            ResultSet rs= ps.executeQuery();
            while (rs.next()){
                accountPojo.setOldacctcloseddate(rs.getDate("acctCloseDt"));
                accountPojo.setOldacctopendate(rs.getDate("acctOpenDt"));
                accountPojo.setOldaccountno(rs.getString("hostAcctId"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return accountPojo;
    }
    public  static  AccountPojo getAccountTableNewData(String newaccount){
        CxConnection connection = null;
        String query = "select * from DDA where hostAcctId = ? ";
        logger.info("query to get new data from DDA for newaccount id :: "+newaccount+" is --"+query);
        AccountPojo accountPojo = new AccountPojo();
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,newaccount);
            ResultSet rs= ps.executeQuery();
            while (rs.next()){
                accountPojo.setNewacctcloseddate(rs.getDate("acctCloseDt"));
                accountPojo.setNewacctopendate(rs.getDate("acctOpenDt"));
                accountPojo.setNewaccountno(rs.getString("hostAcctId"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return accountPojo;
    }
    public  static String todayDate(){
        String todaydate="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            ZoneId defaultZoneId = ZoneId.systemDefault();
            LocalDate today = LocalDate.now();
            LocalDate previousDate =today.minus(1, ChronoUnit.DAYS);// today -1
            Date date1 = Date.from(previousDate.atStartOfDay(defaultZoneId).toInstant());
            todaydate=simpleDateFormat.format(date1).toString();
            logger.info("Yesterday date become: " + todaydate);
        }catch (Exception e){
            e.printStackTrace();
        }
        return todaydate;

    }
    public static void main(String[] args) {
        System.out.println("Into main");
    }

}
