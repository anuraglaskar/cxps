package clari5.custom.customScn.scenarioNew1;

import cxps.apex.utils.CxpsLogger;
import clari5.rdbms.Rdbms;
import clari5.platform.rdbms.RDBMSSession;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class EodReminder extends TimerTask {

    private static final CxpsLogger cxpslogger=CxpsLogger.getLogger(EodReminder.class);
    private  static ThreadData threadData=new ThreadData();

    public static ThreadData getThreadData() {
        return threadData;
    }

    public static void setThreadData(ThreadData threadData) {
        EodReminder.threadData = threadData;
    }

    public  void run(){
        synchronized (this){
            try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();) {
                cxpslogger.info("[EodReminder] Thread has been scheduled" + threadData.getCurrentThreadName());
                ApplMqwriter applMqwriter = new ApplMqwriter();
                //applMqwriter.writeJson();
                applMqwriter.writeJson();
                PreparedStatement updateStatement=connection.prepareStatement(threadData.getUpdateQry());
                updateStatement.setString(1,"C");
                updateStatement.setTimestamp(2,new java.sql.Timestamp(new java.util.Date().getTime()));
                updateStatement.setString(3,threadData.getCurrentThreadName());
                updateStatement.setDate(4,threadData.getThreadDate());
                updateStatement.executeUpdate();
                connection.commit();
                threadData=null;
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }
    public static void insert(String threadName, long threadId, java.sql.Date date,String insertQuery){
        try(RDBMSSession rdbmsSession=Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();
            PreparedStatement insertQueryStatement=connection.prepareStatement(insertQuery);){
            insertQueryStatement.setString(1,threadName);// thread name
            insertQueryStatement.setLong(2,threadId); // id
            insertQueryStatement.setDate(3,date);
            insertQueryStatement.setString(4,"NEW");
            insertQueryStatement.setString(5,threadName+"-"+threadId+"-"+date);
            insertQueryStatement.setTimestamp(6,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.setTimestamp(7,new java.sql.Timestamp(new java.util.Date().getTime()));
            insertQueryStatement.executeUpdate();
            connection.commit();
            cxpslogger.info("[EodReminder] Insertion Completed");
        } catch (SQLException e) {
            cxpslogger.error("[EodReminder] insert()-> insertion Failed"+ e.getCause() + " "+e.getMessage());
        }
    }

    public static void callonEOD(int hrsToExecute,int minToExecute,int secToExecute, String threadName, long threadId, java.sql.Date date1, String insertQuery, String updateQuery,String selectQuery){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY,hrsToExecute);
            calendar.set(Calendar.MINUTE,minToExecute);
            calendar.set(Calendar.SECOND,secToExecute);
     
            Date date= calendar.getTime();
            Timer timer = new Timer();
            threadData.setCurrentThreadName(threadName);
            threadData.setThreadDate(date1);
            threadData.setInsrtQry(insertQuery);
            threadData.setSelectQry(selectQuery);
            threadData.setUpdateQry(updateQuery);
            threadData.setThrdId(threadId);
            EodReminder.setThreadData(threadData);

            while (true){
                Date currentDate=new Date();
                if(date.compareTo(currentDate)>0){
                    timer.schedule(new EodReminder(), date);
                    cxpslogger.info("[EodReminder] Call on EOD..Time has been scheduled");
                    int timetosleeep = (24 * 60 * 60 * 1000);
                    System.out.println("[CustEodReminder] Going to sleep...");
                    Thread.sleep(timetosleeep);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

