package clari5.custom.customCms.freezUnfreez;

import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
public class FreezeUnfreezeDao {
    private Response response= null;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    public HashMap hashMap= null;
    private static CxpsLogger logger = CxpsLogger.getLogger(FreezeUnfreezeDao.class);
    //this is to check if customer number exist or not
    public HashMap checkCustomerExistForFirstTime(String custNo) {
        hashMap=new HashMap();
        String customer_no=null;
        String flag=null;
        String query = "select * from FREZ_UNFREZ_DETAILS where customerNo='" + custNo + "'";
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while(rs.next())
           {
                customer_no=rs.getString(1);
                flag=rs.getString(2);

              }
            if(customer_no==null || "".equals(customer_no)){
                customer_no="noValue";
                flag="noValue";
            }
       hashMap.put("cust_no",customer_no);
       hashMap.put("flag",flag);
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            }
            logger.info("this is op from checkcustomExistForFirstTime"+hashMap);
            return hashMap;
        }
    //this is to check is customer no is present or not
    public boolean checkCustomerExist(String custNo, String condT0CheckFZUfz, boolean flagToException, Response response) {
        boolean op=false;
        String cust_no = "";
       // ResultSet rs = null;
        String query = "select * from FREZ_UNFREZ_DETAILS where customerNo='" + custNo + "'";
        logger.info("select query in checkCustomerExist:: "+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
             rs = stmt.executeQuery(query);
            while (rs.next()) {
                cust_no = rs.getString(1);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (cust_no == "" || cust_no == null) {
               op= insert(custNo, flagToException, response);
            } else {

                if (condT0CheckFZUfz.equalsIgnoreCase("fz")) {
                 op=  updateTofreeze(custNo,flagToException, response);
                } else if (condT0CheckFZUfz.equalsIgnoreCase("uf")) {
                   op= updateToUnfreeze(custNo, flagToException, response);

                }
            }
        logger.info("this is op from checkcustomerExist"+op);
          return op;
    }
    private boolean insert(String custNo, boolean flag, Response response) {
        String query;
        if(flag)
        {
            query = "INSERT INTO FREZ_UNFREZ_DETAILS (customerNo,frzStatus, status,code,  description) values('" + custNo + "','P','','','time out exception')";
        }
        else if(response.getStatus().equalsIgnoreCase("s")){
            query = "INSERT INTO FREZ_UNFREZ_DETAILS (customerNo, frzStatus,status,code,description) values('" + custNo + "','Y','"+response.getStatus()+"','"+response.getErrorCode() +"','"+response.getErrorDescription()+" ')";
        }
        else {
            query = "INSERT INTO FREZ_UNFREZ_DETAILS (customerNo, frzStatus, status,code,description) values('" + custNo + "','N','"+response.getStatus()+"','"+response.getErrorCode() +"','"+response.getErrorDescription()+" ')";
        }
        logger.info("insert query in Freeze:: "+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            int i = stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean updateToUnfreeze(String custNo , boolean flag, Response response) {
        String query;
        if(flag){
            query = "update FREZ_UNFREZ_DETAILS set frzStatus='P',status='',code='',description='time out exception' where customerNo='" + custNo + "'";
        }
        else if(response.getStatus().equalsIgnoreCase("s")){
            query = "update FREZ_UNFREZ_DETAILS set frzStatus='N',status='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where customerNo='" + custNo + "'";
        }
        else {
            query = "update FREZ_UNFREZ_DETAILS set status='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where customerNo='" + custNo + "'";
        }
        logger.info("update to UNFreeze query: "+query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean updateTofreeze(String custNo, boolean flag, Response response) {
        String query;
        if(flag){
            query = "update FREZ_UNFREZ_DETAILS set frzStatus='P',status='',code='',description='time out exception' where customerNo='" + custNo + "'";
        }
        else if(response.getStatus().equalsIgnoreCase("s")){
            query = "update FREZ_UNFREZ_DETAILS set frzStatus='Y',status='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where customerNo='" + custNo + "'";
        }
        else {
            query = "update FREZ_UNFREZ_DETAILS set status='"+response.getStatus()+"',code='"+response.getErrorCode()+"',description='"+response.getErrorDescription()+"' where customerNo='" + custNo + "'";
        }
        logger.info("updateTofreeze query " + query);
        try {
            conn = Rdbms.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

}
