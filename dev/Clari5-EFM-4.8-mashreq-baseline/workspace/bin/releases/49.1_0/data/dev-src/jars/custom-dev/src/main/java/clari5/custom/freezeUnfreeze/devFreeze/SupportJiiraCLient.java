package clari5.custom.freezeUnfreeze.devFreeze;

import clari5.jiraclient.InstanceParams;
import clari5.jiraclient.JiraClient;
import clari5.platform.jira.JiraClientException;
import clari5.platform.jira.JiraUtil;
import clari5.platform.util.CxJson;

import java.io.IOException;

public class SupportJiiraCLient extends JiraClient {
    public SupportJiiraCLient(InstanceParams instanceParams) throws JiraClientException.Configuration {
        super(instanceParams);
    }
    @Override
    public CxJson sendRequest(String request, String method, String data) throws JiraClientException {
        if (this.instanceParams.getConnectTimeout() == 0) {
            this.instanceParams.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT);
        }

        if (this.instanceParams.getRequestTimeout() == 0) {
            this.instanceParams.setRequestTimeout(DEFAULT_REQUEST_TIMEOUT);
        }

        CxJson jsonObject;
        final int maxTries = 5;
        int tries = 0;
        while (true) {
            JiraUtil.registerUser(this.instanceParams.getUserId());
            logger.debug("Registered jira client with credentials");

            try {
                jsonObject = JiraUtil.callJira(request, method, data, this.instanceParams.getConnectTimeout(), this.instanceParams.getRequestTimeout(), logJiraCalls);
                return CxJson.parse(jsonObject.toJson());
            } catch (JiraClientException.InvalidSession | IOException e) {
                if (++tries == (maxTries - 1))
                    throw new JiraClientException.Retry(e.getMessage());
            }
        }
    }
}
