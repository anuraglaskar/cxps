package clari5.custom.customScn11.daemonScn11;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import cxps.events.NFT_AcctStatusChangeEvent;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/****
 * @author Suryakant
 * @since 22-06-2020
 *
 */

public class DataFetchTestIncident extends CxpsRunnable implements ICxResource {

    private static final CxpsLogger logger = CxpsLogger.getLogger(DataFetchTestIncident.class);
    static Hocon hocon;
    static String dataFetchFromUserMapTable;
    static String statusUpdateFromTestIncident;
    static int dDays;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("snew11.conf");
        dataFetchFromUserMapTable = hocon.getString("selectQueryFromUserMappingTable");
        dDays = hocon.getInt("limitDaysForPushUp4");
        statusUpdateFromTestIncident = hocon.getString("updateQueryTestIncident");
    }

    @Override
    protected Object getData() throws RuntimeFatalException {

        logger.info(" Inside the get data of DataFetch from TestIncident");
        return getInsertData();
    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {

        logger.info(" Inside the process data of DataFetch  ");
        if (o instanceof ConcurrentLinkedQueue) {
            ConcurrentLinkedQueue<HashMap> DataQueue = (ConcurrentLinkedQueue<HashMap>) o;

            try (RDBMSSession session = Rdbms.getAppSession()) {
                try (CxConnection connection = session.getCxConnection()) {
                    try (PreparedStatement statement = connection.prepareStatement(dataFetchFromUserMapTable)) {

                        while (!DataQueue.isEmpty()) {

                            HashMap map = DataQueue.poll();
                            String entityId = (String) map.get("entity_id");
                            String fact_name = (String) map.get("fact_name");
                            if(fact_name.equalsIgnoreCase("SNEW11_TEST_INQ_DORMNT_STAFF_ACC_INTU")) {

                            if (entityId.substring(0, 4).equals("A_F_")) {

                                Calendar cal = Calendar.getInstance();
                                Date cur_Date = new Date();
                                cal.setTime(cur_Date);
                                cal.add(Calendar.DATE, -dDays);
                                java.util.Date daysCheck = cal.getTime();

                                String accountId = entityId.substring(4);
                                statement.setString(1, accountId);
                                ResultSet resultSet = statement.executeQuery();
                                HashSet distinctUserId = new HashSet();
                                while (resultSet.next()) {

                                    String userId = resultSet.getString("UserID");
                                    String customerId = resultSet.getString("CUST_ID");
                                    java.sql.Timestamp rcreTime = resultSet.getTimestamp("RCRE_TIME");
                                    Date rcreDate = new Date(rcreTime.getTime());

                                    if (rcreDate.after(daysCheck) && rcreDate.before(cur_Date) && !distinctUserId.contains(userId)) {
                                        map.put("user_id", userId);
                                        map.put("customer_id", customerId);
                                        map.put("sys_time", rcreTime);
                                        map.put("account_id", accountId);
                                        distinctUserId.add(userId);
                                        JSONObject object = getJSONFormat(map, userId);
                                        String eventId = object.getString("event_id");
                                        String jsonString = object.toString();
                                        System.out.println("JsonString : " + jsonString + " " + " Event_id : " + eventId);
                                        ECClient.enqueue("HOST", eventId, jsonString);
                                    }
                                }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void configure(Hocon h) {

        logger.debug("TestIncident data fetch Confgure");
    }

    public static ConcurrentLinkedQueue<HashMap> getInsertData() {
        /*****
         * This method is to get the all Data  of Test Incident and insert in to custom table
         *INSERT INTO CUSTOM_SCNNEW11_TRACKER (updated_on, fact_name, incident_id, event_ts, case_id, entity_id, created_on, Flag) VALUES (?, ?, ?, ?, ?, ?, ?, ?)
         */
        ConcurrentLinkedQueue<HashMap> queue = new ConcurrentLinkedQueue<HashMap>();
        String fact_name = "";
        String entity_id = "";
        Timestamp created_on = null;
        Timestamp updated_on = null;

        Hocon hocon = new Hocon();
        hocon.loadFromContext("snew11.conf");

        String selectQueryFromEventTable = hocon.getString("selectQueryTestIncident");
        //String insertQueryIntoUserMapping = hocon.getString("customScnNew11Tracker");
        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {

                try (PreparedStatement select_stmt = connection.prepareStatement(selectQueryFromEventTable);
                     PreparedStatement update_stmt = connection.prepareStatement(statusUpdateFromTestIncident)) {

                    try (ResultSet resultSet = select_stmt.executeQuery()) {

                        while (resultSet.next()) {

                            HashMap dataMap = new HashMap();

                            fact_name = resultSet.getString("fact_name");
                            dataMap.put("fact_name", fact_name);
                            
                            entity_id = resultSet.getString("entity_id");
                            dataMap.put("entity_id", entity_id);
                            created_on = resultSet.getTimestamp("created_on");
                            dataMap.put("created_on", created_on);

                            updated_on = resultSet.getTimestamp("updated_on");
                            dataMap.put("updated_on", updated_on);
                            dataMap.put("Flag", "C");

                            /**
                             * Updating the status of Test Incident from N to C
                             */

                            update_stmt.setString(1,fact_name);
                            update_stmt.setString(2,entity_id);
                            

                            try {
                                update_stmt.executeUpdate();
                                connection.commit();

                            } catch (Exception e) {
                                logger.info("Exception occured while inserting data in UserMapping Table----> " + e.getMessage());
                            }
                            queue.add(dataMap);
                        }
                    }
                }
                connection.close();
            }

        } catch (Exception e) {
            logger.info("Exception occured while inserting data in UserMapping Table----> " + e.getMessage());
        }
        return queue;
    }

    public static JSONObject getJSONFormat(HashMap map, String userId){

        JSONObject modifiedEvent = new JSONObject();
        JSONObject msgBody = new JSONObject();

        Random random = new Random();
        String eventId = String.valueOf(random.nextInt());
        String systime = "";

        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= new Date();
            systime = simpleDateFormat.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }

        msgBody.put("host_id","F");
        msgBody.put("sys_time",systime);
        msgBody.put("event_id",eventId);
        msgBody.put("account_id",map.get("account_id") != null ? map.get("account_id") : "");
        msgBody.put("user_id",userId);
        msgBody.put("cust_id",map.get("customer_id") != null ? map.get("customer_id") :"");
        msgBody.put("flag","N");

        modifiedEvent.put("EventType", "ft");
        modifiedEvent.put("EventSubType","fundtransfercust");
        modifiedEvent.put("event_name","ft_fundtransfercust");
        modifiedEvent.put("event_id",eventId);
        modifiedEvent.put("msgBody",msgBody.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));
        return modifiedEvent;
    }
}
