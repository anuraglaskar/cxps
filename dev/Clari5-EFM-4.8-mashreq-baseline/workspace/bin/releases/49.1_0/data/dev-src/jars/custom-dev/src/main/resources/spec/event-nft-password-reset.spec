cxps.events.event.nft-password-reset{
  table-name : EVENT_NFT_PASSWORD_RESET
  event-mnemonic: PR
  workspaces : {
    
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
user-id: {db : true ,raw_name : user_id ,type : "string:200"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
addr-network: {db : true ,raw_name : addr_network ,type : "string:200"}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
sys-change: {db : true ,raw_name : sys_change ,type : "string:200"}
channel: {db : true ,raw_name : channel ,type : "string:200"}
channel-initiate: {db : true ,raw_name : channel_initiate ,type : "string:200"}
}
}


