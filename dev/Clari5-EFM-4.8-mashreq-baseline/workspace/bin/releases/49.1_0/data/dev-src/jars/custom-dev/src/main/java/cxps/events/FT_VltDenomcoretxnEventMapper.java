// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_VltDenomcoretxnEventMapper extends EventMapper<FT_VltDenomcoretxnEvent> {

public FT_VltDenomcoretxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_VltDenomcoretxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_VltDenomcoretxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_VltDenomcoretxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_VltDenomcoretxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_VltDenomcoretxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_VltDenomcoretxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getDenomcode());
            preparedStatement.setString(i++, obj.getBranchCode());
            preparedStatement.setString(i++, obj.getTillId());
            preparedStatement.setDouble(i++, obj.getOutflow());
            preparedStatement.setString(i++, obj.getXref());
            preparedStatement.setString(i++, obj.getCcycode());
            preparedStatement.setDouble(i++, obj.getInflow());
            preparedStatement.setTimestamp(i++, obj.getPostingdate());
            preparedStatement.setTimestamp(i++, obj.getInsTime());
            preparedStatement.setDouble(i++, obj.getDenomMulInflow());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_VLT_DENOMCORETXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_VltDenomcoretxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_VltDenomcoretxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_VLT_DENOMCORETXN"));
        putList = new ArrayList<>();

        for (FT_VltDenomcoretxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "DENOMCODE",  obj.getDenomcode());
            p = this.insert(p, "BRANCH_CODE",  obj.getBranchCode());
            p = this.insert(p, "TILL_ID",  obj.getTillId());
            p = this.insert(p, "OUTFLOW", String.valueOf(obj.getOutflow()));
            p = this.insert(p, "XREF",  obj.getXref());
            p = this.insert(p, "CCYCODE",  obj.getCcycode());
            p = this.insert(p, "INFLOW", String.valueOf(obj.getInflow()));
            p = this.insert(p, "POSTINGDATE", String.valueOf(obj.getPostingdate()));
            p = this.insert(p, "INS_TIME", String.valueOf(obj.getInsTime()));
            p = this.insert(p, "DENOM_MUL_INFLOW", String.valueOf(obj.getDenomMulInflow()));
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_VLT_DENOMCORETXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_VLT_DENOMCORETXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_VLT_DENOMCORETXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_VltDenomcoretxnEvent obj = new FT_VltDenomcoretxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setDenomcode(rs.getString("DENOMCODE"));
    obj.setBranchCode(rs.getString("BRANCH_CODE"));
    obj.setTillId(rs.getString("TILL_ID"));
    obj.setOutflow(rs.getDouble("OUTFLOW"));
    obj.setXref(rs.getString("XREF"));
    obj.setCcycode(rs.getString("CCYCODE"));
    obj.setInflow(rs.getDouble("INFLOW"));
    obj.setPostingdate(rs.getTimestamp("POSTINGDATE"));
    obj.setInsTime(rs.getTimestamp("INS_TIME"));
    obj.setDenomMulInflow(rs.getDouble("DENOM_MUL_INFLOW"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_VLT_DENOMCORETXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_VltDenomcoretxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_VltDenomcoretxnEvent> events;
 FT_VltDenomcoretxnEvent obj = new FT_VltDenomcoretxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_VLT_DENOMCORETXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_VltDenomcoretxnEvent obj = new FT_VltDenomcoretxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setDenomcode(getColumnValue(rs, "DENOMCODE"));
            obj.setBranchCode(getColumnValue(rs, "BRANCH_CODE"));
            obj.setTillId(getColumnValue(rs, "TILL_ID"));
            obj.setOutflow(EventHelper.toDouble(getColumnValue(rs, "OUTFLOW")));
            obj.setXref(getColumnValue(rs, "XREF"));
            obj.setCcycode(getColumnValue(rs, "CCYCODE"));
            obj.setInflow(EventHelper.toDouble(getColumnValue(rs, "INFLOW")));
            obj.setPostingdate(EventHelper.toTimestamp(getColumnValue(rs, "POSTINGDATE")));
            obj.setInsTime(EventHelper.toTimestamp(getColumnValue(rs, "INS_TIME")));
            obj.setDenomMulInflow(EventHelper.toDouble(getColumnValue(rs, "DENOM_MUL_INFLOW")));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_VLT_DENOMCORETXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_VLT_DENOMCORETXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"DENOMCODE\",\"BRANCH_CODE\",\"TILL_ID\",\"OUTFLOW\",\"XREF\",\"CCYCODE\",\"INFLOW\",\"POSTINGDATE\",\"INS_TIME\",\"DENOM_MUL_INFLOW\"" +
              " FROM EVENT_FT_VLT_DENOMCORETXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [DENOMCODE],[BRANCH_CODE],[TILL_ID],[OUTFLOW],[XREF],[CCYCODE],[INFLOW],[POSTINGDATE],[INS_TIME],[DENOM_MUL_INFLOW]" +
              " FROM EVENT_FT_VLT_DENOMCORETXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`DENOMCODE`,`BRANCH_CODE`,`TILL_ID`,`OUTFLOW`,`XREF`,`CCYCODE`,`INFLOW`,`POSTINGDATE`,`INS_TIME`,`DENOM_MUL_INFLOW`" +
              " FROM EVENT_FT_VLT_DENOMCORETXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_VLT_DENOMCORETXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"DENOMCODE\",\"BRANCH_CODE\",\"TILL_ID\",\"OUTFLOW\",\"XREF\",\"CCYCODE\",\"INFLOW\",\"POSTINGDATE\",\"INS_TIME\",\"DENOM_MUL_INFLOW\") values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[DENOMCODE],[BRANCH_CODE],[TILL_ID],[OUTFLOW],[XREF],[CCYCODE],[INFLOW],[POSTINGDATE],[INS_TIME],[DENOM_MUL_INFLOW]) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`DENOMCODE`,`BRANCH_CODE`,`TILL_ID`,`OUTFLOW`,`XREF`,`CCYCODE`,`INFLOW`,`POSTINGDATE`,`INS_TIME`,`DENOM_MUL_INFLOW`) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

