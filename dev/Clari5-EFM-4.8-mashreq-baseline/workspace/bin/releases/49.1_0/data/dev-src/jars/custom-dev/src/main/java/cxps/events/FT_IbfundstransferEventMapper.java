// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_IbfundstransferEventMapper extends EventMapper<FT_IbfundstransferEvent> {

public FT_IbfundstransferEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_IbfundstransferEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_IbfundstransferEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_IbfundstransferEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_IbfundstransferEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_IbfundstransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_IbfundstransferEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getPayeeAccountId());
            preparedStatement.setString(i++, obj.getPaymentType());
            preparedStatement.setString(i++, obj.getRemType());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getBenefType());
            preparedStatement.setString(i++, obj.getBankName());
            preparedStatement.setString(i++, obj.getAddrNetworkDerived());
            preparedStatement.setString(i++, obj.getPayeeMashreq());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getCustIdDerived());
            preparedStatement.setString(i++, obj.getPayeeBankName());
            preparedStatement.setString(i++, obj.getPayeeCustId());
            preparedStatement.setTimestamp(i++, obj.getAcctopendate());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setDouble(i++, obj.getAvlBalance());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getCustType());
            preparedStatement.setString(i++, obj.getTxnCrncy());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getIsBenefAdded());
            preparedStatement.setString(i++, obj.getTranMode());
            preparedStatement.setString(i++, obj.getBankId());
            preparedStatement.setString(i++, obj.getTxnRefNo());
            preparedStatement.setString(i++, obj.getSchmCode());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getRchrgeMobNo());
            preparedStatement.setString(i++, obj.getDomesticTrsnfrFlag());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getMccCode());
            preparedStatement.setString(i++, obj.getNetworkType());
            preparedStatement.setDouble(i++, obj.getAvgTxnAmt());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setTimestamp(i++, obj.getBenefAdditionDate());
            preparedStatement.setString(i++, obj.getSchmType());
            preparedStatement.setString(i++, obj.getAcctOwnership());
            preparedStatement.setString(i++, obj.getAccountNo());
            preparedStatement.setDouble(i++, obj.getDailyLimit());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_IBFUNDS_TRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_IbfundstransferEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_IbfundstransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_IBFUNDS_TRANSFER"));
        putList = new ArrayList<>();

        for (FT_IbfundstransferEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "PAYEE_ACCOUNT_ID",  obj.getPayeeAccountId());
            p = this.insert(p, "PAYMENT_TYPE",  obj.getPaymentType());
            p = this.insert(p, "REM_TYPE",  obj.getRemType());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "BENEF_TYPE",  obj.getBenefType());
            p = this.insert(p, "BANK_NAME",  obj.getBankName());
            p = this.insert(p, "ADDR_NETWORK_DERIVED",  obj.getAddrNetworkDerived());
            p = this.insert(p, "PAYEE_MASHREQ",  obj.getPayeeMashreq());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "CUST_ID_DERIVED",  obj.getCustIdDerived());
            p = this.insert(p, "PAYEE_BANK_NAME",  obj.getPayeeBankName());
            p = this.insert(p, "PAYEE_CUST_ID",  obj.getPayeeCustId());
            p = this.insert(p, "ACCTOPENDATE", String.valueOf(obj.getAcctopendate()));
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "AVL_BALANCE", String.valueOf(obj.getAvlBalance()));
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CUST_TYPE",  obj.getCustType());
            p = this.insert(p, "TXN_CRNCY",  obj.getTxnCrncy());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "IS_BENEF_ADDED",  obj.getIsBenefAdded());
            p = this.insert(p, "TRAN_MODE",  obj.getTranMode());
            p = this.insert(p, "BANK_ID",  obj.getBankId());
            p = this.insert(p, "TXN_REF_NO",  obj.getTxnRefNo());
            p = this.insert(p, "SCHM_CODE",  obj.getSchmCode());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "RCHRGE_MOB_NO",  obj.getRchrgeMobNo());
            p = this.insert(p, "DOMESTIC_TRSNFR_FLAG",  obj.getDomesticTrsnfrFlag());
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "MCC_CODE",  obj.getMccCode());
            p = this.insert(p, "NETWORK_TYPE",  obj.getNetworkType());
            p = this.insert(p, "AVG_TXN_AMT", String.valueOf(obj.getAvgTxnAmt()));
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "BENEF_ADDITION_DATE", String.valueOf(obj.getBenefAdditionDate()));
            p = this.insert(p, "SCHM_TYPE",  obj.getSchmType());
            p = this.insert(p, "ACCT_OWNERSHIP",  obj.getAcctOwnership());
            p = this.insert(p, "ACCOUNT_NO",  obj.getAccountNo());
            p = this.insert(p, "DAILY_LIMIT", String.valueOf(obj.getDailyLimit()));
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_IBFUNDS_TRANSFER"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_IBFUNDS_TRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_IBFUNDS_TRANSFER]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_IbfundstransferEvent obj = new FT_IbfundstransferEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setPayeeAccountId(rs.getString("PAYEE_ACCOUNT_ID"));
    obj.setPaymentType(rs.getString("PAYMENT_TYPE"));
    obj.setRemType(rs.getString("REM_TYPE"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setBenefType(rs.getString("BENEF_TYPE"));
    obj.setBankName(rs.getString("BANK_NAME"));
    obj.setAddrNetworkDerived(rs.getString("ADDR_NETWORK_DERIVED"));
    obj.setPayeeMashreq(rs.getString("PAYEE_MASHREQ"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setCustIdDerived(rs.getString("CUST_ID_DERIVED"));
    obj.setPayeeBankName(rs.getString("PAYEE_BANK_NAME"));
    obj.setPayeeCustId(rs.getString("PAYEE_CUST_ID"));
    obj.setAcctopendate(rs.getTimestamp("ACCTOPENDATE"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setAvlBalance(rs.getDouble("AVL_BALANCE"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCustType(rs.getString("CUST_TYPE"));
    obj.setTxnCrncy(rs.getString("TXN_CRNCY"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setIsBenefAdded(rs.getString("IS_BENEF_ADDED"));
    obj.setTranMode(rs.getString("TRAN_MODE"));
    obj.setBankId(rs.getString("BANK_ID"));
    obj.setTxnRefNo(rs.getString("TXN_REF_NO"));
    obj.setSchmCode(rs.getString("SCHM_CODE"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setRchrgeMobNo(rs.getString("RCHRGE_MOB_NO"));
    obj.setDomesticTrsnfrFlag(rs.getString("DOMESTIC_TRSNFR_FLAG"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setMccCode(rs.getString("MCC_CODE"));
    obj.setNetworkType(rs.getString("NETWORK_TYPE"));
    obj.setAvgTxnAmt(rs.getDouble("AVG_TXN_AMT"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setBenefAdditionDate(rs.getTimestamp("BENEF_ADDITION_DATE"));
    obj.setSchmType(rs.getString("SCHM_TYPE"));
    obj.setAcctOwnership(rs.getString("ACCT_OWNERSHIP"));
    obj.setAccountNo(rs.getString("ACCOUNT_NO"));
    obj.setDailyLimit(rs.getDouble("DAILY_LIMIT"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_IBFUNDS_TRANSFER]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_IbfundstransferEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_IbfundstransferEvent> events;
 FT_IbfundstransferEvent obj = new FT_IbfundstransferEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_IBFUNDS_TRANSFER"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_IbfundstransferEvent obj = new FT_IbfundstransferEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setPayeeAccountId(getColumnValue(rs, "PAYEE_ACCOUNT_ID"));
            obj.setPaymentType(getColumnValue(rs, "PAYMENT_TYPE"));
            obj.setRemType(getColumnValue(rs, "REM_TYPE"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setBenefType(getColumnValue(rs, "BENEF_TYPE"));
            obj.setBankName(getColumnValue(rs, "BANK_NAME"));
            obj.setAddrNetworkDerived(getColumnValue(rs, "ADDR_NETWORK_DERIVED"));
            obj.setPayeeMashreq(getColumnValue(rs, "PAYEE_MASHREQ"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setCustIdDerived(getColumnValue(rs, "CUST_ID_DERIVED"));
            obj.setPayeeBankName(getColumnValue(rs, "PAYEE_BANK_NAME"));
            obj.setPayeeCustId(getColumnValue(rs, "PAYEE_CUST_ID"));
            obj.setAcctopendate(EventHelper.toTimestamp(getColumnValue(rs, "ACCTOPENDATE")));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setAvlBalance(EventHelper.toDouble(getColumnValue(rs, "AVL_BALANCE")));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCustType(getColumnValue(rs, "CUST_TYPE"));
            obj.setTxnCrncy(getColumnValue(rs, "TXN_CRNCY"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setIsBenefAdded(getColumnValue(rs, "IS_BENEF_ADDED"));
            obj.setTranMode(getColumnValue(rs, "TRAN_MODE"));
            obj.setBankId(getColumnValue(rs, "BANK_ID"));
            obj.setTxnRefNo(getColumnValue(rs, "TXN_REF_NO"));
            obj.setSchmCode(getColumnValue(rs, "SCHM_CODE"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setRchrgeMobNo(getColumnValue(rs, "RCHRGE_MOB_NO"));
            obj.setDomesticTrsnfrFlag(getColumnValue(rs, "DOMESTIC_TRSNFR_FLAG"));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setMccCode(getColumnValue(rs, "MCC_CODE"));
            obj.setNetworkType(getColumnValue(rs, "NETWORK_TYPE"));
            obj.setAvgTxnAmt(EventHelper.toDouble(getColumnValue(rs, "AVG_TXN_AMT")));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setBenefAdditionDate(EventHelper.toTimestamp(getColumnValue(rs, "BENEF_ADDITION_DATE")));
            obj.setSchmType(getColumnValue(rs, "SCHM_TYPE"));
            obj.setAcctOwnership(getColumnValue(rs, "ACCT_OWNERSHIP"));
            obj.setAccountNo(getColumnValue(rs, "ACCOUNT_NO"));
            obj.setDailyLimit(EventHelper.toDouble(getColumnValue(rs, "DAILY_LIMIT")));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_IBFUNDS_TRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_IBFUNDS_TRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"PAYEE_ACCOUNT_ID\",\"PAYMENT_TYPE\",\"REM_TYPE\",\"ERROR_DESC\",\"CHANNEL\",\"SUCC_FAIL_FLG\",\"BENEF_TYPE\",\"BANK_NAME\",\"ADDR_NETWORK_DERIVED\",\"PAYEE_MASHREQ\",\"DEVICE_ID\",\"CUST_ID_DERIVED\",\"PAYEE_BANK_NAME\",\"PAYEE_CUST_ID\",\"ACCTOPENDATE\",\"ADDR_NETWORK\",\"AVL_BALANCE\",\"TRAN_ID\",\"HOST_ID\",\"CUST_TYPE\",\"TXN_CRNCY\",\"TRAN_AMT\",\"IS_BENEF_ADDED\",\"TRAN_MODE\",\"BANK_ID\",\"TXN_REF_NO\",\"SCHM_CODE\",\"USER_ID\",\"RCHRGE_MOB_NO\",\"DOMESTIC_TRSNFR_FLAG\",\"PAYEE_NAME\",\"SYS_TIME\",\"CUST_ID\",\"ERROR_CODE\",\"MCC_CODE\",\"NETWORK_TYPE\",\"AVG_TXN_AMT\",\"TRAN_TYPE\",\"AVL_BAL\",\"BENEF_ADDITION_DATE\",\"SCHM_TYPE\",\"ACCT_OWNERSHIP\",\"ACCOUNT_NO\",\"DAILY_LIMIT\"" +
              " FROM EVENT_FT_IBFUNDS_TRANSFER";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [PAYEE_ACCOUNT_ID],[PAYMENT_TYPE],[REM_TYPE],[ERROR_DESC],[CHANNEL],[SUCC_FAIL_FLG],[BENEF_TYPE],[BANK_NAME],[ADDR_NETWORK_DERIVED],[PAYEE_MASHREQ],[DEVICE_ID],[CUST_ID_DERIVED],[PAYEE_BANK_NAME],[PAYEE_CUST_ID],[ACCTOPENDATE],[ADDR_NETWORK],[AVL_BALANCE],[TRAN_ID],[HOST_ID],[CUST_TYPE],[TXN_CRNCY],[TRAN_AMT],[IS_BENEF_ADDED],[TRAN_MODE],[BANK_ID],[TXN_REF_NO],[SCHM_CODE],[USER_ID],[RCHRGE_MOB_NO],[DOMESTIC_TRSNFR_FLAG],[PAYEE_NAME],[SYS_TIME],[CUST_ID],[ERROR_CODE],[MCC_CODE],[NETWORK_TYPE],[AVG_TXN_AMT],[TRAN_TYPE],[AVL_BAL],[BENEF_ADDITION_DATE],[SCHM_TYPE],[ACCT_OWNERSHIP],[ACCOUNT_NO],[DAILY_LIMIT]" +
              " FROM EVENT_FT_IBFUNDS_TRANSFER";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`PAYEE_ACCOUNT_ID`,`PAYMENT_TYPE`,`REM_TYPE`,`ERROR_DESC`,`CHANNEL`,`SUCC_FAIL_FLG`,`BENEF_TYPE`,`BANK_NAME`,`ADDR_NETWORK_DERIVED`,`PAYEE_MASHREQ`,`DEVICE_ID`,`CUST_ID_DERIVED`,`PAYEE_BANK_NAME`,`PAYEE_CUST_ID`,`ACCTOPENDATE`,`ADDR_NETWORK`,`AVL_BALANCE`,`TRAN_ID`,`HOST_ID`,`CUST_TYPE`,`TXN_CRNCY`,`TRAN_AMT`,`IS_BENEF_ADDED`,`TRAN_MODE`,`BANK_ID`,`TXN_REF_NO`,`SCHM_CODE`,`USER_ID`,`RCHRGE_MOB_NO`,`DOMESTIC_TRSNFR_FLAG`,`PAYEE_NAME`,`SYS_TIME`,`CUST_ID`,`ERROR_CODE`,`MCC_CODE`,`NETWORK_TYPE`,`AVG_TXN_AMT`,`TRAN_TYPE`,`AVL_BAL`,`BENEF_ADDITION_DATE`,`SCHM_TYPE`,`ACCT_OWNERSHIP`,`ACCOUNT_NO`,`DAILY_LIMIT`" +
              " FROM EVENT_FT_IBFUNDS_TRANSFER";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_IBFUNDS_TRANSFER (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"PAYEE_ACCOUNT_ID\",\"PAYMENT_TYPE\",\"REM_TYPE\",\"ERROR_DESC\",\"CHANNEL\",\"SUCC_FAIL_FLG\",\"BENEF_TYPE\",\"BANK_NAME\",\"ADDR_NETWORK_DERIVED\",\"PAYEE_MASHREQ\",\"DEVICE_ID\",\"CUST_ID_DERIVED\",\"PAYEE_BANK_NAME\",\"PAYEE_CUST_ID\",\"ACCTOPENDATE\",\"ADDR_NETWORK\",\"AVL_BALANCE\",\"TRAN_ID\",\"HOST_ID\",\"CUST_TYPE\",\"TXN_CRNCY\",\"TRAN_AMT\",\"IS_BENEF_ADDED\",\"TRAN_MODE\",\"BANK_ID\",\"TXN_REF_NO\",\"SCHM_CODE\",\"USER_ID\",\"RCHRGE_MOB_NO\",\"DOMESTIC_TRSNFR_FLAG\",\"PAYEE_NAME\",\"SYS_TIME\",\"CUST_ID\",\"ERROR_CODE\",\"MCC_CODE\",\"NETWORK_TYPE\",\"AVG_TXN_AMT\",\"TRAN_TYPE\",\"AVL_BAL\",\"BENEF_ADDITION_DATE\",\"SCHM_TYPE\",\"ACCT_OWNERSHIP\",\"ACCOUNT_NO\",\"DAILY_LIMIT\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[PAYEE_ACCOUNT_ID],[PAYMENT_TYPE],[REM_TYPE],[ERROR_DESC],[CHANNEL],[SUCC_FAIL_FLG],[BENEF_TYPE],[BANK_NAME],[ADDR_NETWORK_DERIVED],[PAYEE_MASHREQ],[DEVICE_ID],[CUST_ID_DERIVED],[PAYEE_BANK_NAME],[PAYEE_CUST_ID],[ACCTOPENDATE],[ADDR_NETWORK],[AVL_BALANCE],[TRAN_ID],[HOST_ID],[CUST_TYPE],[TXN_CRNCY],[TRAN_AMT],[IS_BENEF_ADDED],[TRAN_MODE],[BANK_ID],[TXN_REF_NO],[SCHM_CODE],[USER_ID],[RCHRGE_MOB_NO],[DOMESTIC_TRSNFR_FLAG],[PAYEE_NAME],[SYS_TIME],[CUST_ID],[ERROR_CODE],[MCC_CODE],[NETWORK_TYPE],[AVG_TXN_AMT],[TRAN_TYPE],[AVL_BAL],[BENEF_ADDITION_DATE],[SCHM_TYPE],[ACCT_OWNERSHIP],[ACCOUNT_NO],[DAILY_LIMIT]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`PAYEE_ACCOUNT_ID`,`PAYMENT_TYPE`,`REM_TYPE`,`ERROR_DESC`,`CHANNEL`,`SUCC_FAIL_FLG`,`BENEF_TYPE`,`BANK_NAME`,`ADDR_NETWORK_DERIVED`,`PAYEE_MASHREQ`,`DEVICE_ID`,`CUST_ID_DERIVED`,`PAYEE_BANK_NAME`,`PAYEE_CUST_ID`,`ACCTOPENDATE`,`ADDR_NETWORK`,`AVL_BALANCE`,`TRAN_ID`,`HOST_ID`,`CUST_TYPE`,`TXN_CRNCY`,`TRAN_AMT`,`IS_BENEF_ADDED`,`TRAN_MODE`,`BANK_ID`,`TXN_REF_NO`,`SCHM_CODE`,`USER_ID`,`RCHRGE_MOB_NO`,`DOMESTIC_TRSNFR_FLAG`,`PAYEE_NAME`,`SYS_TIME`,`CUST_ID`,`ERROR_CODE`,`MCC_CODE`,`NETWORK_TYPE`,`AVG_TXN_AMT`,`TRAN_TYPE`,`AVL_BAL`,`BENEF_ADDITION_DATE`,`SCHM_TYPE`,`ACCT_OWNERSHIP`,`ACCOUNT_NO`,`DAILY_LIMIT`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

