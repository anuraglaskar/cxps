// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_IrisApplSalEventMapper extends EventMapper<NFT_IrisApplSalEvent> {

public NFT_IrisApplSalEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_IrisApplSalEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_IrisApplSalEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_IrisApplSalEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_IrisApplSalEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_IrisApplSalEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_IrisApplSalEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getIsEmployerNonsfa());
            preparedStatement.setString(i++, obj.getCommLandline());
            preparedStatement.setString(i++, obj.getNpaWrittenoff());
            preparedStatement.setString(i++, obj.getOffPoBox());
            preparedStatement.setString(i++, obj.getSellerName());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getEida());
            preparedStatement.setString(i++, obj.getTradeLicense());
            preparedStatement.setString(i++, obj.getOffAddress2());
            preparedStatement.setString(i++, obj.getResLandline());
            preparedStatement.setString(i++, obj.getOffAddress1());
            preparedStatement.setTimestamp(i++, obj.getAcctopendate());
            preparedStatement.setString(i++, obj.getIsIbanExternal());
            preparedStatement.setString(i++, obj.getIbanNumber());
            preparedStatement.setString(i++, obj.getPerPoBox());
            preparedStatement.setString(i++, obj.getApplStage());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getVat());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setDouble(i++, obj.getSalaryAmt());
            preparedStatement.setString(i++, obj.getPoBox());
            preparedStatement.setString(i++, obj.getEmailId());
            preparedStatement.setString(i++, obj.getNationality());
            preparedStatement.setTimestamp(i++, obj.getDob());
            preparedStatement.setString(i++, obj.getPassportNo());
            preparedStatement.setTimestamp(i++, obj.getSalaryDate());
            preparedStatement.setString(i++, obj.getSalaryTranType());
            preparedStatement.setTimestamp(i++, obj.getCreatedOn());
            preparedStatement.setString(i++, obj.getCommPoBox());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getMatchedDob());
            preparedStatement.setString(i++, obj.getDrivingLnc());
            preparedStatement.setDouble(i++, obj.getDebitAmount());
            preparedStatement.setTimestamp(i++, obj.getUpdatedOn());
            preparedStatement.setString(i++, obj.getMatchedEid());
            preparedStatement.setString(i++, obj.getLandlineNo());
            preparedStatement.setString(i++, obj.getPerLandline());
            preparedStatement.setString(i++, obj.getAppRefNo());
            preparedStatement.setString(i++, obj.getEmployeeId());
            preparedStatement.setString(i++, obj.getProduct());
            preparedStatement.setString(i++, obj.getPerAddress2());
            preparedStatement.setString(i++, obj.getCommAddress1());
            preparedStatement.setString(i++, obj.getAddress2());
            preparedStatement.setString(i++, obj.getCommAddress2());
            preparedStatement.setString(i++, obj.getAddress1());
            preparedStatement.setString(i++, obj.getMatchedPpno());
            preparedStatement.setString(i++, obj.getPerAddress1());
            preparedStatement.setString(i++, obj.getOffLandline());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getPhNo());
            preparedStatement.setString(i++, obj.getEmployerId());
            preparedStatement.setString(i++, obj.getSellerId());
            preparedStatement.setString(i++, obj.getResPoBox());
            preparedStatement.setString(i++, obj.getMatchedPhno());
            preparedStatement.setString(i++, obj.getSalaryNarative());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getNpaProduct());
            preparedStatement.setTimestamp(i++, obj.getNpaDate());
            preparedStatement.setString(i++, obj.getSuccFailFlag());
            preparedStatement.setString(i++, obj.getResAddress2());
            preparedStatement.setString(i++, obj.getResAddress1());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_IRIS_APPL_SAL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_IrisApplSalEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_IrisApplSalEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_IRIS_APPL_SAL"));
        putList = new ArrayList<>();

        for (NFT_IrisApplSalEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "IS_EMPLOYER_NONSFA",  obj.getIsEmployerNonsfa());
            p = this.insert(p, "COMM_LANDLINE",  obj.getCommLandline());
            p = this.insert(p, "NPA_WRITTENOFF",  obj.getNpaWrittenoff());
            p = this.insert(p, "OFF_PO_BOX",  obj.getOffPoBox());
            p = this.insert(p, "SELLER_NAME",  obj.getSellerName());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "EIDA",  obj.getEida());
            p = this.insert(p, "TRADE_LICENSE",  obj.getTradeLicense());
            p = this.insert(p, "OFF_ADDRESS2",  obj.getOffAddress2());
            p = this.insert(p, "RES_LANDLINE",  obj.getResLandline());
            p = this.insert(p, "OFF_ADDRESS1",  obj.getOffAddress1());
            p = this.insert(p, "ACCTOPENDATE", String.valueOf(obj.getAcctopendate()));
            p = this.insert(p, "IS_IBAN_EXTERNAL",  obj.getIsIbanExternal());
            p = this.insert(p, "IBAN_NUMBER",  obj.getIbanNumber());
            p = this.insert(p, "PER_PO_BOX",  obj.getPerPoBox());
            p = this.insert(p, "APPL_STAGE",  obj.getApplStage());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "VAT",  obj.getVat());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "SALARY_AMT", String.valueOf(obj.getSalaryAmt()));
            p = this.insert(p, "PO_BOX",  obj.getPoBox());
            p = this.insert(p, "EMAIL_ID",  obj.getEmailId());
            p = this.insert(p, "NATIONALITY",  obj.getNationality());
            p = this.insert(p, "DOB", String.valueOf(obj.getDob()));
            p = this.insert(p, "PASSPORT_NO",  obj.getPassportNo());
            p = this.insert(p, "SALARY_DATE", String.valueOf(obj.getSalaryDate()));
            p = this.insert(p, "SALARY_TRAN_TYPE",  obj.getSalaryTranType());
            p = this.insert(p, "CREATED_ON", String.valueOf(obj.getCreatedOn()));
            p = this.insert(p, "COMM_PO_BOX",  obj.getCommPoBox());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "MATCHED_DOB",  obj.getMatchedDob());
            p = this.insert(p, "DRIVING_LNC",  obj.getDrivingLnc());
            p = this.insert(p, "DEBIT_AMOUNT", String.valueOf(obj.getDebitAmount()));
            p = this.insert(p, "UPDATED_ON", String.valueOf(obj.getUpdatedOn()));
            p = this.insert(p, "MATCHED_EID",  obj.getMatchedEid());
            p = this.insert(p, "LANDLINE_NO",  obj.getLandlineNo());
            p = this.insert(p, "PER_LANDLINE",  obj.getPerLandline());
            p = this.insert(p, "APP_REF_NO",  obj.getAppRefNo());
            p = this.insert(p, "EMPLOYEE_ID",  obj.getEmployeeId());
            p = this.insert(p, "PRODUCT",  obj.getProduct());
            p = this.insert(p, "PER_ADDRESS2",  obj.getPerAddress2());
            p = this.insert(p, "COMM_ADDRESS1",  obj.getCommAddress1());
            p = this.insert(p, "ADDRESS2",  obj.getAddress2());
            p = this.insert(p, "COMM_ADDRESS2",  obj.getCommAddress2());
            p = this.insert(p, "ADDRESS1",  obj.getAddress1());
            p = this.insert(p, "MATCHED_PPNO",  obj.getMatchedPpno());
            p = this.insert(p, "PER_ADDRESS1",  obj.getPerAddress1());
            p = this.insert(p, "OFF_LANDLINE",  obj.getOffLandline());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "PH_NO",  obj.getPhNo());
            p = this.insert(p, "EMPLOYER_ID",  obj.getEmployerId());
            p = this.insert(p, "SELLER_ID",  obj.getSellerId());
            p = this.insert(p, "RES_PO_BOX",  obj.getResPoBox());
            p = this.insert(p, "MATCHED_PHNO",  obj.getMatchedPhno());
            p = this.insert(p, "SALARY_NARATIVE",  obj.getSalaryNarative());
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "NPA_PRODUCT",  obj.getNpaProduct());
            p = this.insert(p, "NPA_DATE", String.valueOf(obj.getNpaDate()));
            p = this.insert(p, "SUCC_FAIL_FLAG",  obj.getSuccFailFlag());
            p = this.insert(p, "RES_ADDRESS2",  obj.getResAddress2());
            p = this.insert(p, "RES_ADDRESS1",  obj.getResAddress1());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_IRIS_APPL_SAL"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_IRIS_APPL_SAL]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_IRIS_APPL_SAL]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_IrisApplSalEvent obj = new NFT_IrisApplSalEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setIsEmployerNonsfa(rs.getString("IS_EMPLOYER_NONSFA"));
    obj.setCommLandline(rs.getString("COMM_LANDLINE"));
    obj.setNpaWrittenoff(rs.getString("NPA_WRITTENOFF"));
    obj.setOffPoBox(rs.getString("OFF_PO_BOX"));
    obj.setSellerName(rs.getString("SELLER_NAME"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setEida(rs.getString("EIDA"));
    obj.setTradeLicense(rs.getString("TRADE_LICENSE"));
    obj.setOffAddress2(rs.getString("OFF_ADDRESS2"));
    obj.setResLandline(rs.getString("RES_LANDLINE"));
    obj.setOffAddress1(rs.getString("OFF_ADDRESS1"));
    obj.setAcctopendate(rs.getTimestamp("ACCTOPENDATE"));
    obj.setIsIbanExternal(rs.getString("IS_IBAN_EXTERNAL"));
    obj.setIbanNumber(rs.getString("IBAN_NUMBER"));
    obj.setPerPoBox(rs.getString("PER_PO_BOX"));
    obj.setApplStage(rs.getString("APPL_STAGE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setVat(rs.getString("VAT"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setSalaryAmt(rs.getDouble("SALARY_AMT"));
    obj.setPoBox(rs.getString("PO_BOX"));
    obj.setEmailId(rs.getString("EMAIL_ID"));
    obj.setNationality(rs.getString("NATIONALITY"));
    obj.setDob(rs.getTimestamp("DOB"));
    obj.setPassportNo(rs.getString("PASSPORT_NO"));
    obj.setSalaryDate(rs.getTimestamp("SALARY_DATE"));
    obj.setSalaryTranType(rs.getString("SALARY_TRAN_TYPE"));
    obj.setCreatedOn(rs.getTimestamp("CREATED_ON"));
    obj.setCommPoBox(rs.getString("COMM_PO_BOX"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setMatchedDob(rs.getString("MATCHED_DOB"));
    obj.setDrivingLnc(rs.getString("DRIVING_LNC"));
    obj.setDebitAmount(rs.getDouble("DEBIT_AMOUNT"));
    obj.setUpdatedOn(rs.getTimestamp("UPDATED_ON"));
    obj.setMatchedEid(rs.getString("MATCHED_EID"));
    obj.setLandlineNo(rs.getString("LANDLINE_NO"));
    obj.setPerLandline(rs.getString("PER_LANDLINE"));
    obj.setAppRefNo(rs.getString("APP_REF_NO"));
    obj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
    obj.setProduct(rs.getString("PRODUCT"));
    obj.setPerAddress2(rs.getString("PER_ADDRESS2"));
    obj.setCommAddress1(rs.getString("COMM_ADDRESS1"));
    obj.setAddress2(rs.getString("ADDRESS2"));
    obj.setCommAddress2(rs.getString("COMM_ADDRESS2"));
    obj.setAddress1(rs.getString("ADDRESS1"));
    obj.setMatchedPpno(rs.getString("MATCHED_PPNO"));
    obj.setPerAddress1(rs.getString("PER_ADDRESS1"));
    obj.setOffLandline(rs.getString("OFF_LANDLINE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setPhNo(rs.getString("PH_NO"));
    obj.setEmployerId(rs.getString("EMPLOYER_ID"));
    obj.setSellerId(rs.getString("SELLER_ID"));
    obj.setResPoBox(rs.getString("RES_PO_BOX"));
    obj.setMatchedPhno(rs.getString("MATCHED_PHNO"));
    obj.setSalaryNarative(rs.getString("SALARY_NARATIVE"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setNpaProduct(rs.getString("NPA_PRODUCT"));
    obj.setNpaDate(rs.getTimestamp("NPA_DATE"));
    obj.setSuccFailFlag(rs.getString("SUCC_FAIL_FLAG"));
    obj.setResAddress2(rs.getString("RES_ADDRESS2"));
    obj.setResAddress1(rs.getString("RES_ADDRESS1"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_IRIS_APPL_SAL]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_IrisApplSalEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_IrisApplSalEvent> events;
 NFT_IrisApplSalEvent obj = new NFT_IrisApplSalEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_IRIS_APPL_SAL"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_IrisApplSalEvent obj = new NFT_IrisApplSalEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setIsEmployerNonsfa(getColumnValue(rs, "IS_EMPLOYER_NONSFA"));
            obj.setCommLandline(getColumnValue(rs, "COMM_LANDLINE"));
            obj.setNpaWrittenoff(getColumnValue(rs, "NPA_WRITTENOFF"));
            obj.setOffPoBox(getColumnValue(rs, "OFF_PO_BOX"));
            obj.setSellerName(getColumnValue(rs, "SELLER_NAME"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setEida(getColumnValue(rs, "EIDA"));
            obj.setTradeLicense(getColumnValue(rs, "TRADE_LICENSE"));
            obj.setOffAddress2(getColumnValue(rs, "OFF_ADDRESS2"));
            obj.setResLandline(getColumnValue(rs, "RES_LANDLINE"));
            obj.setOffAddress1(getColumnValue(rs, "OFF_ADDRESS1"));
            obj.setAcctopendate(EventHelper.toTimestamp(getColumnValue(rs, "ACCTOPENDATE")));
            obj.setIsIbanExternal(getColumnValue(rs, "IS_IBAN_EXTERNAL"));
            obj.setIbanNumber(getColumnValue(rs, "IBAN_NUMBER"));
            obj.setPerPoBox(getColumnValue(rs, "PER_PO_BOX"));
            obj.setApplStage(getColumnValue(rs, "APPL_STAGE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setVat(getColumnValue(rs, "VAT"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setSalaryAmt(EventHelper.toDouble(getColumnValue(rs, "SALARY_AMT")));
            obj.setPoBox(getColumnValue(rs, "PO_BOX"));
            obj.setEmailId(getColumnValue(rs, "EMAIL_ID"));
            obj.setNationality(getColumnValue(rs, "NATIONALITY"));
            obj.setDob(EventHelper.toTimestamp(getColumnValue(rs, "DOB")));
            obj.setPassportNo(getColumnValue(rs, "PASSPORT_NO"));
            obj.setSalaryDate(EventHelper.toTimestamp(getColumnValue(rs, "SALARY_DATE")));
            obj.setSalaryTranType(getColumnValue(rs, "SALARY_TRAN_TYPE"));
            obj.setCreatedOn(EventHelper.toTimestamp(getColumnValue(rs, "CREATED_ON")));
            obj.setCommPoBox(getColumnValue(rs, "COMM_PO_BOX"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setMatchedDob(getColumnValue(rs, "MATCHED_DOB"));
            obj.setDrivingLnc(getColumnValue(rs, "DRIVING_LNC"));
            obj.setDebitAmount(EventHelper.toDouble(getColumnValue(rs, "DEBIT_AMOUNT")));
            obj.setUpdatedOn(EventHelper.toTimestamp(getColumnValue(rs, "UPDATED_ON")));
            obj.setMatchedEid(getColumnValue(rs, "MATCHED_EID"));
            obj.setLandlineNo(getColumnValue(rs, "LANDLINE_NO"));
            obj.setPerLandline(getColumnValue(rs, "PER_LANDLINE"));
            obj.setAppRefNo(getColumnValue(rs, "APP_REF_NO"));
            obj.setEmployeeId(getColumnValue(rs, "EMPLOYEE_ID"));
            obj.setProduct(getColumnValue(rs, "PRODUCT"));
            obj.setPerAddress2(getColumnValue(rs, "PER_ADDRESS2"));
            obj.setCommAddress1(getColumnValue(rs, "COMM_ADDRESS1"));
            obj.setAddress2(getColumnValue(rs, "ADDRESS2"));
            obj.setCommAddress2(getColumnValue(rs, "COMM_ADDRESS2"));
            obj.setAddress1(getColumnValue(rs, "ADDRESS1"));
            obj.setMatchedPpno(getColumnValue(rs, "MATCHED_PPNO"));
            obj.setPerAddress1(getColumnValue(rs, "PER_ADDRESS1"));
            obj.setOffLandline(getColumnValue(rs, "OFF_LANDLINE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setPhNo(getColumnValue(rs, "PH_NO"));
            obj.setEmployerId(getColumnValue(rs, "EMPLOYER_ID"));
            obj.setSellerId(getColumnValue(rs, "SELLER_ID"));
            obj.setResPoBox(getColumnValue(rs, "RES_PO_BOX"));
            obj.setMatchedPhno(getColumnValue(rs, "MATCHED_PHNO"));
            obj.setSalaryNarative(getColumnValue(rs, "SALARY_NARATIVE"));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setNpaProduct(getColumnValue(rs, "NPA_PRODUCT"));
            obj.setNpaDate(EventHelper.toTimestamp(getColumnValue(rs, "NPA_DATE")));
            obj.setSuccFailFlag(getColumnValue(rs, "SUCC_FAIL_FLAG"));
            obj.setResAddress2(getColumnValue(rs, "RES_ADDRESS2"));
            obj.setResAddress1(getColumnValue(rs, "RES_ADDRESS1"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_IRIS_APPL_SAL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_IRIS_APPL_SAL]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"IS_EMPLOYER_NONSFA\",\"COMM_LANDLINE\",\"NPA_WRITTENOFF\",\"OFF_PO_BOX\",\"SELLER_NAME\",\"CHANNEL\",\"DEVICE_ID\",\"EIDA\",\"TRADE_LICENSE\",\"OFF_ADDRESS2\",\"RES_LANDLINE\",\"OFF_ADDRESS1\",\"ACCTOPENDATE\",\"IS_IBAN_EXTERNAL\",\"IBAN_NUMBER\",\"PER_PO_BOX\",\"APPL_STAGE\",\"ACCOUNT_ID\",\"VAT\",\"BRANCH_ID\",\"SYS_TIME\",\"SALARY_AMT\",\"PO_BOX\",\"EMAIL_ID\",\"NATIONALITY\",\"DOB\",\"PASSPORT_NO\",\"SALARY_DATE\",\"SALARY_TRAN_TYPE\",\"CREATED_ON\",\"COMM_PO_BOX\",\"IP_COUNTRY\",\"MATCHED_DOB\",\"DRIVING_LNC\",\"DEBIT_AMOUNT\",\"UPDATED_ON\",\"MATCHED_EID\",\"LANDLINE_NO\",\"PER_LANDLINE\",\"APP_REF_NO\",\"EMPLOYEE_ID\",\"PRODUCT\",\"PER_ADDRESS2\",\"COMM_ADDRESS1\",\"ADDRESS2\",\"COMM_ADDRESS2\",\"ADDRESS1\",\"MATCHED_PPNO\",\"PER_ADDRESS1\",\"OFF_LANDLINE\",\"CUST_ID\",\"PH_NO\",\"EMPLOYER_ID\",\"SELLER_ID\",\"RES_PO_BOX\",\"MATCHED_PHNO\",\"SALARY_NARATIVE\",\"IP_ADDRESS\",\"TRAN_DATE\",\"NPA_PRODUCT\",\"NPA_DATE\",\"SUCC_FAIL_FLAG\",\"RES_ADDRESS2\",\"RES_ADDRESS1\"" +
              " FROM EVENT_NFT_IRIS_APPL_SAL";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [IS_EMPLOYER_NONSFA],[COMM_LANDLINE],[NPA_WRITTENOFF],[OFF_PO_BOX],[SELLER_NAME],[CHANNEL],[DEVICE_ID],[EIDA],[TRADE_LICENSE],[OFF_ADDRESS2],[RES_LANDLINE],[OFF_ADDRESS1],[ACCTOPENDATE],[IS_IBAN_EXTERNAL],[IBAN_NUMBER],[PER_PO_BOX],[APPL_STAGE],[ACCOUNT_ID],[VAT],[BRANCH_ID],[SYS_TIME],[SALARY_AMT],[PO_BOX],[EMAIL_ID],[NATIONALITY],[DOB],[PASSPORT_NO],[SALARY_DATE],[SALARY_TRAN_TYPE],[CREATED_ON],[COMM_PO_BOX],[IP_COUNTRY],[MATCHED_DOB],[DRIVING_LNC],[DEBIT_AMOUNT],[UPDATED_ON],[MATCHED_EID],[LANDLINE_NO],[PER_LANDLINE],[APP_REF_NO],[EMPLOYEE_ID],[PRODUCT],[PER_ADDRESS2],[COMM_ADDRESS1],[ADDRESS2],[COMM_ADDRESS2],[ADDRESS1],[MATCHED_PPNO],[PER_ADDRESS1],[OFF_LANDLINE],[CUST_ID],[PH_NO],[EMPLOYER_ID],[SELLER_ID],[RES_PO_BOX],[MATCHED_PHNO],[SALARY_NARATIVE],[IP_ADDRESS],[TRAN_DATE],[NPA_PRODUCT],[NPA_DATE],[SUCC_FAIL_FLAG],[RES_ADDRESS2],[RES_ADDRESS1]" +
              " FROM EVENT_NFT_IRIS_APPL_SAL";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`IS_EMPLOYER_NONSFA`,`COMM_LANDLINE`,`NPA_WRITTENOFF`,`OFF_PO_BOX`,`SELLER_NAME`,`CHANNEL`,`DEVICE_ID`,`EIDA`,`TRADE_LICENSE`,`OFF_ADDRESS2`,`RES_LANDLINE`,`OFF_ADDRESS1`,`ACCTOPENDATE`,`IS_IBAN_EXTERNAL`,`IBAN_NUMBER`,`PER_PO_BOX`,`APPL_STAGE`,`ACCOUNT_ID`,`VAT`,`BRANCH_ID`,`SYS_TIME`,`SALARY_AMT`,`PO_BOX`,`EMAIL_ID`,`NATIONALITY`,`DOB`,`PASSPORT_NO`,`SALARY_DATE`,`SALARY_TRAN_TYPE`,`CREATED_ON`,`COMM_PO_BOX`,`IP_COUNTRY`,`MATCHED_DOB`,`DRIVING_LNC`,`DEBIT_AMOUNT`,`UPDATED_ON`,`MATCHED_EID`,`LANDLINE_NO`,`PER_LANDLINE`,`APP_REF_NO`,`EMPLOYEE_ID`,`PRODUCT`,`PER_ADDRESS2`,`COMM_ADDRESS1`,`ADDRESS2`,`COMM_ADDRESS2`,`ADDRESS1`,`MATCHED_PPNO`,`PER_ADDRESS1`,`OFF_LANDLINE`,`CUST_ID`,`PH_NO`,`EMPLOYER_ID`,`SELLER_ID`,`RES_PO_BOX`,`MATCHED_PHNO`,`SALARY_NARATIVE`,`IP_ADDRESS`,`TRAN_DATE`,`NPA_PRODUCT`,`NPA_DATE`,`SUCC_FAIL_FLAG`,`RES_ADDRESS2`,`RES_ADDRESS1`" +
              " FROM EVENT_NFT_IRIS_APPL_SAL";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_IRIS_APPL_SAL (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"IS_EMPLOYER_NONSFA\",\"COMM_LANDLINE\",\"NPA_WRITTENOFF\",\"OFF_PO_BOX\",\"SELLER_NAME\",\"CHANNEL\",\"DEVICE_ID\",\"EIDA\",\"TRADE_LICENSE\",\"OFF_ADDRESS2\",\"RES_LANDLINE\",\"OFF_ADDRESS1\",\"ACCTOPENDATE\",\"IS_IBAN_EXTERNAL\",\"IBAN_NUMBER\",\"PER_PO_BOX\",\"APPL_STAGE\",\"ACCOUNT_ID\",\"VAT\",\"BRANCH_ID\",\"SYS_TIME\",\"SALARY_AMT\",\"PO_BOX\",\"EMAIL_ID\",\"NATIONALITY\",\"DOB\",\"PASSPORT_NO\",\"SALARY_DATE\",\"SALARY_TRAN_TYPE\",\"CREATED_ON\",\"COMM_PO_BOX\",\"IP_COUNTRY\",\"MATCHED_DOB\",\"DRIVING_LNC\",\"DEBIT_AMOUNT\",\"UPDATED_ON\",\"MATCHED_EID\",\"LANDLINE_NO\",\"PER_LANDLINE\",\"APP_REF_NO\",\"EMPLOYEE_ID\",\"PRODUCT\",\"PER_ADDRESS2\",\"COMM_ADDRESS1\",\"ADDRESS2\",\"COMM_ADDRESS2\",\"ADDRESS1\",\"MATCHED_PPNO\",\"PER_ADDRESS1\",\"OFF_LANDLINE\",\"CUST_ID\",\"PH_NO\",\"EMPLOYER_ID\",\"SELLER_ID\",\"RES_PO_BOX\",\"MATCHED_PHNO\",\"SALARY_NARATIVE\",\"IP_ADDRESS\",\"TRAN_DATE\",\"NPA_PRODUCT\",\"NPA_DATE\",\"SUCC_FAIL_FLAG\",\"RES_ADDRESS2\",\"RES_ADDRESS1\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[IS_EMPLOYER_NONSFA],[COMM_LANDLINE],[NPA_WRITTENOFF],[OFF_PO_BOX],[SELLER_NAME],[CHANNEL],[DEVICE_ID],[EIDA],[TRADE_LICENSE],[OFF_ADDRESS2],[RES_LANDLINE],[OFF_ADDRESS1],[ACCTOPENDATE],[IS_IBAN_EXTERNAL],[IBAN_NUMBER],[PER_PO_BOX],[APPL_STAGE],[ACCOUNT_ID],[VAT],[BRANCH_ID],[SYS_TIME],[SALARY_AMT],[PO_BOX],[EMAIL_ID],[NATIONALITY],[DOB],[PASSPORT_NO],[SALARY_DATE],[SALARY_TRAN_TYPE],[CREATED_ON],[COMM_PO_BOX],[IP_COUNTRY],[MATCHED_DOB],[DRIVING_LNC],[DEBIT_AMOUNT],[UPDATED_ON],[MATCHED_EID],[LANDLINE_NO],[PER_LANDLINE],[APP_REF_NO],[EMPLOYEE_ID],[PRODUCT],[PER_ADDRESS2],[COMM_ADDRESS1],[ADDRESS2],[COMM_ADDRESS2],[ADDRESS1],[MATCHED_PPNO],[PER_ADDRESS1],[OFF_LANDLINE],[CUST_ID],[PH_NO],[EMPLOYER_ID],[SELLER_ID],[RES_PO_BOX],[MATCHED_PHNO],[SALARY_NARATIVE],[IP_ADDRESS],[TRAN_DATE],[NPA_PRODUCT],[NPA_DATE],[SUCC_FAIL_FLAG],[RES_ADDRESS2],[RES_ADDRESS1]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`IS_EMPLOYER_NONSFA`,`COMM_LANDLINE`,`NPA_WRITTENOFF`,`OFF_PO_BOX`,`SELLER_NAME`,`CHANNEL`,`DEVICE_ID`,`EIDA`,`TRADE_LICENSE`,`OFF_ADDRESS2`,`RES_LANDLINE`,`OFF_ADDRESS1`,`ACCTOPENDATE`,`IS_IBAN_EXTERNAL`,`IBAN_NUMBER`,`PER_PO_BOX`,`APPL_STAGE`,`ACCOUNT_ID`,`VAT`,`BRANCH_ID`,`SYS_TIME`,`SALARY_AMT`,`PO_BOX`,`EMAIL_ID`,`NATIONALITY`,`DOB`,`PASSPORT_NO`,`SALARY_DATE`,`SALARY_TRAN_TYPE`,`CREATED_ON`,`COMM_PO_BOX`,`IP_COUNTRY`,`MATCHED_DOB`,`DRIVING_LNC`,`DEBIT_AMOUNT`,`UPDATED_ON`,`MATCHED_EID`,`LANDLINE_NO`,`PER_LANDLINE`,`APP_REF_NO`,`EMPLOYEE_ID`,`PRODUCT`,`PER_ADDRESS2`,`COMM_ADDRESS1`,`ADDRESS2`,`COMM_ADDRESS2`,`ADDRESS1`,`MATCHED_PPNO`,`PER_ADDRESS1`,`OFF_LANDLINE`,`CUST_ID`,`PH_NO`,`EMPLOYER_ID`,`SELLER_ID`,`RES_PO_BOX`,`MATCHED_PHNO`,`SALARY_NARATIVE`,`IP_ADDRESS`,`TRAN_DATE`,`NPA_PRODUCT`,`NPA_DATE`,`SUCC_FAIL_FLAG`,`RES_ADDRESS2`,`RES_ADDRESS1`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

