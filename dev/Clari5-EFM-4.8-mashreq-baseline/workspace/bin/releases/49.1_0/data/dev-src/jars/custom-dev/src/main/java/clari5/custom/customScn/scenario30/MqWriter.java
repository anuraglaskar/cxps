package clari5.custom.customScn.scenario30;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.rdbms.Rdbms;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cxps.apex.utils.StringUtils;
import org.jpos.iso.ISOTagBinaryFieldPackager;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MqWriter {
    public static CxpsLogger logger = CxpsLogger.getLogger(MqWriter.class);
    private static ThreadDetails threadDetails=CustEodReminder.getThreadDetails();
    static {
        ECClient.configure(null);
    }
    public static void writeJson(){
        try(RDBMSSession rdbmsSession= Rdbms.getAppSession();
            Connection connection=rdbmsSession.getCxConnection();){
            PreparedStatement preparedStatement=connection.prepareStatement(threadDetails.getSelectQuery());
            preparedStatement.setString(1,threadDetails.getCurrentThreadName());
            preparedStatement.setDate(2,threadDetails.getThreadDate());

            ResultSet resultSet=preparedStatement.executeQuery();
            if(resultSet.next()){
                logger.info("[MqWriter] Thread Name on this date exist!!");
            }
            else{
                CustEodReminder.insert(threadDetails.getCurrentThreadName(),threadDetails.getThrdId(),threadDetails.getThreadDate(),threadDetails.getInsrtQry());
                logger.info("MqWriter called ");
                WLCustomerMatch wlCustomerMatch= new WLCustomerMatch();
                wlCustomerMatch.getMatchedResult();
                //threadDetails=null;
            }
        }catch(Exception se){
                logger.info("Error While Selecting Records from Thread Audit"+ se.getCause()+ " "+ se.getMessage());
    }

    }
    public static  void  resposeFormatter(String response, String newcif) {
        logger.info("[MqWriter] Formatting the Response");
        String custid="";
        String matched_email="NA";
        String matched_mobile="NA";
        try {
            JsonParser parser = new JsonParser();
            //if(response!=null){
            //JsonObject jsonObject = parser.parse(response).getAsJsonObject();
            if (!StringUtils.isNullOrEmpty(response)) {
              //  JSONObject jsonObject = new JSONObject(response);
                JsonObject jsonObject = parser.parse(response).getAsJsonObject();
                if (jsonObject.has("matchedResults")) {
                jsonObject = jsonObject.getAsJsonObject("matchedResults");
                if (jsonObject.has("matchedRecords")) {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("matchedRecords");
                    for (int j = 0; j < jsonArray.size(); j++) {
                        jsonObject = (JsonObject) jsonArray.get(j);
                        custid = jsonObject.get("id").toString().replaceAll("\"", "");
                        if (custid.equalsIgnoreCase(newcif)) {
                            System.out.println("both are same customer ");
                        } else {
                            JsonArray fieldjsonArray = (JsonArray) jsonObject.get("fields");
                            int size = fieldjsonArray.size();
                            List<CustomerResultPojo> datalist = CustomerData.getCustomerMatchedDatalist(custid);
                            for (CustomerResultPojo customerResultPojo : datalist) {
                                for (int i = 0; i < size; i++) {
                                    JsonObject fieldjsonObject = (JsonObject) fieldjsonArray.get(i);
                                    String score = fieldjsonObject.get("score").toString().replaceAll("\"", "");
                                    //float finalscore = Float.valueOf(score);
                                    Double finalscore = Double.parseDouble(score);
                                    System.out.println("final score -" + finalscore);
                                    if (finalscore == 1.0) {
                                        String name = fieldjsonObject.get("name").toString().replaceAll("\"", "");
                                        String value = fieldjsonObject.get("value").toString().replaceAll("\"", "");
                                        String queryVal = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                        if (name.equalsIgnoreCase("custEmail1")) {
                                            matched_email = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                        } else if (name.equalsIgnoreCase("custMobile1")) {
                                            matched_mobile = fieldjsonObject.get("queryValue").toString().replaceAll("\"", "");
                                        }
                                    }
                                }

                                        logger.info("matched email--" + matched_email + "matched mobile --" + matched_mobile);
                                        CustomerResultPojo finaldata = new CustomerResultPojo();
                                        CustomerResultPojo newdata = CustomerData.getCustomerNewData(newcif);
                                        finaldata.setNew_cxcif(newdata.getNew_cxcif());
                                        finaldata.setNew_eid(newdata.getNew_eid());
                                        finaldata.setNew_custName(newdata.getNew_custName());
                                        finaldata.setNew_custType(newdata.getNew_custType());
                                        finaldata.setNew_passport(newdata.getNew_passport());
                                        finaldata.setNew_email(newdata.getNew_email());
                                        finaldata.setNew_mobile(newdata.getNew_mobile());
                                        finaldata.setNew_compmis1(newdata.getNew_compmis1());
                                        finaldata.setNew_compmis2(newdata.getNew_compmis2());


                                        finaldata.setOld_cif(customerResultPojo.getOld_cif());
                                        finaldata.setOld_email(customerResultPojo.getOld_email());
                                        finaldata.setOld_mobile(customerResultPojo.getOld_mobile());
                                        finaldata.setOld_eid(customerResultPojo.getOld_eid());
                                        finaldata.setOld_cust_name(customerResultPojo.getOld_cust_name());
                                        finaldata.setOld_compmis1(customerResultPojo.getOld_compmis1());
                                        finaldata.setOld_compmis2(customerResultPojo.getOld_compmis2());
                                        finaldata.setOld_custType(customerResultPojo.getOld_custType());
                                        finaldata.setOld_passportNo(customerResultPojo.getOld_passportNo());
                                        finaldata.setMatched_mobile(matched_mobile);
                                        finaldata.setMatched_email(matched_email);

                                        writeJosntoMq(finaldata);
                                        logger.info("[MqWriter] Formatting Completed");

                                        matched_email = "NA";
                                        matched_mobile = "NA";
                                    }

                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  static  boolean writeJosntoMq(CustomerResultPojo finaldata){
        JSONObject json = new JSONObject();
        JSONObject msg = new JSONObject();
        String entity_id = "multiplecustomer";
            String event_id=getRandomNum();
            json.put("EventType", "nft");
            json.put("EventSubType", "multiplecustomer");
            json.put("event_name", "nft_multiplecustomer");
            msg.put("sys_time", getSysTime());
            msg.put("host_id", "F");
            msg.put("event_id", event_id.replaceAll("-",""));
            msg.put("new_cxcif", finaldata.getNew_cxcif().replaceAll("C_F_",""));
            msg.put("new_email", finaldata.getNew_email() != null ? finaldata.getNew_email() : "");
            msg.put("new_mobile", finaldata.getNew_mobile() != null ? finaldata.getNew_mobile() : "");
            msg.put("new_eid", finaldata.getNew_eid() != null ? finaldata.getNew_eid() : "");
            msg.put("new_passport", finaldata.getNew_passport() !=null ? finaldata.getNew_passport() : "");
            msg.put("new_custName", finaldata.getNew_custName() != null ? finaldata.getNew_custName() : "");
            msg.put("new_custType", finaldata.getNew_custType() != null ? finaldata.getNew_custType() : "");
            msg.put("new_compmis1", finaldata.getNew_compmis1() != null ? finaldata.getNew_compmis1() : "");
            msg.put("new_compmis2", finaldata.getNew_compmis2()!= null ? finaldata.getNew_compmis2(): "");
            msg.put("old_cif", finaldata.getOld_cif().replaceAll("C_F_",""));
            msg.put("old_compmis1", finaldata.getOld_compmis1()!= null ? finaldata.getOld_compmis1() : "");
            msg.put("old_compmis2", finaldata.getOld_compmis2() != null ? finaldata.getOld_compmis2() : "");
            msg.put("old_cust_name", finaldata.getOld_cust_name() != null ? finaldata.getOld_cust_name() : "");
            msg.put("old_passportNo", finaldata.getOld_passportNo() != null ? finaldata.getOld_passportNo() : "");
            msg.put("old_eid", finaldata.getOld_eid() != null ? finaldata.getOld_eid() : "");
            msg.put("old_email", finaldata.getOld_email() != null ? finaldata.getOld_email() : "");
            msg.put("old_cust_name", finaldata.getOld_cust_name() != null ? finaldata.getOld_cust_name() : "");
            msg.put("old_custType", finaldata.getOld_custType() != null ? finaldata.getOld_custType() : "");
            msg.put("matched_email", finaldata.getMatched_email() != null ? finaldata.getMatched_email() : "");
            msg.put("matched_mobile", finaldata.getMatched_mobile() != null ? finaldata.getMatched_mobile() : "");
            msg.put("old_mobile", finaldata.getOld_mobile() != null ? finaldata.getOld_mobile() : "");

            json.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                    replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

            logger.info("[MqWriter] Final JSON: " + json.toString());
            boolean status = ECClient.enqueue("HOST", entity_id, event_id, json.toString());
            logger.info("[MqWriter] The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        return false;

    }
    public  static  String getRandomNum(){
        Random rand = new Random();
        int randomnum=rand.nextInt();

        return String.valueOf(randomnum);
    }
    public  static String getSysTime(){
        String systime="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= new Date();
            systime=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return systime;

    }
    public static void main(String []args){
        System.out.println("into mmain--");
    }
}
