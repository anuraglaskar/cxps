cxps.noesis.glossary.entity.SCN11_DETAILS_TRACK {
        db-name = SCN11_DETAILS_TRACK
        generate = false
        tablespace = CXPS_USERS
        attributes = [
        { name= account, column =Account, type ="string:20",key=false}
        { name= cust-id, column =Cust_id, type ="string:20",key=false}
        { name= user-id, column =UserID, type ="string:100",key=false}
        { name= status, column =status, type ="string:10",key=false}
        { name= initial-acct-status, column =initial_acct_status, type="string:20",key=false}
        { name= entity-type , column =entity_type ,type="string:200",key=false}
                  ]
     
}

