// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_APPLMATCHEDREJAPPL", Schema="rice")
public class NFT_ApplmatchedrejapplEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String isEmployerNonsfa;
       @Field(size=50) public String rejCif;
       @Field(size=50) public String rejEid;
       @Field(size=50) public String rejPassportno;
       @Field public java.sql.Timestamp rejectionDate;
       @Field(size=50) public String currentDate;
       @Field(size=20) public String channel;
       @Field(size=50) public String applName;
       @Field public java.sql.Timestamp matchedDateofbirth;
       @Field(size=20) public String eida;
       @Field(size=50) public String tradeLicense;
       @Field public java.sql.Timestamp rejDateofbirth;
       @Field(size=50) public String drivingLnc;
       @Field(size=20) public String matchedEid;
       @Field(size=50) public String ibanNo;
       @Field(size=20) public String matchedMobileno;
       @Field(size=20) public String appRefNo;
       @Field(size=50) public String employeeId;
       @Field(size=20) public String product;
       @Field(size=50) public String declineReason;
       @Field(size=20) public String accountId;
       @Field(size=50) public String rejProductCode;
       @Field(size=50) public String vat;
       @Field(size=50) public String branchId;
       @Field(size=20) public String custId;
       @Field(size=20) public String phNo;
       @Field(size=50) public String employerId;
       @Field public java.sql.Timestamp applDate;
       @Field public java.sql.Timestamp rejApplDate;
       @Field(size=20) public String emailId;
       @Field(size=200) public String rejApplId;
       @Field public java.sql.Timestamp dob;
       @Field(size=50) public String rejMobileno;
       @Field(size=50) public String matchedPassportno;
       @Field(size=50) public String passportNo;


    @JsonIgnore
    public ITable<NFT_ApplmatchedrejapplEvent> t = AEF.getITable(this);

	public NFT_ApplmatchedrejapplEvent(){}

    public NFT_ApplmatchedrejapplEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Applmatchedrejappl");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setIsEmployerNonsfa(json.getString("is_employer_nonsfa"));
            setRejCif(json.getString("rej_cif"));
            setRejEid(json.getString("rej_eid"));
            setRejPassportno(json.getString("rej_passportno"));
            setRejectionDate(EventHelper.toTimestamp(json.getString("rejection_date")));
            setCurrentDate(json.getString("current_date"));
            setChannel(json.getString("channel"));
            setApplName(json.getString("appl_name"));
            setMatchedDateofbirth(EventHelper.toTimestamp(json.getString("matched_dateofbirth")));
            setEida(json.getString("eida"));
            setTradeLicense(json.getString("trade_license"));
            setRejDateofbirth(EventHelper.toTimestamp(json.getString("rej_dateofbirth")));
            setDrivingLnc(json.getString("driving_lnc"));
            setMatchedEid(json.getString("matched_eid"));
            setIbanNo(json.getString("iban_no"));
            setMatchedMobileno(json.getString("matched_mobileno"));
            setAppRefNo(json.getString("app_ref_no"));
            setEmployeeId(json.getString("employee_id"));
            setProduct(json.getString("product"));
            setDeclineReason(json.getString("decline_reason"));
            setAccountId(json.getString("account_id"));
            setRejProductCode(json.getString("rej_product_code"));
            setVat(json.getString("vat"));
            setBranchId(json.getString("branch_id"));
            setCustId(json.getString("cust_id"));
            setPhNo(json.getString("ph_no"));
            setEmployerId(json.getString("employer_id"));
            setApplDate(EventHelper.toTimestamp(json.getString("appl_date")));
            setRejApplDate(EventHelper.toTimestamp(json.getString("rej_appl_date")));
            setEmailId(json.getString("email_id"));
            setRejApplId(json.getString("rej_appl_id"));
            setDob(EventHelper.toTimestamp(json.getString("dob")));
            setRejMobileno(json.getString("rej_mobileno"));
            setMatchedPassportno(json.getString("matched_passportno"));
            setPassportNo(json.getString("passport_no"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NRA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getIsEmployerNonsfa(){ return isEmployerNonsfa; }

    public String getRejCif(){ return rejCif; }

    public String getRejEid(){ return rejEid; }

    public String getRejPassportno(){ return rejPassportno; }

    public java.sql.Timestamp getRejectionDate(){ return rejectionDate; }

    public String getCurrentDate(){ return currentDate; }

    public String getChannel(){ return channel; }

    public String getApplName(){ return applName; }

    public java.sql.Timestamp getMatchedDateofbirth(){ return matchedDateofbirth; }

    public String getEida(){ return eida; }

    public String getTradeLicense(){ return tradeLicense; }

    public java.sql.Timestamp getRejDateofbirth(){ return rejDateofbirth; }

    public String getDrivingLnc(){ return drivingLnc; }

    public String getMatchedEid(){ return matchedEid; }

    public String getIbanNo(){ return ibanNo; }

    public String getMatchedMobileno(){ return matchedMobileno; }

    public String getAppRefNo(){ return appRefNo; }

    public String getEmployeeId(){ return employeeId; }

    public String getProduct(){ return product; }

    public String getDeclineReason(){ return declineReason; }

    public String getAccountId(){ return accountId; }

    public String getRejProductCode(){ return rejProductCode; }

    public String getVat(){ return vat; }

    public String getBranchId(){ return branchId; }

    public String getCustId(){ return custId; }

    public String getPhNo(){ return phNo; }

    public String getEmployerId(){ return employerId; }

    public java.sql.Timestamp getApplDate(){ return applDate; }

    public java.sql.Timestamp getRejApplDate(){ return rejApplDate; }

    public String getEmailId(){ return emailId; }

    public String getRejApplId(){ return rejApplId; }

    public java.sql.Timestamp getDob(){ return dob; }

    public String getRejMobileno(){ return rejMobileno; }

    public String getMatchedPassportno(){ return matchedPassportno; }

    public String getPassportNo(){ return passportNo; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setIsEmployerNonsfa(String val){ this.isEmployerNonsfa = val; }
    public void setRejCif(String val){ this.rejCif = val; }
    public void setRejEid(String val){ this.rejEid = val; }
    public void setRejPassportno(String val){ this.rejPassportno = val; }
    public void setRejectionDate(java.sql.Timestamp val){ this.rejectionDate = val; }
    public void setCurrentDate(String val){ this.currentDate = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setApplName(String val){ this.applName = val; }
    public void setMatchedDateofbirth(java.sql.Timestamp val){ this.matchedDateofbirth = val; }
    public void setEida(String val){ this.eida = val; }
    public void setTradeLicense(String val){ this.tradeLicense = val; }
    public void setRejDateofbirth(java.sql.Timestamp val){ this.rejDateofbirth = val; }
    public void setDrivingLnc(String val){ this.drivingLnc = val; }
    public void setMatchedEid(String val){ this.matchedEid = val; }
    public void setIbanNo(String val){ this.ibanNo = val; }
    public void setMatchedMobileno(String val){ this.matchedMobileno = val; }
    public void setAppRefNo(String val){ this.appRefNo = val; }
    public void setEmployeeId(String val){ this.employeeId = val; }
    public void setProduct(String val){ this.product = val; }
    public void setDeclineReason(String val){ this.declineReason = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setRejProductCode(String val){ this.rejProductCode = val; }
    public void setVat(String val){ this.vat = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setPhNo(String val){ this.phNo = val; }
    public void setEmployerId(String val){ this.employerId = val; }
    public void setApplDate(java.sql.Timestamp val){ this.applDate = val; }
    public void setRejApplDate(java.sql.Timestamp val){ this.rejApplDate = val; }
    public void setEmailId(String val){ this.emailId = val; }
    public void setRejApplId(String val){ this.rejApplId = val; }
    public void setDob(java.sql.Timestamp val){ this.dob = val; }
    public void setRejMobileno(String val){ this.rejMobileno = val; }
    public void setMatchedPassportno(String val){ this.matchedPassportno = val; }
    public void setPassportNo(String val){ this.passportNo = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.currentDate);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Applmatchedrejappl");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Applmatchedrejappl");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}