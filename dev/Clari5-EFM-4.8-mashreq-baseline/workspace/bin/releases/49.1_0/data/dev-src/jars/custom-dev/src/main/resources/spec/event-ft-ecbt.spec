cxps.events.event.ft-ecbt{  
  table-name : EVENT_FT_ECBT
  event-mnemonic: ECB
  workspaces : {
    NONCUSTOMER: "benef-iban,card-no"
  }
 event-attributes : {

tran-type: {db:true ,raw_name : tran_type ,type:"string:20"}
account-id: {db:true ,raw_name : account_id ,type:"string:20"}
benef-iban: {db :true ,raw_name : benef_iban ,type : "string:50"}
benef-int-ext: {db:true ,raw_name : benef_int_ext ,type:"string:20"}
card-no: {db:true ,raw_name : card_no ,type:"string:50"}
cust-id: {db:true ,raw_name : cust_id ,type:"string:20"}
tran-amt: {db:true ,raw_name : tran_amt ,type:"number:20,4"}
channel: {db:true ,raw_name : channel ,type:"string:20"}
is-info-chngd: {db:true ,raw_name : is_info_chngd ,type:"string:20" , derivation:"""cxps.events.CustomDerivator.getIsInfoChngd(this)"""}
info-chngd-time: {db:true ,raw_name : info_chngd_time ,type:timestamp}
}
}
