<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <%@ page language="java" import="clari5.custom.jiraevidencformator.FetchingData"%>
        <%@ page import="org.json.JSONObject" %>
            <% String ip=request.getParameter("id");%>
                <% FetchingData fetchingData= new FetchingData(); %>
                    <%JSONObject json  = fetchingData.fetch(ip);
   System.out.println("json---"+json);  
%>

                        <!DOCTYPE html>
                        <html>

                        <head>
                            <meta charset="UTF-8">

                            <title>Evidence</title>
                            <meta charset="utf-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


                            <script>
                                function formatTable() {
                                    var data = <%=json%>;
                                    var itemDetail = data;
                                    var str = itemDetail.evidence;
                                    var arr = [];

                                    function customStringify(str) {
                                        let counter = 0;
                                        str.split("\r\n\r\n").map(data => {
                                            arr[counter] = data;
                                            counter++;
                                        });
                                    }
                                    customStringify(str);

                                    function printArray(arr) {
                                        var resArr = [];
                                        let evidenceMessage = "";
                                        var evidenceArr = [];
                                        var headerCount;
                                        var finalHTML = "";

                                        for (var i = 0; i < arr.length - 1; i++) {
                                            var tableBody = "";
                                            var evidenceObj = {};
                                            var objCounter = 0;
                                            evidenceArr = arr[i].split("\r\n||");
                                            for (var a = 1; a < evidenceArr.length; a++) {
                                                var c;
						var flag=0;
                                                tableBody = tableBody.concat('<table class="table table-bordered table-striped">');
                                                tableBody = tableBody.concat('<tr>');
                                                var headerVal = evidenceArr[a].split("\||");
                                                for (var k = 0; k < headerVal.length - 1; k++) {
                                                    if (headerVal[k] == "TRNS AMOUNT") {
                                                        c = k;
							flag=1;
                                                    }
                                                    tableBody = tableBody.concat('<td class="bold-text">').concat(headerVal[k]).concat('</td>');
                                                }
                                                tableBody = tableBody.concat('</tr>');
                                                var dataval = headerVal[headerVal.length - 1].split("\r\n|");
                                                dataval = dataval.filter(function(e) {
                                                    return e.replace(/(\r\n|\n|\r)/gm, "")
                                                });
                                                for (var z = 0; z < dataval.length; z++) {
                                                    tableBody = tableBody.concat('<tr>');
                                                    var data = dataval[z].split("|");
                                                    for (var k = 0; k < data.length; k++) {
                                                        if (c == k && flag==1) {
                                                            var num = data[k];
                                                            var num1 = new Intl.NumberFormat("en", {
                                                                style: 'currency',
                                                                currency: 'AED',
                                                                minimumFractionDigits: 2
                                                            });
                                                            var n2 = num1.format(num);
                                                            var n = n2.replace("AED", "");
                                                            tableBody = tableBody.concat('<td style="text-align:right;">').concat(n).concat('</td>');

                                                        } else {
                                                            tableBody = tableBody.concat('<td>').concat(data[k]).concat('</td>');
                                                        }

                                                    }
                                                    tableBody = tableBody.concat('</tr>');
                                                }
                                            }
                                            tableBody = tableBody.concat('</tbody></table>');
                                            finalHTML = finalHTML.concat('</br><b>').concat(evidenceArr[0]).concat(':</b> </br> </br>').concat(tableBody);
                                        }
                                        document.getElementById("tbl").innerHTML = finalHTML;
                                    }
                                    printArray(arr);
                                }
                            </script>

                            <style>
                                .bold-text {
                                    font-weight: bold;
                                    color: white;
                                    background-color: #5F5F5F;
                                }
                                
                                .table-striped>tbody>tr:nth-of-type(odd) {
                                    background-color: #ccc !important;
                                }
                            </style>
                        </head>

                        <body onload="formatTable()">

                            <div class="container">
                                <div class="row">
                                    <div id="tbl" class="col-sm-12">

                                    </div>
                                </div>
                            </div>

                        </body>

                        </html>
