cxps.events.event.ft-vlt-denomcoretxn{
  table-name : EVENT_FT_VLT_DENOMCORETXN
  event-mnemonic: VLT
  workspaces : {
    BRANCH : "branch-code"
  }
  event-attributes : {
branch-code: {db : true ,raw_name : branch_code ,type : "string:50"}
till_id: {db : true ,raw_name : till_id ,type : "string:50"}
xref: {db : true ,raw_name : xref ,type : "string:500"}
postingdate: {db : true ,raw_name : postingdate ,type : timestamp}
ccycode: {db : true ,raw_name : ccycode ,type : "string:50"}
denomcode: {db:true ,raw_name : denomcode ,type:"string:50"}
inflow: {db :true ,raw_name : inflow ,type : "number:20,3"}
outflow: {db :true ,raw_name : outflow ,type : "number:20,3"}
ins-time: {db :true ,raw_name : ins_time ,type : timestamp}
denom_mul_inflow: {db:true ,raw_name : denom-mul-inflow ,type:"number:20,3" , derivation :"""cxps.events.CustomDerivator.getdenomMulInflow(this)"""}
}
}
