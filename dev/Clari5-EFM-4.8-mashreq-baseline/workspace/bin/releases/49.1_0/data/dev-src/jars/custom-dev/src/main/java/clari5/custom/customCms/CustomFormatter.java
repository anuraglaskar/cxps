package clari5.custom.customCms;

import clari5.aml.cms.jira.display.Formatter;
import clari5.hfdb.Hfdb;
import clari5.hfdb.HostEvent;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.platform.util.Hocon;
import clari5.trace.TracerUtil;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.swagger.util.Json;
import org.apache.kafka.common.protocol.types.Field;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

public class CustomFormatter extends Formatter {

    static HashMap cmsEventNames = new HashMap();
    static Hocon fieldHocon = new Hocon();
    static {
        fieldHocon.loadFromContext("CmsEventNameDisplay.conf");
        Hocon moduleHocon = fieldHocon.get("cari5.mashreq.events");

        List<String> moduleList = moduleHocon.getKeysAsList();

        for (String moduleId : moduleList) {
            Hocon customHocon = moduleHocon.get(moduleId);
            Set<String> keys = customHocon.getKeys();
            //Hocon h = customHocon.get(this.getEventSubType());
            Iterator itr =  keys.iterator();
            while(itr.hasNext()){
                String key = (String)itr.next();
                System.out.println("key = "+key+" value = "+customHocon.get(key).getString("eventname"));
                cmsEventNames.put(key,customHocon.get(key).getString("eventname"));
                System.out.println("in static block");
            }

        }

    }

    public static String getEnglish(HostEvent ev, Hocon msgBody) {
        /*System.out.println("event name --"+ev.getEventName());
       if (ev.getEventName().equalsIgnoreCase("")){
           return getEnglishHelper(ev,msgBody);
       }
        System.out.println("this is not account opening event");*/
        // HashMap<String, String> fields = Hfdb.getEventFieldContainer().getEventFields(ev.getEventName());
        HashMap<String, String> fields = Hfdb.getEventFieldContainer().getEventFields(ev.getEventName());
        System.out.println(fields.toString());
        if (fields == null) {
            return ev.getEventId();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("|");
        boolean firstTime = true;
        for (String field : fields.keySet()) {

            String val = msgBody.getString(field, "Not Available");
            if (val != null && val.trim().equals("") && val.isEmpty())
                val = "Not Available";

            if (firstTime) {
                firstTime = false;
            } else {
                sb.append("|");
            }
            try {

                if (!(val.matches("[0-9]+") && val.startsWith("0"))) {
                    BigDecimal value = new BigDecimal(val);
                    String str = value.toPlainString();

                    if (str != null && str.endsWith(".0"))
                        str = str.substring(0, str.length() - 2);
                    sb.append(str);
                } else {
                    sb.append(val);
                }
            } catch (Exception e) {
                sb.append(val);
            }
            //FIXME: handle it using meta info from hocon. introduce meta info in hocon.
            //If you don't understand what does it mean, please ask Lovish / Utsav before it's too late.

            //In order to show double values in non-exponential format.
            try {
                if (field.equalsIgnoreCase("eventname") || field.equalsIgnoreCase("EVENT_NAME")) {
                    if (val.equalsIgnoreCase("Not Available")) {
                        String name = ev.getEventName();
                        if (name.startsWith("cxps.events.")) {
                            name = name.substring(12, 13).toUpperCase() + name.substring(13);
                            sb.setLength(sb.length()-13);
                            if(cmsEventNames.containsKey(name)){
                                sb.append(cmsEventNames.get(name).toString());
                            }
                            else {
                                sb.append(name);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("eventname not derived");
            }
            }
        return sb.toString();
    }
    /*public static String getEnglishHelper(HostEvent ev, Hocon msgBody) {
        System.out.println("getEnglishHelper called");
        String eventid = "vikas1";
        String msgBodyJSON = msgBody.getJSON();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(msgBodyJSON).getAsJsonObject();
        String eventid2 = jsonObject.get("event_id").toString().replaceAll("\'","");
        System.out.println("event id from json:: "+ eventid2);
        List<InsertWlMatch> customdata = getDatalist( eventid);

        // HashMap<String, String> fields = Hfdb.getEventFieldContainer().getEventFields(ev.getEventName());


        StringBuilder sb1= new StringBuilder();
        for (InsertWlMatch match : customdata) {
            HashMap<String, String> fields = Hfdb.getEventFieldContainer().getEventFields(ev.getEventName());
            if (fields == null) {
                return ev.getEventId();
            }

            StringBuilder sb = new StringBuilder();
            sb.append("|");
            boolean firstTime = true;

            for (String field : fields.keySet()) {

                String val = "";
                if (field.equalsIgnoreCase("matched_app_ref_no")) {
                    val=match.getAppRefNo();
                } else if(field.equalsIgnoreCase("matched_cust_id")){
                    val=match.getProductCode();
                } else if(field.equalsIgnoreCase("matched_product")){
                    val=match.getCustId();
                }else if(field.equalsIgnoreCase("matched_account_id")){
                    val=match.getAccountId();
                }else if(field.equalsIgnoreCase("matched_email")){
                    val=match.getEmail();
                }else if(field.equalsIgnoreCase("matched_trade")){
                    val=match.getTrade();
                }else if(field.equalsIgnoreCase("matched_eid")){
                    val=match.getEida();
                }else if(field.equalsIgnoreCase("matched_mobile")){
                    val=match.getMobile();
                } else {
                    val = msgBody.getString(field, "Not Available");
                }
                if (val != null && val.trim().equals("") && val.isEmpty())
                    val = "Not Available";

                if (firstTime) {
                    firstTime = false;
                } else {
                    sb.append("|");
                }
                try {

                    if (!(val.matches("[0-9]+") && val.startsWith("0"))) {
                        BigDecimal value = new BigDecimal(val);
                        String str = value.toPlainString();

                        if (str != null && str.endsWith(".0"))
                            str = str.substring(0, str.length() - 2);
                        sb.append(str);
                    } else {
                        sb.append(val);
                    }
                } catch (Exception e) {
                    sb.append(val);
                }
                //FIXME: handle it using meta info from hocon. introduce meta info in hocon.
                //If you don't understand what does it mean, please ask Lovish / Utsav before it's too late.

                //In order to show double values in non-exponential format.
                try {
                    if (field.equalsIgnoreCase("eventname") || field.equalsIgnoreCase("EVENT_NAME")) {
                        if (val.equalsIgnoreCase("Not Available")) {
                            String name = ev.getEventName();
                            if (name.startsWith("cxps.events.")) {
                                name = name.substring(12, 13).toUpperCase() + name.substring(13);
                                sb.setLength(sb.length() - 13);
                                if (cmsEventNames.containsKey(name)) {
                                    sb.append(cmsEventNames.get(name).toString());
                                } else {
                                    sb.append(name);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("eventname not derived");
                }
            }
            sb1.append(sb);
        }
        System.out.println("string builder in getEnglish Helper "+sb1.toString());
        return sb1.toString();
    }*/
    public static String getEnglish(HostEvent ev) {
        HashMap<String, String> fields = Hfdb.getEventFieldContainer().getEventFields(ev.getEventName());
        if (fields == null) {
            return ev.getEventId();
        } else {
            Hocon hocon = new Hocon();
            hocon.loadJson(ev.getEvent());
            String body = TracerUtil.getMsgBody(hocon).replaceAll("[']", "\"").replaceAll("[^\\x00-\\x7F]", " ");
            Hocon msgBody = new Hocon();
            msgBody.loadJson(body);
            StringBuilder sb = new StringBuilder();
            boolean firstTime = true;

            String val;
            for(Iterator var7 = fields.keySet().iterator(); var7.hasNext(); sb.append(val)) {
                String field = (String)var7.next();
                val = msgBody.getString(field, "not available");
                if (firstTime) {
                    firstTime = false;
                } else {
                    sb.append(",");
                }
            }

            return sb.toString();
        }
    }
    /*public static List<InsertWlMatch> getDatalist(String eventid){
        List<InsertWlMatch> data= new ArrayList<InsertWlMatch>();
         String query="select * from CUSTOM_ACCTOPENING where Event_id = ?";
        System.out.println();
        try{
            CxConnection  connection = getConnection();
            PreparedStatement pt = connection.prepareCall(query);
            pt.setString(1,eventid);
            ResultSet rs= pt.executeQuery();
               while (rs.next()){
                    InsertWlMatch match = new InsertWlMatch();
                    match.setEventId(rs.getString("event_id"));
                    match.setAppRefNo(rs.getString("matched_app_ref_no"));
                    match.setProductCode(rs.getString("matched_cust_id"));
                    match.setCustId(rs.getString("matched_product"));
                    match.setAccountId(rs.getString("matched_account_id"));
                    match.setEmail(rs.getString("matched_email"));
                    match.setTrade(rs.getString("matched_trade"));
                    match.setEida(rs.getString("matched_eid"));
                    match.setMobile(rs.getString("matched_mobile"));
                data.add(match);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }
    protected static CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw  new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }*/
   /* public static  Connection getConnection(){
        String dburl = "jdbc:sqlserver://192.168.5.76;portNumber=1433;databaseName=cxpsadm_vikas_48dev";
        String user = "cxpsadm_vikas_48dev";
        String password = "c_xps123";
        String drivername = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        Connection conn = null;

        try {
            Class.forName(drivername);
            conn = DriverManager.getConnection(dburl, user, password);
            System.out.println("got connection");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }*/

    public static void main(String[] args) {
         /*  String dburl = "jdbc:sqlserver://192.168.5.76;portNumber=1433;databaseName=cxpsadm_vikas_48dev";
        String user = "cxpsadm_vikas_48dev";
        String password = "c_xps123";
        String drivername = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String schemaname = "cxpsadm_vikas_48dev.dbo.";*/

        /*Connection conn = null;
        try {
            Class.forName(drivername);
            conn = DriverManager.getConnection(dburl, user, password);
            System.out.println("got connection");
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
}
