cxps.events.event.nft-userid-maintenance{
  table-name : EVENT_NFT_USERID_MAINTENANCE
  event-mnemonic: UM
  workspaces : {
    
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
user-id: {db : true ,raw_name : user_id ,type : "string:200"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
device-id: {db : true ,raw_name : device_id ,type: "string:20" }
addr-network: {db : true ,raw_name : addr_network ,type : "string:200"}
ip-country: {db : true ,raw_name : ip_country ,type : "string:200"}
ip-city: {db : true ,raw_name : ip_city ,type : "string:200"}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
change-type:{db : true ,raw_name : change_type ,type : "string:200"}
change-mode:{db : true ,raw_name : change_mode ,type : "string:200"}
channel: {db : true ,raw_name : channel ,type : "string:200"}
}
}
