cxps.noesis.glossary.entity.USER_ACCT_MAPPING {
        db-name = USER_ACCT_MAPPING
        generate = false
        

        tablespace = CXPS_USERS
        attributes = [
        { name= account, column =Account, type ="string:20",key=false}
        { name= cust-id, column =Cust_id, type ="string:20",key=false}
        { name= user-id, column =UserID, type ="string:100",key=false}
        { name= rcre-time, column =RCRE_TIME, type =timestamp,key=false}
                  ]
     
}

