cxps.events.event.nft-acct-inq{
  table-name : EVENT_NFT_ACCT_INQ
  event-mnemonic: AI
  workspaces : {
    USER: "user-id"
  }
 event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
channel: {db:true ,raw_name : channel ,type:"string:50"}
source: {db:true ,raw_name : source ,type:"string:50"}
account-id: {db:true ,raw_name :account_id,type:"string:50"}
product-code: {db :true ,raw_name : product_code ,type : "string:50"}
tran-code: {db:true ,raw_name : tran_code,type:"string:50"}
cust-id: {db:true ,raw_name : cust_id ,type:"string:50"}
acct-name: {db:true ,raw_name : acct_name ,type:"string:50"}
emp-id: {db:true ,raw_name : emp_id ,type:"string:50"}
tran-type: {db:true ,raw_name : tran_type ,type:"string:50"}
tran-sub-type: {db:true ,raw_name : tran_sub_type ,type:"string:50"}
part-tran-type: {db:true ,raw_name : part_tran_type ,type:"string:50"}
tran-date: {db :true ,raw_name : tran_date ,type : timestamp}
pstd-date: {db:true ,raw_name : pstd_date ,type: timestamp}
tran-id: {db:true ,raw_name : tran_id ,type:"string:50"}
tran-crncy-code: {db:true ,raw_name : tran_crncy_code,type:"string:50"}
tran-rmks: {db:true ,raw_name : tran_rmks,type:"string:50"}
sys-time: {db:true ,raw_name : sys_time ,type: timestamp}
acct-br-code: {db:true ,raw_name : acct_br_code ,type:"string:50"}
user-id: {db:true ,raw_name : user_id ,type:"string:50"}
txn-br-code: {db :true ,raw_name : txn_br_code ,type : "string:50"}
transaction-ref-no: {db:true ,raw_name : transaction_ref_no ,type:"string:50"}
is-staff:{db : true ,raw_name : is_staff ,type : "string:2" ,derivation:"""cxps.events.CustomDerivator.getIsstaff(this)""" }
compmis1:{db : true ,raw_name : compmis1 ,type : "string:20"}
acct-status:{db : true ,raw_name : acct_status ,type : "string:20"}
is-eligible:{db : true ,raw_name : is_eligible ,type : "string:5"}
}
}
