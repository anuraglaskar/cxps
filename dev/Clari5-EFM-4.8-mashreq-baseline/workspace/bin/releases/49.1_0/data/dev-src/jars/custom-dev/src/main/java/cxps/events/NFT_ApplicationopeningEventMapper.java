// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_ApplicationopeningEventMapper extends EventMapper<NFT_ApplicationopeningEvent> {

public NFT_ApplicationopeningEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_ApplicationopeningEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_ApplicationopeningEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_ApplicationopeningEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_ApplicationopeningEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_ApplicationopeningEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_ApplicationopeningEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setTimestamp(i++, obj.getNewApplopendate());
            preparedStatement.setString(i++, obj.getOldDeviceid());
            preparedStatement.setTimestamp(i++, obj.getOldApplopendate());
            preparedStatement.setString(i++, obj.getNewIpaddress());
            preparedStatement.setString(i++, obj.getOldProduct());
            preparedStatement.setString(i++, obj.getMatchedTradelicense());
            preparedStatement.setString(i++, obj.getOldEmail());
            preparedStatement.setString(i++, obj.getNewAppRefno());
            preparedStatement.setString(i++, obj.getOldTradelincense());
            preparedStatement.setString(i++, obj.getNewMobile());
            preparedStatement.setString(i++, obj.getMatchedEid());
            preparedStatement.setString(i++, obj.getOldChannel());
            preparedStatement.setString(i++, obj.getNewPassport());
            preparedStatement.setString(i++, obj.getOldCustname());
            preparedStatement.setString(i++, obj.getOldMobile());
            preparedStatement.setString(i++, obj.getNewDeviceid());
            preparedStatement.setString(i++, obj.getOldIpaddress());
            preparedStatement.setString(i++, obj.getOldCif());
            preparedStatement.setString(i++, obj.getOldEid());
            preparedStatement.setString(i++, obj.getOldAppRefno());
            preparedStatement.setString(i++, obj.getNewCustname());
            preparedStatement.setString(i++, obj.getMatchedScoreEid());
            preparedStatement.setString(i++, obj.getNewProduct());
            preparedStatement.setString(i++, obj.getMatchedEmail());
            preparedStatement.setString(i++, obj.getNewTradelincense());
            preparedStatement.setString(i++, obj.getOldPassport());
            preparedStatement.setString(i++, obj.getNewChannel());
            preparedStatement.setString(i++, obj.getMatchedMobile());
            preparedStatement.setString(i++, obj.getNewCif());
            preparedStatement.setString(i++, obj.getNewEid());
            preparedStatement.setString(i++, obj.getNewEmail());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_APPLICATIONOPENING]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_ApplicationopeningEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_ApplicationopeningEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_APPLICATIONOPENING"));
        putList = new ArrayList<>();

        for (NFT_ApplicationopeningEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "NEW_APPLOPENDATE", String.valueOf(obj.getNewApplopendate()));
            p = this.insert(p, "OLD_DEVICEID",  obj.getOldDeviceid());
            p = this.insert(p, "OLD_APPLOPENDATE", String.valueOf(obj.getOldApplopendate()));
            p = this.insert(p, "NEW_IPADDRESS",  obj.getNewIpaddress());
            p = this.insert(p, "OLD_PRODUCT",  obj.getOldProduct());
            p = this.insert(p, "MATCHED_TRADELICENSE",  obj.getMatchedTradelicense());
            p = this.insert(p, "OLD_EMAIL",  obj.getOldEmail());
            p = this.insert(p, "NEW_APP_REFNO",  obj.getNewAppRefno());
            p = this.insert(p, "OLD_TRADELINCENSE",  obj.getOldTradelincense());
            p = this.insert(p, "NEW_MOBILE",  obj.getNewMobile());
            p = this.insert(p, "MATCHED_EID",  obj.getMatchedEid());
            p = this.insert(p, "OLD_CHANNEL",  obj.getOldChannel());
            p = this.insert(p, "NEW_PASSPORT",  obj.getNewPassport());
            p = this.insert(p, "OLD_CUSTNAME",  obj.getOldCustname());
            p = this.insert(p, "OLD_MOBILE",  obj.getOldMobile());
            p = this.insert(p, "NEW_DEVICEID",  obj.getNewDeviceid());
            p = this.insert(p, "OLD_IPADDRESS",  obj.getOldIpaddress());
            p = this.insert(p, "OLD_CIF",  obj.getOldCif());
            p = this.insert(p, "OLD_EID",  obj.getOldEid());
            p = this.insert(p, "OLD_APP_REFNO",  obj.getOldAppRefno());
            p = this.insert(p, "NEW_CUSTNAME",  obj.getNewCustname());
            p = this.insert(p, "MATCHED_SCORE_EID",  obj.getMatchedScoreEid());
            p = this.insert(p, "NEW_PRODUCT",  obj.getNewProduct());
            p = this.insert(p, "MATCHED_EMAIL",  obj.getMatchedEmail());
            p = this.insert(p, "NEW_TRADELINCENSE",  obj.getNewTradelincense());
            p = this.insert(p, "OLD_PASSPORT",  obj.getOldPassport());
            p = this.insert(p, "NEW_CHANNEL",  obj.getNewChannel());
            p = this.insert(p, "MATCHED_MOBILE",  obj.getMatchedMobile());
            p = this.insert(p, "NEW_CIF",  obj.getNewCif());
            p = this.insert(p, "NEW_EID",  obj.getNewEid());
            p = this.insert(p, "NEW_EMAIL",  obj.getNewEmail());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_APPLICATIONOPENING"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_APPLICATIONOPENING]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_APPLICATIONOPENING]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_ApplicationopeningEvent obj = new NFT_ApplicationopeningEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setNewApplopendate(rs.getTimestamp("NEW_APPLOPENDATE"));
    obj.setOldDeviceid(rs.getString("OLD_DEVICEID"));
    obj.setOldApplopendate(rs.getTimestamp("OLD_APPLOPENDATE"));
    obj.setNewIpaddress(rs.getString("NEW_IPADDRESS"));
    obj.setOldProduct(rs.getString("OLD_PRODUCT"));
    obj.setMatchedTradelicense(rs.getString("MATCHED_TRADELICENSE"));
    obj.setOldEmail(rs.getString("OLD_EMAIL"));
    obj.setNewAppRefno(rs.getString("NEW_APP_REFNO"));
    obj.setOldTradelincense(rs.getString("OLD_TRADELINCENSE"));
    obj.setNewMobile(rs.getString("NEW_MOBILE"));
    obj.setMatchedEid(rs.getString("MATCHED_EID"));
    obj.setOldChannel(rs.getString("OLD_CHANNEL"));
    obj.setNewPassport(rs.getString("NEW_PASSPORT"));
    obj.setOldCustname(rs.getString("OLD_CUSTNAME"));
    obj.setOldMobile(rs.getString("OLD_MOBILE"));
    obj.setNewDeviceid(rs.getString("NEW_DEVICEID"));
    obj.setOldIpaddress(rs.getString("OLD_IPADDRESS"));
    obj.setOldCif(rs.getString("OLD_CIF"));
    obj.setOldEid(rs.getString("OLD_EID"));
    obj.setOldAppRefno(rs.getString("OLD_APP_REFNO"));
    obj.setNewCustname(rs.getString("NEW_CUSTNAME"));
    obj.setMatchedScoreEid(rs.getString("MATCHED_SCORE_EID"));
    obj.setNewProduct(rs.getString("NEW_PRODUCT"));
    obj.setMatchedEmail(rs.getString("MATCHED_EMAIL"));
    obj.setNewTradelincense(rs.getString("NEW_TRADELINCENSE"));
    obj.setOldPassport(rs.getString("OLD_PASSPORT"));
    obj.setNewChannel(rs.getString("NEW_CHANNEL"));
    obj.setMatchedMobile(rs.getString("MATCHED_MOBILE"));
    obj.setNewCif(rs.getString("NEW_CIF"));
    obj.setNewEid(rs.getString("NEW_EID"));
    obj.setNewEmail(rs.getString("NEW_EMAIL"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_APPLICATIONOPENING]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_ApplicationopeningEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_ApplicationopeningEvent> events;
 NFT_ApplicationopeningEvent obj = new NFT_ApplicationopeningEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_APPLICATIONOPENING"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_ApplicationopeningEvent obj = new NFT_ApplicationopeningEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setNewApplopendate(EventHelper.toTimestamp(getColumnValue(rs, "NEW_APPLOPENDATE")));
            obj.setOldDeviceid(getColumnValue(rs, "OLD_DEVICEID"));
            obj.setOldApplopendate(EventHelper.toTimestamp(getColumnValue(rs, "OLD_APPLOPENDATE")));
            obj.setNewIpaddress(getColumnValue(rs, "NEW_IPADDRESS"));
            obj.setOldProduct(getColumnValue(rs, "OLD_PRODUCT"));
            obj.setMatchedTradelicense(getColumnValue(rs, "MATCHED_TRADELICENSE"));
            obj.setOldEmail(getColumnValue(rs, "OLD_EMAIL"));
            obj.setNewAppRefno(getColumnValue(rs, "NEW_APP_REFNO"));
            obj.setOldTradelincense(getColumnValue(rs, "OLD_TRADELINCENSE"));
            obj.setNewMobile(getColumnValue(rs, "NEW_MOBILE"));
            obj.setMatchedEid(getColumnValue(rs, "MATCHED_EID"));
            obj.setOldChannel(getColumnValue(rs, "OLD_CHANNEL"));
            obj.setNewPassport(getColumnValue(rs, "NEW_PASSPORT"));
            obj.setOldCustname(getColumnValue(rs, "OLD_CUSTNAME"));
            obj.setOldMobile(getColumnValue(rs, "OLD_MOBILE"));
            obj.setNewDeviceid(getColumnValue(rs, "NEW_DEVICEID"));
            obj.setOldIpaddress(getColumnValue(rs, "OLD_IPADDRESS"));
            obj.setOldCif(getColumnValue(rs, "OLD_CIF"));
            obj.setOldEid(getColumnValue(rs, "OLD_EID"));
            obj.setOldAppRefno(getColumnValue(rs, "OLD_APP_REFNO"));
            obj.setNewCustname(getColumnValue(rs, "NEW_CUSTNAME"));
            obj.setMatchedScoreEid(getColumnValue(rs, "MATCHED_SCORE_EID"));
            obj.setNewProduct(getColumnValue(rs, "NEW_PRODUCT"));
            obj.setMatchedEmail(getColumnValue(rs, "MATCHED_EMAIL"));
            obj.setNewTradelincense(getColumnValue(rs, "NEW_TRADELINCENSE"));
            obj.setOldPassport(getColumnValue(rs, "OLD_PASSPORT"));
            obj.setNewChannel(getColumnValue(rs, "NEW_CHANNEL"));
            obj.setMatchedMobile(getColumnValue(rs, "MATCHED_MOBILE"));
            obj.setNewCif(getColumnValue(rs, "NEW_CIF"));
            obj.setNewEid(getColumnValue(rs, "NEW_EID"));
            obj.setNewEmail(getColumnValue(rs, "NEW_EMAIL"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_APPLICATIONOPENING]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_APPLICATIONOPENING]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"NEW_APPLOPENDATE\",\"OLD_DEVICEID\",\"OLD_APPLOPENDATE\",\"NEW_IPADDRESS\",\"OLD_PRODUCT\",\"MATCHED_TRADELICENSE\",\"OLD_EMAIL\",\"NEW_APP_REFNO\",\"OLD_TRADELINCENSE\",\"NEW_MOBILE\",\"MATCHED_EID\",\"OLD_CHANNEL\",\"NEW_PASSPORT\",\"OLD_CUSTNAME\",\"OLD_MOBILE\",\"NEW_DEVICEID\",\"OLD_IPADDRESS\",\"OLD_CIF\",\"OLD_EID\",\"OLD_APP_REFNO\",\"NEW_CUSTNAME\",\"MATCHED_SCORE_EID\",\"NEW_PRODUCT\",\"MATCHED_EMAIL\",\"NEW_TRADELINCENSE\",\"OLD_PASSPORT\",\"NEW_CHANNEL\",\"MATCHED_MOBILE\",\"NEW_CIF\",\"NEW_EID\",\"NEW_EMAIL\"" +
              " FROM EVENT_NFT_APPLICATIONOPENING";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [NEW_APPLOPENDATE],[OLD_DEVICEID],[OLD_APPLOPENDATE],[NEW_IPADDRESS],[OLD_PRODUCT],[MATCHED_TRADELICENSE],[OLD_EMAIL],[NEW_APP_REFNO],[OLD_TRADELINCENSE],[NEW_MOBILE],[MATCHED_EID],[OLD_CHANNEL],[NEW_PASSPORT],[OLD_CUSTNAME],[OLD_MOBILE],[NEW_DEVICEID],[OLD_IPADDRESS],[OLD_CIF],[OLD_EID],[OLD_APP_REFNO],[NEW_CUSTNAME],[MATCHED_SCORE_EID],[NEW_PRODUCT],[MATCHED_EMAIL],[NEW_TRADELINCENSE],[OLD_PASSPORT],[NEW_CHANNEL],[MATCHED_MOBILE],[NEW_CIF],[NEW_EID],[NEW_EMAIL]" +
              " FROM EVENT_NFT_APPLICATIONOPENING";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`NEW_APPLOPENDATE`,`OLD_DEVICEID`,`OLD_APPLOPENDATE`,`NEW_IPADDRESS`,`OLD_PRODUCT`,`MATCHED_TRADELICENSE`,`OLD_EMAIL`,`NEW_APP_REFNO`,`OLD_TRADELINCENSE`,`NEW_MOBILE`,`MATCHED_EID`,`OLD_CHANNEL`,`NEW_PASSPORT`,`OLD_CUSTNAME`,`OLD_MOBILE`,`NEW_DEVICEID`,`OLD_IPADDRESS`,`OLD_CIF`,`OLD_EID`,`OLD_APP_REFNO`,`NEW_CUSTNAME`,`MATCHED_SCORE_EID`,`NEW_PRODUCT`,`MATCHED_EMAIL`,`NEW_TRADELINCENSE`,`OLD_PASSPORT`,`NEW_CHANNEL`,`MATCHED_MOBILE`,`NEW_CIF`,`NEW_EID`,`NEW_EMAIL`" +
              " FROM EVENT_NFT_APPLICATIONOPENING";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_APPLICATIONOPENING (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"NEW_APPLOPENDATE\",\"OLD_DEVICEID\",\"OLD_APPLOPENDATE\",\"NEW_IPADDRESS\",\"OLD_PRODUCT\",\"MATCHED_TRADELICENSE\",\"OLD_EMAIL\",\"NEW_APP_REFNO\",\"OLD_TRADELINCENSE\",\"NEW_MOBILE\",\"MATCHED_EID\",\"OLD_CHANNEL\",\"NEW_PASSPORT\",\"OLD_CUSTNAME\",\"OLD_MOBILE\",\"NEW_DEVICEID\",\"OLD_IPADDRESS\",\"OLD_CIF\",\"OLD_EID\",\"OLD_APP_REFNO\",\"NEW_CUSTNAME\",\"MATCHED_SCORE_EID\",\"NEW_PRODUCT\",\"MATCHED_EMAIL\",\"NEW_TRADELINCENSE\",\"OLD_PASSPORT\",\"NEW_CHANNEL\",\"MATCHED_MOBILE\",\"NEW_CIF\",\"NEW_EID\",\"NEW_EMAIL\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[NEW_APPLOPENDATE],[OLD_DEVICEID],[OLD_APPLOPENDATE],[NEW_IPADDRESS],[OLD_PRODUCT],[MATCHED_TRADELICENSE],[OLD_EMAIL],[NEW_APP_REFNO],[OLD_TRADELINCENSE],[NEW_MOBILE],[MATCHED_EID],[OLD_CHANNEL],[NEW_PASSPORT],[OLD_CUSTNAME],[OLD_MOBILE],[NEW_DEVICEID],[OLD_IPADDRESS],[OLD_CIF],[OLD_EID],[OLD_APP_REFNO],[NEW_CUSTNAME],[MATCHED_SCORE_EID],[NEW_PRODUCT],[MATCHED_EMAIL],[NEW_TRADELINCENSE],[OLD_PASSPORT],[NEW_CHANNEL],[MATCHED_MOBILE],[NEW_CIF],[NEW_EID],[NEW_EMAIL]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`NEW_APPLOPENDATE`,`OLD_DEVICEID`,`OLD_APPLOPENDATE`,`NEW_IPADDRESS`,`OLD_PRODUCT`,`MATCHED_TRADELICENSE`,`OLD_EMAIL`,`NEW_APP_REFNO`,`OLD_TRADELINCENSE`,`NEW_MOBILE`,`MATCHED_EID`,`OLD_CHANNEL`,`NEW_PASSPORT`,`OLD_CUSTNAME`,`OLD_MOBILE`,`NEW_DEVICEID`,`OLD_IPADDRESS`,`OLD_CIF`,`OLD_EID`,`OLD_APP_REFNO`,`NEW_CUSTNAME`,`MATCHED_SCORE_EID`,`NEW_PRODUCT`,`MATCHED_EMAIL`,`NEW_TRADELINCENSE`,`OLD_PASSPORT`,`NEW_CHANNEL`,`MATCHED_MOBILE`,`NEW_CIF`,`NEW_EID`,`NEW_EMAIL`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

