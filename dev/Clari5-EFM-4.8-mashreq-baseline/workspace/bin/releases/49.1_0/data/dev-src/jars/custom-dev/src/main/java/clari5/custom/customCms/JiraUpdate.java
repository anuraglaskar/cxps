package clari5.custom.customCms;

import clari5.aml.cms.newjira.JiraInstance;
import clari5.jiraclient.JiraClient;
import clari5.platform.jira.JiraClientException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class JiraUpdate {
    private static CxpsLogger logger = CxpsLogger.getLogger(JiraUpdate.class);
    public static String customField="";
    public static String dburl="";
    public static String user="";
    public static String password="";
    public static String drivername="";
    public static String schemaname="";
    public static String freezeFieldId="";
    public static String blockFieldId="";
    public static String freezefield="";
    public static String blockfield="";

    static {
        Hocon hocon = new Hocon();
        hocon.loadFromContext("jiradb-module.conf");
        customField = hocon.getString("customField");
        dburl=hocon.getString("dburl");
        user=hocon.getString("user");
        password=hocon.getString("password");
        drivername=hocon.getString("drivername");
        schemaname=hocon.getString("schemaname");
        freezeFieldId=hocon.getString("freezeId");
        blockFieldId=hocon.getString("blockId");
        freezefield=hocon.getString("freezefield");
        blockfield=hocon.getString("blockfield");
    }
    String table_name=schemaname+"CUSTOMFIELDVALUE";
    Connection conn = null;
    Statement stmt = null;
    public  boolean updatejiraForCust(String issueID, String reqString) {
        String query="";
        if (reqString.equalsIgnoreCase("R.M. Freeze")) {
            query = "update  "+table_name+" set stringvalue ='R.M. UnFreeze' where  issue ='"+issueID+"' and customfield ='"+freezeFieldId+"'";
        }else if (reqString.equalsIgnoreCase("R.M. UnFreeze")){
            query = "update  "+table_name+" set stringvalue ='R.M. Freeze' where  issue ='"+issueID+"' and customfield ='"+freezeFieldId+"'";
        }
        logger.info("query to update jira table for freeze unfreeze ::"+query);
        try {
            Class.forName(drivername);
            conn = DriverManager.getConnection(dburl, user, password);
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        logger.info("JIRA Table updation is failed for Freeze Unfreeze");
        return false;
    }
    public  boolean updatejiraForCard(String issueID, String reqString) {
        String query="";
        if (reqString.equalsIgnoreCase("cardblock")) {
            query = "update  "+table_name+" set stringvalue ='Card UnBlock' where  issue ='"+issueID+"' and customfield ='"+blockFieldId+"'";
        }else if (reqString.equalsIgnoreCase("cardUnblock")){
            query = "update  "+table_name+" set stringvalue ='Card Block' where  issue ='"+issueID+"' and customfield ='"+blockFieldId+"'";
        }
        logger.info("query to update jira table for Card block unblock ::"+query);
        try {
            Class.forName(drivername);
            conn = DriverManager.getConnection(dburl, user, password);
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        logger.info("JIRA Table updation is failed for CardBlock UnBlock");
        return false;
    }
    public  boolean callingJiraApi(String issuekey,String issueID,String reqString) {
        String finaldata=jsonForm(reqString);
        boolean flag=updateCUSTOMFIELDVALUE(finaldata,issueID,reqString,issuekey );
        return flag;
    }
    public  boolean updateCUSTOMFIELDVALUE(String finaldata,String issueID,String reqString,String issuekey){
            boolean finalflag=false;
        try {

            JiraInstance instance = new JiraInstance("jira-instance");
            JiraClient jiraClient=new JiraClient(instance.getInstanceParams());
            String BaseUrl=  jiraClient.getBaseURL();
            System.out.println("base url is :: "+BaseUrl);
            String path="/rest/api/2/issue/";
            String finalurl=BaseUrl+path+issuekey;
            String Method="PUT";
           //jiraClient.updateIssueRequest()
            System.out.println("final url --"+finalurl);
            SupportJiiraCLient suppjiraclient=new SupportJiiraCLient(instance.getInstanceParams());
            CxJson cxJson=suppjiraclient.updateIssueRequest(finaldata,issuekey);
            logger.info("cxJson response code---"+cxJson);
            finalflag=true;

        }catch (Exception e){
            logger.info("update through API is failed");
            if(reqString.equalsIgnoreCase("R.M. Freeze") || reqString.equalsIgnoreCase("R.M. UnFreeze"))
                finalflag=updatejiraForCust(issueID,reqString);
            logger.info("customFieldvalue table is updated successfully without jira-api");
            if(reqString.equalsIgnoreCase("cardblock")|| reqString.equalsIgnoreCase("cardUnblock") )
                finalflag=updatejiraForCard(issueID,reqString);
            logger.info("customFieldvalue table is updated successfully without jira-api");
            e.printStackTrace();
        }
       return  finalflag;
    }
    public  String jsonForm(String reqString){
        String jsondata="";
        try {
            if(reqString.equalsIgnoreCase("R.M. Freeze")){
                jsondata = "{\"update\":{\""+freezefield+"\":[{\"set\":\"R.M. UnFreeze\"}]}}";
            }else if(reqString.equalsIgnoreCase("R.M. Unfreeze")){
                jsondata = "{\"update\":{\""+freezefield+"\":[{\"set\":\"R.M. Freeze\"}]}}";
            }else if(reqString.equalsIgnoreCase("cardblock")){
                jsondata = "{\"update\":{\""+blockfield+"\":[{\"set\":\"Card UnBlock\"}]}}";
            }else if(reqString.equalsIgnoreCase("cardUnblock")){
                jsondata = "{\"update\":{\""+blockfield+"\":[{\"set\":\"Card Block\"}]}}";
            }else{
                logger.info("Invalid Request"+reqString);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsondata;
    }


  /*  public static void main(String[] args) {
        CustomCmsServlet customCmsServlet= new CustomCmsServlet();
        JiraUpdate jiraUpdate = new JiraUpdate();

        boolean data=jiraUpdate.callingJiraApi("EFM-641","42505","freeze");
       // boolean flag=jiraUpdate.updatejiraForCust("42505","Freeze");
        //boolean flag=jiraUpdate.updatejiraForCust("42505","UnFreeze");
        //boolean flag=jiraUpdate.updatejiraForCard("42505","cardblock");
        //boolean flag=jiraUpdate.updatejiraForCard("42505","cardUnblock");
        System.out.println("flag----"+data);


    }*/
}
