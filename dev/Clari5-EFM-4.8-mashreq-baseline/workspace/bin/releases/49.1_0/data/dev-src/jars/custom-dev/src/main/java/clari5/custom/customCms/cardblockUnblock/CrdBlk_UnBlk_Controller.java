package clari5.custom.customCms.cardblockUnblock;

import clari5.custom.customCms.cardblockUnblock.Response;
import clari5.custom.customCms.services.CallingApi;
import clari5.custom.customCms.services.StringToXmlParser;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

public class CrdBlk_UnBlk_Controller {



    //NOTE:: in card block custo no is card number

    private String req = "";
    private boolean opFromdb;
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private Date date = new Date();
    private Random rand = new Random();
    private static CxpsLogger logger = CxpsLogger.getLogger(CrdBlk_UnBlk_Controller.class);
    private CallingApi callingApi = new CallingApi();
    private CrdBlk_UnBlk_Dao crdBlk_unBlk_dao = new CrdBlk_UnBlk_Dao();
    StringToXmlParser stringToXmlParser = new StringToXmlParser();

    Response response = new Response();

    public boolean callingToBlkCard(String customerNo, String cAPi, int time,String envType) {

        logger.info("inside calling to unblock customer No :: "+customerNo+" :: and api is :: "+cAPi);
        req = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:car=\"http://www.mbcdm.creditcardservices.com/cardBlocking\" xmlns:mbc=\"http://www.mbcdm.header.com\" xmlns:mbc1=\"http://www.mbcdm.creditcard.com\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <car:EAIServices>\n" +
                "         <car:Header>\n" +
                "            <mbc:SrcAppId>"+envType+"</mbc:SrcAppId>\n" +
                "            <mbc:SrcMsgId>"+rand.nextInt()+"</mbc:SrcMsgId>\n" +
                "            <mbc:SrvCode>TDBBLK</mbc:SrvCode>\n" +
                "            <mbc:SrvName>CardBlockingPrime</mbc:SrvName>\n" +
                "            <mbc:OrgId>AE</mbc:OrgId>\n" +
                "            <mbc:UserId>EFMUSER</mbc:UserId>\n" +
                "         </car:Header>\n" +
                "         <car:Body>\n" +
                "            <car:CardBlockReq>\n" +
                "               <mbc1:CardNo>"+customerNo+"</mbc1:CardNo>\n" +
                "               <mbc1:CardStatus>718</mbc1:CardStatus>\n" +
                "            </car:CardBlockReq>\n" +
                "         </car:Body>\n" +
                "      </car:EAIServices>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
            logger.info("request for card block::",req);
            String op = callingApi.callingMashreqApi(cAPi, req, time);
            logger.info("o/p from callingToBlock api::",op);
        //based on the above response need to change

            if (op.equalsIgnoreCase("time out exception")) {

                opFromdb = crdBlk_unBlk_dao.checkCustomerExist(customerNo, "cb", true, response);
                logger.debug("this is op",opFromdb);
                return opFromdb;
            } else {

                Map m = stringToXmlParser.cardblockparsing(op);
                logger.info("final map is for Block :: "+m);
                if (m.get("error_desc").toString().equalsIgnoreCase("Success")) {
                    response.setErrorCode(m.get("error_code").toString());
                    response.setErrorDescription(m.get("error_desc").toString());
                    response.setStatus(m.get("status").toString());
                    opFromdb = crdBlk_unBlk_dao.checkCustomerExist(customerNo, "cb", false, response);
                    logger.debug("this is op",opFromdb);
                    return opFromdb;

                } else {
                    response.setErrorCode(m.get("error_code").toString());
                    response.setErrorDescription(m.get("error_desc").toString());
                    response.setStatus(m.get("status").toString());
                    opFromdb = crdBlk_unBlk_dao.checkCustomerExist(customerNo, "cb", false, response);                //this is failure response from api so i am returning false response
                    logger.debug("this is op",opFromdb);
                    return false;

                }


            }

    }


    public boolean callingToUnBlock(String customerNo, String cAPi, int time,String envType) {

        logger.info("inside calling to unblock customer No :: "+customerNo+" :: and api is :: "+cAPi);
        //System.out.println("inside calling unblock cust no --"+customerNo+"api ---"+cAPi+"time---"+time);
        req = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:car=\"http://www.mbcdm.creditcardservices.com/cardActivitation\" xmlns:mbc=\"http://www.mbcdm.header.com\" xmlns:mbc1=\"http://www.mbcdm.creditcard.com\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <car:EAIServices>\n" +
                "         <car:Header>\n" +
                "            <mbc:SrcAppId>"+envType+"</mbc:SrcAppId>\n" +
                "            <mbc:SrcMsgId>"+rand.nextInt()+"</mbc:SrcMsgId>\n" +
                "            <mbc:SrvCode>TCRDACT</mbc:SrvCode>\n" +
                "            <mbc:OrgId>AE</mbc:OrgId>\n" +
                "            <mbc:UserId>EFMUSER</mbc:UserId>\n" +
                "         </car:Header>\n" +
                "         <car:Body>\n" +
                "            <car:CardActivationReq>\n" +
                "               <mbc1:CardNo>"+customerNo+"</mbc1:CardNo>\n" +
                "            </car:CardActivationReq>\n" +
                "         </car:Body>\n" +
                "      </car:EAIServices>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        logger.info("request String in unblock ::"+req);
        String op = callingApi.callingMashreqApi(cAPi, req, time);
        logger.info("o/p from callingToUnBlock api :: ",op);
        if (op.equalsIgnoreCase("time out exception")) {
            opFromdb = crdBlk_unBlk_dao.checkCustomerExist(customerNo, "cu", true, response);

            return opFromdb;
        } else {

            Map m = stringToXmlParser.cardUnblookparsing(op);
            logger.info("final map is for UnBlock :: "+m);
            if (m.get("error_desc").toString().equalsIgnoreCase("Success")) {
                response.setErrorCode(m.get("error_code").toString());
                response.setErrorDescription(m.get("error_desc").toString());
                response.setStatus(m.get("status").toString());
                opFromdb = crdBlk_unBlk_dao.checkCustomerExist(customerNo, "cu", false, response);
                return opFromdb;

            } else {
                response.setErrorCode(m.get("error_code").toString());
                response.setErrorDescription(m.get("error_desc").toString());
                response.setStatus(m.get("status").toString());
                opFromdb = crdBlk_unBlk_dao.checkCustomerExist(customerNo, "cb", false, response);                //this is failure response from api so i am returning false response
                return false;
        }
        }
    }

}