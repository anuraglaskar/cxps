// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_WITHDRAWAL_LIMIT", Schema="rice")
public class NFT_WithdrawalLimitEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String request;
       @Field(size=20) public String accountId;
       @Field(size=200) public String errorDesc;
       @Field(size=200) public String channel;
       @Field(size=20) public String succFailFlg;
       @Field(size=20) public Double limitchangetype;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String source;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String custId;
       @Field(size=20) public Double oldLimit;
       @Field(size=20) public String hostId;
       @Field(size=200) public String cardNo;
       @Field(size=20) public Double newLimit;


    @JsonIgnore
    public ITable<NFT_WithdrawalLimitEvent> t = AEF.getITable(this);

	public NFT_WithdrawalLimitEvent(){}

    public NFT_WithdrawalLimitEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("WithdrawalLimit");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setRequest(json.getString("request"));
            setAccountId(json.getString("account_id"));
            setErrorDesc(json.getString("error_desc"));
            setChannel(json.getString("channel"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setLimitchangetype(EventHelper.toDouble(json.getString("limitchangetype")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setSource(json.getString("source"));
            setErrorCode(json.getString("error_code"));
            setCustId(json.getString("cust_id"));
            setOldLimit(EventHelper.toDouble(json.getString("old_limit")));
            setHostId(json.getString("host_id"));
            setCardNo(json.getString("card_no"));
            setNewLimit(EventHelper.toDouble(json.getString("new_limit")));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "WR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getRequest(){ return request; }

    public String getAccountId(){ return accountId; }

    public String getErrorDesc(){ return errorDesc; }

    public String getChannel(){ return channel; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public Double getLimitchangetype(){ return limitchangetype; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getSource(){ return source; }

    public String getErrorCode(){ return errorCode; }

    public String getCustId(){ return custId; }

    public Double getOldLimit(){ return oldLimit; }

    public String getHostId(){ return hostId; }

    public String getCardNo(){ return cardNo; }

    public Double getNewLimit(){ return newLimit; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setRequest(String val){ this.request = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setLimitchangetype(Double val){ this.limitchangetype = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setSource(String val){ this.source = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setOldLimit(Double val){ this.oldLimit = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setCardNo(String val){ this.cardNo = val; }
    public void setNewLimit(Double val){ this.newLimit = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_WithdrawalLimit");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "WithdrawalLimit");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}