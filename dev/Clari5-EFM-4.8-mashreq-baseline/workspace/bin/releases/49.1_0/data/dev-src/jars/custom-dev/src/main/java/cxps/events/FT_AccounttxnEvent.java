// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;

import clari5.iso.formatter.DefaultISOFieldFormatter;

import clari5.iso8583.formatter.FieldFormatter;
import org.json.JSONObject;
import java.util.Calendar;

import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;



@Table(Name="EVENT_FT_ACCOUNTTXN", Schema="rice")
public class FT_AccounttxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String dccId;
       @Field(size=20) public String acctBrCode;
       @Field(size=20) public String cardAcceptorName;
       @Field(size=20) public String bin;
       @Field(size=20) public String payeeCode;
       @Field(size=20) public String channel;
       @Field public java.sql.Timestamp valueDate;
       @Field(size=20) public String manulDebitFlag;
       @Field(size=20) public Double fcyTranamt;
       @Field(size=20) public String source;
       @Field(size=20) public String devOwnerId;
       @Field(size=50) public String compmis1;
       @Field(size=20) public String tranSubType;
       @Field(size=20) public String deviceId;
       @Field(size=50) public String compmis2;
       @Field(size=50) public String coinsDenomination;
       @Field(size=20) public Double atmLimitderive;
       @Field(size=50) public String lcyCurr;
       @Field public java.sql.Timestamp acctopendate;
       @Field(size=20) public String tranId;
       @Field(size=2) public String hostId;
       @Field(size=5) public String designatedFlg;
       @Field(size=20) public Double tranAmt;
       @Field(size=50) public String swiftUaeftsRefNum;
       @Field(size=20) public Double atmLimit;
       @Field(size=20) public String devType;
       @Field(size=20) public String accountId;
       @Field(size=20) public String placeHolder;
       @Field(size=20) public String custIdderive;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String branchId;
       @Field(size=20) public String tranCrncyCode;
       @Field(size=20) public String tranRmks;
       @Field(size=50) public String terminalId;
       @Field(size=20) public Double ecomLimit;
       @Field(size=20) public String acctName;
       @Field(size=20) public String custCardId;
       @Field(size=5) public String is26trancodeExists;
       @Field public java.sql.Timestamp pstdDate;
       @Field(size=50) public String itmWithSelfAcct;
       @Field(size=20) public String succFailFlagDerivd;
       @Field(size=20) public Double avlBal;
       @Field(size=20) public String payeeId;
       @Field(size=20) public String schmType;
       @Field(size=20) public String acctOwnership;
       @Field(size=20) public String txnBrCode;
       @Field(size=20) public String transactionRefNo;
       @Field(size=20) public String txnBrCity;
       @Field(size=20) public String acctOccpCode;
       @Field(size=20) public Double unClrBalAmt;
       @Field(size=20) public String status;
       @Field(size=20) public String countryCode;
       @Field(size=20) public String city;
       @Field public java.sql.Timestamp custInducedDate;
       @Field(size=20) public Double convRate;
       @Field(size=50) public String swiftUaeftsMsgType;
       @Field(size=20) public Double clrBalAmt;
       @Field(size=20) public String responseCode;
       @Field(size=20) public String homeBrCity;
       @Field(size=20) public Double avlBalance;
       @Field(size=20) public Double effAvlBal;
       @Field(size=20) public String tranCategory;
       @Field(size=20) public String entryUser;
       @Field(size=5) public String is27trancodeExists;
       @Field(size=20) public String refNum;
       @Field(size=20) public String acctSolId;
       @Field(size=50) public String idNumber;
       @Field(size=20) public String schmCode;
       @Field(size=20) public String userId;
       @Field(size=20) public Double midRate;
       @Field(size=50) public String fcyCurr;
       @Field(size=50) public String cardserno;
       @Field(size=20) public String tranParticular;
       @Field(size=20) public Double cardlessCashLimit;
       @Field(size=20) public String custId;
       @Field(size=20) public Double posLimit;
       @Field public java.sql.Timestamp instrumtDate;
       @Field(size=20) public String custConst;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=20) public String ipAddress;
       @Field(size=20) public String hdrmkrs;
       @Field(size=20) public String tranType;
       @Field(size=50) public String walletType;
       @Field(size=50) public String merchantId;
       @Field(size=20) public String succFailFlag;
       @Field public java.sql.Timestamp insTime;
       @Field(size=50) public String refCurr;
       @Field(size=20) public String partTranType;


    @JsonIgnore
    public ITable<FT_AccounttxnEvent> t = AEF.getITable(this);

	public FT_AccounttxnEvent(){}

    public FT_AccounttxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Accounttxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setDccId(json.getString("dcc_id"));
            setAcctBrCode(json.getString("acct_br_code"));
            setCardAcceptorName(json.getString("card_acceptor_name"));
            setPayeeCode(json.getString("payee_code"));
            setChannel(json.getString("channel"));
            setValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setManulDebitFlag(json.getString("manul-debit-flag"));
            setFcyTranamt(EventHelper.toDouble(json.getString("fcy_tranamt")));
            setSource(json.getString("source"));
            setDevOwnerId(json.getString("dev_owner_id"));
            setTranSubType(json.getString("tran_sub_type"));
            setDeviceId(json.getString("device_id"));
            setCompmis2(json.getString("compmis2"));
            setCoinsDenomination(json.getString("coins_denomination"));
            setLcyCurr("AED");
            setAcctopendate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setTranId(json.getString("tran_id"));
            setHostId(json.getString("host_id"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setSwiftUaeftsRefNum(json.getString("swift_uaefts_ref_num"));
            setAtmLimit(EventHelper.toDouble(json.getString("atm_limit")));
            setDevType(json.getString("dev_type"));
            setAccountId(json.getString("account_id"));
            setPlaceHolder(json.getString("place_holder"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setBranchId(json.getString("branch_id"));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTranRmks(json.getString("tran_rmks"));
            setTerminalId(json.getString("terminal_id"));
            setEcomLimit(EventHelper.toDouble(json.getString("ecom_limit")));
            setAcctName(json.getString("acct_name"));
            setCustCardId(json.getString("cust_card_id"));
            setPstdDate(EventHelper.toTimestamp(json.getString("pstd_date")));
            setItmWithSelfAcct(json.getString("itm_with_self_acct"));
            setPayeeId(json.getString("payee_id"));
            setSchmType(json.getString("schm_type"));
            setAcctOwnership(json.getString("acct_ownership"));
            setTxnBrCode(json.getString("txn_br_code"));
            setTransactionRefNo(json.getString("transaction_ref_no"));
            setTxnBrCity(json.getString("txn_br_city"));
            setAcctOccpCode(json.getString("acct_occp_code"));
            setUnClrBalAmt(EventHelper.toDouble(json.getString("un_clr_bal_amt")));
            setStatus(json.getString("status"));
            setCountryCode(json.getString("country_code"));
            setCity(json.getString("city"));
            setCustInducedDate(EventHelper.toTimestamp(json.getString("cust_induced_date")));
            setConvRate(EventHelper.toDouble(json.getString("conv_rate")));
            setSwiftUaeftsMsgType(json.getString("swift_uaefts_msg_type"));
            setClrBalAmt(EventHelper.toDouble(json.getString("clr_bal_amt")));
            setResponseCode(json.getString("response_code"));
            setHomeBrCity(json.getString("home_br_city"));
            setEffAvlBal(EventHelper.toDouble(json.getString("eff_avl_bal")));
            setTranCategory(json.getString("tran_category"));
            setEntryUser(json.getString("entry_user"));
            setRefNum(json.getString("ref_num"));
            setAcctSolId(json.getString("acct_sol_id"));
            setIdNumber(json.getString("id_number"));
            setSchmCode(json.getString("schm_code"));
            setUserId(json.getString("user_id"));
            setFcyCurr(json.getString("fcy_curr"));
            setCardserno(json.getString("cardserno"));
            setTranParticular(json.getString("tran_particular"));
            setCardlessCashLimit(EventHelper.toDouble(json.getString("cardless_cash_limit")));
            setCustId(json.getString("cust_id"));
            setPosLimit(EventHelper.toDouble(json.getString("pos_limit")));
            setInstrumtDate(EventHelper.toTimestamp(json.getString("instrumt_date")));
            setIs27trancodeExists(json.getString("is27trancode_exists"));
            setCustConst(json.getString("cust_const"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setIpAddress(json.getString("ip_address"));
            setHdrmkrs(json.getString("hdrmkrs"));
            setTranType(json.getString("tran_type"));
            setWalletType(json.getString("wallet_type"));
            setMerchantId(json.getString("merchant_id"));
            setSuccFailFlag(json.getString("succ_fail_flag"));
            setInsTime(EventHelper.toTimestamp(json.getString("ins_time")));
            setRefCurr(json.getString("ref_curr"));
            setPartTranType(json.getString("part_tran_type"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setAtmLimitderive(cxps.events.CustomDerivator.getAtmLim(this));
        setCustIdderive(cxps.events.CustomDerivator.getCustDerive(this));
        setAvlBal(cxps.events.CustomDerivator.getAvlBal(this));
        setAvlBalance(cxps.events.CustomDerivator.getAvlBalance(this));
	setSuccFailFlagDerivd(cxps.events.CustomDerivator.getSuccFailFlagDerivd(this));
	setBin(cxps.events.CustomDerivator.getBin(this));
	setDesignatedFlg(cxps.events.CustomDerivator.getdesignatedFlg(this));
	setMidRate(cxps.events.CustomDerivator.getmidRate(this));
	setIs26trancodeExists(cxps.events.CustomDerivator.gettrancode(this));
	setCompmis1(cxps.events.CustomDerivator.getcompmis(this));
    }


    public String getJson(FieldFormatter fieldFormatter) {
        DefaultISOFieldFormatter formatter = (DefaultISOFieldFormatter)fieldFormatter;
        JSONObject json = new JSONObject();
        json.put("event_id", this.getEvent_ID(fieldFormatter));
        json.put("EventType", "ft");
        json.put("EventSubType", "accounttxn");
        json.put("event_name", "ft_accounttxn");
        json.put("eventts", this.getfinaldate(fieldFormatter));
        json.put("keys", "");
        //new LinkedHashMap();
        JSONObject body = new JSONObject();
        body.put("host_id", "F");
        body.put("channel", "DC");
        body.put("source", "DC");
        body.put("account_id", fieldFormatter.getField102());
        body.put("event_id", this.getEvent_ID(fieldFormatter));
        //body.put("avl_bal", this.getAvl_blc(fieldFormatter));
        body.put("avl_bal", "0.00");
        String tran_type = this.getTranType(fieldFormatter);
        body.put("tran_type", tran_type);
        body.put("tran_sub_type", tran_type);
        if (tran_type.equalsIgnoreCase("01")) {
            body.put("part_tran_type", "D");
        } else if (tran_type.equalsIgnoreCase("21")) {
            body.put("part_tran_type", "C");
        } else if (tran_type.equalsIgnoreCase("20")) {
            body.put("part_tran_type", "C");
        } else if (tran_type.equalsIgnoreCase("40")) {
            body.put("part_tran_type", "D");
        } else if (tran_type.equalsIgnoreCase("52")) {
            body.put("part_tran_type", "D");
        } else if (tran_type.equalsIgnoreCase("00")) {
            body.put("part_tran_type", "D");
        }

        body.put("tran_date", this.getfinaldate(fieldFormatter));
        body.put("pstd_date", this.getfinaldate(fieldFormatter));
        body.put("tran_amt", this.gettran_amt(fieldFormatter));
        body.put("tran_crncy_code", "AED");
        //removing device id as it was getting some extra string
        //body.put("device_id", this.getDeviceID(fieldFormatter));
        body.put("device_id", "");
        body.put("dev_owner_id", fieldFormatter.getField42());
        body.put("payee_code", fieldFormatter.getField98());
        body.put("payee_id", fieldFormatter.getField98());
        body.put("manul-debit-flag", "");
        body.put("schm_type", "DC");
        body.put("cust_id", "");
        body.put("schm_code", "DC");
        body.put("cust_id", "");
        body.put("acct_name", "");
        body.put("acct_sol_id", "");
        body.put("acct_ownership", "");
        body.put("acctopendate", "");
        body.put("clr_bal_amt", "");
        body.put("un_clr_bal_amt", "");
        body.put("eff_avl_bal", "");
        body.put("tran_id", this.getEvent_ID(fieldFormatter));
        body.put("tran_particular", "");
        body.put("tran_rmks", "");
        body.put("ref_num", "");
        body.put("ip_address", "");
        body.put("sys_time", this.getfinaldate(fieldFormatter));
        body.put("acct_br_code", "");
        body.put("user_id", "");
        body.put("entry_user", "");
        body.put("acct_occp_code", "");
        body.put("txn_br_code", "");
        body.put("cust_card_id", fieldFormatter.getField2());
        body.put("dcc_id", "");
        body.put("card_acceptor_name", "");
        body.put("dev_type", "");
        body.put("tran_category", "");
        body.put("branch_id", "");
        body.put("city", "");
        body.put("cust_const", "");
        body.put("txn_br_city", "");
        body.put("home_br_city", "");
        body.put("status", "");
        body.put("transaction_ref_no", "");
        body.put("hdrmkrs", "");
        body.put("cust_induced_date", "");
        body.put("ecom_limit", "");
        body.put("pos_limit", "");
        body.put("atm_limit", "");
        body.put("cardless_cash_limit", "");
        body.put("response_code",fieldFormatter.getField39());
        body.put("terminal_id",fieldFormatter.getField41());
        body.put("merchant_id",fieldFormatter.getField42());
        body.put("country_code",this.getCountry(fieldFormatter));
        body.put("succ_fail_flag", "");
        json.put("msgBody", body);
        System.out.println("JSON:- " + json.toString());
        return json.toString();
    }



    /* Getters */
    @Override
    public String getMnemonic() { return "FA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getDccId(){ return dccId; }

    public String getAcctBrCode(){ return acctBrCode; }

    public String getCardAcceptorName(){ return cardAcceptorName; }

    public String getPayeeCode(){ return payeeCode; }

    public String getChannel(){ return channel; }

    public java.sql.Timestamp getValueDate(){ return valueDate; }

    public String getManulDebitFlag(){ return manulDebitFlag; }

    public Double getFcyTranamt(){ return fcyTranamt; }

    public String getSource(){ return source; }

    public String getDevOwnerId(){ return devOwnerId; }

    public String getTranSubType(){ return tranSubType; }

    public String getDeviceId(){ return deviceId; }

    public String getCompmis2(){ return compmis2; }

    public String getCoinsDenomination(){ return coinsDenomination; }

    public String getLcyCurr(){ return "AED"; }

    public java.sql.Timestamp getAcctopendate(){ return acctopendate; }

    public String getTranId(){ return tranId; }

    public String getHostId(){ return hostId; }

    public Double getTranAmt(){ return tranAmt; }

    public String getSwiftUaeftsRefNum(){ return swiftUaeftsRefNum; }

    public Double getAtmLimit(){ return atmLimit; }

    public String getDevType(){ return devType; }

    public String getAccountId(){ return accountId; }

    public String getPlaceHolder(){ return placeHolder; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getBranchId(){ return branchId; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getTranRmks(){ return tranRmks; }

    public String getTerminalId(){ return terminalId; }

    public Double getEcomLimit(){ return ecomLimit; }

    public String getAcctName(){ return acctName; }

    public String getCustCardId(){ return custCardId; }

    public java.sql.Timestamp getPstdDate(){ return pstdDate; }

    public String getItmWithSelfAcct(){ return itmWithSelfAcct; }

    public String getPayeeId(){ return payeeId; }

    public String getSchmType(){ return schmType; }

    public String getAcctOwnership(){ return acctOwnership; }

    public String getTxnBrCode(){ return txnBrCode; }

    public String getTransactionRefNo(){ return transactionRefNo; }

    public String getTxnBrCity(){ return txnBrCity; }

    public String getAcctOccpCode(){ return acctOccpCode; }

    public Double getUnClrBalAmt(){ return unClrBalAmt; }

    public String getStatus(){ return status; }

    public String getCountryCode(){ return countryCode; }

    public String getCity(){ return city; }

    public java.sql.Timestamp getCustInducedDate(){ return custInducedDate; }

    public Double getConvRate(){ return convRate; }

    public String getSwiftUaeftsMsgType(){ return swiftUaeftsMsgType; }

    public Double getClrBalAmt(){ return clrBalAmt; }

    public String getResponseCode(){ return responseCode; }

    public String getHomeBrCity(){ return homeBrCity; }

    public Double getEffAvlBal(){ return effAvlBal; }

    public String getTranCategory(){ return tranCategory; }

    public String getEntryUser(){ return entryUser; }

    public String getIs27trancodeExists(){ return is27trancodeExists; }

    public String getRefNum(){ return refNum; }

    public String getAcctSolId(){ return acctSolId; }

    public String getIdNumber(){ return idNumber; }

    public String getSchmCode(){ return schmCode; }

    public String getUserId(){ return userId; }

    public String getFcyCurr(){ return fcyCurr; }

    public String getCardserno(){ return cardserno; }

    public String getTranParticular(){ return tranParticular; }

    public Double getCardlessCashLimit(){ return cardlessCashLimit; }

    public String getCustId(){ return custId; }

    public Double getPosLimit(){ return posLimit; }

    public java.sql.Timestamp getInstrumtDate(){ return instrumtDate; }

    public String getCustConst(){ return custConst; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getIpAddress(){ return ipAddress; }

    public String getHdrmkrs(){ return hdrmkrs; }

    public String getTranType(){ return tranType; }

    public String getWalletType(){ return walletType; }

    public String getMerchantId(){ return merchantId; }

    public String getSuccFailFlag(){ return succFailFlag; }

    public java.sql.Timestamp getInsTime(){ return insTime; }

    public String getRefCurr(){ return refCurr; }

    public String getPartTranType(){ return partTranType; }
    public String getBin(){ return bin; }

    public String getCompmis1(){ return compmis1; }

    public Double getAtmLimitderive(){ return atmLimitderive; }

    public String getDesignatedFlg(){ return designatedFlg; }

    public String getCustIdderive(){ return custIdderive; }

    public String getIs26trancodeExists(){ return is26trancodeExists; }

    public String getSuccFailFlagDerivd(){ return succFailFlagDerivd; }

    public Double getAvlBal(){ return avlBal; }

    public Double getAvlBalance(){ return avlBalance; }

    public Double getMidRate(){ return midRate; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setDccId(String val){ this.dccId = val; }
    public void setAcctBrCode(String val){ this.acctBrCode = val; }
    public void setCardAcceptorName(String val){ this.cardAcceptorName = val; }
    public void setPayeeCode(String val){ this.payeeCode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setValueDate(java.sql.Timestamp val){ this.valueDate = val; }
    public void setManulDebitFlag(String val){ this.manulDebitFlag = val; }
    public void setFcyTranamt(Double val){ this.fcyTranamt = val; }
    public void setSource(String val){ this.source = val; }
    public void setDevOwnerId(String val){ this.devOwnerId = val; }
    public void setCompmis1(String val){ this.compmis1 = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setCompmis2(String val){ this.compmis2 = val; }
    public void setCoinsDenomination(String val){ this.coinsDenomination = val; }
    public void setLcyCurr(String val){ this.lcyCurr = val; }
    public void setAcctopendate(java.sql.Timestamp val){ this.acctopendate = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setSwiftUaeftsRefNum(String val){ this.swiftUaeftsRefNum = val; }
    public void setAtmLimit(Double val){ this.atmLimit = val; }
    public void setDevType(String val){ this.devType = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setPlaceHolder(String val){ this.placeHolder = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranRmks(String val){ this.tranRmks = val; }
    public void setTerminalId(String val){ this.terminalId = val; }
    public void setEcomLimit(Double val){ this.ecomLimit = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setPstdDate(java.sql.Timestamp val){ this.pstdDate = val; }
    public void setItmWithSelfAcct(String val){ this.itmWithSelfAcct = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setSchmType(String val){ this.schmType = val; }
    public void setAcctOwnership(String val){ this.acctOwnership = val; }
    public void setTxnBrCode(String val){ this.txnBrCode = val; }
    public void setTransactionRefNo(String val){ this.transactionRefNo = val; }
    public void setTxnBrCity(String val){ this.txnBrCity = val; }
    public void setAcctOccpCode(String val){ this.acctOccpCode = val; }
    public void setUnClrBalAmt(Double val){ this.unClrBalAmt = val; }
    public void setStatus(String val){ this.status = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setCity(String val){ this.city = val; }
    public void setCustInducedDate(java.sql.Timestamp val){ this.custInducedDate = val; }
    public void setConvRate(Double val){ this.convRate = val; }
    public void setSwiftUaeftsMsgType(String val){ this.swiftUaeftsMsgType = val; }
    public void setClrBalAmt(Double val){ this.clrBalAmt = val; }
    public void setResponseCode(String val){ this.responseCode = val; }
    public void setHomeBrCity(String val){ this.homeBrCity = val; }
    public void setEffAvlBal(Double val){ this.effAvlBal = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setEntryUser(String val){ this.entryUser = val; }
    public void setIs27trancodeExists(String val){ this.is27trancodeExists = val; }
    public void setRefNum(String val){ this.refNum = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setIdNumber(String val){ this.idNumber = val; }
    public void setSchmCode(String val){ this.schmCode = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setFcyCurr(String val){ this.fcyCurr = val; }
    public void setCardserno(String val){ this.cardserno = val; }
    public void setTranParticular(String val){ this.tranParticular = val; }
    public void setCardlessCashLimit(Double val){ this.cardlessCashLimit = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setPosLimit(Double val){ this.posLimit = val; }
    public void setInstrumtDate(java.sql.Timestamp val){ this.instrumtDate = val; }
    public void setCustConst(String val){ this.custConst = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setHdrmkrs(String val){ this.hdrmkrs = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setWalletType(String val){ this.walletType = val; }
    public void setMerchantId(String val){ this.merchantId = val; }
    public void setSuccFailFlag(String val){ this.succFailFlag = val; }
    public void setInsTime(java.sql.Timestamp val){ this.insTime = val; }
    public void setRefCurr(String val){ this.refCurr = val; }
    public void setPartTranType(String val){ this.partTranType = val; }
    public void setBin(String val){ this.bin = val; }
    public void setAtmLimitderive(Double val){ this.atmLimitderive = val; }
    public void setDesignatedFlg(String val){ this.designatedFlg = val; }
    public void setCustIdderive(String val){ this.custIdderive = val; }
    public void setIs26trancodeExists(String val){ this.is26trancodeExists = val; }
    public void setSuccFailFlagDerivd(String val){ this.succFailFlagDerivd = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setAvlBalance(Double val){ this.avlBalance = val; }
    public void setMidRate(Double val){ this.midRate = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        if(this.schmType.equalsIgnoreCase("ITCW") || (this.schmType.equalsIgnoreCase("ITWL"))) {
            String noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.idNumber);
            wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        }
        else if((this.tranType.equalsIgnoreCase("00")) || (this.tranType.equalsIgnoreCase("01"))) {
            String noncustomerKey1= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.terminalId + this.bin);
            wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey1));
        }
	else{
		String noncustomerKey2= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.tranId);
        	wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey2));
	}
	
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        String transactionKey= h.getCxKeyGivenHostKey(WorkspaceName.TRANSACTION, getHostId(), this.tranId);
        wsInfoSet.add(new WorkspaceInfo("Transaction", transactionKey));
        String paymentcardKey= h.getCxKeyGivenHostKey(WorkspaceName.PAYMENTCARD, getHostId(), this.custCardId);
        wsInfoSet.add(new WorkspaceInfo("Paymentcard", paymentcardKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Accounttxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Accounttxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }

    public String getEvent_ID(FieldFormatter fieldFormatter) {
        String stan_no = fieldFormatter.getField11();
        String field12_time=fieldFormatter.getField12();
        String  field13_time = fieldFormatter.getField13();
        //we are not using epoch time
        String eventID = "DC_" + stan_no +field12_time+field13_time;
        return eventID;
    }

    public String getfinaldate(FieldFormatter fieldFormatter) {
        String date = "";
        String month = "";
        String hours = "";
        String minutes = "";
        String second = "";
        int year = Calendar.getInstance().get(1);
        String stringyear = String.valueOf(year);
        String ms = fieldFormatter.getField12();
        String ds = fieldFormatter.getField13();
        String[] dateelement = ds.split("(?<=\\G..)");
        String[] timeelement = ms.split("(?<=\\G..)");
        System.out.println("---" + dateelement);
        month = dateelement[0];
        date = dateelement[1];
        hours = timeelement[0];
        minutes = timeelement[1];
        second = timeelement[2];

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        String final_date = date + "-" + month + "-" + stringyear + " " + hours + ":" + minutes + ":" + second +"."+ formattedDate;
        return final_date;
    }

    public String gettran_amt(FieldFormatter fieldFormatter) {
        String amt = fieldFormatter.getField4();
        Integer value = Integer.parseInt(amt);
        int finalamt = value / 100;
        String stringvalue = String.valueOf(finalamt);
        return stringvalue;
    }


    public String getAvl_blc(FieldFormatter fieldFormatter) {
        String final_amt = "";
        long finalamt = 0L;

        String avl = fieldFormatter.getField4().trim();

        if (avl == null || avl.isEmpty()){
            avl = "000";
        }
        //String sign = avl.substring(7, 8);
        //String[] amt = avl.split(sign);
        int value = Integer.parseInt(avl);
        finalamt = (long)(value / 100);
        /*if (sign.equalsIgnoreCase("D")) {
            finalamt = (long)(value / 100 * -1);
        } else if (sign.equalsIgnoreCase("C")) {
            finalamt = (long)(value / 100);
        }*/

        final_amt = String.valueOf(finalamt);
        return final_amt;
    }



    public String getDeviceID(FieldFormatter fieldFormatter) {
        String cardId = fieldFormatter.getField41();
        String cardLoc = fieldFormatter.getField43();
        if (cardId != null && cardLoc != null) {
            String derivedDeviceID = cardId + cardLoc;
            return derivedDeviceID;
        } else {
            return null;
        }
    }

    public String getTranType(FieldFormatter fieldFormatter) {
        String proc_code = fieldFormatter.getField3();
        String tran_type = proc_code.substring(0, 2);
        return tran_type;
    }
    public  String getCountry(FieldFormatter fieldFormatter){
        String  field43=fieldFormatter.getField43();
        String  code= field43.substring(38);
        return  code;
    }

        /**
        * This method is supposed to return a proper user understandable
        * message to be shown on the UI for end user.
        * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
        * his account on 10th Feb, 2017 at 01:00 pm
        */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
