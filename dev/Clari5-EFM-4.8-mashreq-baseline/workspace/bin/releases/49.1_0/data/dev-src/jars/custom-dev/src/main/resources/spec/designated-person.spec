cxps.noesis.glossary.entity.designated-person {
       db-name = DESIGNATED_PERSON
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
                { name = cif_id, column = cif_id, type = "string:50", key=true }
		{ name = related_party_name, column = related_party_name, type = "string:50"}
		{ name = related_party_type, column = related_party_type, type = "string:50"}
		{ name = passport_number, column = passport_number, type = "string:50"}
		{ name = emirates_id, column = emirates_id, type = "string:50"}
		{ name = nationality, column = nationality, type = "string:50"}
               ]
       }
