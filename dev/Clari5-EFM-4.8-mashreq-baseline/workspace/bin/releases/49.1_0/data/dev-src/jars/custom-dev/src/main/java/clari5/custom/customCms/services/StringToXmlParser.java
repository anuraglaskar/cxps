package clari5.custom.customCms.services;

import cxps.apex.utils.CxpsLogger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
public class StringToXmlParser {

    private static CxpsLogger logger = CxpsLogger.getLogger(StringToXmlParser.class);


    //this method is to parse string into xml
    //we are assuming the response will be as shown below

    public Map freezeparsing(String res) {

        logger.info("res in Parser ::",res);
        System.out.println("coming inside freezeparsing");
        Map hashMap= new HashMap();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            String status="";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(res)));
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("cre:ExceptionDetails");
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) node;
                    String error_code=eElement.getElementsByTagName("mbc:ErrorCode").item(0).getTextContent();
                    String  error_desc= eElement.getElementsByTagName("mbc:ErrorDescription").item(0).getTextContent();
                    System.out.println("error_code :: "+error_code+"::error_desc:: "+error_desc);
                    if(error_desc.equalsIgnoreCase("SUCCESS")){

                      status="S";
                    }
                    else {
                        status="F";
                    }
                 hashMap.put("error_code",error_code);
                 hashMap.put("error_desc",error_desc);
                 hashMap.put("status",status);
                    System.out.println("final map ::"+hashMap );

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("final map to return :: "+hashMap);
        return hashMap;

    }
    public Map unfreezeparsing(String res) {

        logger.info("res in Parser ::",res);
        System.out.println("cmoing inside unfreezeparsing");
        Map hashMap= new HashMap();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            String status="";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(res)));
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("mod:ExceptionDetails");
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) node;
                    String error_code=eElement.getElementsByTagName("mbc:ErrorCode").item(0).getTextContent();
                    String  error_desc= eElement.getElementsByTagName("mbc:ErrorDescription").item(0).getTextContent();
                    System.out.println("error_code :: "+error_code+"::error_desc:: "+error_desc);
                    if(error_desc.equalsIgnoreCase("SUCCESS")){
                        status="S";
                    }
                    else {
                        status="F";
                    }
                    hashMap.put("error_code",error_code);
                    hashMap.put("error_desc",error_desc);
                    hashMap.put("status",status);
                    System.out.println("final map ::"+hashMap );

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("final map to return :: "+hashMap);
        return hashMap;

    }
    public Map cardblockparsing(String res) {

        logger.info("res in card Parser ::",res);
        System.out.println("response is in Parser ::"+res);

        Map hashMap= new HashMap();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            String status="";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(res)));
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("car:ExceptionDetails");
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) node;
                    String error_code=eElement.getElementsByTagName("mbc:ErrorCode").item(0).getTextContent();
                    String  error_desc= eElement.getElementsByTagName("mbc:ErrorDescription").item(0).getTextContent();
                    System.out.println("error_code :: "+error_code+"::error_desc:: "+error_desc);
                    if(error_code.equalsIgnoreCase("SUCCESS")){
                        status="S";
                    }
                    else {
                        status="F";
                    }
                    hashMap.put("error_code",error_code);
                    hashMap.put("error_desc",error_desc);
                    hashMap.put("status",status);
                    System.out.println("final map ::"+hashMap );

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("final map to return :: "+hashMap);
        return hashMap;
    }
    public Map cardUnblookparsing(String res) {

        logger.info("res in card unblock Parser ::",res);
        System.out.println("response is in Parser ::"+res);

        Map hashMap= new HashMap();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            String status="";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(res)));
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("car:ExceptionDetails");
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) node;
                    String error_code=eElement.getElementsByTagName("mbc:ErrorCode").item(0).getTextContent();
                    String  error_desc= eElement.getElementsByTagName("mbc:ErrorDescription").item(0).getTextContent();
                    System.out.println("error_code :: "+error_code+"::error_desc:: "+error_desc);
                    if(error_code.equalsIgnoreCase("SUCCESS")){
                        status="S";
                    }
                    else {
                        status="F";
                    }
                    hashMap.put("error_code",error_code);
                    hashMap.put("error_desc",error_desc);
                    hashMap.put("status",status);
                    System.out.println("final map ::"+hashMap );

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("final map to return :: "+hashMap);
        return hashMap;
    }
}
