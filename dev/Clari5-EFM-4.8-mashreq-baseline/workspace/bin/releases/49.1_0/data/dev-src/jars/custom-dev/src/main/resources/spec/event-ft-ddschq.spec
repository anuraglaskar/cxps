cxps.events.event.ft-ddschq{
  table-name : EVENT_FT_DDSCHQ
  event-mnemonic: DDS
  workspaces : {
    CUSTOMER: "cis-no"
  }
 event-attributes : {
record-type: {db:true ,raw_name : record_type ,type:"string:50"}
record-id: {db:true ,raw_name :record_id,type:"string:50"}
subject-type: {db :true ,raw_name : subject_type ,type : "string:50"}
full-name-en: {db:true ,raw_name : full_name_en,type:"string:50"}
full-name-ar: {db:true ,raw_name : full_name_ar ,type:"string:50"}
dob: {db:true ,raw_name : dob ,type:"string:50"}
nationality: {db:true ,raw_name : nationality ,type:"string:50"}
passport-no: {db:true ,raw_name : passport_no ,type:"string:50"}
emirates-id: {db:true ,raw_name : emirates_id ,type:"string:50"}
trade-name-en: {db:true ,raw_name : trade_name_en ,type:"string:50"}
trade-name-ar: {db :true ,raw_name : trade_name_ar ,type : "string:50"}
trade-lin-no: {db:true ,raw_name : trade_lin_no ,type:"string:50"}
reg-place: {db:true ,raw_name : reg_place ,type:"string:50"}
type-pay-ord: {db:true ,raw_name : type_pay_ord,type:"string:50"}
bnk-identifier: {db:true ,raw_name : bnk_identifier,type:"string:50"}
bene-name: {db:true ,raw_name : bene_name ,type:"string:20"}
iban: {db:true ,raw_name : iban ,type:"string:50"}
cheque-dd-no: {db:true ,raw_name : cheque_dd_no ,type:"string:50"}
amt: {db :true ,raw_name : amt ,type : "number:20,4"}
reason-code: {db:true ,raw_name : reason_code ,type:"string:50"}
dor: {db:true ,raw_name : dor ,type:"string:50"}
ref-date: {db:true ,raw_name : ref_date ,type:timestamp}
desc-code: {db:true ,raw_name : desc_code ,type:"string:50"}
data-date: {db:true ,raw_name : data_date ,type:timestamp}
emirate: {db:true ,raw_name : emirate ,type:"string:50"}
primary-mobile-no: {db :true ,raw_name : primary_mobile_no ,type : "string:50"}
source-name: {db:true ,raw_name : source_name ,type:"string:50"}
cis-no: {db:true ,raw_name : cis_no ,type:"string:50"}

}
}

