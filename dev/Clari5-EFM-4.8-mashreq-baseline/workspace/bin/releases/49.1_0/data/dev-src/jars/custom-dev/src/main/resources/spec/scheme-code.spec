cxps.noesis.glossary.entity.scheme-code {
       db-name =SCHEME_CODE
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = scheme-code, column = SCHEME_CODE, type = "string:20", key=true }

               ]
       }

