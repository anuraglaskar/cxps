package clari5.custom.customScn.scenarioNew1;

import java.sql.Date;

public class ThreadData {
    private   java.sql.Date threadDate=null;
    private  String currentThreadName=null;
    private  long thrdId=0;
    private  String insrtQry=null;
    private String selectQry=null;
    private String updateQry=null;

    public String getSelectQry() {
        return selectQry;
    }

    public void setSelectQry(String selectQry) {
        this.selectQry = selectQry;
    }

    public String getUpdateQry() {
        return updateQry;
    }

    public void setUpdateQry(String updateQry) {
        this.updateQry = updateQry;
    }

    public Date getThreadDate() {

        return threadDate;
    }

    public void setThreadDate(Date threadDate) {
        this.threadDate = threadDate;
    }

    public String getCurrentThreadName() {
        return currentThreadName;
    }

    public void setCurrentThreadName(String currentThreadName) {
        this.currentThreadName = currentThreadName;
    }


    public long getThrdId() {
        return thrdId;
    }

    public void setThrdId(long thrdId) {
        this.thrdId = thrdId;
    }

    public String getInsrtQry() {
        return insrtQry;
    }

    public void setInsrtQry(String insrtQry) {
        this.insrtQry = insrtQry;
    }

    @Override
    public String toString() {
        return "ThreadData{" +
                "threadDate=" + threadDate +
                ", currentThreadName='" + currentThreadName + '\'' +
                ", thrdId=" + thrdId +
                ", insrtQry='" + insrtQry + '\'' +
                ", selectQry='" + selectQry + '\'' +
                ", updateQry='" + updateQry + '\'' +
                '}';
    }
}
