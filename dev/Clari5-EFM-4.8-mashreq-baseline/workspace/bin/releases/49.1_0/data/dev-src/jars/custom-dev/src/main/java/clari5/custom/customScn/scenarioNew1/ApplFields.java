package clari5.custom.customScn.scenarioNew1;

public class ApplFields {

    //reject table fields;
    private String currentdate;
    private String event_id;
    private String rejapplid;
    private String rejappldate;
    private String rejcif;
    private String rejmobileno;
    private String rejpassportno;
    private String rejeid;
    private String rejdateofbirth;
    private String rejproductcode;
    private String rejectiondate;
    private String declinereason;
    //matched fields
    private String matchedmobileno;
    private String matchedpassportno;
    private String matchedeid;
    private String matcheddateofbirth;
    //acctopening fields;
    private String apprefno;
    private String product;
    private String eida;
    private String custid;
    private String phno;
    private String emailid;
    private String channel;
    private String dob;
    private String passportno;
    private String drivinglnc;
    private String tradelicense;
    private String vat;
    private String isemployernonsfa;
    private String employeeid;
    private String accountid;
    private String appldate;
    private String ibanno;
    private String applname;
    private String branchid;
    private String employerid;

    public String getEmployerid() {
        return employerid;
    }

    public void setEmployerid(String employerid) {
        this.employerid = employerid;
    }

    public String getDeclinereason() {
        return declinereason;
    }

    public void setDeclinereason(String declinereason) {
        this.declinereason = declinereason;
    }

    public String getAppldate() {
        return appldate;
    }

    public void setAppldate(String appldate) {
        this.appldate = appldate;
    }

    public String getIbanno() {
        return ibanno;
    }

    public void setIbanno(String ibanno) {
        this.ibanno = ibanno;
    }

    public String getApplname() {
        return applname;
    }

    public void setApplname(String applname) {
        this.applname = applname;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }


    public String getCurrentdate() {
        return currentdate;
    }

    public void setCurrentdate(String currentdate) {
        this.currentdate = currentdate;
    }
    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
    public String getRejapplid() {
        return rejapplid;
    }

    public void setRejapplid(String rejapplid) {
        this.rejapplid = rejapplid;
    }

    public String getRejappldate() {
        return rejappldate;
    }

    public void setRejappldate(String rejappldate) {
        this.rejappldate = rejappldate;
    }

    public String getRejcif() {
        return rejcif;
    }

    public void setRejcif(String rejcif) {
        this.rejcif = rejcif;
    }

    public String getRejmobileno() {
        return rejmobileno;
    }

    public void setRejmobileno(String rejmobileno) {
        this.rejmobileno = rejmobileno;
    }

    public String getRejpassportno() {
        return rejpassportno;
    }

    public void setRejpassportno(String rejpassportno) {
        this.rejpassportno = rejpassportno;
    }

    public String getRejeid() {
        return rejeid;
    }

    public void setRejeid(String rejeid) {
        this.rejeid = rejeid;
    }

    public String getRejdateofbirth() {
        return rejdateofbirth;
    }

    public void setRejdateofbirth(String rejdateofbirth) {
        this.rejdateofbirth = rejdateofbirth;
    }

    public String getRejproductcode() {
        return rejproductcode;
    }

    public void setRejproductcode(String rejproductcode) {
        this.rejproductcode = rejproductcode;
    }

    public String getRejectiondate() {
        return rejectiondate;
    }

    public void setRejectiondate(String rejectiondate) {
        this.rejectiondate = rejectiondate;
    }

    public String getMatchedmobileno() {
        return matchedmobileno;
    }

    public void setMatchedmobileno(String matchedmobileno) {
        this.matchedmobileno = matchedmobileno;
    }

    public String getMatchedpassportno() {
        return matchedpassportno;
    }

    public void setMatchedpassportno(String matchedpassportno) {
        this.matchedpassportno = matchedpassportno;
    }

    public String getMatchedeid() {
        return matchedeid;
    }

    public void setMatchedeid(String matchedeid) {
        this.matchedeid = matchedeid;
    }

    public String getMatcheddateofbirth() {
        return matcheddateofbirth;
    }

    public void setMatcheddateofbirth(String matcheddateofbirth) {
        this.matcheddateofbirth = matcheddateofbirth;
    }

    public String getApprefno() {
        return apprefno;
    }

    public void setApprefno(String apprefno) {
        this.apprefno = apprefno;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getEida() {
        return eida;
    }

    public void setEida(String eida) {
        this.eida = eida;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getPhno() {
        return phno;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassportno() {
        return passportno;
    }

    public void setPassportno(String passportno) {
        this.passportno = passportno;
    }

    public String getDrivinglnc() {
        return drivinglnc;
    }

    public void setDrivinglnc(String drivinglnc) {
        this.drivinglnc = drivinglnc;
    }

    public String getTradelicense() {
        return tradelicense;
    }

    public void setTradelicense(String tradelicense) {
        this.tradelicense = tradelicense;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getIsemployernonsfa() {
        return isemployernonsfa;
    }

    public void setIsemployernonsfa(String isemployernonsfa) {
        this.isemployernonsfa = isemployernonsfa;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

}