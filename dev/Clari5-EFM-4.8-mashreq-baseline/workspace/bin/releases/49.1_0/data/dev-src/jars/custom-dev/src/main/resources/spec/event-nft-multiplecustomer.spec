cxps.events.event.nft-multiplecustomer{  
  table-name : EVENT_NFT_MULTIPLECUSTOMER
  event-mnemonic: MCE
  workspaces : {
    CUSTOMER: "new-cxcif"  
}
 event-attributes : {
new-email: {db:true ,raw_name : new_email ,type:"string:20"}
new-mobile: {db:true ,raw_name : new_mobile ,type:"string:50"}
new-cxcif: {db :true ,raw_name : new_cxcif ,type : "string:20"}
new-eid: {db:true ,raw_name : new_eid ,type:"string:20"}
new-passport: {db:true ,raw_name : new_passport ,type:"string:20"}
new-custname: {db:true ,raw_name : new_custName ,type:"string:20"}
new-custtype: {db:true ,raw_name : new_custType ,type:"string:20"}
new-compmis1: {db:true ,raw_name : new_compmis1 ,type:"string:20"}
new-compmis2: {db:true ,raw_name : new_compmis2 ,type:"string:50"}
old-cif: {db:true ,raw_name : old_cif ,type:"string:20"}
old-compmis1: {db:true ,raw_name : old_compmis1 ,type:"string:50"}
old-compmis2: {db :true ,raw_name : old_compmis2 ,type : "string:20"}
old-custname: {db:true ,raw_name : old_cust_name ,type:"string:20"}
old-passport: {db:true ,raw_name : old_passportNo ,type:"string:20"}
old-eid: {db:true ,raw_name : old_eid ,type:"string:20"}
old-email: {db:true ,raw_name : old_email ,type:"string:20"}
old-mobile: {db:true ,raw_name : old_mobile ,type:"string:20"}
old-custtype: {db:true ,raw_name : old_custType ,type:"string:50"}
matched-email: {db :true ,raw_name : matched_email ,type : "string:20"}
matched-mobile: {db:true ,raw_name : matched_mobile ,type:"string:20"}
}
}
