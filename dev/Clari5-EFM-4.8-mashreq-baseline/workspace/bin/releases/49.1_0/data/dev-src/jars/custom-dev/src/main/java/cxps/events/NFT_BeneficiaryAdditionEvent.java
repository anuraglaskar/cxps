// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_BENEFICIARY_ADDITION", Schema="rice")
public class NFT_BeneficiaryAdditionEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String payeeType;
       @Field(size=200) public String userId;
       @Field(size=200) public String payeeBranchName;
       @Field(size=200) public String errorDesc;
       @Field(size=200) public String channel;
       @Field(size=20) public String succFailFlg;
       @Field(size=200) public String benefType;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String errorCode;
       @Field(size=200) public String payeeMashreq;
       @Field(size=20) public String deviceId;
       @Field(size=200) public String payeeBankName;
       @Field(size=200) public String payeeCustId;
       @Field(size=200) public String payeeBankId;
       @Field(size=200) public String addrNetwork;
       @Field(size=200) public String payeeId;
       @Field(size=20) public String hostId;


    @JsonIgnore
    public ITable<NFT_BeneficiaryAdditionEvent> t = AEF.getITable(this);

	public NFT_BeneficiaryAdditionEvent(){}

    public NFT_BeneficiaryAdditionEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("BeneficiaryAddition");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setPayeeType(json.getString("payee_type"));
            setUserId(json.getString("user_id"));
            setPayeeBranchName(json.getString("payee_branch_name"));
            setErrorDesc(json.getString("error_desc"));
            setChannel(json.getString("channel"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setBenefType(json.getString("benef_type"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setPayeeMashreq(json.getString("payee_mashreq"));
            setDeviceId(json.getString("device_id"));
            setPayeeBankName(json.getString("payee_bank_name"));
            setPayeeCustId(json.getString("payee_cust_id"));
            setPayeeBankId(json.getString("payee_bank_id"));
            setAddrNetwork(json.getString("addr_network"));
            setPayeeId(json.getString("payee_id"));
            setHostId(json.getString("host_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "BA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getPayeeType(){ return payeeType; }

    public String getUserId(){ return userId; }

    public String getPayeeBranchName(){ return payeeBranchName; }

    public String getErrorDesc(){ return errorDesc; }

    public String getChannel(){ return channel; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getBenefType(){ return benefType; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getPayeeMashreq(){ return payeeMashreq; }

    public String getDeviceId(){ return deviceId; }

    public String getPayeeBankName(){ return payeeBankName; }

    public String getPayeeCustId(){ return payeeCustId; }

    public String getPayeeBankId(){ return payeeBankId; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getPayeeId(){ return payeeId; }

    public String getHostId(){ return hostId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setPayeeType(String val){ this.payeeType = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setPayeeBranchName(String val){ this.payeeBranchName = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setBenefType(String val){ this.benefType = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setPayeeMashreq(String val){ this.payeeMashreq = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setPayeeBankName(String val){ this.payeeBankName = val; }
    public void setPayeeCustId(String val){ this.payeeCustId = val; }
    public void setPayeeBankId(String val){ this.payeeBankId = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setHostId(String val){ this.hostId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_BeneficiaryAddition");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "BeneficiaryAddition");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}