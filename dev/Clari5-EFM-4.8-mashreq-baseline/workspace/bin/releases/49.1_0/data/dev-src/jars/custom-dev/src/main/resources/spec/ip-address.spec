cxps.noesis.glossary.entity.ip-address {
       db-name = IP_ADDRESS
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = ip-address, column = IP_ADDRESS, type = "string:50", key=true }

               ]
       }

