package clari5.custom.customScn11.daemonScn11;

import clari5.platform.applayer.CxpsRunnable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.exceptions.RuntimeFatalException;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.platform.util.Hocon;
import clari5.platform.util.ICxResource;
import clari5.rdbms.Rdbms;
import cxps.events.NFT_StaticinfochangeEvent;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

public class StaticInfoChangeJsonCreation extends CxpsRunnable implements ICxResource {


    private static final CxpsLogger logger = CxpsLogger.getLogger(StaticInfoChangeJsonCreation.class);

    static Hocon hocon;
    static ConcurrentLinkedQueue<JSONObject> queue = new ConcurrentLinkedQueue<>();
    static String selectScn11DeatailsTracker;
    static String updatescn11DeatailsTracker;

    static {
        hocon = new Hocon();
        hocon.loadFromContext("snew11.conf");
        selectScn11DeatailsTracker = hocon.getString("selectScnDetailsTracker");
        updatescn11DeatailsTracker = hocon.getString("updateCustScnDetailsTracker");
    }

    @Override
    protected Object getData() throws RuntimeFatalException {
        logger.info("Inside getData method of Daemon for  static info Change change");
        return getScn11DetailsTrack();

    }

    @Override
    protected void processData(Object o) throws RuntimeFatalException {
        logger.info(" Inside the process data of Daemon  ");
        if (o instanceof ConcurrentLinkedQueue) {
            ConcurrentLinkedQueue<HashMap> DataQueue = (ConcurrentLinkedQueue<HashMap>) o;

            while (!DataQueue.isEmpty()) {
                HashMap map = DataQueue.poll();
                String userId = (String) map.get("user_id");
                String custId = (String) map.get("cust_id");
                String account = (String) map.get("account");
                String entity_type = (String) map.get("entity_type");

                JSONObject eventJSON = getJSONFormat( custId,userId, account, entity_type);
                String eventId = eventJSON.getString("event_id");
                String jsonString = eventJSON.toString();
                ECClient.enqueue("HOST", eventId, jsonString);
                /**
                 * updating the status "C" after completion
                 */
                try (RDBMSSession session = Rdbms.getAppSession()) {
                    try (CxConnection connection = session.getCxConnection()) {
                        try ( PreparedStatement statement1 = connection.prepareStatement(updatescn11DeatailsTracker)) {
                            statement1.setString(1, "C");
                            statement1.setString(2, custId);
                            statement1.setString(3, userId);
                            statement1.executeUpdate();
                            connection.commit();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void configure(Hocon h) {
        logger.debug("Json creation for Account status change");
    }

    public synchronized static ConcurrentLinkedQueue<HashMap> getScn11DetailsTrack() {

        logger.info("Inside the method where taking data from SCN11_Datails_Track Table");
        ConcurrentLinkedQueue<HashMap> queue = new ConcurrentLinkedQueue<>();

        try (RDBMSSession session = Rdbms.getAppSession()) {
            try (CxConnection connection = session.getCxConnection()) {
                try (PreparedStatement statement = connection.prepareStatement(selectScn11DeatailsTracker);
                     PreparedStatement statement1 = connection.prepareStatement(updatescn11DeatailsTracker)) {
                    statement.setString(1, "New_Info");

                    try (ResultSet result = statement.executeQuery()) {

                        while (result.next()) {

                            String userId = result.getString("UserID");
                            String custId = result.getString("Cust_id");
                            String account = result.getString("Account");
                            String entity_type = result.getString("entity_type");

                            HashMap map = new HashMap();
                            map.put("user_id", userId);
                            map.put("cust_id", custId);
                            map.put("account", account);
                            map.put("entity_type",entity_type);
                            queue.add(map);
                            /**
                             * Updating the status in custom table
                             */
                            statement1.setString(1, "IP");
                            statement1.setString(2, custId);
                            statement1.setString(3, userId);
                            statement1.executeUpdate();
                            connection.commit();
                        }
                    }

                }
                connection.commit();
                connection.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queue;
    }

    public static JSONObject getJSONFormat(String custId, String userId, String account, String entity_type ) {

        JSONObject modifiedEvent = new JSONObject();
        JSONObject msgBody = new JSONObject();

        Random random = new Random();
        String eventId = String.valueOf(random.nextInt());
        String systime = "";

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            systime = simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        msgBody.put("host_id", "F");
        msgBody.put("sys_time", systime);
        msgBody.put("event_id", eventId);
        msgBody.put("account_id", account != null ? account : "");
        msgBody.put("user_id", userId);
        msgBody.put("channel", "");
        msgBody.put("branch_id", "");
        msgBody.put("cust_id", custId != null ? custId : "");
        msgBody.put("entity_type", entity_type );
        msgBody.put("initsubentityval", "");
        msgBody.put("tran_date", "");
        msgBody.put("finalsubentityval", "");
        msgBody.put("ref_id", "");


        modifiedEvent.put("EventType", "nft");
        modifiedEvent.put("EventSubType", "staticinfochangenew");
        modifiedEvent.put("event_name", "'nft_staticinfochangenew'");
        modifiedEvent.put("event_id", eventId);
        modifiedEvent.put("user_id",userId);
        modifiedEvent.put("msgBody", msgBody.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));
        return modifiedEvent;
    }


}