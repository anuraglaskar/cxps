# Generated Code
clari5.custom.customop.db{
 	entity {
		thread-audit {
          	attributes = [
        	{ name= thread-name, column =thread_name, type ="string:50"}
        	{ name= thread-id, column =thread_id, type ="string:10"}
        	{ name= rcre-time, column =rcre_time, type ="date", key: true}
        	{ name= status, column =status, type ="string:10"}
        	] #end of attributes
	}#end of thread-audit
	}#end of entity
        
}#end of start
