// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_ELECTRONIC_PAYMENT_REQUEST", Schema="rice")
public class FT_ElectronicPaymentReqEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String countryCode;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field(size=20) public String channel;
       @Field(size=20) public String succFailFlg;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=20) public String custId;
       @Field(size=200) public String tranCrncyCode;
       @Field(size=20) public Double cardlessCashLimit;
       @Field(size=200) public String benfName;
       @Field(size=200) public String ipAddress;
       @Field(size=20) public String tranType;
       @Field(size=200) public String transferRemarks;
       @Field(size=20) public Double avlBal;
       @Field(size=20) public String hostId;
       @Field(size=20) public Double tranAmt;


    @JsonIgnore
    public ITable<FT_ElectronicPaymentReqEvent> t = AEF.getITable(this);

	public FT_ElectronicPaymentReqEvent(){}

    public FT_ElectronicPaymentReqEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("ElectronicPaymentReq");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setCountryCode(json.getString("country_code"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setChannel(json.getString("channel"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setBenfName(json.getString("benf_name"));
            setIpAddress(json.getString("ip_address"));
            setTranType(json.getString("tran_type"));
            setTransferRemarks(json.getString("transfer_remarks"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setHostId(json.getString("host_id"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setCustId(cxps.events.CustomDerivator.getCustIdFromAccountEPay(this));setCardlessCashLimit(cxps.events.CustomDerivator.getCardlessCashLimit(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "EP"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getCountryCode(){ return countryCode; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public String getChannel(){ return channel; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getBenfName(){ return benfName; }

    public String getIpAddress(){ return ipAddress; }

    public String getTranType(){ return tranType; }

    public String getTransferRemarks(){ return transferRemarks; }

    public Double getAvlBal(){ return avlBal; }

    public String getHostId(){ return hostId; }

    public Double getTranAmt(){ return tranAmt; }
    public String getCustId(){ return custId; }

    public Double getCardlessCashLimit(){ return cardlessCashLimit; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setBenfName(String val){ this.benfName = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setTransferRemarks(String val){ this.transferRemarks = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setCardlessCashLimit(Double val){ this.cardlessCashLimit = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_ElectronicPaymentReq");
        json.put("event_type", "FT");
        json.put("event_sub_type", "ElectronicPaymentReq");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}