cxps.events.event.nft-acct-opened-same-details{   
  table-name : EVENT_NFT_ACCT_OPENED_SAME_DETAILS
  event-mnemonic: ASD
  workspaces : {
    CUSTOMER: "new-cif"  
}
 event-attributes : {
new-email: {db:true ,raw_name : new_email ,type:"string:20"}
new-mobile: {db:true ,raw_name : new_mobile ,type:"string:50"}
new-cif: {db :true ,raw_name : new_cif ,type : "string:20"}
new-eid: {db:true ,raw_name : new_eid ,type:"string:20"}
new-passport: {db:true ,raw_name : new_passport ,type:"string:20"}
new-custname: {db:true ,raw_name : new_custname ,type:"string:20"}
new-compmis1: {db:true ,raw_name : new_compmis1 ,type:"string:20"}
new-compmis2: {db:true ,raw_name : new_compmis2 ,type:"string:50"}
new-tradelicense: {db:true ,raw_name : new_tradelicense ,type:"string:50"}
new-accountno: {db:true ,raw_name : new_accountno ,type:"string:50"}
new-custdob: {db:true ,raw_name : new_custdob ,type:"string:50"}
new-branchid: {db:true ,raw_name : new_branchid ,type:"string:50"}
new-acctopendate: {db:true ,raw_name : new_acctopendate ,type : timestamp}
new-acctcloseddate: {db:true ,raw_name : new_acctcloseddate ,type : timestamp}
old-email: {db:true ,raw_name : old_email ,type:"string:20"}
old-mobile: {db:true ,raw_name : old_mobile ,type:"string:20"}
old-cif: {db:true ,raw_name : old_cif ,type:"string:20"}
old-eid: {db:true ,raw_name : old_eid ,type:"string:20"}
old-passport: {db:true ,raw_name : old_passport ,type:"string:20"}
old-custname: {db:true ,raw_name : old_custname ,type:"string:20"}
old-compmis1: {db:true ,raw_name : old_compmis1 ,type:"string:50"}
old-compmis2: {db :true ,raw_name : old_compmis2 ,type : "string:20"}
old-tradelicense: {db:true ,raw_name : old_tradelicense ,type:"string:50"}
old-accountno: {db:true ,raw_name : old_accountno ,type:"string:50"}
old-custdob: {db:true ,raw_name : old_custdob ,type:"string:50"}
old-branchid: {db:true ,raw_name : old_branchid ,type:"string:50"}
old-acctopendate: {db:true ,raw_name : old_acctopendate ,type : timestamp}
old-acctcloseddate: {db:true ,raw_name : old_acctcloseddate ,type : timestamp}
matched-mobile: {db:true ,raw_name : matched_mobile ,type:"string:20"}
matched-eid: {db :true ,raw_name : matched_eid ,type : "string:20"}
matched-passport: {db :true ,raw_name : matched_passport ,type : "string:20"}
matched-tradelicense: {db :true ,raw_name : matched_tradelicense ,type : "string:20"}
}
}
