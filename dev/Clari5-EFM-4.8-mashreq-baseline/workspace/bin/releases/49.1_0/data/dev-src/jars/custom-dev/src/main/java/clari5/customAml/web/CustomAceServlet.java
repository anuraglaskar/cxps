package clari5.customAml.web;

import clari5.platform.applayer.Clari5;
import clari5.platform.util.Hocon;
import clari5.utils.upload.FileUploader;
import clari5.utils.upload.IFileUpload;
import clari5.utils.upload.RequestParser;
import clari5.platform.rdbms.RDBMSSession;
import clari5.rdbms.Rdbms;
import com.sun.org.apache.xerces.internal.util.SymbolTable;
import cxps.apex.utils.CxpsLogger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONArray;
import java.sql.*;
import java.util.ArrayList;
import java.lang.Exception;
import static jdk.nashorn.internal.runtime.Context.printStackTrace;



/**
 * Created by Chandraprabha on 9/15/16.
 */

public class CustomAceServlet extends HttpServlet {

    CxpsLogger logger = CxpsLogger.getLogger(CustomAceServlet.class);

    @Override
    public void init(ServletConfig config){
        try {
            String appName = config.getServletContext().getContextPath().substring(1);
            Clari5.bootstrap(appName, appName.toLowerCase() + "-clari5.conf");
        }
        catch(Exception ex){
            ex.printStackTrace();
            logger.error("Unable to bootstrap", ex);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doEither(request, response);
    }

    public boolean doEither(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.debug("CustomAceServlet Called");
        RequestParser requestParser = new RequestParser(request);
        try{
            requestParser.parse();
        }catch(Exception e){
            return error(response, "{ \"error\":\"Error in parsing request\" }");
        }

        String userId = request.getParameter("uId");
        String channel = request.getParameter("chId");
        String menuStr = request.getParameter("menu");
        String cmd = request.getParameter("cmd");
        String payload = request.getParameter("payload");
        logger.info("REQUEST RECD:userId["+userId+"],channel["+channel+"],menu:["+menuStr+"],cmd["+cmd+"],payload["+payload+"]");

        // =================================
        // USER ID and CHANNEL Basic check
        // =================================

        if(userId == null || "".equals(userId)){
            return error(response, "{ \"error\":\"No userId Found\" }");
        }
        if(channel == null || "".equals(channel)){
            return error(response, "{ \"error\":\"No channel Found\" }");
        }
        if(menuStr == null || "".equals(menuStr)){
            return error(response, "{ \"error\":\"No menu Found\" }");
        }

        // =================
        // Token Validation
        // =================

        String token = (String)request.getAttribute("token");
        if(!"dummy".equals(token)){
            //token = request.getParameter(TOKEN.val);
            //if(token == null){
            logger.debug("error:No token found");
            //return error(response, "{ \"error\":\"No token found\" }");
            //}
        }

        if(cmd == null || "".equals(cmd)){
            return error(response, "{ \"error\":\"No command found\" }");
        }

        if(payload == null || "".equals(payload)){
            return error(response, "{ \"error\":\""+cmd+" requires input and passed null\" }");
        }
        String responseStr=null;
        try {
            switch(cmd){
                case "get_customer_details":
                    responseStr=getCustDetails(payload);
                    break;
                case "get_cust_tree":
                    responseStr=getCustTree(payload);
                    break;
                case "get_account_details":
                    responseStr=getAccountDetails(payload);
                    break;
//                case "get_Card_details":
//                    responseStr=getCardDetails(payload);
//                    break;
//                case "get_account_transaction_details" :
//                    responseStr=getTransactonDetails(payload);
//                    break;
//                case "get_jointaccount_details":
//                    responseStr=getJointAccountDetails(payload);
//                    break;
//                case "get_addressChange_details":
//                    responseStr=getAddressChangeDetails(payload);
//                    break;
                default:
                    System.out.println("Unknown command");
            }
        }
        catch(Exception ex){
            logger.error("Invalid payload:["+payload+"]");
            return error(response, "{ \"error\":\"Unable to get Data\" }");
        }

        if(responseStr == null){
            return error(response, "{ \"error\":\""+cmd+" returned error\" }");
        }
        return writeOut(response, responseStr);

    }

    /*
    *   Function to get custtree.
     */
    protected String getCustTree(String payload){
        logger.debug("in getCustTree");
        JSONObject obj = new JSONObject();
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        JSONObject jsonObject = new JSONObject(payload);
        String memberId = jsonObject.getString("custId");
        logger.debug("CustTree:custID:"+memberId);
        try{
            JSONArray aj=getAccountList(memberId);
          //  JSONArray cj=getCardList(memberId);
            obj1.put("acctIds", aj);
          //  obj1.put("cardNums",cj);
            logger.debug("getCustTreeDetails:response: "+obj1.toString());
            return obj1.toString();
        }catch (Exception e){
            printStackTrace(e);
        }
        return "";
    }



    /*
    *   Function to get accountList.
     */
    protected JSONArray getAccountList(String memberId){
        JSONArray jarray = new JSONArray();
        Connection conn = null;
        Statement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select \"acct_id\" from custom_aml_acct_view where \"cust_id\"='"+memberId+"'";
            logger.debug("query to get getAccountList : "+sql);
            ResultSet rs = stmt.executeQuery(sql);
            logger.debug("after exec : "+rs);
            while( rs.next()){
                jarray.put(rs.getString(1));
            }
            logger.debug("getAccountList: conn: "+conn+" stmt: "+stmt+"response: "+jarray);
            return jarray;
        }catch(Exception e){
            logger.debug("exception in getting Account List details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return null;
    }


    /*
    * Function to get account details.
     */
    protected String getAccountDetails(String payload){
        logger.debug("in getAccountDetails");
        JSONObject obj = null;
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        JSONObject jsonObject = new JSONObject(payload);
        String acctId = jsonObject.getString("acctId");
        Connection conn = null;
        Statement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select * from custom_aml_acct_view where \"acct_id\"  ='"+acctId+"'";
            logger.debug("query to get ActDetails : "+sql);
            ResultSet rs = stmt.executeQuery(sql);
            logger.debug("after exec : "+rs);

            while( rs.next()){
		obj = new JSONObject();
                obj.put("1", rs.getString("cust_id")!= null ? rs.getString("cust_id") : "");
                obj.put("2", rs.getString("name")!= null ? rs.getString("name") : "");
                String dd=(rs.getString("opened_date")!= null ? rs.getString("opened_date") : "");
                if(dd.isEmpty()){
                    obj.put("3", dd);
                }else{
                    obj.put("3", dd.substring(0,19));
                }

                obj.put("4", rs.getString("scheme_type")!= null ? rs.getString("scheme_type") : "");
                obj.put("5", rs.getString("acct_curr_type")!= null ? rs.getString("acct_curr_type") : "");
                obj.put("6", rs.getString("scheme_code")!= null ? rs.getString("scheme_code") : "");
                obj.put("7", rs.getString("status")!= null ? rs.getString("status") : "");
                String di=( rs.getString("acctCloseDt")!= null ? rs.getString("acctCloseDt") : "");
                if(di.isEmpty()){
                    obj.put("8",di);

                }else {
                    obj.put("8",di.substring(0,19));

                }

                obj.put("9", rs.getString("mode_of_op")!= null ? rs.getString("mode_of_op") : "");
                obj.put("10", rs.getString("acct_monthly_turnover")!= null ? rs.getString("acct_monthly_turnover") : "");
                jarray.put(obj);
            }
            obj1.put("accountDetails", jarray);
            logger.debug("getActDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting Act details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";
    }

    /*
     * Function to get customer details.
     */
    protected String getCustDetails(String payload){
        logger.debug("in getCustDetails");
        JSONObject obj = null;
        String date="";
        String date1="";
        String custsince="";
        JSONObject obj1 = new JSONObject();
        JSONArray jarray = new JSONArray();
        JSONObject jsonObject = new JSONObject(payload);
        String memberId = jsonObject.getString("custId");
        Connection conn = null;
        Statement stmt = null;
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select * from custom_aml_cust_view where \"cust_id\" ='"+memberId+"'";
            logger.debug("query to get CustDetails : "+sql);
            ResultSet rs = stmt.executeQuery(sql);
            logger.debug("after exec : "+rs);

            while( rs.next()){
                String custid=memberId.substring(4);
	        	obj = new JSONObject();
                obj.put("1", rs.getString("name")!= null ? rs.getString("name") : "");
                obj.put("2", rs.getString("status")!= null ? rs.getString("status") : "");
                String rmStatus=CustomAceServlet.getStaus(custid);
                obj.put("3", rmStatus);
                String freezStatus=CustomAceServlet.getFreezStatus(custid);
                obj.put("4",freezStatus);
                String frezdate=CustomAceServlet.getFreezDate(custid);
                if(frezdate.isEmpty()){
                    obj.put("5",frezdate);
                }else {

                     date = frezdate.substring(0, 19);
                    obj.put("5",date);
                }

                obj.put("6", rs.getString("constitution")!= null ? rs.getString("constitution") : "");
                obj.put("7", rs.getString("custCategory")!= null ? rs.getString("custCategory") : "");
                obj.put("8", rs.getString("gender")!= null ? rs.getString("gender") : "");
                String sDate1=(rs.getString("dob")!= null ? rs.getString("dob") : "");
                if(sDate1.isEmpty()){
                    obj.put("9", sDate1);

                }else{

                    date1=sDate1.substring(0,10);
                    obj.put("9", date1);
                }


                obj.put("10", rs.getString("uin")!= null ? rs.getString("uin") : "");
                obj.put("11", rs.getString("custMaritalStatus")!= null ? rs.getString("custMaritalStatus") : "");
                obj.put("12",rs.getString("VATNo")!= null ? rs.getString("custTradeNo") : "");
                obj.put("13", rs.getString("occupation")!= null ? rs.getString("occupation") : "");
                obj.put("14", rs.getString("industry_type")!= null ? rs.getString("industry_type") : "");
                obj.put("15",rs.getString("custTradeNo")!= null ? rs.getString("custTradeNo") : "");
                String custsice=(rs.getString("opened_date")!= null ? rs.getString("opened_date") : "");
                if(custsice.isEmpty()){
                    obj.put("16", custsice);
                }else{
                    custsince=custsice.substring(0,10);
                    obj.put("16", custsince);
                }


                obj.put("17", rs.getString("mobile1")!= null ? rs.getString("mobile1") : "");
                obj.put("18",rs.getString("mobile2")!= null ? rs.getString("mobile2") : "");
                obj.put("19", rs.getString("email")!= null ? rs.getString("email") : "");
                obj.put("20", rs.getString("mailing_addr")!= null ? rs.getString("mailing_addr") : "");

                jarray.put(obj);
            }
            obj1.put("customerDetails", jarray);
            logger.debug("getCustDetails: conn: "+conn+" stmt: "+stmt+"response: "+obj1.toString());
            return obj1.toString();
        }catch(Exception e){
            logger.debug("exception in getting cust details from db");
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";
    }


    protected boolean writeOut(HttpServletResponse response, String message) throws IOException {
        logger.debug("Writing out["+message+"]");
        try {
            Writer writer = response.getWriter();
            if(writer != null){
                writer.write(message);
            }
            else
                logger.error("Found null http writer");
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return false;
    }

    protected boolean error(HttpServletResponse response, String message) throws IOException {
        logger.error("Error: Processing Request ["+message+"]");
        return writeOut(response, message);
    }

public static String getFreezDate(String custid) {
    Connection conn = null;
    Statement stmt = null;
    String freezdate = "";
    try {
        conn = Rdbms.getAppConnection();
        stmt = conn.createStatement();
        String sql = "select * from FREZ_UNFREZ_DETAILS where \"customerNo\" ='" + custid + "'";
        ResultSet rs = stmt.executeQuery(sql);


        while (rs.next()) {
            freezdate = (rs.getString("frzDate") != null ? rs.getString("frzDate") : "");
        }
        return freezdate;
    } catch (Exception e) {

        e.printStackTrace();
    } finally {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException se2) {
            se2.printStackTrace();
        }
        try {
            if (conn != null)
                conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    return "";

}
    public static String getFreezStatus(String custid){
        Connection conn = null;
        Statement stmt = null;
        String freezdate="";
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select * from FREZ_UNFREZ_DETAILS where \"customerNo\" ='"+custid+"'";
            ResultSet rs = stmt.executeQuery(sql);


            while( rs.next()) {
                freezdate=(rs.getString("frzStatus")!= null ? rs.getString("frzStatus") : "");
            }
            return freezdate;
        }catch(Exception e){

            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";

    }
    public static String getStaus(String custid){
        Connection conn = null;
        Statement stmt = null;
        String freezdate="";
        try{
            conn=Rdbms.getAppConnection();
            stmt = conn.createStatement();
            String sql="select * from FREZ_UNFREZ_DETAILS where \"customerNo\" ='"+custid+"'";
            ResultSet rs = stmt.executeQuery(sql);


            while( rs.next()) {
                freezdate=(rs.getString("status")!= null ? rs.getString("status") : "");
            }
            return freezdate;
        }catch(Exception e){

            e.printStackTrace();
        }finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "";

    }
}
