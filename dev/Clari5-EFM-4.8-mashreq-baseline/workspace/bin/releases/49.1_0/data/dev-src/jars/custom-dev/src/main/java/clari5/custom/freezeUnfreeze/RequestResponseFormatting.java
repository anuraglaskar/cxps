package clari5.custom.freezeUnfreeze;

import clari5.*;

import clari5.platform.logger.CxpsLogger;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;

public class RequestResponseFormatting {
    private static CxpsLogger logger = CxpsLogger.getLogger(RequestResponseFormatting.class);
    public String requestFormatting(String srcAppId, int randomNumber, String srvcode, String orgId, String userId, String customerNumber, String flag) {
        File requestXml = null;
        String xmlRequestString = null;
        try {
            System.out.println("Inside the formating method");
//            ObjectFactory objectFactory = new ObjectFactory();
//            HeaderDetails headerDetails = objectFactory.createHeaderDetails();
//            headerDetails.setSrcAppId(srcAppId);
//            headerDetails.setOrgId(orgId);
//            headerDetails.setSrcMsgId(randomNumber);
//            headerDetails.setSrvCode(srvcode);
//            headerDetails.setUserId(userId);

//            CreateMarkFreezeRequest createMarkFreezeRequest = objectFactory.createCreateMarkFreezeRequest();
//            createMarkFreezeRequest.setCifId(customerNumber);
//            createMarkFreezeRequest.setFraudFreeze(flag);

//            Body body = objectFactory.createBody();
//            body.setCreateMarkFreezeRequest(createMarkFreezeRequest);
//
//            Request request = objectFactory.createRequest();
//            request.setHeader(headerDetails);
//            request.setBody(body);
//
//            //System.out.println(body.toString());
//
//            JAXBContext jaxbContext = JAXBContext.newInstance("clari5");
//            JAXBElement element = objectFactory.createEAIServices(request);
//
//            Marshaller marshaller = jaxbContext.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");
//            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
//            requestXml = new File("request.xml");
//            marshaller.marshal(element, requestXml);

            Reader reader = new FileReader(requestXml);
            BufferedReader bufferedReader = new BufferedReader(reader);

            StringBuilder sb  = new StringBuilder();
            String line = bufferedReader.readLine();

            while(line != null){
                sb.append(line).append("\n");
                line = bufferedReader.readLine();
            }

            xmlRequestString = sb.toString();
            logger.info(xmlRequestString);

          //  marshaller.marshal(element, System.out);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlRequestString ;
    }

//    public void callingApi(String fpi, File xmlFile, int time) {
//        String url = fpi;
//        File input = xmlFile;
//        PostMethod postMethod = new PostMethod(url);
//        RequestEntity entity = new FileRequestEntity(input, "text/xml");
//        HttpClient httpClient = new HttpClient();
//    }
//   public static void main(String[] args){
//        RequestResponseFormatting requestResponseFormatting = new RequestResponseFormatting();
//        requestResponseFormatting.requestFormatting("afa",123,"ghgag","gdhag","ahdah","0000379","Y");
//
//    }
}