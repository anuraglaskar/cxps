// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_AccounttxnEventMapper extends EventMapper<FT_AccounttxnEvent> {

public FT_AccounttxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_AccounttxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_AccounttxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_AccounttxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_AccounttxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_AccounttxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_AccounttxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getDccId());
            preparedStatement.setString(i++, obj.getAcctBrCode());
            preparedStatement.setString(i++, obj.getIs26trancodeExists());
            preparedStatement.setString(i++, obj.getCardAcceptorName());
            preparedStatement.setString(i++, obj.getBin());
            preparedStatement.setString(i++, obj.getPayeeCode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setTimestamp(i++, obj.getValueDate());
            preparedStatement.setString(i++, obj.getManulDebitFlag());
            preparedStatement.setDouble(i++, obj.getFcyTranamt());
            preparedStatement.setString(i++, obj.getSource());
            preparedStatement.setString(i++, obj.getDevOwnerId());
            preparedStatement.setString(i++, obj.getCompmis1());
            preparedStatement.setString(i++, obj.getTranSubType());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getCompmis2());
            preparedStatement.setString(i++, obj.getCoinsDenomination());
            preparedStatement.setDouble(i++, obj.getAtmLimitderive());
            preparedStatement.setString(i++, obj.getLcyCurr());
            preparedStatement.setTimestamp(i++, obj.getAcctopendate());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getDesignatedFlg());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getSwiftUaeftsRefNum());
            preparedStatement.setDouble(i++, obj.getAtmLimit());
            preparedStatement.setString(i++, obj.getDevType());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getPlaceHolder());
            preparedStatement.setString(i++, obj.getCustIdderive());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getTranRmks());
            preparedStatement.setString(i++, obj.getTerminalId());
            preparedStatement.setDouble(i++, obj.getEcomLimit());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setTimestamp(i++, obj.getPstdDate());
            preparedStatement.setString(i++, obj.getItmWithSelfAcct());
            preparedStatement.setString(i++, obj.getSuccFailFlagDerivd());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getSchmType());
            preparedStatement.setString(i++, obj.getAcctOwnership());
            preparedStatement.setString(i++, obj.getTxnBrCode());
            preparedStatement.setString(i++, obj.getTransactionRefNo());
            preparedStatement.setString(i++, obj.getTxnBrCity());
            preparedStatement.setString(i++, obj.getAcctOccpCode());
            preparedStatement.setDouble(i++, obj.getUnClrBalAmt());
            preparedStatement.setString(i++, obj.getStatus());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setString(i++, obj.getCity());
            preparedStatement.setTimestamp(i++, obj.getCustInducedDate());
            preparedStatement.setDouble(i++, obj.getConvRate());
            preparedStatement.setString(i++, obj.getSwiftUaeftsMsgType());
            preparedStatement.setDouble(i++, obj.getClrBalAmt());
            preparedStatement.setString(i++, obj.getResponseCode());
            preparedStatement.setString(i++, obj.getHomeBrCity());
            preparedStatement.setDouble(i++, obj.getAvlBalance());
            preparedStatement.setDouble(i++, obj.getEffAvlBal());
            preparedStatement.setString(i++, obj.getTranCategory());
            preparedStatement.setString(i++, obj.getEntryUser());
            preparedStatement.setString(i++, obj.getRefNum());
            preparedStatement.setString(i++, obj.getAcctSolId());
            preparedStatement.setString(i++, obj.getIdNumber());
            preparedStatement.setString(i++, obj.getSchmCode());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setDouble(i++, obj.getMidRate());
            preparedStatement.setString(i++, obj.getFcyCurr());
            preparedStatement.setString(i++, obj.getCardserno());
            preparedStatement.setString(i++, obj.getTranParticular());
            preparedStatement.setDouble(i++, obj.getCardlessCashLimit());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setDouble(i++, obj.getPosLimit());
            preparedStatement.setTimestamp(i++, obj.getInstrumtDate());
            preparedStatement.setString(i++, obj.getIs27trancodeExists());
            preparedStatement.setString(i++, obj.getCustConst());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setString(i++, obj.getHdrmkrs());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getWalletType());
            preparedStatement.setString(i++, obj.getMerchantId());
            preparedStatement.setString(i++, obj.getSuccFailFlag());
            preparedStatement.setTimestamp(i++, obj.getInsTime());
            preparedStatement.setString(i++, obj.getRefCurr());
            preparedStatement.setString(i++, obj.getPartTranType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_ACCOUNTTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_AccounttxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_AccounttxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_ACCOUNTTXN"));
        putList = new ArrayList<>();

        for (FT_AccounttxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "DCC_ID",  obj.getDccId());
            p = this.insert(p, "ACCT_BR_CODE",  obj.getAcctBrCode());
            p = this.insert(p, "IS26TRANCODE_EXISTS",  obj.getIs26trancodeExists());
            p = this.insert(p, "CARD_ACCEPTOR_NAME",  obj.getCardAcceptorName());
            p = this.insert(p, "BIN",  obj.getBin());
            p = this.insert(p, "PAYEE_CODE",  obj.getPayeeCode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "VALUE_DATE", String.valueOf(obj.getValueDate()));
            p = this.insert(p, "MANUL_DEBIT_FLAG",  obj.getManulDebitFlag());
            p = this.insert(p, "FCY_TRANAMT", String.valueOf(obj.getFcyTranamt()));
            p = this.insert(p, "SOURCE",  obj.getSource());
            p = this.insert(p, "DEV_OWNER_ID",  obj.getDevOwnerId());
            p = this.insert(p, "COMPMIS1",  obj.getCompmis1());
            p = this.insert(p, "TRAN_SUB_TYPE",  obj.getTranSubType());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "COMPMIS2",  obj.getCompmis2());
            p = this.insert(p, "COINS_DENOMINATION",  obj.getCoinsDenomination());
            p = this.insert(p, "ATM_LIMITDERIVE", String.valueOf(obj.getAtmLimitderive()));
            p = this.insert(p, "LCY_CURR",  obj.getLcyCurr());
            p = this.insert(p, "ACCTOPENDATE", String.valueOf(obj.getAcctopendate()));
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "DESIGNATED_FLG",  obj.getDesignatedFlg());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "SWIFT_UAEFTS_REF_NUM",  obj.getSwiftUaeftsRefNum());
            p = this.insert(p, "ATM_LIMIT", String.valueOf(obj.getAtmLimit()));
            p = this.insert(p, "DEV_TYPE",  obj.getDevType());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "PLACE_HOLDER",  obj.getPlaceHolder());
            p = this.insert(p, "CUST_IDDERIVE",  obj.getCustIdderive());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_RMKS",  obj.getTranRmks());
            p = this.insert(p, "TERMINAL_ID",  obj.getTerminalId());
            p = this.insert(p, "ECOM_LIMIT", String.valueOf(obj.getEcomLimit()));
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "PSTD_DATE", String.valueOf(obj.getPstdDate()));
            p = this.insert(p, "ITM_WITH_SELF_ACCT",  obj.getItmWithSelfAcct());
            p = this.insert(p, "SUCC_FAIL_FLAG_DERIVD",  obj.getSuccFailFlagDerivd());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "SCHM_TYPE",  obj.getSchmType());
            p = this.insert(p, "ACCT_OWNERSHIP",  obj.getAcctOwnership());
            p = this.insert(p, "TXN_BR_CODE",  obj.getTxnBrCode());
            p = this.insert(p, "TRANSACTION_REF_NO",  obj.getTransactionRefNo());
            p = this.insert(p, "TXN_BR_CITY",  obj.getTxnBrCity());
            p = this.insert(p, "ACCT_OCCP_CODE",  obj.getAcctOccpCode());
            p = this.insert(p, "UN_CLR_BAL_AMT", String.valueOf(obj.getUnClrBalAmt()));
            p = this.insert(p, "STATUS",  obj.getStatus());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "CITY",  obj.getCity());
            p = this.insert(p, "CUST_INDUCED_DATE", String.valueOf(obj.getCustInducedDate()));
            p = this.insert(p, "CONV_RATE", String.valueOf(obj.getConvRate()));
            p = this.insert(p, "SWIFT_UAEFTS_MSG_TYPE",  obj.getSwiftUaeftsMsgType());
            p = this.insert(p, "CLR_BAL_AMT", String.valueOf(obj.getClrBalAmt()));
            p = this.insert(p, "RESPONSE_CODE",  obj.getResponseCode());
            p = this.insert(p, "HOME_BR_CITY",  obj.getHomeBrCity());
            p = this.insert(p, "AVL_BALANCE", String.valueOf(obj.getAvlBalance()));
            p = this.insert(p, "EFF_AVL_BAL", String.valueOf(obj.getEffAvlBal()));
            p = this.insert(p, "TRAN_CATEGORY",  obj.getTranCategory());
            p = this.insert(p, "ENTRY_USER",  obj.getEntryUser());
            p = this.insert(p, "REF_NUM",  obj.getRefNum());
            p = this.insert(p, "ACCT_SOL_ID",  obj.getAcctSolId());
            p = this.insert(p, "ID_NUMBER",  obj.getIdNumber());
            p = this.insert(p, "SCHM_CODE",  obj.getSchmCode());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "MID_RATE", String.valueOf(obj.getMidRate()));
            p = this.insert(p, "FCY_CURR",  obj.getFcyCurr());
            p = this.insert(p, "CARDSERNO",  obj.getCardserno());
            p = this.insert(p, "TRAN_PARTICULAR",  obj.getTranParticular());
            p = this.insert(p, "CARDLESS_CASH_LIMIT", String.valueOf(obj.getCardlessCashLimit()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "POS_LIMIT", String.valueOf(obj.getPosLimit()));
            p = this.insert(p, "INSTRUMT_DATE", String.valueOf(obj.getInstrumtDate()));
            p = this.insert(p, "IS27TRANCODE_EXISTS",  obj.getIs27trancodeExists());
            p = this.insert(p, "CUST_CONST",  obj.getCustConst());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "HDRMKRS",  obj.getHdrmkrs());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "WALLET_TYPE",  obj.getWalletType());
            p = this.insert(p, "MERCHANT_ID",  obj.getMerchantId());
            p = this.insert(p, "SUCC_FAIL_FLAG",  obj.getSuccFailFlag());
            p = this.insert(p, "INS_TIME", String.valueOf(obj.getInsTime()));
            p = this.insert(p, "REF_CURR",  obj.getRefCurr());
            p = this.insert(p, "PART_TRAN_TYPE",  obj.getPartTranType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_ACCOUNTTXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_ACCOUNTTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_ACCOUNTTXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_AccounttxnEvent obj = new FT_AccounttxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setDccId(rs.getString("DCC_ID"));
    obj.setAcctBrCode(rs.getString("ACCT_BR_CODE"));
    obj.setIs26trancodeExists(rs.getString("IS26TRANCODE_EXISTS"));
    obj.setCardAcceptorName(rs.getString("CARD_ACCEPTOR_NAME"));
    obj.setBin(rs.getString("BIN"));
    obj.setPayeeCode(rs.getString("PAYEE_CODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setValueDate(rs.getTimestamp("VALUE_DATE"));
    obj.setManulDebitFlag(rs.getString("MANUL_DEBIT_FLAG"));
    obj.setFcyTranamt(rs.getDouble("FCY_TRANAMT"));
    obj.setSource(rs.getString("SOURCE"));
    obj.setDevOwnerId(rs.getString("DEV_OWNER_ID"));
    obj.setCompmis1(rs.getString("COMPMIS1"));
    obj.setTranSubType(rs.getString("TRAN_SUB_TYPE"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setCompmis2(rs.getString("COMPMIS2"));
    obj.setCoinsDenomination(rs.getString("COINS_DENOMINATION"));
    obj.setAtmLimitderive(rs.getDouble("ATM_LIMITDERIVE"));
    obj.setLcyCurr(rs.getString("LCY_CURR"));
    obj.setAcctopendate(rs.getTimestamp("ACCTOPENDATE"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setDesignatedFlg(rs.getString("DESIGNATED_FLG"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setSwiftUaeftsRefNum(rs.getString("SWIFT_UAEFTS_REF_NUM"));
    obj.setAtmLimit(rs.getDouble("ATM_LIMIT"));
    obj.setDevType(rs.getString("DEV_TYPE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setPlaceHolder(rs.getString("PLACE_HOLDER"));
    obj.setCustIdderive(rs.getString("CUST_IDDERIVE"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranRmks(rs.getString("TRAN_RMKS"));
    obj.setTerminalId(rs.getString("TERMINAL_ID"));
    obj.setEcomLimit(rs.getDouble("ECOM_LIMIT"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setPstdDate(rs.getTimestamp("PSTD_DATE"));
    obj.setItmWithSelfAcct(rs.getString("ITM_WITH_SELF_ACCT"));
    obj.setSuccFailFlagDerivd(rs.getString("SUCC_FAIL_FLAG_DERIVD"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setSchmType(rs.getString("SCHM_TYPE"));
    obj.setAcctOwnership(rs.getString("ACCT_OWNERSHIP"));
    obj.setTxnBrCode(rs.getString("TXN_BR_CODE"));
    obj.setTransactionRefNo(rs.getString("TRANSACTION_REF_NO"));
    obj.setTxnBrCity(rs.getString("TXN_BR_CITY"));
    obj.setAcctOccpCode(rs.getString("ACCT_OCCP_CODE"));
    obj.setUnClrBalAmt(rs.getDouble("UN_CLR_BAL_AMT"));
    obj.setStatus(rs.getString("STATUS"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setCity(rs.getString("CITY"));
    obj.setCustInducedDate(rs.getTimestamp("CUST_INDUCED_DATE"));
    obj.setConvRate(rs.getDouble("CONV_RATE"));
    obj.setSwiftUaeftsMsgType(rs.getString("SWIFT_UAEFTS_MSG_TYPE"));
    obj.setClrBalAmt(rs.getDouble("CLR_BAL_AMT"));
    obj.setResponseCode(rs.getString("RESPONSE_CODE"));
    obj.setHomeBrCity(rs.getString("HOME_BR_CITY"));
    obj.setAvlBalance(rs.getDouble("AVL_BALANCE"));
    obj.setEffAvlBal(rs.getDouble("EFF_AVL_BAL"));
    obj.setTranCategory(rs.getString("TRAN_CATEGORY"));
    obj.setEntryUser(rs.getString("ENTRY_USER"));
    obj.setRefNum(rs.getString("REF_NUM"));
    obj.setAcctSolId(rs.getString("ACCT_SOL_ID"));
    obj.setIdNumber(rs.getString("ID_NUMBER"));
    obj.setSchmCode(rs.getString("SCHM_CODE"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setMidRate(rs.getDouble("MID_RATE"));
    obj.setFcyCurr(rs.getString("FCY_CURR"));
    obj.setCardserno(rs.getString("CARDSERNO"));
    obj.setTranParticular(rs.getString("TRAN_PARTICULAR"));
    obj.setCardlessCashLimit(rs.getDouble("CARDLESS_CASH_LIMIT"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setPosLimit(rs.getDouble("POS_LIMIT"));
    obj.setInstrumtDate(rs.getTimestamp("INSTRUMT_DATE"));
    obj.setIs27trancodeExists(rs.getString("IS27TRANCODE_EXISTS"));
    obj.setCustConst(rs.getString("CUST_CONST"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setHdrmkrs(rs.getString("HDRMKRS"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setWalletType(rs.getString("WALLET_TYPE"));
    obj.setMerchantId(rs.getString("MERCHANT_ID"));
    obj.setSuccFailFlag(rs.getString("SUCC_FAIL_FLAG"));
    obj.setInsTime(rs.getTimestamp("INS_TIME"));
    obj.setRefCurr(rs.getString("REF_CURR"));
    obj.setPartTranType(rs.getString("PART_TRAN_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_ACCOUNTTXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_AccounttxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_AccounttxnEvent> events;
 FT_AccounttxnEvent obj = new FT_AccounttxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_ACCOUNTTXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_AccounttxnEvent obj = new FT_AccounttxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setDccId(getColumnValue(rs, "DCC_ID"));
            obj.setAcctBrCode(getColumnValue(rs, "ACCT_BR_CODE"));
            obj.setIs26trancodeExists(getColumnValue(rs, "IS26TRANCODE_EXISTS"));
            obj.setCardAcceptorName(getColumnValue(rs, "CARD_ACCEPTOR_NAME"));
            obj.setBin(getColumnValue(rs, "BIN"));
            obj.setPayeeCode(getColumnValue(rs, "PAYEE_CODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setValueDate(EventHelper.toTimestamp(getColumnValue(rs, "VALUE_DATE")));
            obj.setManulDebitFlag(getColumnValue(rs, "MANUL_DEBIT_FLAG"));
            obj.setFcyTranamt(EventHelper.toDouble(getColumnValue(rs, "FCY_TRANAMT")));
            obj.setSource(getColumnValue(rs, "SOURCE"));
            obj.setDevOwnerId(getColumnValue(rs, "DEV_OWNER_ID"));
            obj.setCompmis1(getColumnValue(rs, "COMPMIS1"));
            obj.setTranSubType(getColumnValue(rs, "TRAN_SUB_TYPE"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setCompmis2(getColumnValue(rs, "COMPMIS2"));
            obj.setCoinsDenomination(getColumnValue(rs, "COINS_DENOMINATION"));
            obj.setAtmLimitderive(EventHelper.toDouble(getColumnValue(rs, "ATM_LIMITDERIVE")));
            obj.setLcyCurr(getColumnValue(rs, "LCY_CURR"));
            obj.setAcctopendate(EventHelper.toTimestamp(getColumnValue(rs, "ACCTOPENDATE")));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setDesignatedFlg(getColumnValue(rs, "DESIGNATED_FLG"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setSwiftUaeftsRefNum(getColumnValue(rs, "SWIFT_UAEFTS_REF_NUM"));
            obj.setAtmLimit(EventHelper.toDouble(getColumnValue(rs, "ATM_LIMIT")));
            obj.setDevType(getColumnValue(rs, "DEV_TYPE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setPlaceHolder(getColumnValue(rs, "PLACE_HOLDER"));
            obj.setCustIdderive(getColumnValue(rs, "CUST_IDDERIVE"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranRmks(getColumnValue(rs, "TRAN_RMKS"));
            obj.setTerminalId(getColumnValue(rs, "TERMINAL_ID"));
            obj.setEcomLimit(EventHelper.toDouble(getColumnValue(rs, "ECOM_LIMIT")));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setPstdDate(EventHelper.toTimestamp(getColumnValue(rs, "PSTD_DATE")));
            obj.setItmWithSelfAcct(getColumnValue(rs, "ITM_WITH_SELF_ACCT"));
            obj.setSuccFailFlagDerivd(getColumnValue(rs, "SUCC_FAIL_FLAG_DERIVD"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setSchmType(getColumnValue(rs, "SCHM_TYPE"));
            obj.setAcctOwnership(getColumnValue(rs, "ACCT_OWNERSHIP"));
            obj.setTxnBrCode(getColumnValue(rs, "TXN_BR_CODE"));
            obj.setTransactionRefNo(getColumnValue(rs, "TRANSACTION_REF_NO"));
            obj.setTxnBrCity(getColumnValue(rs, "TXN_BR_CITY"));
            obj.setAcctOccpCode(getColumnValue(rs, "ACCT_OCCP_CODE"));
            obj.setUnClrBalAmt(EventHelper.toDouble(getColumnValue(rs, "UN_CLR_BAL_AMT")));
            obj.setStatus(getColumnValue(rs, "STATUS"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setCity(getColumnValue(rs, "CITY"));
            obj.setCustInducedDate(EventHelper.toTimestamp(getColumnValue(rs, "CUST_INDUCED_DATE")));
            obj.setConvRate(EventHelper.toDouble(getColumnValue(rs, "CONV_RATE")));
            obj.setSwiftUaeftsMsgType(getColumnValue(rs, "SWIFT_UAEFTS_MSG_TYPE"));
            obj.setClrBalAmt(EventHelper.toDouble(getColumnValue(rs, "CLR_BAL_AMT")));
            obj.setResponseCode(getColumnValue(rs, "RESPONSE_CODE"));
            obj.setHomeBrCity(getColumnValue(rs, "HOME_BR_CITY"));
            obj.setAvlBalance(EventHelper.toDouble(getColumnValue(rs, "AVL_BALANCE")));
            obj.setEffAvlBal(EventHelper.toDouble(getColumnValue(rs, "EFF_AVL_BAL")));
            obj.setTranCategory(getColumnValue(rs, "TRAN_CATEGORY"));
            obj.setEntryUser(getColumnValue(rs, "ENTRY_USER"));
            obj.setRefNum(getColumnValue(rs, "REF_NUM"));
            obj.setAcctSolId(getColumnValue(rs, "ACCT_SOL_ID"));
            obj.setIdNumber(getColumnValue(rs, "ID_NUMBER"));
            obj.setSchmCode(getColumnValue(rs, "SCHM_CODE"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setMidRate(EventHelper.toDouble(getColumnValue(rs, "MID_RATE")));
            obj.setFcyCurr(getColumnValue(rs, "FCY_CURR"));
            obj.setCardserno(getColumnValue(rs, "CARDSERNO"));
            obj.setTranParticular(getColumnValue(rs, "TRAN_PARTICULAR"));
            obj.setCardlessCashLimit(EventHelper.toDouble(getColumnValue(rs, "CARDLESS_CASH_LIMIT")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setPosLimit(EventHelper.toDouble(getColumnValue(rs, "POS_LIMIT")));
            obj.setInstrumtDate(EventHelper.toTimestamp(getColumnValue(rs, "INSTRUMT_DATE")));
            obj.setIs27trancodeExists(getColumnValue(rs, "IS27TRANCODE_EXISTS"));
            obj.setCustConst(getColumnValue(rs, "CUST_CONST"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setHdrmkrs(getColumnValue(rs, "HDRMKRS"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setWalletType(getColumnValue(rs, "WALLET_TYPE"));
            obj.setMerchantId(getColumnValue(rs, "MERCHANT_ID"));
            obj.setSuccFailFlag(getColumnValue(rs, "SUCC_FAIL_FLAG"));
            obj.setInsTime(EventHelper.toTimestamp(getColumnValue(rs, "INS_TIME")));
            obj.setRefCurr(getColumnValue(rs, "REF_CURR"));
            obj.setPartTranType(getColumnValue(rs, "PART_TRAN_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_ACCOUNTTXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_ACCOUNTTXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"DCC_ID\",\"ACCT_BR_CODE\",\"IS26TRANCODE_EXISTS\",\"CARD_ACCEPTOR_NAME\",\"BIN\",\"PAYEE_CODE\",\"CHANNEL\",\"VALUE_DATE\",\"MANUL_DEBIT_FLAG\",\"FCY_TRANAMT\",\"SOURCE\",\"DEV_OWNER_ID\",\"COMPMIS1\",\"TRAN_SUB_TYPE\",\"DEVICE_ID\",\"COMPMIS2\",\"COINS_DENOMINATION\",\"ATM_LIMITDERIVE\",\"LCY_CURR\",\"ACCTOPENDATE\",\"TRAN_ID\",\"HOST_ID\",\"DESIGNATED_FLG\",\"TRAN_AMT\",\"SWIFT_UAEFTS_REF_NUM\",\"ATM_LIMIT\",\"DEV_TYPE\",\"ACCOUNT_ID\",\"PLACE_HOLDER\",\"CUST_IDDERIVE\",\"SYS_TIME\",\"BRANCH_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TERMINAL_ID\",\"ECOM_LIMIT\",\"ACCT_NAME\",\"CUST_CARD_ID\",\"PSTD_DATE\",\"ITM_WITH_SELF_ACCT\",\"SUCC_FAIL_FLAG_DERIVD\",\"AVL_BAL\",\"PAYEE_ID\",\"SCHM_TYPE\",\"ACCT_OWNERSHIP\",\"TXN_BR_CODE\",\"TRANSACTION_REF_NO\",\"TXN_BR_CITY\",\"ACCT_OCCP_CODE\",\"UN_CLR_BAL_AMT\",\"STATUS\",\"COUNTRY_CODE\",\"CITY\",\"CUST_INDUCED_DATE\",\"CONV_RATE\",\"SWIFT_UAEFTS_MSG_TYPE\",\"CLR_BAL_AMT\",\"RESPONSE_CODE\",\"HOME_BR_CITY\",\"AVL_BALANCE\",\"EFF_AVL_BAL\",\"TRAN_CATEGORY\",\"ENTRY_USER\",\"REF_NUM\",\"ACCT_SOL_ID\",\"ID_NUMBER\",\"SCHM_CODE\",\"USER_ID\",\"MID_RATE\",\"FCY_CURR\",\"CARDSERNO\",\"TRAN_PARTICULAR\",\"CARDLESS_CASH_LIMIT\",\"CUST_ID\",\"POS_LIMIT\",\"INSTRUMT_DATE\",\"IS27TRANCODE_EXISTS\",\"CUST_CONST\",\"TRAN_DATE\",\"IP_ADDRESS\",\"HDRMKRS\",\"TRAN_TYPE\",\"WALLET_TYPE\",\"MERCHANT_ID\",\"SUCC_FAIL_FLAG\",\"INS_TIME\",\"REF_CURR\",\"PART_TRAN_TYPE\"" +
              " FROM EVENT_FT_ACCOUNTTXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [DCC_ID],[ACCT_BR_CODE],[IS26TRANCODE_EXISTS],[CARD_ACCEPTOR_NAME],[BIN],[PAYEE_CODE],[CHANNEL],[VALUE_DATE],[MANUL_DEBIT_FLAG],[FCY_TRANAMT],[SOURCE],[DEV_OWNER_ID],[COMPMIS1],[TRAN_SUB_TYPE],[DEVICE_ID],[COMPMIS2],[COINS_DENOMINATION],[ATM_LIMITDERIVE],[LCY_CURR],[ACCTOPENDATE],[TRAN_ID],[HOST_ID],[DESIGNATED_FLG],[TRAN_AMT],[SWIFT_UAEFTS_REF_NUM],[ATM_LIMIT],[DEV_TYPE],[ACCOUNT_ID],[PLACE_HOLDER],[CUST_IDDERIVE],[SYS_TIME],[BRANCH_ID],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TERMINAL_ID],[ECOM_LIMIT],[ACCT_NAME],[CUST_CARD_ID],[PSTD_DATE],[ITM_WITH_SELF_ACCT],[SUCC_FAIL_FLAG_DERIVD],[AVL_BAL],[PAYEE_ID],[SCHM_TYPE],[ACCT_OWNERSHIP],[TXN_BR_CODE],[TRANSACTION_REF_NO],[TXN_BR_CITY],[ACCT_OCCP_CODE],[UN_CLR_BAL_AMT],[STATUS],[COUNTRY_CODE],[CITY],[CUST_INDUCED_DATE],[CONV_RATE],[SWIFT_UAEFTS_MSG_TYPE],[CLR_BAL_AMT],[RESPONSE_CODE],[HOME_BR_CITY],[AVL_BALANCE],[EFF_AVL_BAL],[TRAN_CATEGORY],[ENTRY_USER],[REF_NUM],[ACCT_SOL_ID],[ID_NUMBER],[SCHM_CODE],[USER_ID],[MID_RATE],[FCY_CURR],[CARDSERNO],[TRAN_PARTICULAR],[CARDLESS_CASH_LIMIT],[CUST_ID],[POS_LIMIT],[INSTRUMT_DATE],[IS27TRANCODE_EXISTS],[CUST_CONST],[TRAN_DATE],[IP_ADDRESS],[HDRMKRS],[TRAN_TYPE],[WALLET_TYPE],[MERCHANT_ID],[SUCC_FAIL_FLAG],[INS_TIME],[REF_CURR],[PART_TRAN_TYPE]" +
              " FROM EVENT_FT_ACCOUNTTXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`DCC_ID`,`ACCT_BR_CODE`,`IS26TRANCODE_EXISTS`,`CARD_ACCEPTOR_NAME`,`BIN`,`PAYEE_CODE`,`CHANNEL`,`VALUE_DATE`,`MANUL_DEBIT_FLAG`,`FCY_TRANAMT`,`SOURCE`,`DEV_OWNER_ID`,`COMPMIS1`,`TRAN_SUB_TYPE`,`DEVICE_ID`,`COMPMIS2`,`COINS_DENOMINATION`,`ATM_LIMITDERIVE`,`LCY_CURR`,`ACCTOPENDATE`,`TRAN_ID`,`HOST_ID`,`DESIGNATED_FLG`,`TRAN_AMT`,`SWIFT_UAEFTS_REF_NUM`,`ATM_LIMIT`,`DEV_TYPE`,`ACCOUNT_ID`,`PLACE_HOLDER`,`CUST_IDDERIVE`,`SYS_TIME`,`BRANCH_ID`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TERMINAL_ID`,`ECOM_LIMIT`,`ACCT_NAME`,`CUST_CARD_ID`,`PSTD_DATE`,`ITM_WITH_SELF_ACCT`,`SUCC_FAIL_FLAG_DERIVD`,`AVL_BAL`,`PAYEE_ID`,`SCHM_TYPE`,`ACCT_OWNERSHIP`,`TXN_BR_CODE`,`TRANSACTION_REF_NO`,`TXN_BR_CITY`,`ACCT_OCCP_CODE`,`UN_CLR_BAL_AMT`,`STATUS`,`COUNTRY_CODE`,`CITY`,`CUST_INDUCED_DATE`,`CONV_RATE`,`SWIFT_UAEFTS_MSG_TYPE`,`CLR_BAL_AMT`,`RESPONSE_CODE`,`HOME_BR_CITY`,`AVL_BALANCE`,`EFF_AVL_BAL`,`TRAN_CATEGORY`,`ENTRY_USER`,`REF_NUM`,`ACCT_SOL_ID`,`ID_NUMBER`,`SCHM_CODE`,`USER_ID`,`MID_RATE`,`FCY_CURR`,`CARDSERNO`,`TRAN_PARTICULAR`,`CARDLESS_CASH_LIMIT`,`CUST_ID`,`POS_LIMIT`,`INSTRUMT_DATE`,`IS27TRANCODE_EXISTS`,`CUST_CONST`,`TRAN_DATE`,`IP_ADDRESS`,`HDRMKRS`,`TRAN_TYPE`,`WALLET_TYPE`,`MERCHANT_ID`,`SUCC_FAIL_FLAG`,`INS_TIME`,`REF_CURR`,`PART_TRAN_TYPE`" +
              " FROM EVENT_FT_ACCOUNTTXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_ACCOUNTTXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"DCC_ID\",\"ACCT_BR_CODE\",\"IS26TRANCODE_EXISTS\",\"CARD_ACCEPTOR_NAME\",\"BIN\",\"PAYEE_CODE\",\"CHANNEL\",\"VALUE_DATE\",\"MANUL_DEBIT_FLAG\",\"FCY_TRANAMT\",\"SOURCE\",\"DEV_OWNER_ID\",\"COMPMIS1\",\"TRAN_SUB_TYPE\",\"DEVICE_ID\",\"COMPMIS2\",\"COINS_DENOMINATION\",\"ATM_LIMITDERIVE\",\"LCY_CURR\",\"ACCTOPENDATE\",\"TRAN_ID\",\"HOST_ID\",\"DESIGNATED_FLG\",\"TRAN_AMT\",\"SWIFT_UAEFTS_REF_NUM\",\"ATM_LIMIT\",\"DEV_TYPE\",\"ACCOUNT_ID\",\"PLACE_HOLDER\",\"CUST_IDDERIVE\",\"SYS_TIME\",\"BRANCH_ID\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TERMINAL_ID\",\"ECOM_LIMIT\",\"ACCT_NAME\",\"CUST_CARD_ID\",\"PSTD_DATE\",\"ITM_WITH_SELF_ACCT\",\"SUCC_FAIL_FLAG_DERIVD\",\"AVL_BAL\",\"PAYEE_ID\",\"SCHM_TYPE\",\"ACCT_OWNERSHIP\",\"TXN_BR_CODE\",\"TRANSACTION_REF_NO\",\"TXN_BR_CITY\",\"ACCT_OCCP_CODE\",\"UN_CLR_BAL_AMT\",\"STATUS\",\"COUNTRY_CODE\",\"CITY\",\"CUST_INDUCED_DATE\",\"CONV_RATE\",\"SWIFT_UAEFTS_MSG_TYPE\",\"CLR_BAL_AMT\",\"RESPONSE_CODE\",\"HOME_BR_CITY\",\"AVL_BALANCE\",\"EFF_AVL_BAL\",\"TRAN_CATEGORY\",\"ENTRY_USER\",\"REF_NUM\",\"ACCT_SOL_ID\",\"ID_NUMBER\",\"SCHM_CODE\",\"USER_ID\",\"MID_RATE\",\"FCY_CURR\",\"CARDSERNO\",\"TRAN_PARTICULAR\",\"CARDLESS_CASH_LIMIT\",\"CUST_ID\",\"POS_LIMIT\",\"INSTRUMT_DATE\",\"IS27TRANCODE_EXISTS\",\"CUST_CONST\",\"TRAN_DATE\",\"IP_ADDRESS\",\"HDRMKRS\",\"TRAN_TYPE\",\"WALLET_TYPE\",\"MERCHANT_ID\",\"SUCC_FAIL_FLAG\",\"INS_TIME\",\"REF_CURR\",\"PART_TRAN_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[DCC_ID],[ACCT_BR_CODE],[IS26TRANCODE_EXISTS],[CARD_ACCEPTOR_NAME],[BIN],[PAYEE_CODE],[CHANNEL],[VALUE_DATE],[MANUL_DEBIT_FLAG],[FCY_TRANAMT],[SOURCE],[DEV_OWNER_ID],[COMPMIS1],[TRAN_SUB_TYPE],[DEVICE_ID],[COMPMIS2],[COINS_DENOMINATION],[ATM_LIMITDERIVE],[LCY_CURR],[ACCTOPENDATE],[TRAN_ID],[HOST_ID],[DESIGNATED_FLG],[TRAN_AMT],[SWIFT_UAEFTS_REF_NUM],[ATM_LIMIT],[DEV_TYPE],[ACCOUNT_ID],[PLACE_HOLDER],[CUST_IDDERIVE],[SYS_TIME],[BRANCH_ID],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TERMINAL_ID],[ECOM_LIMIT],[ACCT_NAME],[CUST_CARD_ID],[PSTD_DATE],[ITM_WITH_SELF_ACCT],[SUCC_FAIL_FLAG_DERIVD],[AVL_BAL],[PAYEE_ID],[SCHM_TYPE],[ACCT_OWNERSHIP],[TXN_BR_CODE],[TRANSACTION_REF_NO],[TXN_BR_CITY],[ACCT_OCCP_CODE],[UN_CLR_BAL_AMT],[STATUS],[COUNTRY_CODE],[CITY],[CUST_INDUCED_DATE],[CONV_RATE],[SWIFT_UAEFTS_MSG_TYPE],[CLR_BAL_AMT],[RESPONSE_CODE],[HOME_BR_CITY],[AVL_BALANCE],[EFF_AVL_BAL],[TRAN_CATEGORY],[ENTRY_USER],[REF_NUM],[ACCT_SOL_ID],[ID_NUMBER],[SCHM_CODE],[USER_ID],[MID_RATE],[FCY_CURR],[CARDSERNO],[TRAN_PARTICULAR],[CARDLESS_CASH_LIMIT],[CUST_ID],[POS_LIMIT],[INSTRUMT_DATE],[IS27TRANCODE_EXISTS],[CUST_CONST],[TRAN_DATE],[IP_ADDRESS],[HDRMKRS],[TRAN_TYPE],[WALLET_TYPE],[MERCHANT_ID],[SUCC_FAIL_FLAG],[INS_TIME],[REF_CURR],[PART_TRAN_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`DCC_ID`,`ACCT_BR_CODE`,`IS26TRANCODE_EXISTS`,`CARD_ACCEPTOR_NAME`,`BIN`,`PAYEE_CODE`,`CHANNEL`,`VALUE_DATE`,`MANUL_DEBIT_FLAG`,`FCY_TRANAMT`,`SOURCE`,`DEV_OWNER_ID`,`COMPMIS1`,`TRAN_SUB_TYPE`,`DEVICE_ID`,`COMPMIS2`,`COINS_DENOMINATION`,`ATM_LIMITDERIVE`,`LCY_CURR`,`ACCTOPENDATE`,`TRAN_ID`,`HOST_ID`,`DESIGNATED_FLG`,`TRAN_AMT`,`SWIFT_UAEFTS_REF_NUM`,`ATM_LIMIT`,`DEV_TYPE`,`ACCOUNT_ID`,`PLACE_HOLDER`,`CUST_IDDERIVE`,`SYS_TIME`,`BRANCH_ID`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TERMINAL_ID`,`ECOM_LIMIT`,`ACCT_NAME`,`CUST_CARD_ID`,`PSTD_DATE`,`ITM_WITH_SELF_ACCT`,`SUCC_FAIL_FLAG_DERIVD`,`AVL_BAL`,`PAYEE_ID`,`SCHM_TYPE`,`ACCT_OWNERSHIP`,`TXN_BR_CODE`,`TRANSACTION_REF_NO`,`TXN_BR_CITY`,`ACCT_OCCP_CODE`,`UN_CLR_BAL_AMT`,`STATUS`,`COUNTRY_CODE`,`CITY`,`CUST_INDUCED_DATE`,`CONV_RATE`,`SWIFT_UAEFTS_MSG_TYPE`,`CLR_BAL_AMT`,`RESPONSE_CODE`,`HOME_BR_CITY`,`AVL_BALANCE`,`EFF_AVL_BAL`,`TRAN_CATEGORY`,`ENTRY_USER`,`REF_NUM`,`ACCT_SOL_ID`,`ID_NUMBER`,`SCHM_CODE`,`USER_ID`,`MID_RATE`,`FCY_CURR`,`CARDSERNO`,`TRAN_PARTICULAR`,`CARDLESS_CASH_LIMIT`,`CUST_ID`,`POS_LIMIT`,`INSTRUMT_DATE`,`IS27TRANCODE_EXISTS`,`CUST_CONST`,`TRAN_DATE`,`IP_ADDRESS`,`HDRMKRS`,`TRAN_TYPE`,`WALLET_TYPE`,`MERCHANT_ID`,`SUCC_FAIL_FLAG`,`INS_TIME`,`REF_CURR`,`PART_TRAN_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

