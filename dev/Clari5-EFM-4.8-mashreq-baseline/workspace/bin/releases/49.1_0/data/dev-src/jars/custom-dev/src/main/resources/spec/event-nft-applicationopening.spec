cxps.events.event.nft-applicationopening{  
  table-name : EVENT_NFT_APPLICATIONOPENING
  event-mnemonic: APP
  workspaces : {
    CUSTOMER: "new_cif"   
   }
event-attributes : {

new-app_refno: {db:true ,raw_name : new_app_refno ,type:"string:20"}
new-product: {db:true ,raw_name : new_product ,type:"string:20"}
new-cif: {db :true ,raw_name : new_cif ,type : "string:20"}
new-eid: {db:true ,raw_name : new_eid ,type:"string:20"}
new-passport: {db:true ,raw_name : new_passport ,type:"string:20"}
new-email: {db:true ,raw_name : new_email ,type:"string:20"}
new-mobile: {db:true ,raw_name : new_mobile ,type:"string:20"}
new-applopendate: {db:true ,raw_name : new_applopendate ,type : timestamp}
new-custname: {db:true ,raw_name : new_custname ,type:"string:20"}
new-deviceid: {db:true ,raw_name : new_deviceid ,type:"string:20"}
new-ipaddress: {db:true ,raw_name : new_ipaddress ,type:"string:20"}
new-tradelincense: {db:true ,raw_name : new_tradelincense ,type:"string:20"}
old-app_refno: {db:true ,raw_name : old_app_refno ,type:"string:20"}
old-product: {db:true ,raw_name : old_product ,type:"string:20"}
old-cif: {db :true ,raw_name : old_cif ,type : "string:20"}
old-eid: {db:true ,raw_name : old_eid ,type:"string:20"}
old-passport: {db:true ,raw_name : old_passport ,type:"string:20"}
old-email: {db:true ,raw_name : old_email ,type:"string:20"}
old-mobile: {db:true ,raw_name : old_mobile ,type:"string:20"}
old-applopendate: {db:true ,raw_name : old_applopendate ,type : timestamp}
old-custname: {db:true ,raw_name : old_custname ,type:"string:20"}
old-deviceid: {db:true ,raw_name : old_deviceid ,type:"string:20"}
old-ipaddress: {db:true ,raw_name : old_ipaddress ,type:"string:20"}
old-tradelincense: {db:true ,raw_name : old_tradelincense ,type:"string:20"}
matched-eid: {db:true ,raw_name : matched_eid ,type:"string:20"}
matched-tradelicense: {db:true ,raw_name : matched_tradelicense ,type:"string:20"}
matched-email: {db :true ,raw_name : matched_email ,type : "string:20"}
matched-mobile: {db:true ,raw_name : matched_mobile ,type:"string:20"}
new-channel: {db:true ,raw_name : new_channel ,type:"string:20"}
old-channel: {db:true ,raw_name : old_channel ,type:"string:20"}
matched-score-eid: {db:true ,raw_name : matched_score_eid ,type:"string:20"}
}
}

