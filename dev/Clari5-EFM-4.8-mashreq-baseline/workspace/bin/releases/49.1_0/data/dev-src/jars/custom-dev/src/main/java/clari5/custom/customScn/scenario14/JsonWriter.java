package clari5.custom.customScn.scenario14;

import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.util.ECClient;
import clari5.rdbms.Rdbms;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class JsonWriter {
    public static String curr_mon_count = "";
    public static String last_mon_count = "";
    public static String avg_six_mon_count = "";
    public static String curr_mon_name = "";
    public static String last_mon_name = "";


    public static CxpsLogger logger = CxpsLogger.getLogger(JsonWriter.class);

    static {
        ECClient.configure(null);
    }

    private static ThreadInfo threadInfo = CallOnEndOfMonth.getThreadInfp();

    public static boolean send() {

        // need to add a if else logic
        try (RDBMSSession rdbmsSession = Rdbms.getAppSession();
             Connection connection = rdbmsSession.getCxConnection();) {
            PreparedStatement preparedStatement = connection.prepareStatement(threadInfo.getSelectQry());
            preparedStatement.setString(1, threadInfo.getCurrentThreadName());
            preparedStatement.setDate(2, threadInfo.getThreadDate());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                logger.info("[JsonWriter] Data Already Exist in Thread Audit Table on today's date");
                return false;
            } else {
                CallOnEndOfMonth.insert(threadInfo.getCurrentThreadName(), threadInfo.getThrdId(), threadInfo.getThreadDate(), threadInfo.getInsrtQry());
                logger.info("[JsonWriter]Calling send");
                JSONObject json = new JSONObject();
                JSONObject msg = new JSONObject();

                DataUtil dataUtil = new DataUtil();
                TimeCalculator timeCalculator = new TimeCalculator();
                String entity_id = "Mashreqprivatecifopening";
                json.put("EventType", "nft");
                json.put("EventSubType", "privatecifopening");
                json.put("event_name", "nft_privatecifopening");
                String event_id = dataUtil.randomNum();
                //String finaljson = json.toString();
                curr_mon_count = dataUtil.currentMonthCount();
                logger.info("current month count --" + curr_mon_count);
                msg.put("curr_mon_count", curr_mon_count);
                last_mon_count = dataUtil.lastMonthCount();
                msg.put("last_mon_count", last_mon_count);
                avg_six_mon_count = dataUtil.avgSixMonthCount();
                msg.put("avg_six_mon_count", avg_six_mon_count);
                curr_mon_name = timeCalculator.curr_month_name();
                msg.put("curr_mon_name", curr_mon_name);
                last_mon_name = timeCalculator.last_month_name();
                msg.put("last_mon_name", last_mon_name);
                msg.put("date", timeCalculator.todayDate());
                msg.put("sys_time", timeCalculator.getSysTime());
                msg.put("host_id", "F");
                msg.put("event_id", System.currentTimeMillis());

                json.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
                        replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));

                logger.info("Final JSON: " + json.toString());
                //logger.info("final custom json ---" + json.toString());
                //System.out.println("input fileds " + entity_id + "---" + event_id + "---" + json.toString());
                boolean status = ECClient.enqueue("HOST", entity_id, event_id, json.toString());
                logger.info("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
                threadInfo=null;
                return false;

                //threadPojo=null;
            }
        } catch (Exception e) {
            logger.info("[AppMqWriter] Error While Selecting Data from Thread Audit Table" + e.getCause() + " " + e.getMessage());
            return false;
        }


    }

    public static void main(String []args){
        System.out.println("into mmain--");
        send();
    }
}
