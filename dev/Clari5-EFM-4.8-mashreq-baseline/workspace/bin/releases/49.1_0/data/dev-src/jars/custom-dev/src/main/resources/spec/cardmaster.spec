cxps.noesis.glossary.entity.cardmaster {
       db-name = CARDMASTER
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
                { name = cif_id, column = cif_id, type = "string:50", key=true }
		{ name = card_number, column = card_number, type = "string:50"}
		{ name = card_ser_no, column = card_ser_no, type = "string:50"}
		{ name = card_status, column = card_status, type = "string:50"}
               ]
       }
