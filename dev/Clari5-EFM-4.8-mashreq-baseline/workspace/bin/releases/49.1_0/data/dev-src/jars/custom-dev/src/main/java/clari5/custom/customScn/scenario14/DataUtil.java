package clari5.custom.customScn.scenario14;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;


public class DataUtil {
    private static final CxpsLogger logger = CxpsLogger.getLogger(DataUtil.class);

    public  CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw  new RuntimeException("RDBMS as a resource is empty");
      /*  String dburl = "jdbc:sqlserver://192.168.5.76;portNumber=1433;databaseName=cxpsadm_vikas_48dev";
        String user = "cxpsadm_vikas_48dev";
        String password = "c_xps123";
        String drivername = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String schemaname = "cxpsadm_vikas_48dev.dbo.";*/

        /*Connection conn = null;
        try {
            Class.forName(drivername);
            conn = DriverManager.getConnection(dburl, user, password);
            //System.out.println("got connection");
        } catch (Exception e) {
            e.printStackTrace();
        }*/
            return rdbms.getCxConnection();
    }
    TimeCalculator timeCalculator= new TimeCalculator();
    public String currentMonthCount(){
        int count=0;
        String firstdate=timeCalculator.fistdateofCurrentMonth();
        String todaydate=timeCalculator.todayDate();
        String query="select count(*) from CUSTOMER where COMPMIS1 ='RBG' and COMPMIS2 in ('CSS0006','CSS002','CSS0028','CSS0053','CSS0062','CSS0063','CSS0075') and CUSTSINCE <= CAST('"+todaydate+"' as datetime) AND CUSTSINCE >= CAST('"+firstdate+"'as datetime)";
        logger.info("query to get cif count of current month"+query);
        try{
            CxConnection connection= getConnection();
            PreparedStatement ps=connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count=rs.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("count --"+count+"---"+String.valueOf(count));
        return String.valueOf(count);
    }
    public String lastMonthCount(){
        String firstdate=timeCalculator.firstDatePreMonth();
        String lastdate=timeCalculator.lastDatePreMonth();
        int count=0;
        String query="select count(*) from CUSTOMER where COMPMIS1 ='RBG' and COMPMIS2 in ('CSS0006','CSS002','CSS0028','CSS0053','CSS0062','CSS0063','CSS0075') and CUSTSINCE <= CAST('"+lastdate+"' as datetime) AND CUSTSINCE >= CAST('"+firstdate+"'as datetime)";
        logger.info("query to get last month count"+query);
        try{
            CxConnection connection= getConnection();
            PreparedStatement ps=connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count=rs.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return String.valueOf(count);
    }
    public String avgSixMonthCount(){
        String firstdate=timeCalculator.lastdateofpreSixMonth();
        String lastdate=timeCalculator.firstdatepresixMonth();
        int count=0;
        float avgcout=0;
        String query="select count(*) from CUSTOMER where COMPMIS1 ='RBG' and COMPMIS2 in ('CSS0006','CSS002','CSS0028','CSS0053','CSS0062','CSS0063','CSS0075') and CUSTSINCE >= CAST('"+lastdate+"' as datetime) AND CUSTSINCE <= CAST('"+firstdate+"'as datetime)";
        logger.info("query to get count of previous six month"+query);
        try{
            CxConnection connection= getConnection();
            PreparedStatement ps=connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count=rs.getInt(1);
            }
            avgcout=((float) (count/6.0));

        }catch (Exception e){
            e.printStackTrace();
        }
        return String.valueOf(avgcout);
    }
    public  String randomNum(){
         Random rand = new Random();
           int randomnum=rand.nextInt();

        return String.valueOf(randomnum);
    }
}
