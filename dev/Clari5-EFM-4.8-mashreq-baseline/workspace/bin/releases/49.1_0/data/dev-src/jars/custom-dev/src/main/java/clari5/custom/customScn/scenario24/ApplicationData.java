package clari5.custom.customScn.scenario24;

import clari5.custom.customScn.scenario34.AccountData;
import clari5.custom.customScn.scenario34.AccountPojo;
import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.rdbms.RDBMS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApplicationData {
    public static clari5.platform.logger.CxpsLogger logger = CxpsLogger.getLogger(ApplicationData.class);

    public static CxConnection getConnection() {
        RDBMS rdbms = Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is empty");
        return rdbms.getCxConnection();
    }
    public static List<ApplicationField> getAppFieldstoMacth() {
        CxConnection connection = null;
        String todaydate = todayDate();
        String query = "select * from EVENT_NFT_ACCTOPENING where CONVERT(varchar,event_date,23) = '"+todaydate+"'";
        logger.info("query to get data from EVENT_NFT_ACCTOPENING table of today's opened application::"+query);
        List<ApplicationField> finaldatalist = new ArrayList<>();
        try {
            connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ApplicationField applicationField = new ApplicationField();
                applicationField.setNeweid(rs.getString("eida"));
                applicationField.setNewtradelincense(rs.getString("trade_license"));
                applicationField.setNewmobile(rs.getString("ph_no"));
                applicationField.setNewemail(rs.getString("email_id"));
                applicationField.setNewapprefno(rs.getString("app_ref_no"));
                finaldatalist.add(applicationField);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return finaldatalist;
    }
    public static List<ApplicationField> getAppOldData(String app_ref_no) {
        CxConnection connection = null;
        String query = "select * from EVENT_NFT_ACCTOPENING where succ_fail_flag ='I' and app_ref_no = ?  ";
        logger.info("Query to get old data from EVENT_NFT_ACCTOPENING for app_ref_no::" +app_ref_no+ " is --"+query);
        List<ApplicationField> finaldatalist = new ArrayList<>();
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,app_ref_no);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                ApplicationField applicationField = new ApplicationField();
                applicationField.setOldcif(rs.getString("cust_id"));
                applicationField.setOldcustname(rs.getString("appl_name"));
                applicationField.setOldmobile(rs.getString("ph_no"));
                applicationField.setOldpassport(rs.getString("passport_no"));
                applicationField.setOldemail(rs.getString("email_id"));
                applicationField.setOldeid(rs.getString("eida"));
                applicationField.setOldapplopendate(rs.getDate("event_date"));
                applicationField.setOldapprefno(rs.getString("app_ref_no"));
                applicationField.setOldtradelincense(rs.getString("trade_license"));
                applicationField.setOldipaddress(rs.getString("ip_address"));
                applicationField.setOlddeviceid(rs.getString("device_id"));
                applicationField.setOldproduct(rs.getString("product"));
                applicationField.setOldChannel(rs.getString("channel"));

                finaldatalist.add(applicationField);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return finaldatalist;
    }
    public static ApplicationField getAppNewdata(String newAppRefNum) {
        CxConnection connection = null;
        String todaydate = todayDate();
        ApplicationField applicationField = new ApplicationField();
        String query = "select * from EVENT_NFT_ACCTOPENING where succ_fail_flag ='I' and app_ref_no=? and CONVERT(varchar,event_date,23) = '"+todaydate+"'";
        logger.info("Query to get New data from customer for mactched data :: "+query);
        try {
             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,newAppRefNum);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                applicationField.setNewcif(rs.getString("cust_id"));
                applicationField.setNewcustname(rs.getString("appl_name"));
                applicationField.setNewmobile(rs.getString("ph_no"));
                applicationField.setNewpassport(rs.getString("passport_no"));
                applicationField.setNewemail(rs.getString("email_id"));
                applicationField.setNeweid(rs.getString("eida"));
                applicationField.setNewapplopendate(rs.getDate("event_date"));
                applicationField.setNewapprefno(rs.getString("app_ref_no"));
                applicationField.setNewtradelincense(rs.getString("trade_license"));
                applicationField.setNewipaddress(rs.getString("ip_address"));
                applicationField.setNewdeviceid(rs.getString("device_id"));
                applicationField.setNewproduct(rs.getString("product"));
                applicationField.setNewChannel(rs.getString("channel"));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return applicationField;
    }
    public static ApplicationField getAppNewdataforpartialmatch(String matched_eida ) {
        CxConnection connection = null;
        String todaydate = todayDate();
        ApplicationField applicationField = new ApplicationField();
        String query = "select * from EVENT_NFT_ACCTOPENING where eida =  ? and CONVERT(varchar,event_date,23) = '"+todaydate+"'";
        logger.info("Query to get New data from customer for mactched data :: "+query);
        try {

             connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,matched_eida);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                applicationField.setNewcif(rs.getString("cust_id"));
                applicationField.setNewcustname(rs.getString("appl_name"));
                applicationField.setNewmobile(rs.getString("ph_no"));
                applicationField.setNewpassport(rs.getString("passport_no"));
                applicationField.setNewemail(rs.getString("email_id"));
                applicationField.setNeweid(rs.getString("eida"));
                applicationField.setNewapplopendate(rs.getDate("event_date"));
                applicationField.setNewapprefno(rs.getString("app_ref_no"));
                applicationField.setNewtradelincense(rs.getString("trade_license"));
                applicationField.setNewipaddress(rs.getString("ip_address"));
                applicationField.setNewdeviceid(rs.getString("device_id"));
                applicationField.setNewproduct(rs.getString("product"));
                applicationField.setNewChannel(rs.getString("channel"));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return applicationField;
    }
    public  static String todayDate(){
        String todaydate="";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
            Date date= new Date();
            todaydate=simpleDateFormat.format(date).toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return todaydate;

    }
}
