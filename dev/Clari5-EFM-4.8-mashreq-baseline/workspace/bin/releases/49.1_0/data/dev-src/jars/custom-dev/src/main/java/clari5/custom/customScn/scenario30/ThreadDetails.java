package clari5.custom.customScn.scenario30;

import java.sql.Date;

public class ThreadDetails {
    private   java.sql.Date threadDate=null;
    private  String currentThreadName=null;
    private  String currentUpdateQuery=null;
    private  long thrdId=0;
    private  String insrtQry=null;

    public String getSelectQuery() {
        return selectQuery;
    }

    public void setSelectQuery(String selectQuery) {
        this.selectQuery = selectQuery;
    }

    private  boolean flag;
    private String selectQuery=null;


    public Date getThreadDate() {
        return threadDate;
    }

    public void setThreadDate(Date threadDate) {
        this.threadDate = threadDate;
    }

    public String getCurrentThreadName() {
        return currentThreadName;
    }

    public void setCurrentThreadName(String currentThreadName) {
        this.currentThreadName = currentThreadName;
    }

    public String getCurrentUpdateQuery() {
        return currentUpdateQuery;
    }

    public void setCurrentUpdateQuery(String currentUpdateQuery) {
        this.currentUpdateQuery = currentUpdateQuery;
    }

    public long getThrdId() {
        return thrdId;
    }

    public void setThrdId(long thrdId) {
        this.thrdId = thrdId;
    }

    public String getInsrtQry() {
        return insrtQry;
    }

    public void setInsrtQry(String insrtQry) {
        this.insrtQry = insrtQry;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
