cxps.events.event.ft-electronic-payment-req{
  table-name : EVENT_FT_ELECTRONIC_PAYMENT_REQUEST
  event-mnemonic: EP
  workspaces : {
    ACCOUNT: "account-id"
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
channel: {db : true ,raw_name : channel ,type : "string:20"}
account-id: {db : true ,raw_name : account_id ,type : "string:20"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:20", derivation:"""cxps.events.CustomDerivator.getCustIdFromAccountEPay(this)"""}
user-id: {db : true ,raw_name : user_id ,type : "string:20"}
tran-type: {db : true ,raw_name : tran_type ,type : "string:20"}
tran-amt: {db :true ,raw_name : tran_amt ,type : "number:20,3"}
tran-crncy-code: {db : true ,raw_name : tran_crncy_code ,type : "string:200"}
ip-address: {db : true ,raw_name : ip_address ,type : "string:200"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
country-code: {db : true ,raw_name : country_code ,type : "string:20"}
benf-name: {db : true ,raw_name : benf_name ,type : "string:200"}
transfer-remarks: {db : true ,raw_name : transfer_remarks ,type : "string:200"}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
cardless-cash-limit: {db : true ,type : "number:20,3", derivation:"""cxps.events.CustomDerivator.getCardlessCashLimit(this)"""}
avl-bal: {db :true ,raw_name : avl_bal ,type : "number:20,3"}

}
}
