cxps.noesis.glossary.entity.scenario26-tran-code {
       db-name = scenario26_TRAN_CODE
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = tran-code, column = TRAN_CODE, type = "string:20", key=true }
               ]
       }
