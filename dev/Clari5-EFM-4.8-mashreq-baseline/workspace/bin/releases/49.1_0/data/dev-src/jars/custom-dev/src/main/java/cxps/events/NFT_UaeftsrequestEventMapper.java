// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_UaeftsrequestEventMapper extends EventMapper<NFT_UaeftsrequestEvent> {

public NFT_UaeftsrequestEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_UaeftsrequestEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_UaeftsrequestEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_UaeftsrequestEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_UaeftsrequestEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_UaeftsrequestEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_UaeftsrequestEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getEid());
            preparedStatement.setTimestamp(i++, obj.getTranDate());
            preparedStatement.setString(i++, obj.getAppId());
            preparedStatement.setString(i++, obj.getInstanceId());
            preparedStatement.setString(i++, obj.getIbanNum());
            preparedStatement.setTimestamp(i++, obj.getStatToDate());
            preparedStatement.setTimestamp(i++, obj.getStatFromDate());
            preparedStatement.setString(i++, obj.getPassportNo());
            preparedStatement.setString(i++, obj.getAppRef());
            preparedStatement.setString(i++, obj.getBankCode());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_UAEFTSREQUEST]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_UaeftsrequestEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_UaeftsrequestEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_UAEFTSREQUEST"));
        putList = new ArrayList<>();

        for (NFT_UaeftsrequestEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "EID",  obj.getEid());
            p = this.insert(p, "TRAN_DATE", String.valueOf(obj.getTranDate()));
            p = this.insert(p, "APP_ID",  obj.getAppId());
            p = this.insert(p, "INSTANCE_ID",  obj.getInstanceId());
            p = this.insert(p, "IBAN_NUM",  obj.getIbanNum());
            p = this.insert(p, "STAT_TO_DATE", String.valueOf(obj.getStatToDate()));
            p = this.insert(p, "STAT_FROM_DATE", String.valueOf(obj.getStatFromDate()));
            p = this.insert(p, "PASSPORT_NO",  obj.getPassportNo());
            p = this.insert(p, "APP_REF",  obj.getAppRef());
            p = this.insert(p, "BANK_CODE",  obj.getBankCode());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_UAEFTSREQUEST"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_UAEFTSREQUEST]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_UAEFTSREQUEST]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_UaeftsrequestEvent obj = new NFT_UaeftsrequestEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setEid(rs.getString("EID"));
    obj.setTranDate(rs.getTimestamp("TRAN_DATE"));
    obj.setAppId(rs.getString("APP_ID"));
    obj.setInstanceId(rs.getString("INSTANCE_ID"));
    obj.setIbanNum(rs.getString("IBAN_NUM"));
    obj.setStatToDate(rs.getTimestamp("STAT_TO_DATE"));
    obj.setStatFromDate(rs.getTimestamp("STAT_FROM_DATE"));
    obj.setPassportNo(rs.getString("PASSPORT_NO"));
    obj.setAppRef(rs.getString("APP_REF"));
    obj.setBankCode(rs.getString("BANK_CODE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_UAEFTSREQUEST]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_UaeftsrequestEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_UaeftsrequestEvent> events;
 NFT_UaeftsrequestEvent obj = new NFT_UaeftsrequestEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_UAEFTSREQUEST"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_UaeftsrequestEvent obj = new NFT_UaeftsrequestEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setEid(getColumnValue(rs, "EID"));
            obj.setTranDate(EventHelper.toTimestamp(getColumnValue(rs, "TRAN_DATE")));
            obj.setAppId(getColumnValue(rs, "APP_ID"));
            obj.setInstanceId(getColumnValue(rs, "INSTANCE_ID"));
            obj.setIbanNum(getColumnValue(rs, "IBAN_NUM"));
            obj.setStatToDate(EventHelper.toTimestamp(getColumnValue(rs, "STAT_TO_DATE")));
            obj.setStatFromDate(EventHelper.toTimestamp(getColumnValue(rs, "STAT_FROM_DATE")));
            obj.setPassportNo(getColumnValue(rs, "PASSPORT_NO"));
            obj.setAppRef(getColumnValue(rs, "APP_REF"));
            obj.setBankCode(getColumnValue(rs, "BANK_CODE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_UAEFTSREQUEST]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_UAEFTSREQUEST]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"EID\",\"TRAN_DATE\",\"APP_ID\",\"INSTANCE_ID\",\"IBAN_NUM\",\"STAT_TO_DATE\",\"STAT_FROM_DATE\",\"PASSPORT_NO\",\"APP_REF\",\"BANK_CODE\"" +
              " FROM EVENT_NFT_UAEFTSREQUEST";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [EID],[TRAN_DATE],[APP_ID],[INSTANCE_ID],[IBAN_NUM],[STAT_TO_DATE],[STAT_FROM_DATE],[PASSPORT_NO],[APP_REF],[BANK_CODE]" +
              " FROM EVENT_NFT_UAEFTSREQUEST";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`EID`,`TRAN_DATE`,`APP_ID`,`INSTANCE_ID`,`IBAN_NUM`,`STAT_TO_DATE`,`STAT_FROM_DATE`,`PASSPORT_NO`,`APP_REF`,`BANK_CODE`" +
              " FROM EVENT_NFT_UAEFTSREQUEST";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_UAEFTSREQUEST (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"EID\",\"TRAN_DATE\",\"APP_ID\",\"INSTANCE_ID\",\"IBAN_NUM\",\"STAT_TO_DATE\",\"STAT_FROM_DATE\",\"PASSPORT_NO\",\"APP_REF\",\"BANK_CODE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[EID],[TRAN_DATE],[APP_ID],[INSTANCE_ID],[IBAN_NUM],[STAT_TO_DATE],[STAT_FROM_DATE],[PASSPORT_NO],[APP_REF],[BANK_CODE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`EID`,`TRAN_DATE`,`APP_ID`,`INSTANCE_ID`,`IBAN_NUM`,`STAT_TO_DATE`,`STAT_FROM_DATE`,`PASSPORT_NO`,`APP_REF`,`BANK_CODE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

