cxps.events.event.nft-ivr-inq{
  table-name : EVENT_NFT_IVR_INQ
  event-mnemonic: IVR
  workspaces : {

   CUSTOMER: "cust-id"
   
}
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
channel: {db : true ,raw_name : channel ,type : "string:20"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:20"}
registrd-unregistrd-flg: {db : true ,raw_name : registrd_unregistrd_flg,type: "string:20" }
mob-land-num: {db : true ,raw_name : mob_land_num ,type : "string:20"}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
inq-type: {db : true ,raw_name : inq_type ,type : "string:200"}
acct-id: {db : true ,raw_name : acct_id ,type : "string:200"}
}
}
