// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AcctStatusChangeEventMapper extends EventMapper<NFT_AcctStatusChangeEvent> {

public NFT_AcctStatusChangeEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AcctStatusChangeEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AcctStatusChangeEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AcctStatusChangeEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AcctStatusChangeEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AcctStatusChangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AcctStatusChangeEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAcctStatusChangeCheck());
            preparedStatement.setTimestamp(i++, obj.getAcctOpenDate());
            preparedStatement.setString(i++, obj.getFlag());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getIntialAcctStatus());
            preparedStatement.setString(i++, obj.getFinalAcctStatus());
            preparedStatement.setString(i++, obj.getAcctName());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_ACCT_STATUS_CHANGE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AcctStatusChangeEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AcctStatusChangeEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCT_STATUS_CHANGE"));
        putList = new ArrayList<>();

        for (NFT_AcctStatusChangeEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCT_STATUS_CHANGE_CHECK",  obj.getAcctStatusChangeCheck());
            p = this.insert(p, "ACCT_OPEN_DATE", String.valueOf(obj.getAcctOpenDate()));
            p = this.insert(p, "FLAG",  obj.getFlag());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "INTIAL_ACCT_STATUS",  obj.getIntialAcctStatus());
            p = this.insert(p, "FINAL_ACCT_STATUS",  obj.getFinalAcctStatus());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_ACCT_STATUS_CHANGE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_ACCT_STATUS_CHANGE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_ACCT_STATUS_CHANGE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AcctStatusChangeEvent obj = new NFT_AcctStatusChangeEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAcctStatusChangeCheck(rs.getString("ACCT_STATUS_CHANGE_CHECK"));
    obj.setAcctOpenDate(rs.getTimestamp("ACCT_OPEN_DATE"));
    obj.setFlag(rs.getString("FLAG"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setIntialAcctStatus(rs.getString("INTIAL_ACCT_STATUS"));
    obj.setFinalAcctStatus(rs.getString("FINAL_ACCT_STATUS"));
    obj.setAcctName(rs.getString("ACCT_NAME"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_ACCT_STATUS_CHANGE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AcctStatusChangeEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AcctStatusChangeEvent> events;
 NFT_AcctStatusChangeEvent obj = new NFT_AcctStatusChangeEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCT_STATUS_CHANGE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AcctStatusChangeEvent obj = new NFT_AcctStatusChangeEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAcctStatusChangeCheck(getColumnValue(rs, "ACCT_STATUS_CHANGE_CHECK"));
            obj.setAcctOpenDate(EventHelper.toTimestamp(getColumnValue(rs, "ACCT_OPEN_DATE")));
            obj.setFlag(getColumnValue(rs, "FLAG"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setIntialAcctStatus(getColumnValue(rs, "INTIAL_ACCT_STATUS"));
            obj.setFinalAcctStatus(getColumnValue(rs, "FINAL_ACCT_STATUS"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCT_STATUS_CHANGE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCT_STATUS_CHANGE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCT_STATUS_CHANGE_CHECK\",\"ACCT_OPEN_DATE\",\"FLAG\",\"ACCOUNT_ID\",\"USER_ID\",\"AVL_BAL\",\"CHANNEL\",\"BRANCH_ID\",\"CUST_ID\",\"INTIAL_ACCT_STATUS\",\"FINAL_ACCT_STATUS\",\"ACCT_NAME\"" +
              " FROM EVENT_NFT_ACCT_STATUS_CHANGE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCT_STATUS_CHANGE_CHECK],[ACCT_OPEN_DATE],[FLAG],[ACCOUNT_ID],[USER_ID],[AVL_BAL],[CHANNEL],[BRANCH_ID],[CUST_ID],[INTIAL_ACCT_STATUS],[FINAL_ACCT_STATUS],[ACCT_NAME]" +
              " FROM EVENT_NFT_ACCT_STATUS_CHANGE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCT_STATUS_CHANGE_CHECK`,`ACCT_OPEN_DATE`,`FLAG`,`ACCOUNT_ID`,`USER_ID`,`AVL_BAL`,`CHANNEL`,`BRANCH_ID`,`CUST_ID`,`INTIAL_ACCT_STATUS`,`FINAL_ACCT_STATUS`,`ACCT_NAME`" +
              " FROM EVENT_NFT_ACCT_STATUS_CHANGE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_ACCT_STATUS_CHANGE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCT_STATUS_CHANGE_CHECK\",\"ACCT_OPEN_DATE\",\"FLAG\",\"ACCOUNT_ID\",\"USER_ID\",\"AVL_BAL\",\"CHANNEL\",\"BRANCH_ID\",\"CUST_ID\",\"INTIAL_ACCT_STATUS\",\"FINAL_ACCT_STATUS\",\"ACCT_NAME\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCT_STATUS_CHANGE_CHECK],[ACCT_OPEN_DATE],[FLAG],[ACCOUNT_ID],[USER_ID],[AVL_BAL],[CHANNEL],[BRANCH_ID],[CUST_ID],[INTIAL_ACCT_STATUS],[FINAL_ACCT_STATUS],[ACCT_NAME]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCT_STATUS_CHANGE_CHECK`,`ACCT_OPEN_DATE`,`FLAG`,`ACCOUNT_ID`,`USER_ID`,`AVL_BAL`,`CHANNEL`,`BRANCH_ID`,`CUST_ID`,`INTIAL_ACCT_STATUS`,`FINAL_ACCT_STATUS`,`ACCT_NAME`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

