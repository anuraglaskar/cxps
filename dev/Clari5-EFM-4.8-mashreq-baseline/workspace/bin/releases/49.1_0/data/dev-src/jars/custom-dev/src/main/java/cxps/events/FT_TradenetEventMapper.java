// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_TradenetEventMapper extends EventMapper<FT_TradenetEvent> {

public FT_TradenetEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_TradenetEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_TradenetEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_TradenetEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_TradenetEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_TradenetEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_TradenetEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setDouble(i++, obj.getAmount());
            preparedStatement.setString(i++, obj.getIsSelfTxn());
            preparedStatement.setString(i++, obj.getLeaveType());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setTimestamp(i++, obj.getLeaveStartDate());
            preparedStatement.setString(i++, obj.getSecurityId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getEmpId());
            preparedStatement.setString(i++, obj.getSecurityClass());
            preparedStatement.setString(i++, obj.getStaffFlg());
            preparedStatement.setString(i++, obj.getIsTraderBankStaff());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getTraderId());
            preparedStatement.setTimestamp(i++, obj.getLeaveEndDate());
            preparedStatement.setString(i++, obj.getHostId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_TRADENET]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_TradenetEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_TradenetEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADENET"));
        putList = new ArrayList<>();

        for (FT_TradenetEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "AMOUNT", String.valueOf(obj.getAmount()));
            p = this.insert(p, "IS_SELF_TXN",  obj.getIsSelfTxn());
            p = this.insert(p, "LEAVE_TYPE",  obj.getLeaveType());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "LEAVE_START_DATE", String.valueOf(obj.getLeaveStartDate()));
            p = this.insert(p, "SECURITY_ID",  obj.getSecurityId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "EMP_ID",  obj.getEmpId());
            p = this.insert(p, "SECURITY_CLASS",  obj.getSecurityClass());
            p = this.insert(p, "STAFF_FLG",  obj.getStaffFlg());
            p = this.insert(p, "IS_TRADER_BANK_STAFF",  obj.getIsTraderBankStaff());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "TRADER_ID",  obj.getTraderId());
            p = this.insert(p, "LEAVE_END_DATE", String.valueOf(obj.getLeaveEndDate()));
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_TRADENET"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_TRADENET]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_TRADENET]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_TradenetEvent obj = new FT_TradenetEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setAmount(rs.getDouble("AMOUNT"));
    obj.setIsSelfTxn(rs.getString("IS_SELF_TXN"));
    obj.setLeaveType(rs.getString("LEAVE_TYPE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setLeaveStartDate(rs.getTimestamp("LEAVE_START_DATE"));
    obj.setSecurityId(rs.getString("SECURITY_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setEmpId(rs.getString("EMP_ID"));
    obj.setSecurityClass(rs.getString("SECURITY_CLASS"));
    obj.setStaffFlg(rs.getString("STAFF_FLG"));
    obj.setIsTraderBankStaff(rs.getString("IS_TRADER_BANK_STAFF"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setTraderId(rs.getString("TRADER_ID"));
    obj.setLeaveEndDate(rs.getTimestamp("LEAVE_END_DATE"));
    obj.setHostId(rs.getString("HOST_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_TRADENET]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_TradenetEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_TradenetEvent> events;
 FT_TradenetEvent obj = new FT_TradenetEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_TRADENET"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_TradenetEvent obj = new FT_TradenetEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setAmount(EventHelper.toDouble(getColumnValue(rs, "AMOUNT")));
            obj.setIsSelfTxn(getColumnValue(rs, "IS_SELF_TXN"));
            obj.setLeaveType(getColumnValue(rs, "LEAVE_TYPE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setLeaveStartDate(EventHelper.toTimestamp(getColumnValue(rs, "LEAVE_START_DATE")));
            obj.setSecurityId(getColumnValue(rs, "SECURITY_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setEmpId(getColumnValue(rs, "EMP_ID"));
            obj.setSecurityClass(getColumnValue(rs, "SECURITY_CLASS"));
            obj.setStaffFlg(getColumnValue(rs, "STAFF_FLG"));
            obj.setIsTraderBankStaff(getColumnValue(rs, "IS_TRADER_BANK_STAFF"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setTraderId(getColumnValue(rs, "TRADER_ID"));
            obj.setLeaveEndDate(EventHelper.toTimestamp(getColumnValue(rs, "LEAVE_END_DATE")));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADENET]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_TRADENET]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCT_ID\",\"AMOUNT\",\"IS_SELF_TXN\",\"LEAVE_TYPE\",\"CHANNEL\",\"LEAVE_START_DATE\",\"SECURITY_ID\",\"CUST_ID\",\"EMP_ID\",\"SECURITY_CLASS\",\"STAFF_FLG\",\"IS_TRADER_BANK_STAFF\",\"TRAN_TYPE\",\"TRADER_ID\",\"LEAVE_END_DATE\",\"HOST_ID\"" +
              " FROM EVENT_FT_TRADENET";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCT_ID],[AMOUNT],[IS_SELF_TXN],[LEAVE_TYPE],[CHANNEL],[LEAVE_START_DATE],[SECURITY_ID],[CUST_ID],[EMP_ID],[SECURITY_CLASS],[STAFF_FLG],[IS_TRADER_BANK_STAFF],[TRAN_TYPE],[TRADER_ID],[LEAVE_END_DATE],[HOST_ID]" +
              " FROM EVENT_FT_TRADENET";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCT_ID`,`AMOUNT`,`IS_SELF_TXN`,`LEAVE_TYPE`,`CHANNEL`,`LEAVE_START_DATE`,`SECURITY_ID`,`CUST_ID`,`EMP_ID`,`SECURITY_CLASS`,`STAFF_FLG`,`IS_TRADER_BANK_STAFF`,`TRAN_TYPE`,`TRADER_ID`,`LEAVE_END_DATE`,`HOST_ID`" +
              " FROM EVENT_FT_TRADENET";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_TRADENET (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCT_ID\",\"AMOUNT\",\"IS_SELF_TXN\",\"LEAVE_TYPE\",\"CHANNEL\",\"LEAVE_START_DATE\",\"SECURITY_ID\",\"CUST_ID\",\"EMP_ID\",\"SECURITY_CLASS\",\"STAFF_FLG\",\"IS_TRADER_BANK_STAFF\",\"TRAN_TYPE\",\"TRADER_ID\",\"LEAVE_END_DATE\",\"HOST_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCT_ID],[AMOUNT],[IS_SELF_TXN],[LEAVE_TYPE],[CHANNEL],[LEAVE_START_DATE],[SECURITY_ID],[CUST_ID],[EMP_ID],[SECURITY_CLASS],[STAFF_FLG],[IS_TRADER_BANK_STAFF],[TRAN_TYPE],[TRADER_ID],[LEAVE_END_DATE],[HOST_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCT_ID`,`AMOUNT`,`IS_SELF_TXN`,`LEAVE_TYPE`,`CHANNEL`,`LEAVE_START_DATE`,`SECURITY_ID`,`CUST_ID`,`EMP_ID`,`SECURITY_CLASS`,`STAFF_FLG`,`IS_TRADER_BANK_STAFF`,`TRAN_TYPE`,`TRADER_ID`,`LEAVE_END_DATE`,`HOST_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

