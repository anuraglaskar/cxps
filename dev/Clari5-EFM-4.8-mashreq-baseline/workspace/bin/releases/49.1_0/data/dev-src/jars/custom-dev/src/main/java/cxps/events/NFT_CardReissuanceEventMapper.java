// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_CardReissuanceEventMapper extends EventMapper<NFT_CardReissuanceEvent> {

public NFT_CardReissuanceEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_CardReissuanceEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_CardReissuanceEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_CardReissuanceEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_CardReissuanceEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_CardReissuanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_CardReissuanceEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setDouble(i++, obj.getCardAtmLmt());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setDouble(i++, obj.getCardPosLmt());
            preparedStatement.setString(i++, obj.getCardType());
            preparedStatement.setString(i++, obj.getCardValidFrm());
            preparedStatement.setString(i++, obj.getNewCardFlg());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getCardValidTo());
            preparedStatement.setString(i++, obj.getCardNo());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_CARD_REISSUANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_CardReissuanceEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_CardReissuanceEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CARD_REISSUANCE"));
        putList = new ArrayList<>();

        for (NFT_CardReissuanceEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "CARD_ATM_LMT", String.valueOf(obj.getCardAtmLmt()));
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "CARD_POS_LMT", String.valueOf(obj.getCardPosLmt()));
            p = this.insert(p, "CARD_TYPE",  obj.getCardType());
            p = this.insert(p, "CARD_VALID_FRM",  obj.getCardValidFrm());
            p = this.insert(p, "NEW_CARD_FLG",  obj.getNewCardFlg());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CARD_VALID_TO",  obj.getCardValidTo());
            p = this.insert(p, "CARD_NO",  obj.getCardNo());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_CARD_REISSUANCE"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_CARD_REISSUANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_CARD_REISSUANCE]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_CardReissuanceEvent obj = new NFT_CardReissuanceEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setCardAtmLmt(rs.getDouble("CARD_ATM_LMT"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setCardPosLmt(rs.getDouble("CARD_POS_LMT"));
    obj.setCardType(rs.getString("CARD_TYPE"));
    obj.setCardValidFrm(rs.getString("CARD_VALID_FRM"));
    obj.setNewCardFlg(rs.getString("NEW_CARD_FLG"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCardValidTo(rs.getString("CARD_VALID_TO"));
    obj.setCardNo(rs.getString("CARD_NO"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_CARD_REISSUANCE]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_CardReissuanceEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_CardReissuanceEvent> events;
 NFT_CardReissuanceEvent obj = new NFT_CardReissuanceEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_CARD_REISSUANCE"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_CardReissuanceEvent obj = new NFT_CardReissuanceEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setCardAtmLmt(EventHelper.toDouble(getColumnValue(rs, "CARD_ATM_LMT")));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setCardPosLmt(EventHelper.toDouble(getColumnValue(rs, "CARD_POS_LMT")));
            obj.setCardType(getColumnValue(rs, "CARD_TYPE"));
            obj.setCardValidFrm(getColumnValue(rs, "CARD_VALID_FRM"));
            obj.setNewCardFlg(getColumnValue(rs, "NEW_CARD_FLG"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCardValidTo(getColumnValue(rs, "CARD_VALID_TO"));
            obj.setCardNo(getColumnValue(rs, "CARD_NO"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CARD_REISSUANCE]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_CARD_REISSUANCE]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"CARD_ATM_LMT\",\"ERROR_DESC\",\"CHANNEL\",\"SUCC_FAIL_FLG\",\"SYS_TIME\",\"ERROR_CODE\",\"CUST_ID\",\"CARD_POS_LMT\",\"CARD_TYPE\",\"CARD_VALID_FRM\",\"NEW_CARD_FLG\",\"HOST_ID\",\"CARD_VALID_TO\",\"CARD_NO\"" +
              " FROM EVENT_NFT_CARD_REISSUANCE";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [CARD_ATM_LMT],[ERROR_DESC],[CHANNEL],[SUCC_FAIL_FLG],[SYS_TIME],[ERROR_CODE],[CUST_ID],[CARD_POS_LMT],[CARD_TYPE],[CARD_VALID_FRM],[NEW_CARD_FLG],[HOST_ID],[CARD_VALID_TO],[CARD_NO]" +
              " FROM EVENT_NFT_CARD_REISSUANCE";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`CARD_ATM_LMT`,`ERROR_DESC`,`CHANNEL`,`SUCC_FAIL_FLG`,`SYS_TIME`,`ERROR_CODE`,`CUST_ID`,`CARD_POS_LMT`,`CARD_TYPE`,`CARD_VALID_FRM`,`NEW_CARD_FLG`,`HOST_ID`,`CARD_VALID_TO`,`CARD_NO`" +
              " FROM EVENT_NFT_CARD_REISSUANCE";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_CARD_REISSUANCE (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"CARD_ATM_LMT\",\"ERROR_DESC\",\"CHANNEL\",\"SUCC_FAIL_FLG\",\"SYS_TIME\",\"ERROR_CODE\",\"CUST_ID\",\"CARD_POS_LMT\",\"CARD_TYPE\",\"CARD_VALID_FRM\",\"NEW_CARD_FLG\",\"HOST_ID\",\"CARD_VALID_TO\",\"CARD_NO\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[CARD_ATM_LMT],[ERROR_DESC],[CHANNEL],[SUCC_FAIL_FLG],[SYS_TIME],[ERROR_CODE],[CUST_ID],[CARD_POS_LMT],[CARD_TYPE],[CARD_VALID_FRM],[NEW_CARD_FLG],[HOST_ID],[CARD_VALID_TO],[CARD_NO]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`CARD_ATM_LMT`,`ERROR_DESC`,`CHANNEL`,`SUCC_FAIL_FLG`,`SYS_TIME`,`ERROR_CODE`,`CUST_ID`,`CARD_POS_LMT`,`CARD_TYPE`,`CARD_VALID_FRM`,`NEW_CARD_FLG`,`HOST_ID`,`CARD_VALID_TO`,`CARD_NO`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

