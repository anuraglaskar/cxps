package clari5.custom.test;


import clari5.platform.logger.CxpsLogger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;

@Path("/test")
public class TestListner {

    public static CxpsLogger logger = CxpsLogger.getLogger(TestListner.class);

    @POST
    @Path("/debit")
    @Consumes(MediaType.TEXT_HTML)
    @Produces(MediaType.TEXT_HTML)
    public Response postDebitParse(InputStream reqXML) {
        return Response.ok("HELLO SURYA").build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.TEXT_HTML)
    @Path("/sample")
    public String load(String req) {
        try{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(req)));
        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("cre:EAIServices");

        if(nList.getLength()>0) {

            return "<cre:ExceptionDetails xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"\"> " +
                    "<mbc:ErrorCode>200</mbc:ErrorCode>" +
                    "<mbc:ErrorDescription>SUCCESS</mbc:ErrorDescription>" +
                    "</cre:ExceptionDetails>";
        }else{

            return "<mod:ExceptionDetails xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"\"> " +
                    "<mbc:ErrorCode>200</mbc:ErrorCode>" +
                    "<mbc:ErrorDescription>SUCCESS</mbc:ErrorDescription>" +
                    "</mod:ExceptionDetails>";
        }
       }catch (Exception e){
            e.printStackTrace();
        }

        return null;
     }
}
