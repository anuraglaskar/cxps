cxps.events.event.nft-uaeftsrequest{  
  table-name : EVENT_NFT_UAEFTSREQUEST
  event-mnemonic: NUAE
  workspaces : {
    NONCUSTOMER: "iban-num"
  }
event-attributes : {
iban-num: {db:true ,raw_name : iban_num ,type:"string:50"}
bank-code: {db:true ,raw_name : bank_code ,type:"string:50"}
eid: {db :true ,raw_name : eid ,type : "string:50"}
passport-no: {db:true ,raw_name : passport_no ,type:"string:50"}
tran-date: {db:true ,raw_name : tran_date ,type:timestamp}
stat-from-date: {db:true ,raw_name : stat_from_date ,type:timestamp}
stat-to-date: {db:true ,raw_name : stat_to_date ,type:timestamp}
instance-id: {db:true ,raw_name : instance_id ,type:"string:50"}
app-id: {db:true ,raw_name : app_id ,type:"string:50"}
app-ref: {db:true ,raw_name : app_ref ,type:"string:50"}
}
}
