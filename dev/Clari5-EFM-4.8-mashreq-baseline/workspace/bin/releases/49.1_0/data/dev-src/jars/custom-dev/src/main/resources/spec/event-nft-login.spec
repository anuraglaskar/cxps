cxps.events.event.nft-login{
  table-name : EVENT_NFT_LOGIN
  event-mnemonic: LG
  workspaces : {
    
    CUSTOMER: "cust-id"
    NONCUSTOMER: "addr-network"  
}
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
channel: {db : true ,raw_name : channel ,type : "string:20"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:20"}
user-id: {db : true ,raw_name : user_id ,type : "string:20"}
device-id: {db : true ,raw_name : device_id ,type: "string:20" }
addr-network: {db : true ,raw_name : addr_network ,type : "string:200"}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
device-type: {db : true ,raw_name : device_type ,type : "string:200"}
last-login-ip: {db : true ,raw_name : last_login_ip ,type : "string:200"}
last-logints:{db : true ,raw_name : last_logints ,type : timestamp}
ip_country: { type:"string:50",derivation :"""cxps.events.CustomDerivator.getIpCountry(this)"""}
}
}
