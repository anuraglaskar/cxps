// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_ApplmatchedrejapplEventMapper extends EventMapper<NFT_ApplmatchedrejapplEvent> {

public NFT_ApplmatchedrejapplEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_ApplmatchedrejapplEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_ApplmatchedrejapplEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_ApplmatchedrejapplEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_ApplmatchedrejapplEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_ApplmatchedrejapplEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_ApplmatchedrejapplEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getIsEmployerNonsfa());
            preparedStatement.setString(i++, obj.getRejCif());
            preparedStatement.setString(i++, obj.getRejEid());
            preparedStatement.setString(i++, obj.getRejPassportno());
            preparedStatement.setTimestamp(i++, obj.getRejectionDate());
            preparedStatement.setString(i++, obj.getCurrentDate());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getApplName());
            preparedStatement.setTimestamp(i++, obj.getMatchedDateofbirth());
            preparedStatement.setString(i++, obj.getEida());
            preparedStatement.setString(i++, obj.getTradeLicense());
            preparedStatement.setTimestamp(i++, obj.getRejDateofbirth());
            preparedStatement.setString(i++, obj.getDrivingLnc());
            preparedStatement.setString(i++, obj.getMatchedEid());
            preparedStatement.setString(i++, obj.getIbanNo());
            preparedStatement.setString(i++, obj.getMatchedMobileno());
            preparedStatement.setString(i++, obj.getAppRefNo());
            preparedStatement.setString(i++, obj.getEmployeeId());
            preparedStatement.setString(i++, obj.getProduct());
            preparedStatement.setString(i++, obj.getDeclineReason());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getRejProductCode());
            preparedStatement.setString(i++, obj.getVat());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getPhNo());
            preparedStatement.setString(i++, obj.getEmployerId());
            preparedStatement.setTimestamp(i++, obj.getApplDate());
            preparedStatement.setTimestamp(i++, obj.getRejApplDate());
            preparedStatement.setString(i++, obj.getEmailId());
            preparedStatement.setString(i++, obj.getRejApplId());
            preparedStatement.setTimestamp(i++, obj.getDob());
            preparedStatement.setString(i++, obj.getRejMobileno());
            preparedStatement.setString(i++, obj.getMatchedPassportno());
            preparedStatement.setString(i++, obj.getPassportNo());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_APPLMATCHEDREJAPPL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_ApplmatchedrejapplEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_ApplmatchedrejapplEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_APPLMATCHEDREJAPPL"));
        putList = new ArrayList<>();

        for (NFT_ApplmatchedrejapplEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "IS_EMPLOYER_NONSFA",  obj.getIsEmployerNonsfa());
            p = this.insert(p, "REJ_CIF",  obj.getRejCif());
            p = this.insert(p, "REJ_EID",  obj.getRejEid());
            p = this.insert(p, "REJ_PASSPORTNO",  obj.getRejPassportno());
            p = this.insert(p, "REJECTION_DATE", String.valueOf(obj.getRejectionDate()));
            p = this.insert(p, "CURRENT_DATE",  obj.getCurrentDate());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "APPL_NAME",  obj.getApplName());
            p = this.insert(p, "MATCHED_DATEOFBIRTH", String.valueOf(obj.getMatchedDateofbirth()));
            p = this.insert(p, "EIDA",  obj.getEida());
            p = this.insert(p, "TRADE_LICENSE",  obj.getTradeLicense());
            p = this.insert(p, "REJ_DATEOFBIRTH", String.valueOf(obj.getRejDateofbirth()));
            p = this.insert(p, "DRIVING_LNC",  obj.getDrivingLnc());
            p = this.insert(p, "MATCHED_EID",  obj.getMatchedEid());
            p = this.insert(p, "IBAN_NO",  obj.getIbanNo());
            p = this.insert(p, "MATCHED_MOBILENO",  obj.getMatchedMobileno());
            p = this.insert(p, "APP_REF_NO",  obj.getAppRefNo());
            p = this.insert(p, "EMPLOYEE_ID",  obj.getEmployeeId());
            p = this.insert(p, "PRODUCT",  obj.getProduct());
            p = this.insert(p, "DECLINE_REASON",  obj.getDeclineReason());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "REJ_PRODUCT_CODE",  obj.getRejProductCode());
            p = this.insert(p, "VAT",  obj.getVat());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "PH_NO",  obj.getPhNo());
            p = this.insert(p, "EMPLOYER_ID",  obj.getEmployerId());
            p = this.insert(p, "APPL_DATE", String.valueOf(obj.getApplDate()));
            p = this.insert(p, "REJ_APPL_DATE", String.valueOf(obj.getRejApplDate()));
            p = this.insert(p, "EMAIL_ID",  obj.getEmailId());
            p = this.insert(p, "REJ_APPL_ID",  obj.getRejApplId());
            p = this.insert(p, "DOB", String.valueOf(obj.getDob()));
            p = this.insert(p, "REJ_MOBILENO",  obj.getRejMobileno());
            p = this.insert(p, "MATCHED_PASSPORTNO",  obj.getMatchedPassportno());
            p = this.insert(p, "PASSPORT_NO",  obj.getPassportNo());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_APPLMATCHEDREJAPPL"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_APPLMATCHEDREJAPPL]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_APPLMATCHEDREJAPPL]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_ApplmatchedrejapplEvent obj = new NFT_ApplmatchedrejapplEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setIsEmployerNonsfa(rs.getString("IS_EMPLOYER_NONSFA"));
    obj.setRejCif(rs.getString("REJ_CIF"));
    obj.setRejEid(rs.getString("REJ_EID"));
    obj.setRejPassportno(rs.getString("REJ_PASSPORTNO"));
    obj.setRejectionDate(rs.getTimestamp("REJECTION_DATE"));
    obj.setCurrentDate(rs.getString("CURRENT_DATE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setApplName(rs.getString("APPL_NAME"));
    obj.setMatchedDateofbirth(rs.getTimestamp("MATCHED_DATEOFBIRTH"));
    obj.setEida(rs.getString("EIDA"));
    obj.setTradeLicense(rs.getString("TRADE_LICENSE"));
    obj.setRejDateofbirth(rs.getTimestamp("REJ_DATEOFBIRTH"));
    obj.setDrivingLnc(rs.getString("DRIVING_LNC"));
    obj.setMatchedEid(rs.getString("MATCHED_EID"));
    obj.setIbanNo(rs.getString("IBAN_NO"));
    obj.setMatchedMobileno(rs.getString("MATCHED_MOBILENO"));
    obj.setAppRefNo(rs.getString("APP_REF_NO"));
    obj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
    obj.setProduct(rs.getString("PRODUCT"));
    obj.setDeclineReason(rs.getString("DECLINE_REASON"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setRejProductCode(rs.getString("REJ_PRODUCT_CODE"));
    obj.setVat(rs.getString("VAT"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setPhNo(rs.getString("PH_NO"));
    obj.setEmployerId(rs.getString("EMPLOYER_ID"));
    obj.setApplDate(rs.getTimestamp("APPL_DATE"));
    obj.setRejApplDate(rs.getTimestamp("REJ_APPL_DATE"));
    obj.setEmailId(rs.getString("EMAIL_ID"));
    obj.setRejApplId(rs.getString("REJ_APPL_ID"));
    obj.setDob(rs.getTimestamp("DOB"));
    obj.setRejMobileno(rs.getString("REJ_MOBILENO"));
    obj.setMatchedPassportno(rs.getString("MATCHED_PASSPORTNO"));
    obj.setPassportNo(rs.getString("PASSPORT_NO"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_APPLMATCHEDREJAPPL]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_ApplmatchedrejapplEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_ApplmatchedrejapplEvent> events;
 NFT_ApplmatchedrejapplEvent obj = new NFT_ApplmatchedrejapplEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_APPLMATCHEDREJAPPL"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_ApplmatchedrejapplEvent obj = new NFT_ApplmatchedrejapplEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setIsEmployerNonsfa(getColumnValue(rs, "IS_EMPLOYER_NONSFA"));
            obj.setRejCif(getColumnValue(rs, "REJ_CIF"));
            obj.setRejEid(getColumnValue(rs, "REJ_EID"));
            obj.setRejPassportno(getColumnValue(rs, "REJ_PASSPORTNO"));
            obj.setRejectionDate(EventHelper.toTimestamp(getColumnValue(rs, "REJECTION_DATE")));
            obj.setCurrentDate(getColumnValue(rs, "CURRENT_DATE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setApplName(getColumnValue(rs, "APPL_NAME"));
            obj.setMatchedDateofbirth(EventHelper.toTimestamp(getColumnValue(rs, "MATCHED_DATEOFBIRTH")));
            obj.setEida(getColumnValue(rs, "EIDA"));
            obj.setTradeLicense(getColumnValue(rs, "TRADE_LICENSE"));
            obj.setRejDateofbirth(EventHelper.toTimestamp(getColumnValue(rs, "REJ_DATEOFBIRTH")));
            obj.setDrivingLnc(getColumnValue(rs, "DRIVING_LNC"));
            obj.setMatchedEid(getColumnValue(rs, "MATCHED_EID"));
            obj.setIbanNo(getColumnValue(rs, "IBAN_NO"));
            obj.setMatchedMobileno(getColumnValue(rs, "MATCHED_MOBILENO"));
            obj.setAppRefNo(getColumnValue(rs, "APP_REF_NO"));
            obj.setEmployeeId(getColumnValue(rs, "EMPLOYEE_ID"));
            obj.setProduct(getColumnValue(rs, "PRODUCT"));
            obj.setDeclineReason(getColumnValue(rs, "DECLINE_REASON"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setRejProductCode(getColumnValue(rs, "REJ_PRODUCT_CODE"));
            obj.setVat(getColumnValue(rs, "VAT"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setPhNo(getColumnValue(rs, "PH_NO"));
            obj.setEmployerId(getColumnValue(rs, "EMPLOYER_ID"));
            obj.setApplDate(EventHelper.toTimestamp(getColumnValue(rs, "APPL_DATE")));
            obj.setRejApplDate(EventHelper.toTimestamp(getColumnValue(rs, "REJ_APPL_DATE")));
            obj.setEmailId(getColumnValue(rs, "EMAIL_ID"));
            obj.setRejApplId(getColumnValue(rs, "REJ_APPL_ID"));
            obj.setDob(EventHelper.toTimestamp(getColumnValue(rs, "DOB")));
            obj.setRejMobileno(getColumnValue(rs, "REJ_MOBILENO"));
            obj.setMatchedPassportno(getColumnValue(rs, "MATCHED_PASSPORTNO"));
            obj.setPassportNo(getColumnValue(rs, "PASSPORT_NO"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_APPLMATCHEDREJAPPL]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_APPLMATCHEDREJAPPL]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"IS_EMPLOYER_NONSFA\",\"REJ_CIF\",\"REJ_EID\",\"REJ_PASSPORTNO\",\"REJECTION_DATE\",\"CURRENT_DATE\",\"CHANNEL\",\"APPL_NAME\",\"MATCHED_DATEOFBIRTH\",\"EIDA\",\"TRADE_LICENSE\",\"REJ_DATEOFBIRTH\",\"DRIVING_LNC\",\"MATCHED_EID\",\"IBAN_NO\",\"MATCHED_MOBILENO\",\"APP_REF_NO\",\"EMPLOYEE_ID\",\"PRODUCT\",\"DECLINE_REASON\",\"ACCOUNT_ID\",\"REJ_PRODUCT_CODE\",\"VAT\",\"BRANCH_ID\",\"CUST_ID\",\"PH_NO\",\"EMPLOYER_ID\",\"APPL_DATE\",\"REJ_APPL_DATE\",\"EMAIL_ID\",\"REJ_APPL_ID\",\"DOB\",\"REJ_MOBILENO\",\"MATCHED_PASSPORTNO\",\"PASSPORT_NO\"" +
              " FROM EVENT_NFT_APPLMATCHEDREJAPPL";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [IS_EMPLOYER_NONSFA],[REJ_CIF],[REJ_EID],[REJ_PASSPORTNO],[REJECTION_DATE],[CURRENT_DATE],[CHANNEL],[APPL_NAME],[MATCHED_DATEOFBIRTH],[EIDA],[TRADE_LICENSE],[REJ_DATEOFBIRTH],[DRIVING_LNC],[MATCHED_EID],[IBAN_NO],[MATCHED_MOBILENO],[APP_REF_NO],[EMPLOYEE_ID],[PRODUCT],[DECLINE_REASON],[ACCOUNT_ID],[REJ_PRODUCT_CODE],[VAT],[BRANCH_ID],[CUST_ID],[PH_NO],[EMPLOYER_ID],[APPL_DATE],[REJ_APPL_DATE],[EMAIL_ID],[REJ_APPL_ID],[DOB],[REJ_MOBILENO],[MATCHED_PASSPORTNO],[PASSPORT_NO]" +
              " FROM EVENT_NFT_APPLMATCHEDREJAPPL";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`IS_EMPLOYER_NONSFA`,`REJ_CIF`,`REJ_EID`,`REJ_PASSPORTNO`,`REJECTION_DATE`,`CURRENT_DATE`,`CHANNEL`,`APPL_NAME`,`MATCHED_DATEOFBIRTH`,`EIDA`,`TRADE_LICENSE`,`REJ_DATEOFBIRTH`,`DRIVING_LNC`,`MATCHED_EID`,`IBAN_NO`,`MATCHED_MOBILENO`,`APP_REF_NO`,`EMPLOYEE_ID`,`PRODUCT`,`DECLINE_REASON`,`ACCOUNT_ID`,`REJ_PRODUCT_CODE`,`VAT`,`BRANCH_ID`,`CUST_ID`,`PH_NO`,`EMPLOYER_ID`,`APPL_DATE`,`REJ_APPL_DATE`,`EMAIL_ID`,`REJ_APPL_ID`,`DOB`,`REJ_MOBILENO`,`MATCHED_PASSPORTNO`,`PASSPORT_NO`" +
              " FROM EVENT_NFT_APPLMATCHEDREJAPPL";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_APPLMATCHEDREJAPPL (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"IS_EMPLOYER_NONSFA\",\"REJ_CIF\",\"REJ_EID\",\"REJ_PASSPORTNO\",\"REJECTION_DATE\",\"CURRENT_DATE\",\"CHANNEL\",\"APPL_NAME\",\"MATCHED_DATEOFBIRTH\",\"EIDA\",\"TRADE_LICENSE\",\"REJ_DATEOFBIRTH\",\"DRIVING_LNC\",\"MATCHED_EID\",\"IBAN_NO\",\"MATCHED_MOBILENO\",\"APP_REF_NO\",\"EMPLOYEE_ID\",\"PRODUCT\",\"DECLINE_REASON\",\"ACCOUNT_ID\",\"REJ_PRODUCT_CODE\",\"VAT\",\"BRANCH_ID\",\"CUST_ID\",\"PH_NO\",\"EMPLOYER_ID\",\"APPL_DATE\",\"REJ_APPL_DATE\",\"EMAIL_ID\",\"REJ_APPL_ID\",\"DOB\",\"REJ_MOBILENO\",\"MATCHED_PASSPORTNO\",\"PASSPORT_NO\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[IS_EMPLOYER_NONSFA],[REJ_CIF],[REJ_EID],[REJ_PASSPORTNO],[REJECTION_DATE],[CURRENT_DATE],[CHANNEL],[APPL_NAME],[MATCHED_DATEOFBIRTH],[EIDA],[TRADE_LICENSE],[REJ_DATEOFBIRTH],[DRIVING_LNC],[MATCHED_EID],[IBAN_NO],[MATCHED_MOBILENO],[APP_REF_NO],[EMPLOYEE_ID],[PRODUCT],[DECLINE_REASON],[ACCOUNT_ID],[REJ_PRODUCT_CODE],[VAT],[BRANCH_ID],[CUST_ID],[PH_NO],[EMPLOYER_ID],[APPL_DATE],[REJ_APPL_DATE],[EMAIL_ID],[REJ_APPL_ID],[DOB],[REJ_MOBILENO],[MATCHED_PASSPORTNO],[PASSPORT_NO]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`IS_EMPLOYER_NONSFA`,`REJ_CIF`,`REJ_EID`,`REJ_PASSPORTNO`,`REJECTION_DATE`,`CURRENT_DATE`,`CHANNEL`,`APPL_NAME`,`MATCHED_DATEOFBIRTH`,`EIDA`,`TRADE_LICENSE`,`REJ_DATEOFBIRTH`,`DRIVING_LNC`,`MATCHED_EID`,`IBAN_NO`,`MATCHED_MOBILENO`,`APP_REF_NO`,`EMPLOYEE_ID`,`PRODUCT`,`DECLINE_REASON`,`ACCOUNT_ID`,`REJ_PRODUCT_CODE`,`VAT`,`BRANCH_ID`,`CUST_ID`,`PH_NO`,`EMPLOYER_ID`,`APPL_DATE`,`REJ_APPL_DATE`,`EMAIL_ID`,`REJ_APPL_ID`,`DOB`,`REJ_MOBILENO`,`MATCHED_PASSPORTNO`,`PASSPORT_NO`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

