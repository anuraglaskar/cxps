cxps.events.event.nft-beneficiary-addition{
  table-name : EVENT_NFT_BENEFICIARY_ADDITION
  event-mnemonic: BA
  workspaces : {
   
    CUSTOMER: "cust-id"
  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
user-id: {db : true ,raw_name : user_id ,type : "string:200"}
cust-id: {db : true ,raw_name : cust_id ,type : "string:200"}
device-id: {db : true ,raw_name : device_id ,type: "string:20" }
addr-network: {db : true ,raw_name : addr_network ,type : "string:200"}
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
payee-type: {db : true ,raw_name : payee_type ,type : "string:200"}
payee-id: {db : true ,raw_name : payee_id ,type : "string:200"}
payee-bank-name: {db : true ,raw_name : payee_bank_name ,type : "string:200"}
payee-bank-id: {db : true ,raw_name : payee_bank_id ,type : "string:200"}
payee-branch-name: {db : true ,raw_name : payee_branch_name ,type : "string:200"}
channel: {db : true ,raw_name : channel ,type : "string:200"}
benef-type: {db : true ,raw_name : benef_type ,type : "string:200"}
payee-mashreq: {db : true ,raw_name : payee_mashreq ,type : "string:200"}
payee-cust-id: {db : true ,raw_name : payee_cust_id ,type : "string:200"}
}
}


