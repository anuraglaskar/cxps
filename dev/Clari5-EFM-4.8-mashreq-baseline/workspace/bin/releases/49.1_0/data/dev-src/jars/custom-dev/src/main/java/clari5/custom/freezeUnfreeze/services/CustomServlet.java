package clari5.custom.freezeUnfreeze.services;



import clari5.custom.freezeUnfreeze.devFreeze.FreezeUnfreezeController;
import clari5.custom.freezeUnfreeze.devFreeze.JiraUpdate;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.logger.CxpsLogger;
import clari5.platform.util.Hocon;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static clari5.rdbms.Rdbms.getAppConnection;

public class CustomServlet extends HttpServlet  {

    private static CxpsLogger logger = CxpsLogger.getLogger(CustomServlet.class);
    private FreezeUnfreezeController freezeUnfreezeController = new FreezeUnfreezeController();


    public  void doGet(HttpServletRequest req , HttpServletResponse res){

        logger.debug("coming inside doGet method of CustomCmsServlet");
        Hocon h = new Hocon();
        h.loadFromContext("FreezUnfreezBlockApiDetails.conf");
        String fApi=h.getString("freez_UnFrz_api");

        int time=h.getInt("timeToWaitForRes");
        String envType=h.getString("env_type");
        logger.info("this is freez api "+fApi+" time ---"+time);

        String custNo = req.getParameter("CustomerId");
        String flagValue = req.getParameter("Action");

        logger.info("Customer Number is : "+custNo+" Action is"+flagValue);

        PrintWriter out=null;
        String resp=null;
        res.setContentType("text/html");
        try {
            String customerQuery = "SELECT * from CUSTOMER where hostCustId=?";
            String CustomerExist = "N";

            try (Connection connection = getAppConnection();
                 PreparedStatement psmt = connection.prepareStatement(customerQuery);
            ) {
                psmt.setString(1, custNo);
                ResultSet resultSet = psmt.executeQuery();
                if (resultSet.next()) {
                    CustomerExist = "Y";
                }

             connection.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (flagValue.equalsIgnoreCase( "Freeze")) {
                logger.info("inside freeze in servlet");
                out = res.getWriter();


                if (CustomerExist.equals("Y")) {
                    String flag = freezeUnfreezeController.callingToFreez(custNo, fApi, time, envType);
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert(\'" + flag + "\');");
                    out.println("window.location.href='FreezeUnfreeze.html'");
                    out.println("</script>");

                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Customer Id does not exist');");
                    out.println("window.location.href='FreezeUnfreeze.html'");
                    out.println("</script>");
                }


                } else if (flagValue.equalsIgnoreCase("UnFreeze")) {

                    logger.debug(" un freez method");
                    out = res.getWriter();
                    //call unfreez api
                    if (CustomerExist.equals("Y")) {
                        String flag = freezeUnfreezeController.callingToUnFreez(custNo, fApi, time, envType);
                        // boolean flag = true;
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert(\'" + flag + "\');");
                        out.println("window.location.href='FreezeUnfreeze.html'");
                        out.println("</script>");
                    }else{
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Customer Id does not exist');");
                        out.println("window.location.href='FreezeUnfreeze.html'");
                        out.println("</script>");
                    }

                    }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
