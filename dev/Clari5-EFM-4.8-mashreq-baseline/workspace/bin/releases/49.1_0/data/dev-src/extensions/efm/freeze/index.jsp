<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF8" %>
    <%@ page import="javax.servlet.http.HttpSession" %>
        <%
Cookie cookie = null;
Cookie[] cookies = null;
cookies = request.getCookies();

String session_id="";
if( cookies != null ) {
    for (int i = 0; i < cookies.length; i++) {
         cookie = cookies[i];
         if(cookie.getName().equals("session_id")){
            session_id = cookie.getValue();
          break;
        }
     }
}
      cookies= null;
      String id=request.getParameter("id");
%>

            <!DOCTYPE html>
            <html>

            <head>
                <title>Freeze</title>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="shortcut icon" href="./image/clari5-favicon.png">
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <link rel="stylesheet" href="css/styles.css">
                <link rel="stylesheet" href="css/jquery-ui.min.css">
                <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
                <link rel="stylesheet" href="css/select.dataTables.min.css">
                <link rel="stylesheet" href="css/jquery.dataTables.min.css">
                <link rel="stylesheet" href="css/styles.css">
                <script src="js/jquery.min.js"></script>
                <script src="js/popper.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jquery-ui.min.js"></script>
                <script defer src="js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
                <script defer src="js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
                <script src="js/jquery.dataTables.min.js"></script>
                <script src="js/dataTables.select.min.js"></script>
                <script src="js/dataTables.bootstrap4.min.js"></script>
                <script src="js/script.js"></script>
                <script src="/efm/JavaScriptServlet"></script>
                <script>
                    function init() {
                        $('.showIssue').hide();
                        $('.ermsg').hide();
                        var sessionId = '<%=session_id %>';
                        var issuekey = '<%=id %>';
                        getUserName(sessionId);
                        getDataList(issuekey, function(returnValue) {
                            if (returnValue.custId.length != 0 && returnValue.custId != "No Data Found") {
                                $(".tags").autocomplete({
                                    source: returnValue.custId
                                });
                            }
                        });
                    }
                </script>
                <style>
                    .ui-autocomplete {
                        z-index: 2000;
                    }
                </style>
            </head>

            <body onload="init()">
                <nav class="navbar navbar-expand-sm bg-light navbar-light">
                    <img src="./image/logo.png" alt="Clari5" width="10%">
                    <h3 class="mx-auto txts-color"></h3>
                    <img src="./image/mashreq.png" id="imgMashreq" alt="Mashreq" width="10%" align="right">
                </nav>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="d-flex my-3 p-2 bg-color ">
                                <span class="font-weight-bolder mr-3">Customer Details</span>
                                <button type="button" class=" btn gry-color btn-sm text-white" style="margin-left:0px;" onclick="ManualModalLoad()" data-toggle="modal" data-target="#freezeModalCust">Manual customer freeze</button>

                            </div>
                            <div class="shadow-sm p-4 my-1 bg-white">
                                <div id="rcorners" style="height:auto;">
                                    <form style="padding:15px">
                                        <div class="row form-inline">
                                            <div class="col-sm-4 bold roco">
                                                <label for="email " class="mr-sm-2 float-left">CUSTOMER ID</label>
                                            </div>
                                            <div class="col-sm-1 bold roco">
                                                <label for="email " class="mr-sm-2 float-left">:</label>
                                            </div>
                                            <div class="col-sm-7 roco">
                                                <div class="input-group">
                                                    <select class="form-control" id="customerId" style="width:306px;" oninput="revertHighLighter('cust','this.id')" onchange="getStatus('cust','customerId','custStatus','N')">
                                            <option id="selectCust" value="0">-- Select --</option>
                                        </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 bold roco"> </div>
                                            <div class="col-sm-5 roco" id="customerId_text" class="textHighLight float-right" style="width:100%"></div>
                                            <div class="col-sm-2 roco1" id="cust_audit"><a href="#auditDt" style="float:right" id="cust_audit_link" onclick="showAudit('customerId')">Audit log</a></div>
                                        </div>
                                        <div class="row form-inline" id="custStatus" style="margin-top: 14px;" hidden>
                                            <div class="col-sm-4 bold roco">
                                                <label for="email " class="mr-sm-2 float-left">STATUS</label>
                                            </div>
                                            <div class="col-sm-1 bold roco">
                                                <label for="email " class="mr-sm-2 float-left">:</label>
                                            </div>
                                            <div class="col-sm-7 roco">
                                                <input class="form-control" id="custStat" style="width: 296px !important;" disabled>
                                                <div id="custStatus_text" class="textHighLight" style="width:100%" hidden></div>
                                            </div>
                                        </div>
                                        <div class="wrap" id="block_cust" hidden>
                                            <button type="button" class="btn btn-warning" onclick="doBlock('b','customerId')">RM
                                    Freeze</button>
                                        </div>
                                        <div id="unblock_cust" class="wrap" hidden>
                                            <button type="button" class="btn btn-warning" onclick="doBlock('u','customerId')">RM
                                    Unfreeze</button>
                                        </div>
                                    </form>
                                    <br>
                                    <div class="freeze_response">
                                        <div id="frzSuc_cust"></div>
                                        <div id="frzErr_cust"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="d-flex my-3 p-2 bg-color">
                                <span class="font-weight-bolder mr-3">Card Details</span>
                                <button type="button" class="btn gry-color btn-sm text-white" style="margin-left:0px;" onclick="ManualModalLoad()" data-toggle="modal" data-target="#freezeModal">Manual card block</button>
                            </div>
                            <div class="shadow-sm p-4 my-1 bg-white">
                                <div id='cardDT'> </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="audit_div">
                        <div class="col-sm-12">
                            <div class="d-flex my-3 p-2 bg-color ">
                                <div class="col-sm-12">
                                    <span class="font-weight-bolder mr-3">Audit Log</span>
                                    <a id="fileDownld" href="/efm/servlet/DownloadServlet" download hidden> <button type="button" title="Download Audit Log" class="btn excel" style="float:right;padding:0px"><i class='fas fa-file-excel'
                                    style='font-size:24px;color:green'></i></button></a>
                                </div>
                            </div>
                            <form style="padding:10px;" id="auditForm">
                                <div class="row form-inline">
                                    <div class="col-sm-1 bold roco">
                                        <label for="email " class="mr-sm-2 float-left">Type:</label>
                                    </div>
                                    <div class="col-sm-4 roco">
                                        <select class="form-control" id="audit_type" onchange="revertManual(this.id,'type')" style="width: 306px !important;">
                                <option value="">Select</option>
                                <option value="Customer Number">Customer</option>
                                <option value="Card Number">Card</option>
                            </select>
                                    </div>
                                    <div class="col-sm-1 bold roco">
                                        <label for="email " class="mr-sm-2 float-left">Number:</label>
                                    </div>
                                    <div class="col-sm-4 roco">
                                        <input class="form-control" maxlength="16" id="audit_value" onchange="revertManual(this.id,'val')" style="width: 306px !important;">
                                    </div>
                                    <div class="col-sm-2 bold roco">
                                        <button type="button" class="btn btn-warning" name="auditManl" onclick="auditManual();">Get
                                Log</button>
                                    </div>
                                </div>
                                <div class="row form-inline">
                                    <div class="col-sm-1 roco"></div>
                                    <div class="col-sm-4 roco" id="audit_type_text" class="textHighLight"></div>
                                    <div class="col-sm-1 roco"></div>
                                    <div class="col-sm-4 roco" id="audit_value_text" class="textHighLight"></div>
                                    <div class="col-sm-2 roco"></div>
                                </div>
                            </form>
                            <div id="audit_value">
                                <div class="shadow-sm p-4 my-1 bg-white">
                                    <div class="shadow-sm p-4 my-1 bg-white">
                                        <div id='auditDt'> </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                    </div>
                </div>
                </div>
                <!-- Freeze Modal -->
                <div id="freezeModal" class="modal fade InputModal" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="freezeModal" aria-hidden="true">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content increaseHeight">
                            <div class="modal-header">
                                <h6 class="modal-title"><strong>Block / Unblock</strong></h6>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form style="padding:10px;" id="cardForm">
                                    <div class="row form-inline">
                                        <div class="col-sm-4 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">CARD NUMBER</label>
                                        </div>
                                        <div class="col-sm-1 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">:</label>
                                        </div>
                                        <div class="col-sm-7 roco">
                                            <div class="input-group">
                                                <input class="form-control" maxlength="16" id="cardNumber" style="width: 300px !important;" placeholder="xxxxxxxxxxxxxxxx" onchange="revertHighLighter('card',this.id)" oninput="revertHighLighter('card',this.id) ">
                                                <div class="input-group-append"><a class="btn btn-warning" id="getCardStatus" title="Get block status" onclick="getStatus('card','cardNumber','cardStatus','N')"><i class='fas fa-arrow-circle-right'
                                                style='color:white'></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 roco"></div>
                                        <div class="col-sm-7 roco" id="cardNumber_text" class="textHighLight" hidden></div>
                                    </div>
                                    <div class="row form-inline" hidden id="cardStatus">
                                        <div class="col-sm-4 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">STATUS</label>
                                        </div>
                                        <div class="col-sm-1 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">:</label>
                                        </div>
                                        <div class="col-sm-7 roco">
                                            <input class="form-control" id="cardStat" style="width: 306px !important;margin-top:14px;" disabled>
                                            <div id="status_text" class="textHighLight" style="width:100%" hidden></div>
                                        </div>
                                    </div>
                                    <div class="wrap" id="block_card" hidden>
                                        <button type="button" class="btn btn-warning" onclick="doBlock('b','cardNumber')">Block</button>
                                    </div>
                                    <div id="unblock_card" class="wrap" hidden>
                                        <button type="button" class="btn btn-warning" onclick="doBlock('u','cardNumber')">Unblock</button>
                                    </div>
                                </form><br>
                                <div style="text-align:center;"><i id="spin_card" style="font-size:30px;" class="fa fa-spinner fa-spin" hidden></i>
                                    <div class="freeze_response">
                                        <div id="frzSuc_card"></div>
                                        <div id="frzErr_card"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="freezeModalCust" class="modal fade InputModal" data-backdrop="static" data-keyboard="false" role="dialog" tabindex="-1" aria-labelledby="freezeModal" aria-hidden="true">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content increaseHeight">
                            <div class="modal-header">
                                <h6 class="modal-title"><strong>R.M Freeze/ UnFreeze</strong></h6>
                                <button type="button" id ="cardModalClose" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form style="padding:10px;" id="custModalForm">
                                    <div class="row form-inline">
                                        <div class="col-sm-4 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">CUSTOMER ID</label>
                                        </div>
                                        <div class="col-sm-1 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">:</label>
                                        </div>
                                        <div class="col-sm-7 roco">
                                            <div class="input-group">
                                                <input class="form-control" maxlength="9" id="custModal" style="width: 300px !important;" placeholder="xxxxxxxxx" onchange="revertHighLighter('custModal',this.id)" oninput="revertHighLighter('custModal',this.id) ">
                                                <div class="input-group-append"><a class="btn btn-warning" id="getCustStatus" title="Get freeze status" onclick="getStatus('custModal','custModal','custModalStatus','N')"><i class='fas fa-arrow-circle-right'
                                                style='color:white'></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 roco"></div>
                                        <div class="col-sm-7 roco" id="custModal_text" class="textHighLight" hidden></div>
                                    </div>
                                    <div class="row form-inline" hidden id="custModalStatus">
                                        <div class="col-sm-4 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">STATUS</label>
                                        </div>
                                        <div class="col-sm-1 bold roco">
                                            <label for="email " class="mr-sm-2 float-left">:</label>
                                        </div>
                                        <div class="col-sm-7 roco">
                                            <input class="form-control" id="custModalStat" style="width: 306px !important;margin-top:14px;" disabled>
                                            <div id="custModal_text" class="textHighLight" style="width:100%" hidden></div>
                                        </div>
                                    </div>
                                    <div class="wrap" id="block_custModal" hidden>
                                        <button type="button" class="btn btn-warning" onclick="doBlock('b','custModal')">R.M Freeze</button>
                                    </div>
                                    <div id="unblock_custModal" class="wrap" hidden>
                                        <button type="button" class="btn btn-warning" onclick="doBlock('u','custModal')">R.M
                                UnFreeze</button>
                                    </div>
                                </form><br>
                                <div style="text-align:center;"><i id="spin_custModal" style="font-size:30px;" class="fa fa-spinner fa-spin" hidden></i>
                                    <div class="freeze_response_custModal">
                                        <div id="frzSuc_custModal"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Freeze MOdal ends-->

                <!--spinner start-->
                <div class="modal" id="spin" data-backdrop="static" aria-modal="true ">
                    <div class="modal-dialog text-center ">
                        <div class="spinner "></div>
                    </div>
                </div>
                <!--spinner end-->
                <!--res start-->
                <div class="modal" id="resp" data-backdrop="static" aria-modal="true">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="respata"></div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light okbtn" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--res end-->
                <!-- Footer -->
                <footer class="page-footer font-small bg-light ">
                    <!-- Copyright -->
                    <div class="footer-copyright text-center py-3"><i class='fas fa-copyright'></i>
                        <a href="http://www.customerxps.com" target="_blank">Customer<span style="color :red;">XP</span>s Software
                Pvt. Ltd.</a>
                    </div>
                    <!-- Copyright -->
                </footer>
                <!-- Footer -->
            </body>

            </html>