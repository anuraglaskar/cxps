[{
	"Id": "1",
	"Name": "Customer Name",
	"position" : 1
}, {
	"Id": "2",
	"Name": "CIF Status",
	"position" : 2
}, {
	"Id": "3",
	"Name": "RM Freeze Status",
	"position" : 3
}, {
	"Id": "4",
	"Name": "Account Freeze Status",
	"position" : 4
}, {
	"Id": "5",
	"Name": "Account Freeze Date",
	"position" : 5
}, {
	"Id": "6",
	"Name": "Constitution",
	"position" : 6
}, {
	"Id": "7",
	"Name": "Customer Category",
	"position" : 7
}, {
	"Id": "8",
	"Name": "Gender",
	"position" : 8
}, {
	"Id": "9",
	"Name": "Date of birth",
	"position" : 9
}, {
	"Id": "10",
	"Name": "EID",
	"position" : 10
}, {
	"Id": "11",
	"Name": "Marital Status",
	"position" : 11
}, {
	"Id": 12,
	"Name": "VAT No",
	"position" : 12
}, {
	"Id": 13,
	"Name": "Occupation",
	"position" : 13
}, {
	"Id": 14,
	"Name": "Customer Industry",
	"position" : 14
}, {
	"Id": 15,
	"Name": "Trade license No"
}, {
	"Id": 16,
	"Name": "Customer Since",
	"position" : 16
}, {
	"Id": 17,
	"Name": "Mobile No",
	"position" : 17
}, {
	"Id": 18,
	"Name": "Landline No",
	"position" : 18
}, {
	"Id": 19,
	"Name": "Email",
	"position" : 19
}, {
	"Id": 20,
	"Name": "Communication Address",
	"position" : 20
}]
