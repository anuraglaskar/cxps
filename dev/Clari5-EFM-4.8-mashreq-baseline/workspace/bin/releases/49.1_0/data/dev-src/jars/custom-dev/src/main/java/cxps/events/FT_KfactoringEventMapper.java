// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_KfactoringEventMapper extends EventMapper<FT_KfactoringEvent> {

public FT_KfactoringEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_KfactoringEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_KfactoringEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_KfactoringEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_KfactoringEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_KfactoringEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_KfactoringEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getFactoringContractNumber());
            preparedStatement.setString(i++, obj.getInvoicenoDiscountedChequeNo());
            preparedStatement.setTimestamp(i++, obj.getInvoiceChequeDate());
            preparedStatement.setTimestamp(i++, obj.getContractEndDateValidity());
            preparedStatement.setDouble(i++, obj.getInvoiceChequeAmount());
            preparedStatement.setString(i++, obj.getCustomerAccountNumber());
            preparedStatement.setString(i++, obj.getCifId());
            preparedStatement.setString(i++, obj.getInvoiceChequeCurrency());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_KFACTORING]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_KfactoringEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_KfactoringEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_KFACTORING"));
        putList = new ArrayList<>();

        for (FT_KfactoringEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "FACTORING_CONTRACT_NUMBER",  obj.getFactoringContractNumber());
            p = this.insert(p, "INVOICENO_DISCOUNTED_CHEQUE_NO",  obj.getInvoicenoDiscountedChequeNo());
            p = this.insert(p, "INVOICE_CHEQUE_DATE", String.valueOf(obj.getInvoiceChequeDate()));
            p = this.insert(p, "CONTRACT_END_DATE_VALIDITY", String.valueOf(obj.getContractEndDateValidity()));
            p = this.insert(p, "INVOICE_CHEQUE_AMOUNT", String.valueOf(obj.getInvoiceChequeAmount()));
            p = this.insert(p, "CUSTOMER_ACCOUNT_NUMBER",  obj.getCustomerAccountNumber());
            p = this.insert(p, "CIF_ID",  obj.getCifId());
            p = this.insert(p, "INVOICE_CHEQUE_CURRENCY",  obj.getInvoiceChequeCurrency());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_KFACTORING"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_KFACTORING]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_KFACTORING]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_KfactoringEvent obj = new FT_KfactoringEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setFactoringContractNumber(rs.getString("FACTORING_CONTRACT_NUMBER"));
    obj.setInvoicenoDiscountedChequeNo(rs.getString("INVOICENO_DISCOUNTED_CHEQUE_NO"));
    obj.setInvoiceChequeDate(rs.getTimestamp("INVOICE_CHEQUE_DATE"));
    obj.setContractEndDateValidity(rs.getTimestamp("CONTRACT_END_DATE_VALIDITY"));
    obj.setInvoiceChequeAmount(rs.getDouble("INVOICE_CHEQUE_AMOUNT"));
    obj.setCustomerAccountNumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
    obj.setCifId(rs.getString("CIF_ID"));
    obj.setInvoiceChequeCurrency(rs.getString("INVOICE_CHEQUE_CURRENCY"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_KFACTORING]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_KfactoringEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_KfactoringEvent> events;
 FT_KfactoringEvent obj = new FT_KfactoringEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_KFACTORING"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_KfactoringEvent obj = new FT_KfactoringEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setFactoringContractNumber(getColumnValue(rs, "FACTORING_CONTRACT_NUMBER"));
            obj.setInvoicenoDiscountedChequeNo(getColumnValue(rs, "INVOICENO_DISCOUNTED_CHEQUE_NO"));
            obj.setInvoiceChequeDate(EventHelper.toTimestamp(getColumnValue(rs, "INVOICE_CHEQUE_DATE")));
            obj.setContractEndDateValidity(EventHelper.toTimestamp(getColumnValue(rs, "CONTRACT_END_DATE_VALIDITY")));
            obj.setInvoiceChequeAmount(EventHelper.toDouble(getColumnValue(rs, "INVOICE_CHEQUE_AMOUNT")));
            obj.setCustomerAccountNumber(getColumnValue(rs, "CUSTOMER_ACCOUNT_NUMBER"));
            obj.setCifId(getColumnValue(rs, "CIF_ID"));
            obj.setInvoiceChequeCurrency(getColumnValue(rs, "INVOICE_CHEQUE_CURRENCY"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_KFACTORING]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_KFACTORING]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"FACTORING_CONTRACT_NUMBER\",\"INVOICENO_DISCOUNTED_CHEQUE_NO\",\"INVOICE_CHEQUE_DATE\",\"CONTRACT_END_DATE_VALIDITY\",\"INVOICE_CHEQUE_AMOUNT\",\"CUSTOMER_ACCOUNT_NUMBER\",\"CIF_ID\",\"INVOICE_CHEQUE_CURRENCY\"" +
              " FROM EVENT_FT_KFACTORING";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [FACTORING_CONTRACT_NUMBER],[INVOICENO_DISCOUNTED_CHEQUE_NO],[INVOICE_CHEQUE_DATE],[CONTRACT_END_DATE_VALIDITY],[INVOICE_CHEQUE_AMOUNT],[CUSTOMER_ACCOUNT_NUMBER],[CIF_ID],[INVOICE_CHEQUE_CURRENCY]" +
              " FROM EVENT_FT_KFACTORING";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`FACTORING_CONTRACT_NUMBER`,`INVOICENO_DISCOUNTED_CHEQUE_NO`,`INVOICE_CHEQUE_DATE`,`CONTRACT_END_DATE_VALIDITY`,`INVOICE_CHEQUE_AMOUNT`,`CUSTOMER_ACCOUNT_NUMBER`,`CIF_ID`,`INVOICE_CHEQUE_CURRENCY`" +
              " FROM EVENT_FT_KFACTORING";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_KFACTORING (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"FACTORING_CONTRACT_NUMBER\",\"INVOICENO_DISCOUNTED_CHEQUE_NO\",\"INVOICE_CHEQUE_DATE\",\"CONTRACT_END_DATE_VALIDITY\",\"INVOICE_CHEQUE_AMOUNT\",\"CUSTOMER_ACCOUNT_NUMBER\",\"CIF_ID\",\"INVOICE_CHEQUE_CURRENCY\") values(?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[FACTORING_CONTRACT_NUMBER],[INVOICENO_DISCOUNTED_CHEQUE_NO],[INVOICE_CHEQUE_DATE],[CONTRACT_END_DATE_VALIDITY],[INVOICE_CHEQUE_AMOUNT],[CUSTOMER_ACCOUNT_NUMBER],[CIF_ID],[INVOICE_CHEQUE_CURRENCY]) values(?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`FACTORING_CONTRACT_NUMBER`,`INVOICENO_DISCOUNTED_CHEQUE_NO`,`INVOICE_CHEQUE_DATE`,`CONTRACT_END_DATE_VALIDITY`,`INVOICE_CHEQUE_AMOUNT`,`CUSTOMER_ACCOUNT_NUMBER`,`CIF_ID`,`INVOICE_CHEQUE_CURRENCY`) values(?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

