//once the file is generated, donot copy the generated file, meld the files and merge
cxps.events.event.ft-ibfundstransfer{
  table-name : EVENT_FT_IBFUNDS_TRANSFER
  event-mnemonic: IFT
  workspaces : {
    
    ACCOUNT : "account-no"
    CUSTOMER: "cust-id"
    NONCUSTOMER : "addr-network + payee-account-id, rchrge-mob-no"

  }
  event-attributes : {
host-id: {db : true ,raw_name : host_id ,type : "string:20"}
sys-time: {db : true ,raw_name : sys_time ,type : timestamp}
cust-id-derived: {db : true ,raw_name : cust_id_deriveed ,type : "string:20", derivation:"""cxps.events.CustomDerivator.getCustIdDerived(this)"""}
cust-id: {db : true ,raw_name : cust_id ,type : "string:20"}
user-id: {db : true ,raw_name : user_id ,type : "string:20"}
schm-type: {db : true ,raw_name : schm_type ,type : "string:20"}
schm-code: {db : true ,raw_name : schm_code ,type : "string:20"} 
acct-ownership: {db : true ,raw_name : acct_ownership ,type : "string:20"}
acctopendate: {db : true ,raw_name : acctopendate ,type : timestamp}
cust-type: {db : true ,raw_name : cust_type ,type : "string:20"}
device-id: {db : true ,raw_name : device_id ,type: "string:20" }
succ-fail-flg: {db : true ,raw_name : succ_fail_flg ,type : "string:20"}
error-code: {db : true ,raw_name : error_code ,type : "string:200"}
error-desc: {db : true ,raw_name : error_desc ,type : "string:200"}
tran-amt: {db :true ,raw_name : tran_amt ,type : "number:20,3"}
txn-crncy: {db : true ,raw_name : txn_crncy ,type : "string:200"}
avg-txn-amt: {db :true ,raw_name : avg_txn_amt ,type : "number:20,3"}
avl-bal: {db :true ,raw_name : avl_bal ,type : "number:20,3", derivation:"""cxps.events.CustomDerivator.getAvlBal(this)"""}
tran-type: {db : true ,raw_name : tran_type ,type : "string:20"}
addr-network: {db : true ,raw_name : addr_network, type : "string:200"}
addr-network-derived: {db : true ,raw_name : addr_network_derived , type : "string:200", derivation:"""cxps.events.CustomDerivator.getAddrNetworkderived(this)"""}
domestic-trsnfr-flag: {db : true ,raw_name : domestic_trsnfr_flag ,type : "string:20"}
network-type: {db : true ,raw_name : network_type ,type : "string:20"}
bank-id: {db : true ,raw_name : bank_id ,type : "string:20"}
bank-name: {db : true ,raw_name : bank_name ,type : "string:20"}
payee-name: {db : true ,raw_name : payee_name ,type : "string:20"}
payee-cust-id: {db : true ,raw_name : payee_cust_id ,type : "string:20"}
payee-account-id: {db : true ,raw_name : payee_account_id ,type : "string:20"}
payee-bank-name: {db : true ,raw_name : payee_bank_name ,type : "string:20"}
tran-id: {db : true ,raw_name : tran_id ,type : "string:20"}
txn-ref-no: {db : true ,raw_name : txn_ref_no ,type : "string:20"}
channel: {db : true ,raw_name : channel ,type : "string:20"}
daily-limit: {db :true ,raw_name : daily_limit ,type : "number:20,3"}
mcc-code : {db : true ,raw_name : mcc_code ,type : "string:20"}
account-no: {db : true ,raw_name : account_no ,type : "string:20"}
payee-mashreq: {db : true ,raw_name : payee_mashreq ,type : "string:20"}
payment-type: {db : true ,raw_name : payment_type ,type : "string:20"}
benef-addition-date: {db : true ,raw_name : benef_addition_date ,type : timestamp}
rem-type: {db : true ,raw_name : rem_type ,type : "string:20"}
tran-mode: {db : true ,raw_name : tran_mode ,type : "string:20"}
benef-type: {db : true ,raw_name : benef_type ,type : "string:20"}
rchrge-mob-no: {db : true ,raw_name : rchrge_mob_no ,type : "string:20"}
is-benef-added: {db : true,raw_name : is_benef_added , type : "string:5", derivation:"""cxps.events.CustomDerivator.getIsBenefAdded(this)"""}
avl-balance: {db :true ,raw_name : avl_balance ,type : "number:20,3" ,derivation:"""cxps.events.CustomDerivator.getAvlBalance(this)""" }
}
}
