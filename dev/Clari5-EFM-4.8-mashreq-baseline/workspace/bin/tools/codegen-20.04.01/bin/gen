:
# ===========================================================================
# GEN
# This is a unified utility to generate code for all types of specifications.
# - XSD -> Java
# 	 - It also patches them post creation for nillability
# - Hocon -> Java
# ===========================================================================

__usage() {
	echo "Usage: $0 (settings.gradle|<project-folder> ...)"
    exit 1
}

xmlgen() {

    local _cwd=`pwd`
    TMPFILE=/tmp/xx.$$

	for folder in $*
	do
		echo $folder | grep -q '^/' || folder=$_cwd/$folder
        [ -d ${folder}/src/gen/java ] || mkdir -p ${folder}/src/gen/java
        cd ${folder}/src/main/resources/xml_spec
        for xsd in `find . -name '*.xsd'`
        do
            echo $xsd
            pkg=`dirname $xsd | sed -e 's,/,.,g' -e 's/^\.*//'`
            xjc -d ${folder}/src/gen/java -p $pkg $xsd
        done

        # Uncomment following line if no patching required
        continue

        # Patching the generated file for nillability
        cd ${folder}/src/gen/java
        for dir in `find . -name xml -type d`
        do
            cd $dir
            for file in `find . -name '*.java'`
            do
                sed -e 's/\(@XmlElement([^)]*\))/\1, nillable = true)/' -e 's/nillable = true,//g' $file > $TMPFILE
                mv $TMPFILE $file
            done
            cd ${folder}/src/gen/java
        done
	done
    cd ${_cwd}
}

specgen() {
    
    local _cwd=`pwd`

    projects=""
    for dir in $*
    do
        [ -d ${dir}/src ] && projects="${projects} ${dir}/src"
    done
    
    C_PATH=""
	bdir=`dirname ${basepath}`
    for lib in `ls ${bdir}/lib/*.jar`
    do
        C_PATH="${lib}:${C_PATH}"
    done

	java -cp "${C_PATH}" clari5.tools.gen.CodeGen -b "${_cwd}" ${projects}
}

# -----------------
# MAIN ACTION BLOCK
# -----------------
[ $# -eq 0 ] && __usage

if [ $# -eq 1 ] && echo $1 | grep -q settings.gradle
then
	file=$1
	[ ! -f $file ] && echo "Unable to read [$file]" && exit 1
	bdir=`dirname $file`
	[ "$bdir" != "." ] && echo "You need to be in folder containing settings.gradle" && exit 2
	dirs=`grep '^[ 	]*include' $file | tr "'" '\012' | grep -v include | sort -u | grep -v -e ',' -e '^[ 	]*$' | sed 's,:,/,g'`
else
	dirs=$*
fi

# Get basepath
if \ls -l $0 | grep -q -- '->'
then
	basepath=`ls -l $0 | sed 's/^.*->//' | sed -e 's/^[ 	]*//' -e 's/[ 	]*$//'`
	basepath=`dirname $basepath`
else
	basepath=`dirname $0`
fi
# DEBUG
# echo $basepath
cwd=`pwd`
cd ${cwd}
cd ${basepath}
basepath=`pwd`

specfolder=""
xsdfolder=""

cd ${cwd}
for folder in $dirs
do
    [ ! -d ${folder} ] && echo "Dir ${folder} not found. Skipping ..." && continue
	[ -d $folder/src/main/resources/spec ] && specfolder="$specfolder ${folder}"
	[ -d $folder/src/main/resources/xml_spec ] && xsdfolder="$xsdfolder ${folder}"
done

[ ! -z "$specfolder" ] && specgen `echo $specfolder`
[ ! -z "$xsdfolder" ] && xmlgen `echo $xsdfolder`

exit 0
