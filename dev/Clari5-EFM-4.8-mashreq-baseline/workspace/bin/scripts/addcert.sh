#!/usr/bin/env bash
# Author : Lovish

main() {
    
    local tmpDir=${d}/tmp
    local crtFile=cxpsucs-certificate.pem
    local crtUrl=${GLB_DEVRES_URL}/self-signed-certs/${crtFile}
    local cacerts=${JAVA_HOME}/jre/lib/security/cacerts

    echo " [Installing certificate into java keystore]"
    printf "Downloading certificate ... "
    mkdir -p ${tmpDir}
    rm -f ${crtFile}
    ( cd ${tmpDir} && wget --no-check-certificate -q ${crtUrl} ) || return 1
    echo "DONE"

    # Remove existing certificate from java keystore
    sudo ${JAVA_HOME}/bin/keytool -delete -alias tomcat -storepass changeit -keystore ${cacerts} 2>/dev/null

    # Add new certificate into java keystore
    sudo ${JAVA_HOME}/bin/keytool -import -alias tomcat -file ${tmpDir}/${crtFile} -storepass changeit -keystore ${cacerts} -noprompt

}

main $*
