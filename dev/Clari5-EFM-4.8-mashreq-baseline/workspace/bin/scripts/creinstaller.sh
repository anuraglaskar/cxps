#!/usr/bin/env bash

error_message() {

printf "\033[01;31m-----------------------------------------------------------------------------
|    Installers are only created by PRODUCT team for this version.          |
|    To add your changes in product, create a plugin via [creplugin.sh]     |
-----------------------------------------------------------------------------\033[00m
"
}

main(){
    
    # Get Basedir
    CWD=`pwd`
    BASEDIR=$( cd $CWD && cd `dirname $0` && cd .. && pwd )

    local plugin=$( grep -w '^[     ]*PLUGIN[   ]*=' ${CXPS_WORKSPACE}/bin/releases/${CL5_REL_NAME}/versions.properties \
        | tail -1 | cut -d '=' -f2 | tr -d '    ' )

    if echo ${plugin} | grep -qiE 'yes|true' ; then
        if [[ "${CL5_CUSTOM_ID}" != "dev" ]]; then
            error_message
            exit
        fi
    fi

    if [[ ! "$1" = "--unsafe" ]]; then
        # Check if any changes
        changes=`svn status -u --ignore-externals ${CXPS_WORKSPACE} | grep -v -e '^Status' -e '^X'`
        [ ! -z "${changes}" ] && echo "Error: Changes found. Local Repository not in sync." && return 1
    else
        shift 1
    fi

    [ -e ${MISSING_ASSEMBLY_FILE} ] && echo "Error: Build has not happened, missing assembly" && return 1

    # Create package meta file
    printf "Creating package-meta.conf ... "
    chmod +x ${BASEDIR}/groovy/src/crepackagemeta.groovy
    ( unset JAVA_OPTS && ${BASEDIR}/groovy/src/crepackagemeta.groovy )
    [ $? -ne 0 ] && echo "error!" && echo "Error: Unable to create package meta" && return 1 
    echo "done!"

    # Copy release notes
    if [ -e ${BASEDIR}/releases/${CL5_REL_NAME}/index.html ]; then
        cp ${BASEDIR}/releases/${CL5_REL_NAME}/index.html ${CL5_WS_REP}/ReleaseNotes.html
    else
        echo "Warning: Missing release notes as [${BASEDIR}/releases/${CL5_REL_NAME}/index.html]"
        echo "Missing release notes" > ${CL5_WS_REP}/ReleaseNotes.html
    fi

    # Create the executable installer bash
    printf "Packing up the installer "
    ${BASEDIR}/scripts/packager.sh && echo " done!"

    # Publish the installer if there's a call for publish
    if [[ "$1" = "publish" ]]; then
        inst_name=$( ${BASEDIR}/scripts/packager.sh --name )        
        dest="resources/cxpsdev-resources/4.8/published/installers/${CL5_CUSTOM_ID}"
        echo "Publishing installer to .../${dest}"
        scp ${w}/${inst_name} cxsvn@192.168.5.56:www/${dest}
    fi
}

# ----------
# Main call
# ----------
main $*
