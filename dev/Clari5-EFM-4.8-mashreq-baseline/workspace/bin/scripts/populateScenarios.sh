#!/usr/bin/env bash
# Author: Lovish

# The program only runs the seed for PATCH_DUMP table
main() {
    
    # Get basedir
    local BASEDIR=$( cd $(pwd) && cd $( dirname $0 ) && pwd )

    # Execute the script for Executing DB
    ${BASEDIR}/dbexec.sh -tables PATCH_DUMP
}

# ------------
# Main Block
# ------------
main $*
