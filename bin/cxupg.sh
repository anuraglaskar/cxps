#!/bin/bash

. ${CXPS_BIN}/cxpsvars.sh

DEVRES=${GLB_RES_URL}/resources

upgradeDevenv()
{
	version=$1
	[ -z "$version" ] && return 0
	printf "Version $version available. Install [Y/N] ? "
	read ans
	if [ "$ans" = "Y" ] ; then
		[ ! -d ${CXPS_PROGRAMS}/upgrades ] && mkdir -p ${CXPS_PROGRAMS}/upgrades
		cd ${CXPS_PROGRAMS}/upgrades
		file="devsetup-${version}.bash"
		[ -f ${file} ] && \rm -f ${file}
		download ${DEVRES}/${file}
		[ -f ${file} ] && bash ./${file} upgrade && return 0
		echo "Unable to download ${file} from ${DEVRES}"
		return 1
	fi
}

isHigher()
{
	patch=""
	if [ "$1" = "patch" ] ; then
		patch=patch
		shift 1
	fi

	v1=$1
	v2=$2

	major1=`echo $v1 | cut -f1 -d'.'`
	minor1=`echo $v1 | cut -f2 -d'.'`
	patch1=`echo $v1 | cut -f3 -d'.'`
	[ -z "$patch1" ] && patch1="0"

	major2=`echo $v2 | cut -f1 -d'.'`
	minor2=`echo $v2 | cut -f2 -d'.'`
	patch2=`echo $v2 | cut -f3 -d'.'`
	[ -z "$patch2" ] && patch2="0"

	if [ ! -z "$patch" ] ; then
		if [ $major1 -ne $major2 -o $minor1 -ne $minor2 ] ; then
			return 1
		fi
	fi

	[ $major1 -gt $major2 ] && echo $v1 && return 0
	[ $major2 -gt $major1 ] && echo $v2 && return 0

	[ $minor1 -gt $minor2 ] && echo $v1 && return 0
	[ $minor2 -gt $minor1 ] && echo $v2 && return 0

	[ $patch1 -gt $patch2 ] && echo $v1 && return 0
	[ $patch2 -gt $patch1 ] && echo $v2 && return 0

	echo "EQUAL"
	return 1
}

deriveVersion()
{
	patch=$1
	nextVersion=$CXPS_DEVENV_VERSION
	LIST=/tmp/upg-filelist.$$
	curl --silent --insecure --connect-timeout 1 --retry 1 ${DEVRES}/ > $LIST
	[ $? -ne 0 ] && return 1
	cat $LIST | 
		${GREP} devsetup | 					# filter  only devsetup
		${SED} 's/^.*href=//' | 			# Clean up till href
		cut -f2 -d'"' | 					# extract the file
		${SED} 's/^[^0-9]*\([0-9]\)/\1/' |	# Remove "devsetup[-fat]-"
		${SED} 's/.bash//' |				# Remove ".bash"
		sort -u	| 							# Get the unique version list

	while read ver
	do
		n=`isHigher $patch "$nextVersion" "$ver"`
		[ $? -eq 0 ] && nextVersion=$n
		echo $nextVersion
	done | 
	tail -1
	\rm -f $LIST
	return 0
}

# -------------------
# Main Action Block
# -------------------

if [ -z "$CXPS_DEVENV_VERSION" ] ; then
	echo "Error: CXPS_DEVENV_VERSION variable is not set. Check your env"
	exit 1
fi

patch=""
if [ "$1" = "patch" ] ; then
	shift 1
	patch=patch
fi

if [ $# -eq 1 ] ; then
	version=$1
else
	version=`deriveVersion $patch`
	[ $? -ne 0 ] && exit 1
fi

if [ "$CXPS_DEVENV_VERSION" = "$version" ] ; then
	[ -z "$patch" ] && echo "Your current version $version is already the latest"
	exit 0
fi

upgradeDevenv ${version}
